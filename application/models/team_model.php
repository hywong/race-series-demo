<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Team_model extends CI_Model {

  public function get_existing_teams()
  {
    foreach($_SESSION['registration'][$_SESSION['registration']['active_user']]['activities'] as $eventActivity)
    {
      $expl = explode("_", $eventActivity);
      $eventIDs[] = reset($expl);
    }

    $sql = "SELECT entT.strTeamName AS teamName, entT.intTeamID AS teamID, relT.intEventID AS eventID FROM relTeam AS relT 
    INNER JOIN entTeam AS entT ON entT.intTeamID = relT.intTeamID 
    WHERE relT.intEventID IN (".implode(",", $eventIDs).");";

    $tempSession = $_SESSION['registration'];
    unset($tempSession[$_SESSION['registration']['active_user']]);
    unset($tempSession['last_modified']);
    unset($tempSession['active_user']);

    $teams = array();

    foreach($tempSession as $id => $details)
    {
      foreach($details['teams'] as $eID => $teamDetails)
      {
        if(!isset($teams[$eID]))
          $teams[$eID][''] = '';

        if(isset($teamDetails['team_action']))
        {
          if($teamDetails['team_action'] == "create")
          {
            $teams[$eID]['TEMPTEAM_'.$id] = $teamDetails['team_name_create'];
          }
        }
      }
    }

    $stmt = $this->db->prepare($sql);
    if($stmt->execute())
    {
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if($results)
      {
        foreach($results as $row)
        {
          $teams[$row['eventID']][$row['teamID']] = $row['teamName'];
        }
      }

      foreach($eventIDs as $id)
      {
        if(!isset($teams[$id]))
          $teams[$id][""] = "No teams exist";
      }

      return $teams;
    }
    else
    {
      //TODO: Alert - failed to retrieve team list
    }

  }

  public function get_form_sections($teams)
  {
    $sections = array();
    $teamSelections = array();
    if(!$teams)
    {
      $teamSelections[""] = "No teams exist";
    }
    else
    {
      $teamSelections = $teams;
    }

    foreach($_SESSION['registration'][$_SESSION['registration']['active_user']]['activities'] as $activity)
    {
      $thisEventID = reset(explode("_", $activity));
      $sections[$_SESSION['general'][$thisEventID]["display"]] = array(
        "Create or Join" => array(
          "type" => "select",
          "name" => "eventteam_team_action_".$thisEventID,
          "label" => "Are you creating or joining an existing team?",
          "id" => "selTeamAction".$thisEventID,
          "class" => "team-action",
          "data" => array(
            "" => "",
            "join" => translate("Joining an existing team"),
            "create" => translate("Creating a new team")
            )
          ),
        "Existing Teams" => array(
          "type" => "select",
          "name" => "eventteam_team_name_".$thisEventID,
          "label" => "Team Name",
          "id" => "selExistingTeamName".$thisEventID,
          "parent-class" => "team-name-exist",
          "class" => "team-exist-field team-validate-teamname",
          "data" => $teamSelections[$thisEventID]
          ),
        "Existing Team Password" => array(
          "type" => "password",
          "name" => "eventteam_team_password_".$thisEventID,
          "label" => "Team Password",
          "parent-class" => "team-password-exist",
          "class" => "team-exist-field team-validate-password",
          "id" => "inpExistingTeamPassword"
          ),
        "New Team Name" => array(
          "type" => "text",
          "name" => "eventteam_team_name_create_".$thisEventID,
          "label" => "New Team Name",
          "id" => "txtNewTeamName",
          "class" => "team-create-field",
          "parent-class" => "team-name-create"
          ),
        "New Team Password" => array(
          "type" => "password",
          "name" => "eventteam_team_password_create_".$thisEventID,
          "label" => "New Team Password",
          "id" => "txtNewTeamPassword",
          "class" => "team-create-field",
          "parent-class" => "team-password-create"
          )
        );
    }

    return $sections;
  }
 
}

/* End of file team_model.php */
/* Location: ./application/models/team_model.php */