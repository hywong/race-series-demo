<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model {

	//Takes an email and password and verifies against the admin accounts in the database
    public function verifyLogin($email,$password) {
		$retValue = "notValid";
		
		try {
			$sql = "SELECT * FROM entAdminAccount AS a
				INNER JOIN lkpAdminAccountType AS b ON b.intAdminAccountTypeID = a.intAdminAccountTypeID
				WHERE strAdminUserName = '" . $email . "'";
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
				
		//Check if any results were returned
		if (isset($result[0]) && count($result[0]) > 0) {
			$aPassword = $result[0]['strAdminPassword'];
			//This is how to check for blowfish encryption
			if (crypt($password, $aPassword) == $aPassword) { 
				$retValue = "valid";
				$sessionData['admin'] = array(
					"adminAccountID" => $result[0]['intAdminAccountID'],
					"adminAccountType" => $result[0]['strAdminAccountType'],
					"adminUserName" => $result[0]['strAdminUserName'],
					"adminEventID" => 1);
				$this->session->set_userdata($sessionData);
			}
		}
		
        return $retValue;
    }
	
	//Gets all admin accounts
	public function getAllAdminAccount() {
		$retArray = array();
		
		try {
			$sql = "SELECT * FROM entAdminAccount AS a
				INNER JOIN lkpAdminAccountType AS b ON b.intAdminAccountTypeID = a.intAdminAccountTypeID";
				
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
			
		if (isset($result[0]) && count($result[0]) > 0) {
			$retArray = $result;
		}
		
		return $retArray;
	}
	
	//Gets all admin account types
	public function getAllAdminAccountType() {
		$retArray = array();
		
		try {
			$sql = "SELECT intAdminAccountTypeID, strAdminAccountType FROM lkpAdminAccountType";
				
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
			
		if (isset($result) && count($result) > 0) {
			$retArray = $result;
		}
		
		return $retArray;
	}
	
	//Gets the number of registrations by date
	public function getIndivRegistrationCountsByDate() {
		$retArray = array();
		
		$aSession = $this->session->userdata('admin');
		
		try {
			//$sql = "SELECT COUNT(intTransactionID) AS aCount, DATE(dttDateCreated) AS dateOnly FROM entTransaction GROUP BY dateOnly";
		
			$sql = "SELECT COUNT(dta.intTransactionID) AS aCount, dta.dateOnly FROM (
				SELECT et.intTransactionID, DATE(et.dttDateCreated) AS dateOnly FROM entTransaction AS et 
				INNER JOIN relAccountTransaction AS rat ON rat.intTransactionID = et.intTransactionID AND rat.strRefundStatus IS NULL 
				INNER JOIN lkpTransactionType AS ltt ON ltt.intTransactionTypeID = rat.intTransactionTypeID 
				INNER JOIN relAccountActivity AS raa ON raa.intAccountID = rat.intAccountID AND raa.strStatusChange IS NULL";
				
			if ($aSession['adminEventID'] != 1) {
				$sql .= " AND raa.intEventID = " . $aSession['adminEventID'];	
			}
			$sql .= " WHERE ltt.strTransactionType = 'individualRegistration' OR ltt.strTransactionType = 'staffRegistration' OR ltt.strTransactionType = 'transfer'
			) AS dta
			GROUP BY dta.dateOnly";
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
			
		if (isset($result) && count($result) > 0) {
			$retArray = $result;
		}
		
		return $retArray;
	}
	
	public function getGroupRegistrationCountsByDate() {
		$retArray = array();
		
		$aSession = $this->session->userdata('admin');
		
		try {
			/*
			$sql = "SELECT COUNT(dta.intTransactionID) AS aCount, dta.dateOnly FROM (
				SELECT et.intTransactionID, DATE(et.dttDateCreated) AS dateOnly FROM entTransaction AS et 
				INNER JOIN relAccountTransaction AS rat ON rat.intTransactionID = et.intTransactionID AND rat.strRefundStatus IS NULL
				INNER JOIN lkpTransactionType AS ltt ON ltt.intTransactionTypeID = rat.intTransactionTypeID
				INNER JOIN relAccountPersonalInformation AS rapi ON rapi.intAccountID = rat.intAccountID
				INNER JOIN lkpPersonalInformationType AS lpit ON lpit.intPersonalInformationTypeID = rapi.intPersonalInformationTypeID AND lpit.strPersonalInformationType = 'groupMember'
				INNER JOIN entAccount AS ea ON ea.intAccountID = rat.intAccountID
				INNER JOIN lkpActiveStatus AS las ON las.intActiveStatusID = ea.intActiveStatusID AND LOWER(las.strActiveStatus) = 'active'
				INNER JOIN relAccountActivity AS raa ON raa.intAccountID = rat.intAccountID AND raa.strStatusChange IS NULL";
				
			if ($aSession['adminEventID'] != 1) {
				$sql .= " AND raa.intEventID = " . $aSession['adminEventID'];	
			}
			$sql .= " WHERE ltt.strTransactionType = 'groupRegistration' GROUP BY et.intTransactionID
			) AS dta
			GROUP BY dta.dateOnly";
			*/
			
			$sql = "SELECT COUNT(rapi.intPersonalInformationID) AS aCount, DATE(et.dttDateCreated) AS dateOnly FROM entTransaction AS et 
				INNER JOIN relAccountTransaction AS rat ON rat.intTransactionID = et.intTransactionID AND rat.strRefundStatus IS NULL 
				INNER JOIN lkpTransactionType AS ltt ON ltt.intTransactionTypeID = rat.intTransactionTypeID 
				INNER JOIN relAccountPersonalInformation AS rapi ON rapi.intAccountID = rat.intAccountID 
				INNER JOIN lkpPersonalInformationType AS lpit ON lpit.intPersonalInformationTypeID = rapi.intPersonalInformationTypeID AND lpit.strPersonalInformationType = 'groupMember' 
				INNER JOIN entAccount AS ea ON ea.intAccountID = rat.intAccountID 
				INNER JOIN lkpActiveStatus AS las ON las.intActiveStatusID = ea.intActiveStatusID AND LOWER(las.strActiveStatus) = 'active' 
				INNER JOIN relAccountActivity AS raa ON raa.intAccountID = rat.intAccountID AND raa.strStatusChange IS NULL";
				
			if ($aSession['adminEventID'] != 1) {
				$sql .= " AND raa.intEventID = " . $aSession['adminEventID'];	
			}
			$sql .= " WHERE ltt.strTransactionType = 'groupRegistration'
				GROUP BY dateOnly";

			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
			
		if (isset($result) && count($result) > 0) {
			$retArray = $result;
		}
		
		return $retArray;
	}
	
	//Gets the number of registrations by event
	public function getIndivRegistrationCountsByEvent() {
		$retArray = array();
		
		try {			
			$sql = "SELECT COUNT(raa.intEventID) AS aCount, raa.intEventID, le.strEvent AS aLabel FROM relAccountActivity AS raa
				INNER JOIN lkpEvent AS le ON le.intEventID = raa.intEventID
				INNER JOIN relAccountTransaction AS rat ON rat.intAccountID = raa.intAccountID AND rat.strRefundStatus IS NULL
				INNER JOIN lkpTransactionType AS ltt ON ltt.intTransactionTypeID = rat.intTransactionTypeID
				WHERE raa.strStatusChange IS NULL AND (ltt.strTransactionType = 'individualRegistration' OR ltt.strTransactionType = 'staffRegistration' OR ltt.strTransactionType = 'transfer')
				GROUP BY raa.intEventID";
							
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
			
		if (isset($result) && count($result) > 0) {
			$retArray = $result;
		}
		
		return $retArray;
	}
	
	public function getGroupRegistrationCountsByEvent() {
		$retArray = array();
		
		try {			
			$sql = "SELECT COUNT(rapi.intRelAccountPersonalInformationID) AS aCount, raa.intEventID, le.strEvent AS aLabel FROM relAccountPersonalInformation AS rapi
				INNER JOIN lkpPersonalInformationType AS lpit ON lpit.intPersonalInformationTypeID = rapi.intPersonalInformationTypeID AND lpit.strPersonalInformationType = 'groupMember'
				INNER JOIN entAccount AS ea ON ea.intAccountID = rapi.intAccountID
				INNER JOIN lkpActiveStatus AS las ON las.intActiveStatusID = ea.intActiveStatusID AND LOWER(las.strActiveStatus) = 'active'
				INNER JOIN relAccountTransaction AS rat ON rat.intAccountID = rapi.intAccountID AND rat.strRefundStatus IS NULL
				INNER JOIN lkpTransactionType AS ltt ON ltt.intTransactionTypeID = rat.intTransactionTypeID AND ltt.strTransactionType = 'groupRegistration'
				INNER JOIN relAccountActivity AS raa ON raa.intAccountID = rat.intAccountID AND raa.strStatusChange IS NULL
				INNER JOIN lkpEvent AS le ON le.intEventID = raa.intEventID
				GROUP BY rapi.intRelAccountPersonalInformationID";
							
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
			
		if (isset($result) && count($result) > 0) {
			$retArray = $result;
		}
		
		return $retArray;
	}
	
	//Gets the number of registrations by activity
	public function getIndivRegistrationCountsByActivity() {
		$retArray = array();
		
		$aSession = $this->session->userdata('admin');
		
		try {				
			$sql = "SELECT COUNT(raa.intActivityID) AS aCount, raa.intActivityID, le.strActivity AS aLabel FROM relAccountActivity AS raa
				INNER JOIN lkpActivity AS le ON le.intActivityID = raa.intActivityID
				INNER JOIN relAccountTransaction AS rat ON rat.intAccountID = raa.intAccountID AND rat.strRefundStatus IS NULL
				INNER JOIN lkpTransactionType AS ltt ON ltt.intTransactionTypeID = rat.intTransactionTypeID
				WHERE raa.strStatusChange IS NULL AND raa.intEventID = " . $aSession['adminEventID'] . " AND (ltt.strTransactionType = 'individualRegistration' OR ltt.strTransactionType = 'staffRegistration' OR ltt.strTransactionType = 'transfer')
				GROUP BY raa.intActivityID";		

			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
			
		if (isset($result) && count($result) > 0) {
			$retArray = $result;
		}
		
		return $retArray;
	}
	
	public function getGroupRegistrationCountsByActivity() {
		$retArray = array();
		
		$aSession = $this->session->userdata('admin');
		
		try {			
			$sql = "SELECT COUNT(rapi.intRelAccountPersonalInformationID) AS aCount, raa.intActivityID, le.strActivity AS aLabel FROM relAccountPersonalInformation AS rapi
				INNER JOIN lkpPersonalInformationType AS lpit ON lpit.intPersonalInformationTypeID = rapi.intPersonalInformationTypeID AND lpit.strPersonalInformationType = 'groupMember'
				INNER JOIN entAccount AS ea ON ea.intAccountID = rapi.intAccountID
				INNER JOIN lkpActiveStatus AS las ON las.intActiveStatusID = ea.intActiveStatusID AND LOWER(las.strActiveStatus) = 'active'
				INNER JOIN relAccountTransaction AS rat ON rat.intAccountID = rapi.intAccountID AND rat.strRefundStatus IS NULL
				INNER JOIN lkpTransactionType AS ltt ON ltt.intTransactionTypeID = rat.intTransactionTypeID AND ltt.strTransactionType = 'groupRegistration'
				INNER JOIN relAccountActivity AS raa ON raa.intAccountID = rat.intAccountID AND raa.strStatusChange IS NULL AND raa.intEventID = " . $aSession['adminEventID'] . "
				INNER JOIN lkpActivity AS le ON le.intActivityID = raa.intActivityID
				GROUP BY rapi.intRelAccountPersonalInformationID";
							
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
			
		if (isset($result) && count($result) > 0) {
			$retArray = $result;
		}
		
		return $retArray;
	}
	
	//Gets recent transactions
	public function getRecentTransactions() {
		$retArray = array();
		
		$aSession = $this->session->userdata('admin');
		
		try {
			if ($aSession['adminEventID'] == 1) {
				$sql = "SELECT et.intCartID, et.intTransactionID, ltt.strTransactionType, et.dblFinalTotal, et.dttDateCreated FROM entTransaction AS et
					INNER JOIN relAccountTransaction AS rat ON rat.intTransactionID = et.intTransactionID AND rat.strRefundStatus IS NULL
					INNER JOIN lkpTransactionType AS ltt ON ltt.intTransactionTypeID = rat.intTransactionTypeID	
					INNER JOIN relAccountActivity AS raa ON raa.intAccountID = rat.intAccountID AND raa.strStatusChange IS NULL
					GROUP BY et.intTransactionID
					ORDER BY et.dttDateCreated DESC 
					LIMIT 8";
			}
			else {
				$sql = "SELECT * FROM
					(SELECT et1.intCartID, et1.intTransactionID, ltt1.strTransactionType, et1.dblFinalTotal, et1.dttDateCreated FROM entTransaction AS et1
						INNER JOIN relAccountTransaction AS rat1 ON rat1.intTransactionID = et1.intTransactionID AND rat1.strRefundStatus IS NULL
						INNER JOIN lkpTransactionType AS ltt1 ON ltt1.intTransactionTypeID = rat1.intTransactionTypeID
						INNER JOIN relAccountActivity AS raa1 ON raa1.intAccountID = rat1.intAccountID AND raa1.intEventID = " . $aSession['adminEventID'] . " AND raa1.strStatusChange IS NULL
						WHERE ltt1.strTransactionType = 'individualRegistration' OR ltt1.strTransactionType = 'groupRegistration' OR ltt1.strTransactionType = 'staffRegistration'
						GROUP BY et1.intTransactionID
					UNION
					SELECT et2.intCartID, et2.intTransactionID, ltt2.strTransactionType, et2.dblFinalTotal, et2.dttDateCreated FROM entTransaction AS et2
						INNER JOIN relAccountTransaction AS rat2 ON rat2.intTransactionID = et2.intTransactionID AND rat2.strRefundStatus IS NULL
						INNER JOIN lkpTransactionType AS ltt2 ON ltt2.intTransactionTypeID = rat2.intTransactionTypeID
						INNER JOIN relAddonTransaction AS raat2 ON raat2.intTransactionID = et2.intTransactionID AND raat2.strRefundStatus IS NULL AND raat2.intEventID = " . $aSession['adminEventID'] . "
						WHERE ltt2.strTransactionType = 'extraPurchase'
						GROUP BY et2.intTransactionID 
					UNION
					SELECT et3.intCartID, et3.intTransactionID, ltt3.strTransactionType, et3.dblFinalTotal, et3.dttDateCreated FROM entTransaction AS et3
						INNER JOIN relAccountTransaction AS rat3 ON rat3.intTransactionID = et3.intTransactionID AND rat3.strRefundStatus IS NULL
						INNER JOIN lkpTransactionType AS ltt3 ON ltt3.intTransactionTypeID = rat3.intTransactionTypeID
						INNER JOIN relDonationTransaction AS rdt3 ON rdt3.intTransactionID = et3.intTransactionID AND rdt3.strRefundStatus IS NULL AND (rdt3.intEventID = 1 OR rdt3.intEventID = " . $aSession['adminEventID'] . ")
						WHERE ltt3.strTransactionType = 'oneTimeDonation' OR ltt3.strTransactionType = 'oneTimePledge'
						GROUP BY et3.intTransactionID
					UNION
					SELECT et4.intCartID, et4.intTransactionID, ltt4.strTransactionType, et4.dblFinalTotal, et4.dttDateCreated FROM entTransaction AS et4
						INNER JOIN relAccountTransaction AS rat4 ON rat4.intTransactionID = et4.intTransactionID AND rat4.strRefundStatus IS NULL
						INNER JOIN lkpTransactionType AS ltt4 ON ltt4.intTransactionTypeID = rat4.intTransactionTypeID
						INNER JOIN relDutTransaction AS rdt4 ON rdt4.intTransactionID = et4.intTransactionID AND rdt4.strRefundStatus IS NULL AND rdt4.intEventID = " . $aSession['adminEventID'] . "
						WHERE ltt4.strTransactionType = 'downgrade' OR ltt4.strTransactionType = 'upgrade' OR ltt4.strTransactionType = 'transfer'
						GROUP BY et4.intTransactionID) AS dta
					ORDER BY dttDateCreated DESC
					LIMIT 8";
			}
				
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
			
		if (isset($result) && count($result) > 0) {
			$retArray = $result;
		}
		
		return $retArray;
	}
	
	public function getTotalsByCharity() {
		$retArray = array();
		
		$aSession = $this->session->userdata('admin');
		
		try {			
			$sql = "SELECT SUM(rdt.dblDonationSubTotal) AS aSum, lc.strCharity FROM relDonationTransaction AS rdt
				INNER JOIN lkpCharity AS lc ON lc.intCharityID = rdt.intCharityID
				INNER JOIN relAccountActivity AS raa ON raa.intAccountID = rdt.intAccountID AND raa.strStatusChange IS NULL
				WHERE rdt.strRefundStatus IS NULL";
				if ($aSession['adminEventID'] != "1") {
					$sql .= " AND rdt.intEventID = 1 OR rdt.intEventID = " . $aSession['adminEventID'];
				}

			$sql .= " GROUP BY rdt.intCharityID";
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
			
		if (isset($result) && count($result) > 0) {
			$countResult = count($result);
			for ($i = 0; $i < $countResult; $i++) {
				$retArray[$result[$i]['strCharity']] = $result[$i]['aSum'];
			}			
		}
		
		return $retArray;
	}

	//Add a new admin account
	public function addAdminAccount() {
		$retValue = "goodtogo";
		
		$aPost = $this->input->sanitizeForDbPost();
		$aPassword = b_encrypt($aPost['apassword']);
		
		try {
			$sql = "INSERT INTO entAdminAccount (intAdminAccountTypeID,strAdminName,strAdminUserName,strAdminPassword,dttDateCreated) VALUES (" . $aPost['aaccess'] . ",'" . $aPost['aname'] . "','" . $aPost['aemail'] . "','" . $aPassword . "',NOW())";
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}			
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}

		return $retValue;
	}
	
	//Remove an admin account
	public function removeAdminAccount($anAdminAccountID) {
		$retValue = "goodtogo";
		
		try {
			$sql = "DELETE FROM entAdminAccount WHERE intAdminAccountID = " . $anAdminAccountID;
				
			$query = $this->db->query($sql);	
			if (!$query) {
				throw new Exception();
			}
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}

		return $retValue;
	}
	
	//Get all discount codes
	public function getAllDiscountCodes() {
		$retArray = array();
		
		try {
			$sql = "SELECT a.intDiscountCodeID, a.strDiscountCode, a.dblDiscountAmount, a.intQuantity, DATE(a.dttExpiration) AS dttExpiration, c.strActivity, d.strEvent, e.strDiscountCodeType, f.strActiveStatus FROM entDiscountCode AS a
				INNER JOIN relDiscountCode AS b ON b.intDiscountCodeID = a.intDiscountCodeID
				INNER JOIN lkpActivity AS c ON c.intActivityID = b.intActivityID
				INNER JOIN lkpEvent AS d ON d.intEventID = b.intEventID
				INNER JOIN lkpDiscountCodeType AS e ON e.intDiscountCodeTypeID = b.intDiscountCodeTypeID
				INNER JOIN lkpActiveStatus AS f ON f.intActiveStatusID = b.intActiveStatusID
				ORDER BY a.intDiscountCodeID ASC";
				
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}	
			
		if (isset($result) && count($result) > 0) {
			$retArray = $result;
		}
		
		return $retArray;
	}
	
	//Get the number of discount codes used and their discount code id.  This method will return an array where the key is the intDiscountCodeID and the value is the number of uses
	public function getDiscountCodesUsed() {
		$retArray = array();
		
		try {
			$sql = "SELECT intDiscountCodeID, COUNT(intDiscountCodeID) AS aCount FROM relRegistrationTransaction WHERE strRefundStatus IS NULL GROUP BY intDiscountCodeID";
				
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}	
			
		if (isset($result) && count($result) > 0) {
			$tempArray = $result;
			$countTempArray = count($tempArray);
			for ($i = 0; $i < $countTempArray; $i++) {
				$retArray[$tempArray[$i]['intDiscountCodeID']] = $tempArray[$i]['aCount'];
			}
		}
				
		return $retArray;
	}
	
	//Get all discount code types
	public function getAllDiscountCodeType() {
		$retArray = array();
		
		try {
			$sql = "SELECT intDiscountCodeTypeID, strDiscountCodeType FROM lkpDiscountCodeType";
				
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
			
		if (isset($result) && count($result) > 0) {
			$retArray = $result;
		}
		
		return $retArray;
	}
	
	//Get all events
	public function getAllEvent() {
		$retArray = array();
		
		try {
			$sql = "SELECT intEventID, strEvent FROM lkpEvent";
				
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}			
			
		if (isset($result) && count($result) > 0) {
			$retArray = $result;
		}
		
		return $retArray;
	}
	
	//Get all activities
	public function getAllActivity($eventID = "") {
		$retArray = array();
		
		try {
			if ($eventID == "") {
				$sql = "SELECT intActivityID, strActivity FROM lkpActivity";
			}
			else {
				$sql = "SELECT la.intActivityID, la.strActivity FROM relEventActivityCap AS reac
				INNER JOIN lkpActivity AS la ON la.intActivityID = reac.intActivityID
				WHERE reac.intEventID = " . $eventID;
			}
				
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			$retArray = $result;
		}
		
		return $retArray;
	}
	
	//Get all statuses
	public function getAllActiveStatus() {
		$retArray = array();
		
		try {
			$sql = "SELECT intActiveStatusID, strActiveStatus FROM lkpActiveStatus";
				
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}	
			
		if (isset($result) && count($result) > 0) {
			$retArray = $result;
		}
		
		return $retArray;
	}
	
	//Add discount code
	public function addDiscountCode() {
		$retValue = "goodtogo";
		$aPost = $this->input->sanitizeForDbPost();
		$anExpirationDate = $aPost['year'] . "-" . $aPost['month'] . "-" . $aPost['day'];
		
		try {
			$sql = "START TRANSACTION;
				INSERT INTO entDiscountCode (strDiscountCode,dblDiscountAmount,intQuantity,dttExpiration,dttDateCreated) VALUES ('" . $aPost['code'] . "'," . $aPost['amount'] . "," . $aPost['qty'] . ",'" . $anExpirationDate . "',NOW());
				SET @intDiscountCodeID = last_insert_id();
				INSERT INTO relDiscountCode (intEventID,intActivityID,intDiscountCodeID,intDiscountCodeTypeID,intActiveStatusID) VALUES (" . $aPost['event'] . "," . $aPost['activity'] . ",@intDiscountCodeID," . $aPost['type'] . "," . $aPost['status'] . ");
				COMMIT;";
				
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		return $retValue;
	}
	
	//Edit discount code
	public function editDiscountCode() {
		$retValue = "goodtogo";
		$aPost = $this->input->sanitizeForDbPost();
		$anExpirationDate = $aPost['year'] . "-" . $aPost['month'] . "-" . $aPost['day'];
		
		try {
			$sql = "START TRANSACTION;
				UPDATE entDiscountCode SET strDiscountCode = '" . $aPost['code'] . "',
					dblDiscountAmount = " . $aPost['amount'] . ",
					intQuantity = " . $aPost['qty'] . ",
					dttExpiration = '" . $anExpirationDate . "',					
					dttDateModified = NOW()
					WHERE intDiscountCodeID = " . $aPost['cid'] . ";
				UPDATE relDiscountCode SET intEventID = " . $aPost['event'] . ",
					intActivityID = " . $aPost['activity'] . ",
					intDiscountCodeTypeID = " . $aPost['type'] . ",
					intActiveStatusID = " . $aPost['status'] . "
					WHERE intRelDiscountCodeID = " . $aPost['crid'] . ";
				COMMIT;";		
		
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		return $retValue;
	}
	
	//Get discount code details by intDiscountCodeID
	public function getDiscountCodeByID($discountCodeID) {
		$retArray = array();
		
		try {
			$sql = "SELECT a.intDiscountCodeID, a.strDiscountCode, a.dblDiscountAmount, a.intQuantity, DATE(a.dttExpiration) AS dttExpiration, b.intRelDiscountCodeID, b.intEventID, b.intActivityID, b.intDiscountCodeTypeID, b.intActiveStatusID FROM entDiscountCode AS a
				INNER JOIN relDiscountCode AS b ON b.intDiscountCodeID = a.intDiscountCodeID
				WHERE a.intDiscountCodeID = " . $discountCodeID;
				
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			$retArray = $result;
		}
		
		return $retArray[0];
	}
	
	//Get all activities by intEventID.  Returns an array where the key is the intActivityID
	public function getActivityByEventID() {
		$retArray = array();
		
		$aSession = $this->session->userdata('admin');
		
		try {
			$sql = "SELECT a.intActivityID, a.strActivity, b.intCap FROM lkpActivity AS a
				INNER JOIN relEventActivityCap AS b ON b.intActivityID = a.intActivityID
				WHERE b.intEventID = " . $aSession['adminEventID'];
				
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}	
			
		if (isset($result) && count($result) > 0) {
			$tempArray = $result;
			$countTempArray = count($tempArray);
			for ($i = 0; $i < $countTempArray; $i++) {
				$retArray[$tempArray[$i]['intActivityID']]['strActivity'] = $tempArray[$i]['strActivity'];
				$retArray[$tempArray[$i]['intActivityID']]['intCap'] = $tempArray[$i]['intCap'];
			}
		}
		
		return $retArray;
	}
	
	public function getActivityForModifyParticipant() {
		$retArray = array();
		
		$aSession = $this->session->userdata('admin');
		
		try {
			$sql = "SELECT a.intActivityID, a.strActivity FROM lkpActivity AS a
				INNER JOIN relEventActivityCap AS b ON b.intActivityID = a.intActivityID
				WHERE b.intEventID = " . $aSession['adminEventID'];
				
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}	
			
		if (isset($result) && count($result) > 0) {
			$countResult = count($result);
			for ($i = 0; $i < $countResult; $i++) {
				$retArray[$result[$i]['intActivityID']] = $result[$i]['strActivity'];
			}
		}
		
		return $retArray;
	}
	
	//Add an activity cap
	public function updateActivityCap() {
		$retValue = "goodtogo";
		
		$aPost = $this->input->sanitizeForDbPost();
		$aSession = $this->session->userdata('admin');		
		
		try {
			$sql = "UPDATE relEventActivityCap SET intCap = " . $aPost['cap'] . " WHERE intEventID = " . $aSession['adminEventID'] . " AND intActivityID = " . $aPost['activity'];
				
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}		

		return $retValue;
	}
	
	//Gets the count of the number of participants registered per activity.  Returns an array where the key is the intActivityID and the value is the number of participants registered for that activity
	public function getRegisteredCountForActivity() {
		$retArray = array();
		
		$aSession = $this->session->userdata('admin');
		
		try {
			$sql = "SELECT intActivityID, COUNT(intActivityID) AS 'aCount' FROM relAccountActivity AS raa
				INNER JOIN relAccountPersonalInformation AS rapi ON rapi.intAccountID = raa.intAccountID
				INNER JOIN lkpPersonalInformationType AS lpit ON lpit.intPersonalInformationTypeID = rapi.intPersonalInformationTypeID
				WHERE raa.intEventID = " . $aSession['adminEventID'] . " AND strStatusChange IS NULL AND (lpit.strPersonalInformationType = 'individual' OR lpit.strPersonalInformationType = 'individualBillingMember' OR lpit.strPersonalInformationType = 'groupMember')
				GROUP BY raa.intActivityID";
								
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
			
		if (isset($result) && count($result) > 0) {
			$tempArray = $result;
			$countTempArray = count($tempArray);
			for ($i = 0; $i < $countTempArray; $i++) {
				$retArray[$tempArray[$i]['intActivityID']] = $tempArray[$i]['aCount'];
			}
		}
		
		return $retArray;
	}
	
	//Gets the activity cap details by intActivityID
	public function getActivityCapByActivityID($activityID) {
		$retArray = array();
		
		$aSession = $this->session->userdata('admin');
		
		try {
			$sql = "SELECT a.intActivityID, a.strActivity, b.intCap FROM lkpActivity AS a
				INNER JOIN relEventActivityCap AS b ON b.intActivityID = a.intActivityID
				WHERE b.intEventID = " . $aSession['adminEventID'] . " AND b.intActivityID = " . $activityID;
				
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			$tempArray = $result;
			$countTempArray = count($tempArray);
			for ($i = 0; $i < $countTempArray; $i++) {
				$retArray['intActivityID'] = $tempArray[$i]['intActivityID'];
				$retArray['strActivity'] = $tempArray[$i]['strActivity'];
				$retArray['intCap'] = $tempArray[$i]['intCap'];
			}
		}
		
		return $retArray;
	}
	
	//Gets the dates that enables the transfer tool to show up and disappear in the participant dashboard
	public function getTransferAvailabilityDates() {
		$retArray = array();
		
		$aSession = $this->session->userdata('admin');
		
		try {
			$sql = "SELECT a.dttTransferStartDate, a.dttTransferEndDate FROM lkpTransferAvailability AS a
				INNER JOIN relTransferAvailability AS b ON b.intTransferAvailabilityID = a.intTransferAvailabilityID
				WHERE b.intEventID = " . $aSession['adminEventID'];
				
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}	
			
		if (isset($result) && count($result) > 0) {
			$retArray = $result[0];		
			$formatStartDate = strtotime($retArray['dttTransferStartDate']);
			$retArray['dttTransferStartDate'] = date('m/d/Y g:i A',$formatStartDate);
			$formatEndDate = strtotime($retArray['dttTransferEndDate']);
			$retArray['dttTransferEndDate'] = date('m/d/Y g:i A',$formatEndDate);
		}
		
		return $retArray;
	}
	
	//Adds a transfer availability open date and close date
	public function addTransferAvailabilityDate() {
		$retValue = "goodtogo";
		
		$aSession = $this->session->userdata('admin');
		$aPost = $this->input->sanitizeForDbPost();
		
		try {		
			$sql = "START TRANSACTION;
				INSERT INTO lkpTransferAvailability (dttTransferStartDate,dttTransferEndDate,dttDateCreated) VALUES (STR_TO_DATE('" . $aPost['txtTransferOpenDate'] . "','%m/%d/%Y %h:%i %p'),STR_TO_DATE('" . $aPost['txtTransferCloseDate'] . "','%m/%d/%Y %h:%i %p'),NOW());
				SET @intTransferAvailabilityID = last_insert_id();
				INSERT INTO relTransferAvailability (intTransferAvailabilityID,intEventID) VALUES (@intTransferAvailabilityID," . $aSession['adminEventID'] . ");
				COMMIT;";
									
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}			
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}

		return $retValue;
	}
	
	//Update the transfer availability open date and close date
	public function editTransferAvailabilityDate() {
		$retValue = "goodtogo";
		
		$aSession = $this->session->userdata('admin');
		$aPost = $this->input->sanitizeForDbPost();
				
		try {
			$sql = "START TRANSACTION;
				SELECT @intTransferAvailabilityID := intTransferAvailabilityID FROM relTransferAvailability WHERE intEventID = " . $aSession['adminEventID'] . ";
				UPDATE lkpTransferAvailability SET dttTransferStartDate = STR_TO_DATE('" . $aPost['txtTransferOpenDate'] . "','%m/%d/%Y %h:%i %p'),
					dttTransferEndDate = STR_TO_DATE('" . $aPost['txtTransferCloseDate'] . "','%m/%d/%Y %h:%i %p'),
					dttDateModified = NOW()
					WHERE intTransferAvailabilityID = @intTransferAvailabilityID;
				COMMIT;";

			$query = $this->db->query($sql);	
			if (!$query) {
				throw new Exception();
			}
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}

		return $retValue;
	}
	
	//Gets the dates that enables the participant modify tool to show up and disappear in the participant dashboard
	public function getParticipantModifyAvailabilityDates() {
		$retArray = array();
		
		$aSession = $this->session->userdata('admin');
		
		try {
			$sql = "SELECT a.dttPublicModifyStartDate, a.dttPublicModifyEndDate FROM lkpPublicModifyAvailability AS a
				INNER JOIN relPublicModifyAvailability AS b ON b.intPublicModifyAvailabilityID = a.intPublicModifyAvailabilityID
				WHERE b.intEventID = " . $aSession['adminEventID'];
				
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			$retArray = $result[0];		
			$formatStartDate = strtotime($retArray['dttPublicModifyStartDate']);
			$retArray['dttPublicModifyStartDate'] = date('m/d/Y g:i A',$formatStartDate);
			$formatEndDate = strtotime($retArray['dttPublicModifyEndDate']);
			$retArray['dttPublicModifyEndDate'] = date('m/d/Y g:i A',$formatEndDate);
		}
		
		return $retArray;
	}
	
	//Adds a participant modify availability open date and close date
	public function addParticipantModifyAvailabilityDate() {
		$retValue = "goodtogo";
		
		$aSession = $this->session->userdata('admin');
		$aPost = $this->input->sanitizeForDbPost();
				
		try {
			$sql = "START TRANSACTION;
				INSERT INTO lkpPublicModifyAvailability (dttPublicModifyStartDate,dttPublicModifyEndDate,dttDateCreated) VALUES (STR_TO_DATE('" . $aPost['txtParticipantModifyOpenDate'] . "','%m/%d/%Y %h:%i %p'),STR_TO_DATE('" . $aPost['txtParticipantModifyCloseDate'] . "','%m/%d/%Y %h:%i %p'),NOW());
				SET @intPublicModifyAvailabilityID = last_insert_id();
				INSERT INTO relPublicModifyAvailability (intPublicModifyAvailabilityID,intEventID) VALUES (@intPublicModifyAvailabilityID," . $aSession['adminEventID'] . ");
				COMMIT;";
							
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}

		return $retValue;
	}
	
	//Update the participant modify availability open date and close date
	public function editParticipantModifyAvailabilityDate() {
		$retValue = "goodtogo";
		
		$aSession = $this->session->userdata('admin');
		$aPost = $this->input->sanitizeForDbPost();
		
		try {		
			$sql = "START TRANSACTION;
				SELECT @intPublicModifyAvailabilityID := intPublicModifyAvailabilityID FROM relPublicModifyAvailability WHERE intEventID = " . $aSession['adminEventID'] . ";
				UPDATE lkpPublicModifyAvailability SET dttPublicModifyStartDate = STR_TO_DATE('" . $aPost['txtParticipantModifyOpenDate'] . "','%m/%d/%Y %h:%i %p'),
					dttPublicModifyEndDate = STR_TO_DATE('" . $aPost['txtParticipantModifyCloseDate'] . "','%m/%d/%Y %h:%i %p'),
					dttDateModified = NOW()
					WHERE intPublicModifyAvailabilityID = @intPublicModifyAvailabilityID;
				COMMIT;";
		
			$query = $this->db->query($sql);	
			if (!$query) {
				throw new Exception();
			}
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}

		return $retValue;
	}
	
	//Get all participants depending on the eventID selected in the admin navigation side bar
	public function getAllParticipants() {
		$retArray = array();
		
		$aSession = $this->session->userdata('admin');
		
		try {
			$sql = "SELECT a.strFirstName, a.strLastName, c.strActivity FROM entPersonalInformation AS a
				LEFT JOIN relParticipantShirt AS b on b.intPersonalInformationID = a.intPersonalInformationID
				LEFT JOIN lkpActivity AS c on c.intActivityID = b.intActivityID
				WHERE b.intEventID = " . $aSession['adminEventID'];
				
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}	
			
		if (isset($result) && count($result) > 0) {
			$retArray = $result;					
		}
		
		return $retArray;
	}
	
	//Get all staff entries
	public function getAllStaffEntries() {
		$retArray = array();
		
		$aSession = $this->session->userdata('admin');
		
		try {
			$sql = "SELECT a.intTransactionID, f.strFirstName, f.strLastName, h.strActivity, a.dttDateCreated, a.dblFinalTotal, b.intTransactionPaidStatusID, b.strTransactionPaidStatus FROM entTransaction AS a 
				INNER JOIN lkpTransactionPaidStatus AS b ON b.intTransactionPaidStatusID = a.intTransactionPaidStatusID
				INNER JOIN relRegistrationTransaction AS c ON c.intTransactionID = a.intTransactionID AND c.strRefundStatus IS NULL
				INNER JOIN lkpRegistrationTransactionType AS d ON d.intRegistrationTransactionTypeID = c.intRegistrationTransactionTypeID
				INNER JOIN relAccountPersonalInformation AS e ON e.intAccountID = c.intAccountID
				INNER JOIN entPersonalInformation AS f ON f.intPersonalInformationID = e.intPersonalInformationID
				INNER JOIN relAccountActivity AS g ON g.intAccountID = c.intAccountID
				INNER JOIN lkpActivity AS h ON h.intActivityID = g.intActivityID
				WHERE strRegistrationTransactionType = 'staffEntry' AND g.strStatusChange IS NULL AND g.intEventID = " . $aSession['adminEventID'];
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			$retArray = $result;					
		}
		
		return $retArray;
	}
	
	//Get all paid statuses
	public function getAllPaidStatuses() {
		$retArray = array();
				
		try {
			$sql = "SELECT intTransactionPaidStatusID, strTransactionPaidStatus FROM lkpTransactionPaidStatus";
				
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}			
			
		if (isset($result) && count($result) > 0) {
			$retArray = $result;					
		}
		
		return $retArray;
	}
	
	//Update the staff entry paid statuses
	public function updateStaffEntryPaidStatuses() {
		$retValue = "goodtogo";
		$sql = "START TRANSACTION;";
		
		$aPost = $this->input->sanitizeForDbPost();
		
		foreach ($aPost as $postKey => $postValue) {
			if (stripos($postKey, "paidStatus") === 0) {
				$explodeKey = explode("_",$postKey);
				$getTransactionID = $explodeKey[1];
				$sql .= " UPDATE entTransaction SET intTransactionPaidStatusID = " . $postValue . ", dttDateModified = NOW() WHERE intTransactionID = " . $getTransactionID . ";";
			}
		}
		$sql .= " COMMIT;";
		
		try {
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}			
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}

		return $retValue;
	}
	
	//Generate the race data to be used by the race data download
	public function getRaceData() {
		$retArray = array();
		$eventDate = "";
		
		$aSession = $this->session->userdata('admin');
		
		try {
			$sql = "SELECT dttEventDate FROM lkpEvent WHERE intEventID = " . $aSession['adminEventID'];
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}	
			
		if (isset($result) && count($result) > 0) {
			$eventDate = $result[0]['dttEventDate'];					
		}
		
		//-------------------------------
		
		try {
			if ($aSession['adminEventID'] == 1) {
				$sql = "SELECT d.intPersonalInformationID, le.strEvent, h.strActivity, b.strUserName, d.strFirstName, d.strLastName, d.strAddress, d.strCity, d.strProvince, d.strCountry, d.strPostalCode, d.strHomePhone, d.strMobilePhone, d.strEmail, d.strGender, d.dteDateOfBirth, d.intAgeOnRegistration, 
					(SELECT DATE_FORMAT('" . date("Y") . "-12-31', '%Y') - DATE_FORMAT(d.dteDateOfBirth, '%Y') - (DATE_FORMAT('" . date("Y") . "-12-31', '00-%m-%d') < DATE_FORMAT(d.dteDateOfBirth, '00-%m-%d'))) AS ageOnDec31, 
					(SELECT DATE_FORMAT((
						SELECT le2.dttEventDate FROM lkpEvent AS le2 WHERE le2.intEventID = a.intEventID
					), '%Y') - DATE_FORMAT(d.dteDateOfBirth, '%Y') - (DATE_FORMAT((
						SELECT le3.dttEventDate FROM lkpEvent AS le3 WHERE le3.intEventID = a.intEventID
					), '00-%m-%d') < DATE_FORMAT(d.dteDateOfBirth, '00-%m-%d'))) AS ageOnRaceDay, 
					d.strSeriesNewsletter, d.strNationality, d.strNationalityDuration, d.strIndustryType, d.strIndustryTypeOther, d.strYearlySalary, d.strEmergencyContactName, d.strEmergencyContactNumber, d.strMedicalCondition, f.strShirtSize, 
					CONCAT(g.intFinishTimeHour, 'h:', g.intFinishTimeMinute, 'm') AS aFinishTime, 
					k.strLearnAboutEvent, k.strLearnAboutEventOther, k.strFirstTimeParticipating, k.strHowManyTimesParticipated, k.strWhyParticipating, l.strIsCaptain, m.strTeamName, m.strGroupName, 
					b.dttDateCreated FROM relAccountActivity AS a
							INNER JOIN entAccount AS b ON b.intAccountID = a.intAccountID AND b.intActiveStatusID = 1
							INNER JOIN relAccountTransaction AS rat ON rat.intAccountID = b.intAccountID AND rat.strRefundStatus IS NULL AND (rat.intTransactionTypeID = 1 OR rat.intTransactionTypeID = 2 OR rat.intTransactionTypeID = 3 OR rat.intTransactionTypeID = 9)					
							INNER JOIN relAccountPersonalInformation AS c ON c.intAccountID = a.intAccountID
							INNER JOIN entPersonalInformation AS d ON d.intPersonalInformationID = c.intPersonalInformationID
							INNER JOIN relParticipantShirt AS e ON e.intPersonalInformationID = d.intPersonalInformationID AND e.intEventID = a.intEventID
							INNER JOIN lkpShirt AS f ON f.intShirtID = e.intShirtID
							INNER JOIN relParticipantFinishTime AS g ON g.intPersonalInformationID = d.intPersonalInformationID AND g.intEventID = a.intEventID
							INNER JOIN lkpActivity AS h ON h.intActivityID = a.intActivityID
							INNER JOIN lkpEvent AS le ON le.intEventID = a.intEventID
							LEFT JOIN relExtraField AS j ON j.intAccountID = a.intAccountID AND j.intEventID = a.intEventID
							LEFT JOIN entExtraField AS k ON k.intExtraFieldID = j.intExtraFieldID
							LEFT JOIN relTeam AS l ON l.intPersonalInformationID = d.intPersonalInformationID AND l.intEventID = a.intEventID
							LEFT JOIN entTeam AS m ON m.intTeamID = l.intTeamID
							WHERE a.strStatusChange IS NULL
							ORDER BY d.intPersonalInformationID";
			}
			else {
				//HAD TO HARD CODE SOME VALUES IN THIS QUERY TO IMPROVE THE PROCESSING TIME OF THE QUERY.  FOR SOME REASON LINKING TO TWO EXTRA LOOKUP TABLES VASTLY INCREASED THE PROCESSING TIME
				$sql = "SELECT d.intPersonalInformationID, h.strActivity, b.strUserName, d.strFirstName, d.strLastName, d.strAddress, d.strCity, d.strProvince, d.strCountry, d.strPostalCode, d.strHomePhone, d.strMobilePhone, d.strEmail, d.strGender, d.dteDateOfBirth, d.intAgeOnRegistration, 
				(SELECT DATE_FORMAT('" . date("Y") . "-12-31', '%Y') - DATE_FORMAT(d.dteDateOfBirth, '%Y') - (DATE_FORMAT('" . date("Y") . "-12-31', '00-%m-%d') < DATE_FORMAT(d.dteDateOfBirth, '00-%m-%d'))) AS ageOnDec31, 
				(SELECT DATE_FORMAT('" . $eventDate . "', '%Y') - DATE_FORMAT(d.dteDateOfBirth, '%Y') - (DATE_FORMAT('" . $eventDate . "', '00-%m-%d') < DATE_FORMAT(d.dteDateOfBirth, '00-%m-%d'))) AS ageOnRaceDay, 
				d.strSeriesNewsletter, d.strNationality, d.strNationalityDuration, d.strIndustryType, d.strIndustryTypeOther, d.strYearlySalary, d.strEmergencyContactName, d.strEmergencyContactNumber, d.strMedicalCondition, f.strShirtSize, 
				CONCAT(g.intFinishTimeHour, 'h:', g.intFinishTimeMinute, 'm') AS aFinishTime, 
				k.strLearnAboutEvent, k.strLearnAboutEventOther, k.strFirstTimeParticipating, k.strHowManyTimesParticipated, k.strWhyParticipating, l.strIsCaptain, m.strTeamName, m.strGroupName, b.dttDateCreated FROM relAccountActivity AS a
						INNER JOIN entAccount AS b ON b.intAccountID = a.intAccountID AND b.intActiveStatusID = 1
						INNER JOIN relAccountTransaction AS rat ON rat.intAccountID = b.intAccountID AND rat.strRefundStatus IS NULL AND (rat.intTransactionTypeID = 1 OR rat.intTransactionTypeID = 2 OR rat.intTransactionTypeID = 3 OR rat.intTransactionTypeID = 9)					
						INNER JOIN relAccountPersonalInformation AS c ON c.intAccountID = a.intAccountID
						INNER JOIN entPersonalInformation AS d ON d.intPersonalInformationID = c.intPersonalInformationID
						INNER JOIN relParticipantShirt AS e ON e.intPersonalInformationID = d.intPersonalInformationID AND e.intEventID = " . $aSession['adminEventID'] . "
						INNER JOIN lkpShirt AS f ON f.intShirtID = e.intShirtID
						INNER JOIN relParticipantFinishTime AS g ON g.intPersonalInformationID = d.intPersonalInformationID AND g.intEventID = " . $aSession['adminEventID'] . "
						INNER JOIN lkpActivity AS h ON h.intActivityID = a.intActivityID
						LEFT JOIN relExtraField AS j ON j.intAccountID = a.intAccountID AND j.intEventID = " . $aSession['adminEventID'] . "
						LEFT JOIN entExtraField AS k ON k.intExtraFieldID = j.intExtraFieldID
						LEFT JOIN relTeam AS l ON l.intPersonalInformationID = d.intPersonalInformationID AND l.intEventID = " . $aSession['adminEventID'] . "
						LEFT JOIN entTeam AS m ON m.intTeamID = l.intTeamID
						WHERE a.strStatusChange IS NULL AND a.intEventID = " . $aSession['adminEventID'] . "
						GROUP BY d.intPersonalInformationID";
			}

			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}	
			
		if (isset($result) && count($result) > 0) {
			$retArray = $result;					
		}
		
		return $retArray;
	}
	
	public function getAccountData() {
		$retArray = array();
		
		$aSession = $this->session->userdata('admin');
		
		try {				
			if ($aSession['adminEventID'] == 1) {
				$sql = "SELECT * FROM
					(SELECT et1.intTransactionID, et1.intCartID, ltt1.strTransactionType, raa1.intAccountID, epi1.strFirstName, epi1.strLastName, et1.dblActivityCostTotal, et1.dblAddonTotal, et1.dblDutTotal, et1.dblDiscountTotal, et1.dblTaxTotal, et1.dblEolFee, et1.dblDonationTotal, et1.dblFinalTotal, et1.dblRemit, et1.dttDateCreated FROM entTransaction AS et1
						INNER JOIN relAccountTransaction AS rat1 ON rat1.intTransactionID = et1.intTransactionID AND rat1.strRefundStatus IS NULL
						INNER JOIN lkpTransactionType AS ltt1 ON ltt1.intTransactionTypeID = rat1.intTransactionTypeID
						INNER JOIN relAccountActivity AS raa1 ON raa1.intAccountID = rat1.intAccountID AND raa1.strStatusChange IS NULL
						INNER JOIN relAccountPersonalInformation AS rapi1 ON rapi1.intAccountID = raa1.intAccountID
						INNER JOIN entPersonalInformation AS epi1 ON epi1.intPersonalInformationID = rapi1.intPersonalInformationID
						WHERE ltt1.strTransactionType = 'individualRegistration' OR ltt1.strTransactionType = 'groupRegistration' OR ltt1.strTransactionType = 'staffRegistration'
						GROUP BY et1.intTransactionID
					UNION
					SELECT et2.intTransactionID, et2.intCartID, ltt2.strTransactionType, rat2.intAccountID, epi2.strFirstName, epi2.strLastName, et2.dblActivityCostTotal, et2.dblAddonTotal, et2.dblDutTotal, et2.dblDiscountTotal, et2.dblTaxTotal, et2.dblEolFee, et2.dblDonationTotal, et2.dblFinalTotal, et2.dblRemit, et2.dttDateCreated FROM entTransaction AS et2
						INNER JOIN relAccountTransaction AS rat2 ON rat2.intTransactionID = et2.intTransactionID AND rat2.strRefundStatus IS NULL
						INNER JOIN lkpTransactionType AS ltt2 ON ltt2.intTransactionTypeID = rat2.intTransactionTypeID
						INNER JOIN relAccountPersonalInformation AS rapi2 ON rapi2.intAccountID = rat2.intAccountID
						INNER JOIN entPersonalInformation AS epi2 ON epi2.intPersonalInformationID = rapi2.intPersonalInformationID
						INNER JOIN relAddonTransaction AS raat2 ON raat2.intTransactionID = et2.intTransactionID AND raat2.strRefundStatus IS NULL
						WHERE ltt2.strTransactionType = 'extraPurchase'
						GROUP BY et2.intTransactionID 
					UNION
					SELECT et3.intTransactionID, et3.intCartID, ltt3.strTransactionType, rat3.intAccountID, epi3.strFirstName, epi3.strLastName, et3.dblActivityCostTotal, et3.dblAddonTotal, et3.dblDutTotal, et3.dblDiscountTotal, et3.dblTaxTotal, et3.dblEolFee, et3.dblDonationTotal, et3.dblFinalTotal, et3.dblRemit, et3.dttDateCreated FROM entTransaction AS et3
						INNER JOIN relAccountTransaction AS rat3 ON rat3.intTransactionID = et3.intTransactionID AND rat3.strRefundStatus IS NULL
						INNER JOIN lkpTransactionType AS ltt3 ON ltt3.intTransactionTypeID = rat3.intTransactionTypeID
						INNER JOIN relAccountPersonalInformation AS rapi3 ON rapi3.intAccountID = rat3.intAccountID
						INNER JOIN entPersonalInformation AS epi3 ON epi3.intPersonalInformationID = rapi3.intPersonalInformationID
						INNER JOIN relDonationTransaction AS rdt3 ON rdt3.intTransactionID = et3.intTransactionID AND rdt3.strRefundStatus IS NULL
						WHERE ltt3.strTransactionType = 'oneTimeDonation' OR ltt3.strTransactionType = 'oneTimePledge'
						GROUP BY et3.intTransactionID
					UNION
					SELECT et4.intTransactionID, et4.intCartID, ltt4.strTransactionType, rat4.intAccountID, epi4.strFirstName, epi4.strLastName, et4.dblActivityCostTotal, et4.dblAddonTotal, et4.dblDutTotal, et4.dblDiscountTotal, et4.dblTaxTotal, et4.dblEolFee, et4.dblDonationTotal, et4.dblFinalTotal, et4.dblRemit, et4.dttDateCreated FROM entTransaction AS et4
						INNER JOIN relAccountTransaction AS rat4 ON rat4.intTransactionID = et4.intTransactionID AND rat4.strRefundStatus IS NULL
						INNER JOIN lkpTransactionType AS ltt4 ON ltt4.intTransactionTypeID = rat4.intTransactionTypeID
						INNER JOIN relAccountPersonalInformation AS rapi4 ON rapi4.intAccountID = rat4.intAccountID
						INNER JOIN entPersonalInformation AS epi4 ON epi4.intPersonalInformationID = rapi4.intPersonalInformationID
						INNER JOIN relDutTransaction AS rdt4 ON rdt4.intTransactionID = et4.intTransactionID AND rdt4.strRefundStatus IS NULL
						WHERE ltt4.strTransactionType = 'downgrade' OR ltt4.strTransactionType = 'upgrade' OR ltt4.strTransactionType = 'transfer'
						GROUP BY et4.intTransactionID) AS dta
					ORDER BY dttDateCreated ASC";
			}
			else {
				$sql = "SELECT * FROM
						(SELECT et1.intTransactionID, et1.intCartID, ltt1.strTransactionType, raa1.intAccountID, epi1.strFirstName, epi1.strLastName, et1.dblActivityCostTotal, et1.dblAddonTotal, et1.dblDutTotal, et1.dblDiscountTotal, et1.dblTaxTotal, et1.dblEolFee, et1.dblDonationTotal, et1.dblFinalTotal, et1.dblRemit, et1.dttDateCreated FROM entTransaction AS et1
							INNER JOIN relAccountTransaction AS rat1 ON rat1.intTransactionID = et1.intTransactionID AND rat1.strRefundStatus IS NULL
							INNER JOIN lkpTransactionType AS ltt1 ON ltt1.intTransactionTypeID = rat1.intTransactionTypeID
							INNER JOIN relAccountActivity AS raa1 ON raa1.intAccountID = rat1.intAccountID AND raa1.strStatusChange IS NULL AND raa1.intEventID = " . $aSession['adminEventID'] . "
							INNER JOIN relAccountPersonalInformation AS rapi1 ON rapi1.intAccountID = raa1.intAccountID
							INNER JOIN entPersonalInformation AS epi1 ON epi1.intPersonalInformationID = rapi1.intPersonalInformationID
							WHERE ltt1.strTransactionType = 'individualRegistration' OR ltt1.strTransactionType = 'groupRegistration' OR ltt1.strTransactionType = 'staffRegistration'
							GROUP BY et1.intTransactionID
						UNION
						SELECT et2.intTransactionID, et2.intCartID, ltt2.strTransactionType, rat2.intAccountID, epi2.strFirstName, epi2.strLastName, et2.dblActivityCostTotal, et2.dblAddonTotal, et2.dblDutTotal, et2.dblDiscountTotal, et2.dblTaxTotal, et2.dblEolFee, et2.dblDonationTotal, et2.dblFinalTotal, et2.dblRemit, et2.dttDateCreated FROM entTransaction AS et2
							INNER JOIN relAccountTransaction AS rat2 ON rat2.intTransactionID = et2.intTransactionID AND rat2.strRefundStatus IS NULL
							INNER JOIN lkpTransactionType AS ltt2 ON ltt2.intTransactionTypeID = rat2.intTransactionTypeID
							INNER JOIN relAccountPersonalInformation AS rapi2 ON rapi2.intAccountID = rat2.intAccountID
							INNER JOIN entPersonalInformation AS epi2 ON epi2.intPersonalInformationID = rapi2.intPersonalInformationID
							INNER JOIN relAddonTransaction AS raat2 ON raat2.intTransactionID = et2.intTransactionID AND raat2.strRefundStatus IS NULL AND raat2.intEventID = " . $aSession['adminEventID'] . "
							WHERE ltt2.strTransactionType = 'extraPurchase'
							GROUP BY et2.intTransactionID 
						UNION
						SELECT et3.intTransactionID, et3.intCartID, ltt3.strTransactionType, rat3.intAccountID, epi3.strFirstName, epi3.strLastName, et3.dblActivityCostTotal, et3.dblAddonTotal, et3.dblDutTotal, et3.dblDiscountTotal, et3.dblTaxTotal, et3.dblEolFee, et3.dblDonationTotal, et3.dblFinalTotal, et3.dblRemit, et3.dttDateCreated FROM entTransaction AS et3
							INNER JOIN relAccountTransaction AS rat3 ON rat3.intTransactionID = et3.intTransactionID AND rat3.strRefundStatus IS NULL
							INNER JOIN lkpTransactionType AS ltt3 ON ltt3.intTransactionTypeID = rat3.intTransactionTypeID
							INNER JOIN relAccountPersonalInformation AS rapi3 ON rapi3.intAccountID = rat3.intAccountID
							INNER JOIN entPersonalInformation AS epi3 ON epi3.intPersonalInformationID = rapi3.intPersonalInformationID
							INNER JOIN relDonationTransaction AS rdt3 ON rdt3.intTransactionID = et3.intTransactionID AND rdt3.strRefundStatus IS NULL AND (rdt3.intEventID = 1 OR rdt3.intEventID = " . $aSession['adminEventID'] . ")
							WHERE ltt3.strTransactionType = 'oneTimeDonation' OR ltt3.strTransactionType = 'oneTimePledge'
							GROUP BY et3.intTransactionID
						UNION
						SELECT et4.intTransactionID, et4.intCartID, ltt4.strTransactionType, rat4.intAccountID, epi4.strFirstName, epi4.strLastName, et4.dblActivityCostTotal, et4.dblAddonTotal, et4.dblDutTotal, et4.dblDiscountTotal, et4.dblTaxTotal, et4.dblEolFee, et4.dblDonationTotal, et4.dblFinalTotal, et4.dblRemit, et4.dttDateCreated FROM entTransaction AS et4
							INNER JOIN relAccountTransaction AS rat4 ON rat4.intTransactionID = et4.intTransactionID AND rat4.strRefundStatus IS NULL
							INNER JOIN lkpTransactionType AS ltt4 ON ltt4.intTransactionTypeID = rat4.intTransactionTypeID
							INNER JOIN relAccountPersonalInformation AS rapi4 ON rapi4.intAccountID = rat4.intAccountID
							INNER JOIN entPersonalInformation AS epi4 ON epi4.intPersonalInformationID = rapi4.intPersonalInformationID
							INNER JOIN relDutTransaction AS rdt4 ON rdt4.intTransactionID = et4.intTransactionID AND rdt4.strRefundStatus IS NULL AND rdt4.intEventID = " . $aSession['adminEventID'] . "
							WHERE ltt4.strTransactionType = 'downgrade' OR ltt4.strTransactionType = 'upgrade' OR ltt4.strTransactionType = 'transfer'
							GROUP BY et4.intTransactionID) AS dta
						ORDER BY dttDateCreated ASC";
			}
					
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			$retArray = $result;					
		}
		
		return $retArray;
	}
	
	public function getAddonData() {
		$retArray = array();
		
		$aSession = $this->session->userdata('admin');
		
		try {
			if ($aSession['adminEventID'] == 1) {
				$sql = "SELECT g.intPersonalInformationID, le.strEvent, g.strFirstName, g.strLastName, b.strAddon, a.intQty, c.strAddonTransactionType, i.strAddonShirt, h.dttDateCreated FROM relAddonTransaction AS a
					INNER JOIN lkpAddon AS b ON b.intAddonID = a.intAddonID
					INNER JOIN lkpAddonTransactionType AS c ON c.intAddonTransactionTypeID = a.intAddonTransactionTypeID				
					INNER JOIN entPersonalInformation AS g ON g.intPersonalInformationID = a.intPersonalInformationID
					INNER JOIN entTransaction AS h ON h.intTransactionID = a.intTransactionID
					INNER JOIN relAccountActivity AS raa ON raa.intAccountID = a.intAccountID AND raa.intEventID = a.intEventID AND raa.strStatusChange IS NULL
					INNER JOIN lkpEvent AS le ON le.intEventID = a.intEventID
					LEFT JOIN lkpAddonShirt AS i ON i.intAddonShirtID = a.intAddonShirtID 
					WHERE a.strRefundStatus IS NULL
					ORDER BY g.intPersonalInformationID";
			}
			else {
				$sql = "SELECT g.intPersonalInformationID, g.strFirstName, g.strLastName, b.strAddon, a.intQty, c.strAddonTransactionType, i.strAddonShirt, h.dttDateCreated FROM relAddonTransaction AS a
					INNER JOIN lkpAddon AS b ON b.intAddonID = a.intAddonID
					INNER JOIN lkpAddonTransactionType AS c ON c.intAddonTransactionTypeID = a.intAddonTransactionTypeID				
					INNER JOIN entPersonalInformation AS g ON g.intPersonalInformationID = a.intPersonalInformationID
					INNER JOIN entTransaction AS h ON h.intTransactionID = a.intTransactionID
					INNER JOIN relAccountActivity AS raa ON raa.intAccountID = a.intAccountID AND raa.intEventID = a.intEventID AND raa.strStatusChange IS NULL
					LEFT JOIN lkpAddonShirt AS i ON i.intAddonShirtID = a.intAddonShirtID 
					WHERE a.strRefundStatus IS NULL AND a.intEventID = " . $aSession['adminEventID'];
			}
					
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}	
			
		if (isset($result) && count($result) > 0) {
			$retArray = $result;					
		}
		
		return $retArray;
	}
	
	public function getPledgeDonationData() {
		$retArray = array();
		
		$aSession = $this->session->userdata('admin');
		
		try {
			if ($aSession['adminEventID'] == 1) {
				$sql = "SELECT f.intPersonalInformationID, g.strFirstName, g.strLastName, b.strCharity, a.dblDonationSubTotal, c.strDonationTransactionType, h.dttDateCreated FROM relDonationTransaction AS a
					INNER JOIN lkpCharity AS b ON b.intCharityID = a.intCharityID
					INNER JOIN lkpDonationTransactionType AS c ON c.intDonationTransactionTypeID = a.intDonationTransactionTypeID
					INNER JOIN entAccount AS d ON d.intAccountID = a.intAccountID
					INNER JOIN relAccountActivity AS e ON e.intAccountID = a.intAccountID AND e.strStatusChange IS NULL
					INNER JOIN relAccountPersonalInformation AS f ON f.intAccountID = a.intAccountID
					INNER JOIN entPersonalInformation AS g ON g.intPersonalInformationID = f.intPersonalInformationID
					INNER JOIN entTransaction AS h ON h.intTransactionID = a.intTransactionID
					WHERE a.strRefundStatus IS NULL
					GROUP BY a.intRelDonationTransactionID
					ORDER BY f.intPersonalInformationID";
			}
			else {
				$sql = "SELECT f.intPersonalInformationID, g.strFirstName, g.strLastName, b.strCharity, a.dblDonationSubTotal, c.strDonationTransactionType, h.dttDateCreated FROM relDonationTransaction AS a
					INNER JOIN lkpCharity AS b ON b.intCharityID = a.intCharityID
					INNER JOIN lkpDonationTransactionType AS c ON c.intDonationTransactionTypeID = a.intDonationTransactionTypeID
					INNER JOIN entAccount AS d ON d.intAccountID = a.intAccountID
					INNER JOIN relAccountActivity AS e ON e.intAccountID = a.intAccountID AND e.intEventID = " . $aSession['adminEventID'] . " AND e.strStatusChange IS NULL
					INNER JOIN relAccountPersonalInformation AS f ON f.intAccountID = a.intAccountID
					INNER JOIN entPersonalInformation AS g ON g.intPersonalInformationID = f.intPersonalInformationID
					INNER JOIN entTransaction AS h ON h.intTransactionID = a.intTransactionID
					WHERE a.strRefundStatus IS NULL
					GROUP BY a.intRelDonationTransactionID
					ORDER BY f.intPersonalInformationID";
			}
					
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			$retArray = $result;					
		}
		
		return $retArray;
	}
	
	public function getTshirtCaps($activityID) {
		$retArray = array();
		
		$aSession = $this->session->userdata('admin');
		
		try {
			$sql = "SELECT b.intShirtID, a.intShirtCap, b.strShirtSize FROM relShirt AS a
				INNER JOIN lkpShirt AS b ON b.intShirtID = a.intShirtID
				WHERE a.intEventID = " . $aSession['adminEventID'] . " AND a.intActivityID = " . $activityID . "
				ORDER BY b.intShirtID ASC";
							
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}	
			
		if (isset($result) && count($result) > 0) {
			$retArray = $result;					
		}
		
		return $retArray;
	}
	
	//Gets the Tshirt Assigned details.  Returns an array where the key is the intShirtID and the value is the count of how many of those shirts have been assigned
	public function getTshirtAssigned($activityID) {
		$retArray = array();
		
		$aSession = $this->session->userdata('admin');
		
		try {
			$sql = "SELECT intShirtID, COUNT(intShirtID) AS 'aCount' FROM relParticipantShirt 
				WHERE intEventID = " . $aSession['adminEventID'] . " AND intActivityID = " . $activityID . " GROUP BY intShirtID";
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			$countResult = count($result);
			for ($i = 0; $i < $countResult;	$i++) {
				$retArray[$result[$i]['intShirtID']] = $result[$i]['aCount'];
			}			
		}
		
		return $retArray;
	}
	
	public function getTshirtCapByTshirtID($activityID,$tshirtID) {
		$retArray = array();
		
		$aSession = $this->session->userdata('admin');
		
		try {
			$sql = "SELECT a.intShirtID, b.strShirtSize, a.intActivityID, c.strActivity, a.intShirtCap FROM relShirt AS a
				INNER JOIN lkpShirt AS b ON b.intShirtID = a.intShirtID
				INNER JOIN lkpActivity AS c ON c.intActivityID = a.intActivityID
				WHERE a.intEventID = " . $aSession['adminEventID'] . " AND a.intActivityID = " . $activityID . " AND a.intShirtID = " . $tshirtID;
							
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}	
			
		if (isset($result) && count($result) > 0) {
			$retArray = $result[0];					
		}
		
		return $retArray;
	}
	
	public function editTshirtCap() {
		$retValue = "goodtogo";
		$aSession = $this->session->userdata('admin');
		$aPost = $this->input->sanitizeForDbPost();
		
		try {
			$sql = "UPDATE relShirt SET intShirtCap = '" . $aPost['cap'] . "'
					WHERE intShirtID = " . $aPost['tshirt'] . " AND intEventID = " . $aSession['adminEventID'] . " AND intActivityID = " . $aPost['activity'];
					
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		return $retValue;
	}
	
	public function getAllParticipantTokens() {
		$retArray = array();
			
		$aSession = $this->session->userdata('admin');
		
		try {
			$sql = "SELECT a.intPersonalInformationID, a.strFirstName, a.strLastName, a.strEmail, c.strActivity, lpit.strPersonalInformationType FROM entPersonalInformation AS a
				INNER JOIN relAccountPersonalInformation AS rapi on rapi.intPersonalInformationID = a.intPersonalInformationID
				INNER JOIN lkpPersonalInformationType AS lpit on lpit.intPersonalInformationTypeID = rapi.intPersonalInformationTypeID
				INNER JOIN relAccountActivity AS raa ON raa.intAccountID = rapi.intAccountID AND raa.strStatusChange IS NULL
				INNER JOIN relParticipantShirt AS b on b.intPersonalInformationID = a.intPersonalInformationID AND b.intEventID = " . $aSession['adminEventID'] . "
				INNER JOIN lkpActivity AS c on c.intActivityID = b.intActivityID
				WHERE raa.intEventID = " . $aSession['adminEventID'] . "
				GROUP BY a.intPersonalInformationID";
						
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}	
			
		if (isset($result) && count($result) > 0) {
			$countResult = count($result);
			for ($i = 0; $i < $countResult; $i++) {
				$anID = $result[$i]['intPersonalInformationID'];
				$aFirstName = $result[$i]['strFirstName'];
				$aLastName = $result[$i]['strLastName'];
				$anEmail = $result[$i]['strEmail'];
				$anActivity = $result[$i]['strActivity'];
				$aPersonalInformationType = $result[$i]['strPersonalInformationType'];
				
				$tokenArray = array($anID,$anEmail,$aFirstName,$aLastName);
				$tempArray = array("name" => ($aFirstName . " " . $aLastName), "value" => $anID, "race" => $anActivity, "email" => $anEmail, "personalInformationType" => $aPersonalInformationType, "tokens" => $tokenArray);
				$retArray[$i] = $tempArray;
			}						
		}
		
		//$tokenArray = array("JME1402506567","100 Colonnade","TESTHYWADMIN5","TESTHYWADMIN5b");
		//$tempArray = array("name" => "TESTHYWADMIN5 TESTHYWADMIN5b", "value" => "JME1402506567", "race" => "Run/Walk", "email" => "100 Colonnade", "tokens" => $tokenArray);
		//$retArray[0] = $tempArray;
		//echo "<pre>"; print_r($retArray); echo "</pre>"; exit;
		
		return $retArray;
	}
	
	//Gets all the participant information by intPersonalInformationID
	public function getParticipantInfoByPID() {
		$retArray = array();						
		
		$aSession = $this->session->userdata('admin');
		$aPost = $this->input->sanitizeForDbPost();
		$intPersonalInformationID = $aPost['invoice'];
		$strPersonalInformationType = $aPost['piType'];
		
		if ($strPersonalInformationType == "individual" || $strPersonalInformationType == "individualBillingMember") {
			$sql = "SELECT a.strFirstName, a.strLastName, a.strAddress, a.strCity, a.strProvince, a.strCountry, a.strPostalCode, a.strHomePhone, a.strMobilePhone, a.strEmail, a.strGender, a.dteDateOfBirth, a.intAgeOnRegistration, a.strCategory, a.strSeriesNewsletter, a.strNationality, a.strNationalityDuration, a.strIndustryType, a.strIndustryTypeOther, a.strYearlySalary, a.strEmergencyContactName, a.strEmergencyContactNumber, a.strMedicalCondition, b.intShirtID, c.intFinishTimeHour, c.intFinishTimeMinute, d.intTeamID, d.strIsCaptain, g.strLearnAboutEvent, g.strLearnAboutEventOther, g.strFirstTimeParticipating, g.strHowManyTimesParticipated, g.strWhyParticipating, la.strActivity, ls.strShirtDescription, et.strTeamName FROM entPersonalInformation AS a
				INNER JOIN relParticipantShirt AS b ON b.intPersonalInformationID = a.intPersonalInformationID AND b.intActivityID = " . $aPost['activity'] . " AND b.intEventID = " . $aSession['adminEventID'] . "
				INNER JOIN lkpActivity AS la ON la.intActivityID = b.intActivityID
				INNER JOIN lkpShirt AS ls ON ls.intShirtID = b.intShirtID
				INNER JOIN relParticipantFinishTime AS c ON c.intPersonalInformationID = a.intPersonalInformationID AND c.intEventID = " . $aSession['adminEventID'] . "
				LEFT JOIN relTeam AS d ON d.intPersonalInformationID = a.intPersonalInformationID AND d.intEventID = " . $aSession['adminEventID'] . "
				LEFT JOIN entTeam AS et ON et.intTeamID = d.intTeamID
				LEFT JOIN relAccountPersonalInformation AS e ON e.intPersonalInformationID = a.intPersonalInformationID
				LEFT JOIN relExtraField AS f ON f.intAccountID = e.intAccountID AND f.intEventID = " . $aSession['adminEventID'] . "
				LEFT JOIN entExtraField AS g ON g.intExtraFieldID = f.intExtraFieldID
				WHERE a.intPersonalInformationID = " . $intPersonalInformationID;
		}
		else {
			$sql = "SELECT a.strFirstName, a.strLastName, a.strAddress, a.strCity, a.strProvince, a.strCountry, a.strPostalCode, a.strHomePhone, a.strMobilePhone, a.strEmail, a.strGender, a.dteDateOfBirth, a.intAgeOnRegistration, b.intShirtID, c.intFinishTimeHour, c.intFinishTimeMinute, d.intTeamID, d.strIsCaptain, la.strActivity, ls.strShirtDescription, et.strTeamName FROM entPersonalInformation AS a
				INNER JOIN relParticipantShirt AS b ON b.intPersonalInformationID = a.intPersonalInformationID AND b.intActivityID = " . $aPost['activity'] . " AND b.intEventID = " . $aSession['adminEventID'] . "
				INNER JOIN lkpActivity AS la ON la.intActivityID = b.intActivityID
				INNER JOIN lkpShirt AS ls ON ls.intShirtID = b.intShirtID
				INNER JOIN relParticipantFinishTime AS c ON c.intPersonalInformationID = a.intPersonalInformationID AND c.intEventID = " . $aSession['adminEventID'] . "
				LEFT JOIN relTeam AS d ON d.intPersonalInformationID = a.intPersonalInformationID AND d.intEventID = " . $aSession['adminEventID'] . "
				LEFT JOIN entTeam AS et ON et.intTeamID = d.intTeamID
				WHERE a.intPersonalInformationID = " . $intPersonalInformationID;
		}
			
		try {
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			$retExtraFieldArray = array();
			$retArray = array(
				"pTeam" => array(
					"type" => "hidden",                
					"label" => "",
					"value" => $result[0]['intTeamID'],
					"previousValue" => "",
					"required" => ""
				),
				"selActivity" => array(
					"type" => "select",                
					"label" => "Activity",
					"value" => $aPost['activity'],
					"required" => "required",
					"previousValue" => $result[0]['strActivity'],
					"data" => $this->getActivityForModifyParticipant()
				),
				"tshirt" => array(
					"type" => "select",                
					"label" => "Technical T-shirt",
					"value" => $result[0]['intShirtID'],
					"required" => "required",
					"previousValue" => $result[0]['strShirtDescription'],
					"data" => $this->getTshirtForParticipantModify($aPost['activity'],$result[0]['intShirtID'])
				),
				"first_name" => array(
					"type" => "text",                
					"label" => "First Name",
					"value" => $result[0]['strFirstName'],
					"required" => "required"
				),
				"last_name" => array(
					"type" => "text",                
					"label" => "Last Name",
					"value" => $result[0]['strLastName'],
					"required" => "required"
				),
				"address" => array(
					"type" => "text",                
					"label" => "Address",
					"value" => $result[0]['strAddress'],
					"required" => "required"
				),
				"city" => array(
					"type" => "text",                
					"label" => "City",
					"value" => $result[0]['strCity'],
					"required" => "required"
				),
				"province" => array(
					"type" => "select",                
					"label" => "Province/State",
					"value" => $result[0]['strProvince'],
					"required" => "required",
					"data" => getProvinceState()
				),
				"country" => array(
					"type" => "select",                
					"label" => "Country",
					"value" => $result[0]['strCountry'],
					"required" => "required",
					"data" => getCountry()
				),
				"postal" => array(
					"type" => "text",                
					"label" => "Postal Code/Zip Code",
					"value" => $result[0]['strPostalCode'],
					"required" => "required"
				),
				"home_phone" => array(
					"type" => "text",                
					"label" => "Home Phone",
					"value" => $result[0]['strHomePhone'],
					"required" => "required"
				),
				"mobile_phone" => array(
					"type" => "text",                
					"label" => "Mobile Phone",
					"value" => $result[0]['strMobilePhone'],
					"required" => ""
				),
				"email" => array(
					"type" => "text",                
					"label" => "Email",
					"value" => $result[0]['strEmail'],
					"required" => "required"
				),
				"date_of_birth" => array(
					"type" => "text",                
					"label" => "Date of Birth",
					"value" => $result[0]['dteDateOfBirth'],
					"required" => "required",
					"readonly" => "readonly"
				),
				"gender" => array(
					"type" => "select",                
					"label" => "Gender",
					"value" => $result[0]['strGender'],
					"required" => "required",
					"data" => array(
						"M" => "Male",
						"F" => "Female"
					)
				),				
				"expected_finish_hour" => array(
					"type" => "select",                
					"label" => "Expected Finish Time (Hour)",
					"value" => $result[0]['intFinishTimeHour'],
					"required" => "required",
					"data" => $this->getFinishTimeHour()
				),
				"expected_finish_minute" => array(
					"type" => "select",                
					"label" => "Expected Finish Time (Minute)",
					"value" => $result[0]['intFinishTimeMinute'],
					"required" => "required",
					"data" => $this->getFinishTimeMinute()
				),
				"team" => array(
					"type" => "select",                
					"label" => "Team",
					"value" => ((isset($result[0]['intTeamID']))?$result[0]['intTeamID']:''),
					"required" => "",
					"previousValue" => $result[0]['strTeamName'],
					"data" => $this->getTeam()
				),
				"team_captain" => array(
					"type" => "select",                
					"label" => "Team Captain?",
					"value" => ((isset($result[0]['strIsCaptain']))?$result[0]['strIsCaptain']:''),
					"required" => "",
					"data" => array(
						"" => "",
						"Yes" => "Yes",
						"No" => "No"
					)
				),
				"category" => array(
					"type" => "checkbox",                
					"label" => "Category",
					"value" => ((isset($result[0]['strCategory']))?$result[0]['strCategory']:''),
					"required" => "",
					"data" => array(
						"Recreational Wheelchair" => "Recreational Wheelchair",
						"Guide" => "Guide",
						"Nordic Poles" => "Nordic Poles"
					)
				),
				"series_newsletter" => array(
					"type" => "select",                
					"label" => "Series Newsletter",
					"value" => ((isset($result[0]['strSeriesNewsletter']))?$result[0]['strSeriesNewsletter']:''),
					"required" => "",
					"data" => array(
						"" => "",
						"Yes" => "Yes",
						"No" => "No"
					)
				),
				"nationality" => array(
					"type" => "select",                
					"label" => "Nationality",
					"value" => ((isset($result[0]['strNationality']))?$result[0]['strNationality']:''),
					"required" => "",
					"data" => array(
						"" => "",
						"Canadian" => "Canadian",
						"Landed immigrant" => "Landed immigrant",
						"Refugee" => "Refugee",
						"Other" => "Other"
					)
				),
				"nationality_duration" => array(
					"type" => "select",                
					"label" => "Please specify how many months you have been in Canada",
					"value" => ((isset($result[0]['strNationalityDuration']))?$result[0]['strNationalityDuration']:''),
					"required" => "",
					"data" => array(
						"" => "",
						"Less than 1 month" => ">1 month",
						"1" => "1",
						"2" => "2",
						"3" => "3",
						"4" => "4",
						"5" => "5",
						"6" => "6",
						"7" => "7",
						"8" => "8",
						"9" => "9",
						"10" => "10",
						"11" => "11",
						"12+" => "12+"
					)
				),
				"type_of_industry" => array(
					"type" => "select",                
					"label" => "In what type of industry do you work?",
					"value" => ((isset($result[0]['strIndustryType']))?$result[0]['strIndustryType']:''),
					"required" => "",
					"data" => array(
						"" => "",
						"Administration" => "Administration",
						"Banking/Finance" => "Banking/Finance",
						"Communications/Marketing" => "Communications/Marketing",
						"Education (k-12)" => "Education (k-12)",
						"Education (post-secondary)" => "Education (post-secondary)",
						"Event Planning" => "Event Planning",
						"Government" => "Government",
						"Health Care" => "Health Care",
						"Hospitality" => "Hospitality",
						"Insurance" => "Insurance",
						"Manufacturing" => "Manufacturing",
						"Retail" => "Retail",
						"Transportation" => "Transportation",
						"Other" => "Other"
					)
				),
				"type_of_industry_other" => array(
					"type" => "text",                
					"label" => "Type of Industry (Other)",
					"value" => ((isset($result[0]['strIndustryTypeOther']))?$result[0]['strIndustryTypeOther']:''),
					"required" => ""
				),
				"yearly_salary" => array(
					"type" => "select",                
					"label" => "Yearly Salary",
					"value" => ((isset($result[0]['strYearlySalary']))?$result[0]['strYearlySalary']:''),
					"required" => "",
					"data" => array(
						"" => "Select",
						"Less than 30,000" => ">30,000",
						"30,001 - 50,000" => "30,001 - 50,000",
						"50,001 – 70,000" => "50,001 – 70,000",
						"70,001 - 90,000" => "70,001 - 90,000",
						"90,001 – 110,000" => "90,001 – 110,000",
						"110,001+" => "110,001+"
					)
				),
				"emergency_name" => array(
					"type" => "text",                
					"label" => "Emergency Contact Name",
					"value" => ((isset($result[0]['strEmergencyContactName']))?$result[0]['strEmergencyContactName']:''),
					"required" => ""
				),
				"emergency_number" => array(
					"type" => "text",                
					"label" => "Emergency Contact Number",
					"value" => ((isset($result[0]['strEmergencyContactNumber']))?$result[0]['strEmergencyContactNumber']:''),
					"required" => ""
				),
				"medical_conditions" => array(
					"type" => "text",                
					"label" => "Medical Conditions",
					"value" => ((isset($result[0]['strMedicalCondition']))?$result[0]['strMedicalCondition']:''),
					"required" => ""
				)
			);
			
			if ($strPersonalInformationType == "individual" || $strPersonalInformationType == "individualBillingMember") {
				$retExtraFieldArray = array(					
					"learn_about_event" => array(
						"type" => "select",                
						"label" => "How did you learn about this event?",
						"value" => $result[0]['strLearnAboutEvent'],
						"required" => "",
						"data" => array(
							"" => "",
							"Affiliated Charity" => "Affiliated Charity",
							"Expo at another race" => "Expo at another race",
							"Friends & Family" => "Friends & Family",
							"Insert in Race Kit" => "Insert in Race Kit",
							"Magazine" => "Magazine",
							"Metro Newspaper" => "Metro Newspaper",
							"Running Store" => "Running Store",
							"Social Media" => "Social Media",
							"Website" => "Website",
							"Other" => "Other (if other, please specify)"
						)
					),
					"learn_about_event_other" => array(
						"type" => "text",                
						"label" => "Learn About Event (Other)",
						"value" => $result[0]['strLearnAboutEventOther'],
						"required" => ""
					),
					"first_time_participating" => array(
						"type" => "select",                
						"label" => "Is this your first time participating in this event?",
						"value" => $result[0]['strFirstTimeParticipating'],
						"required" => "",
						"data" => array(
							"" => "",
							"Yes" => "Yes",
							"No" => "No"
						)
					),
					"times_participated" => array(
						"type" => "select",                
						"label" => "How many times have you participated before?",
						"value" => $result[0]['strHowManyTimesParticipated'],
						"required" => "",
						"data" => array(
							"" => "",
							1 => 1,
							2 => 2,
							3 => 3,
							4 => 4,
							"5+" => "5+"
						)
					),
					"why_participating" => array(
						"type" => "text",                
						"label" => "Why are you participating in this event?",
						"value" => $result[0]['strWhyParticipating'],
						"required" => ""
					)
				);
			}
			$retArray = array_merge($retArray,$retExtraFieldArray);
		}		
		
		return $retArray;
	}
	
	public function getTshirtForParticipantModify($activityID,$shirtID) {
		$retArray = array();
		
		$aSession = $this->session->userdata('admin');
		
		$arrShirtCount = array();
		try {
			//Get the count of all participant shirts that have been taken so far based on event and activity
			$sql = "SELECT COUNT(intShirtID) AS aCount, intShirtID FROM relParticipantShirt WHERE intEventID = " . $aSession['adminEventID'] . " and intActivityID = " . $activityID . " GROUP BY intShirtID";
									
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}

		if (isset($result) && count($result) > 0) {			
			$countResult = count($result);
			for ($i = 0; $i < $countResult; $i++) {
				$arrShirtCount[$result[$i]['intShirtID']] = $result[$i]['aCount'];
			}
		}
		
		try {			
			//Get all shirts and their caps based on event and activity
			$sql = "SELECT ls.intShirtID, ls.strShirtDescription, rs.intShirtCap FROM relShirt AS rs
				INNER JOIN lkpShirt AS ls ON ls.intShirtID = rs.intShirtID 
				WHERE rs.intEventID = " . $aSession['adminEventID'] . " AND rs.intActivityID = " . $activityID . " ORDER BY ls.intShirtID";

			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}	
			
		if (isset($result) && count($result) > 0) {			
			$countResult = count($result);
			for ($i = 0; $i < $countResult; $i++) {
				$getShirtID = $result[$i]['intShirtID'];
				$shirtCap = $result[$i]['intShirtCap'];
				
				$numShirtTaken = ((isset($arrShirtCount[$getShirtID]))?$arrShirtCount[$getShirtID]:0);
				if (($shirtCap - $numShirtTaken) > 0 || $getShirtID == $shirtID) {					
					$retArray[$getShirtID] = $result[$i]['strShirtDescription'];
				}
			}
		}
		
		//------------------------
		/*
		try {
			$sql = "SELECT a.intShirtID, b.strShirtDescription FROM relShirt AS a
				INNER JOIN lkpShirt AS b ON b.intShirtID = a.intShirtID
				WHERE a.intEventID = " . $aSession['adminEventID'];
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}	
			
		if (isset($result) && count($result) > 0) {
			$countResult = count($result);
			for ($i = 0; $i < $countResult; $i++) {
				$retArray[$result[$i]['intShirtID']] = $result[$i]['strShirtDescription'];
			}
		}
		*/
				
		return $retArray;
	}
	
	public function getFinishTimeHour() {
		$retArray = array();
		
		for ($i = 0; $i < 24; $i++) {
			$retArray[$i] = $i;
		}
		
		return $retArray;
	}
	
	public function getFinishTimeMinute() {
		$retArray = array();
		
		for ($i = 0; $i < 59; $i++) {
			$retArray[$i] = $i;
		}
		
		return $retArray;
	}
	
	public function getTeam() {
		$retArray = array();
		
		try {
			$sql = "SELECT intTeamID, strTeamName FROM entTeam";
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			$retArray[''] = "";
			$countResult = count($result);
			for ($i = 0; $i < $countResult; $i++) {
				$retArray[$result[$i]['intTeamID']] = $result[$i]['strTeamName'];
			}
		}
				
		return $retArray;
	}
	
	public function updateParticipantDetails() {
		$retValue = "goodtogo";
		
		$aPost = $this->input->sanitizeForDbPost();
		$aSession = $this->session->userdata('admin');
	
		$aCategory = implode("|",$aPost['category']);
				
		$sql = "START TRANSACTION;
			UPDATE entPersonalInformation SET 
				strFirstName = '" . $aPost['first_name'] . "',
				strLastName = '" . $aPost['last_name'] . "',
				strAddress = '" . $aPost['address'] . "',
				strCity = '" . $aPost['city'] . "',
				strProvince = '" . $aPost['province'] . "',
				strCountry = '" . $aPost['country'] . "',
				strPostalCode = '" . $aPost['postal'] . "',
				strHomePhone = '" . $aPost['home_phone'] . "',
				strMobilePhone = '" . $aPost['mobile_phone'] . "',
				strEmail = '" . $aPost['email'] . "',
				dteDateOfBirth = '" . $aPost['date_of_birth'] . "',
				strGender = '" . $aPost['gender'] . "' ,
				strCategory = '" . $aCategory . "',
				strSeriesNewsletter = '" . $aPost['series_newsletter'] . "',
				strNationality = '" . $aPost['nationality'] . "',
				strNationalityDuration = '" . $aPost['nationality_duration'] . "',
				strIndustryType = '" . $aPost['type_of_industry'] . "',
				strIndustryTypeOther = '" . $aPost['type_of_industry_other'] . "',
				strYearlySalary = '" . $aPost['yearly_salary'] . "',
				strEmergencyContactName = '" . $aPost['emergency_name'] . "',
				strEmergencyContactNumber = '" . $aPost['emergency_number'] . "',
				strMedicalCondition = '" . $aPost['medical_conditions'] . "'
				WHERE intPersonalInformationID = " . $aPost['pid'] . ";	
				
			UPDATE relParticipantFinishTime SET 
				intFinishTimeHour = " . $aPost['expected_finish_hour'] . ",
				intFinishTimeMinute = " . $aPost['expected_finish_minute'] . "
				WHERE intPersonalInformationID = " . $aPost['pid'] . " AND intEventID = " . $aSession['adminEventID'] . ";";
				
		//Check if the activity has been changed
		if ($aPost['pActivity'] == $aPost['selActivity']) {
			$sql .= " UPDATE relParticipantShirt SET 
					intShirtID = " . $aPost['tshirt'] . " 
					WHERE intPersonalInformationID = " . $aPost['pid'] . " AND intActivityID = " . $aPost['pActivity'] . " AND intEventID = " . $aSession['adminEventID'] . ";";
		}
		else {
			$sql .= " UPDATE relParticipantShirt SET 
					intShirtID = " . $aPost['tshirt'] . ",
					intActivityID = " . $aPost['selActivity'] . " 
					WHERE intPersonalInformationID = " . $aPost['pid'] . " AND intActivityID = " . $aPost['pActivity'] . " AND intEventID = " . $aSession['adminEventID'] . ";
					
				SELECT @dblActivityPrice := dblActivityPrice, @intAccountID := intAccountID FROM relAccountActivity WHERE strStatusChange IS NULL AND intActivityID = " . $aPost['pActivity'] . " AND intEventID = " . $aSession['adminEventID'] . " AND intAccountID = (
						SELECT intAccountID FROM relAccountPersonalInformation WHERE intPersonalInformationID = " . $aPost['pid'] . ");
						
				UPDATE relAccountActivity SET 
					strStatusChange = 'adminDashboardModified',
					dttDateModified = NOW()
					WHERE strStatusChange IS NULL AND intActivityID = " . $aPost['pActivity'] . " AND intEventID = " . $aSession['adminEventID'] . " AND intAccountID = @intAccountID;
					
				INSERT INTO relAccountActivity (intActivityID, intEventID, intAccountID, dblActivityPrice, dttDateCreated) VALUES (" . $aPost['selActivity'] . ", " . $aSession['adminEventID'] . ", @intAccountID, @dblActivityPrice, NOW());";
		}
				
		//Check if a team was selected
		if (trim($aPost['team']) == "") {
			$sql .= " DELETE FROM relTeam WHERE intPersonalInformationID = " . $aPost['pid'] . " AND intEventID = " . $aSession['adminEventID'] . ";";
		}
		else {
			//No previous team so insert the new team
			if (trim($aPost['pTeam']) == "") {
				$sql .= " INSERT INTO relTeam (intTeamID, intPersonalInformationID, intEventID, strIsCaptain) VALUES (" . $aPost['team'] . ", " . $aPost['pid'] . ", " . $aSession['adminEventID'] . ", '" . strtolower($aPost['team_captain']) . "');";
			}
			//There was a previous team so update the team info
			else {
				$sql .= " UPDATE relTeam SET 
						intTeamID = " . $aPost['team'] . ",
						strIsCaptain = '" . strtolower($aPost['team_captain']) . "'
						WHERE intPersonalInformationID = " . $aPost['pid'] . " AND intEventID = " . $aSession['adminEventID'] . ";";
			}
		}
				
		if ($aPost['pType'] == "individual" || $aPost['pType'] == "individualBillingMember") {			
			$sql .= " SELECT @intExtraFieldID := intExtraFieldID FROM relExtraField AS a
					INNER JOIN relAccountPersonalInformation AS b ON b.intAccountID = a.intAccountID
					WHERE b.intPersonalInformationID = " . $aPost['pid'] . " AND a.intEventID = " . $aSession['adminEventID'] . ";
					
				UPDATE entExtraField SET					
					strLearnAboutEvent = '" . $aPost['learn_about_event'] . "',
					strLearnAboutEventOther = '" . $aPost['learn_about_event_other'] . "',
					strFirstTimeParticipating = '" . $aPost['first_time_participating'] . "',
					strHowManyTimesParticipated = '" . $aPost['times_participated'] . "',
					strWhyParticipating = '" . $aPost['why_participating'] . "'
					WHERE intExtraFieldID = @intExtraFieldID;";
		}
		$sql .= " COMMIT;";
			
		try {
			$query = $this->db->query($sql);	
			if (!$query) {
				throw new Exception();
			}
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}

		return $retValue;
	}
	
	public function processChangeTeamPassword() {
		$aPost = $this->input->sanitizeForDbPost();
		
		$teamID = $aPost['selTeam'];
		$teamName = $aPost['teamName'];
		$newPassword = b_encrypt($aPost['teamPassword']);
		
		try {
			$sql = "UPDATE entTeam SET strTeamName = '" . $teamName . "', strTeamPassword = '" . $newPassword . "' WHERE intTeamID = " . $teamID;
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
	}
	
	public function getTeamByEventID() {
		$retArray = array();
		
		$aSession = $this->session->userdata('admin');
		
		try {
			$sql = "SELECT et.intTeamID, et.strTeamName FROM entTeam AS et
				INNER JOIN relTeam AS rt ON rt.intTeamID = et.intTeamID
				WHERE rt.intEventID = " . $aSession['adminEventID'];
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			//$retArray[''] = "";
			$countResult = count($result);
			for ($i = 0; $i < $countResult; $i++) {
				$retArray[$result[$i]['intTeamID']] = $result[$i]['strTeamName'];
			}
		}
				
		return $retArray;
	}
	
	public function getAllTransactionData() {
		$retArray = array();
			
		try {
			$sql = "SELECT * FROM 
				(SELECT et.intCartID, rat.intAccountID, et.intTransactionID, ltt.strTransactionType, lpit.strPersonalInformationType, epi.strFirstName, epi.strLastName, epi.strCity, epi.strProvince, epi.strHomePhone, epi.strEmail, et.dblDonationTotal, et.dblActivityCostTotal, et.dblAddonTotal, et.dblDutTotal, et.dblDiscountTotal, et.dblTaxTotal, et.dblFinalTotal, et.dblEolFee, et.dblRemit, et.dttDateCreated, rrt.strDiscountType, rrt.intDiscountCodeID, rrt.dblDiscount, rrt.dblArtezDiscount FROM entTransaction AS et
					INNER JOIN relAccountTransaction AS rat ON rat.intTransactionID = et.intTransactionID
					INNER JOIN lkpTransactionType AS ltt ON ltt.intTransactionTypeID = rat.intTransactionTypeID AND (ltt.strTransactionType = 'individualRegistration' OR ltt.strTransactionType = 'groupRegistration')
					INNER JOIN relAccountPersonalInformation AS rapi ON rapi.intAccountID = rat.intAccountID
					INNER JOIN lkpPersonalInformationType AS lpit ON lpit.intPersonalInformationTypeID = rapi.intPersonalInformationTypeID AND (lpit.strPersonalInformationType = 'individualBillingMember' OR lpit.strPersonalInformationType = 'groupBillingMember' OR lpit.strPersonalInformationType = 'individual')
					INNER JOIN entPersonalInformation AS epi ON epi.intPersonalInformationID = rapi.intPersonalInformationID
					LEFT JOIN relRegistrationTransaction AS rrt ON rrt.intAccountID = rat.intAccountID
				UNION
				SELECT et2.intCartID, rat2.intAccountID, et2.intTransactionID, ltt2.strTransactionType, lpit2.strPersonalInformationType,  epi2.strFirstName, epi2.strLastName, epi2.strCity, epi2.strProvince, epi2.strHomePhone, epi2.strEmail, et2.dblDonationTotal, et2.dblActivityCostTotal, et2.dblAddonTotal, et2.dblDutTotal, et2.dblDiscountTotal, et2.dblTaxTotal, et2.dblFinalTotal, et2.dblEolFee, et2.dblRemit, et2.dttDateCreated, rrt2.strDiscountType, rrt2.intDiscountCodeID, rrt2.dblDiscount, rrt2.dblArtezDiscount FROM entTransaction AS et2
					INNER JOIN relAccountTransaction AS rat2 ON rat2.intTransactionID = et2.intTransactionID
					INNER JOIN lkpTransactionType AS ltt2 ON ltt2.intTransactionTypeID = rat2.intTransactionTypeID AND (ltt2.strTransactionType = 'extraPurchase' OR ltt2.strTransactionType = 'oneTimeDonation' OR ltt2.strTransactionType = 'oneTimePledge' OR ltt2.strTransactionType = 'downgrade' OR ltt2.strTransactionType = 'upgrade' OR ltt2.strTransactionType = 'transfer')
					INNER JOIN relAccountPersonalInformation AS rapi2 ON rapi2.intAccountID = rat2.intAccountID
					INNER JOIN lkpPersonalInformationType AS lpit2 ON lpit2.intPersonalInformationTypeID = rapi2.intPersonalInformationTypeID
					INNER JOIN entPersonalInformation AS epi2 ON epi2.intPersonalInformationID = rapi2.intPersonalInformationID
					LEFT JOIN relRegistrationTransaction AS rrt2 ON rrt2.intAccountID = rat2.intAccountID
				) AS sq
				ORDER BY dttDateCreated ASC";
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			$retArray = $result;
		}
				
		return $retArray;
	}
	
	public function getAllActivityEventCost() {
		$retArray = array();
			
		try {
			$sql = "SELECT raa.intAccountID, le.strEvent, raa.dblActivityPrice FROM relAccountActivity AS raa 
				INNER JOIN lkpEvent AS le ON le.intEventID = raa.intEventID
				WHERE raa.strStatusChange IS NULL OR raa.strStatusChange = 'changedActivity'
				ORDER BY raa.intAccountID, raa.dttDateCreated";
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			$countResult = count($result);
			for ($i = 0; $i < $countResult; $i++) {
				if (!isset($retArray[$result[$i]['intAccountID']][$result[$i]['strEvent']])) {
					$retArray[$result[$i]['intAccountID']][$result[$i]['strEvent']] = $result[$i]['dblActivityPrice'];
				}
			}
		}
				
		return $retArray;
	}
	
	public function getAllDiscountCodeEvents() {
		$retArray = array();
			
		try {
			$sql = "SELECT edc.intDiscountCodeID, le.strEvent FROM entDiscountCode AS edc
				INNER JOIN relDiscountCode AS rdc ON rdc.intDiscountCodeID = edc.intDiscountCodeID
				INNER JOIN lkpEvent AS le ON le.intEventID = rdc.intEventID";
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			$countResult = count($result);
			for ($i = 0; $i < $countResult; $i++) {
				$retArray[$result[$i]['intDiscountCodeID']] = $result[$i]['strEvent'];
			}
		}
				
		return $retArray;
	}
	
	public function getAllDonationData() {
		$retArray = array();
			
		try {
			$sql = "SELECT et.intCartID, le.strEvent, ldtt.strDonationTransactionType, lc.strCharity, rdt.dblDonationSubTotal, epi.strFirstName, epi.strLastName, epi.strCity, epi.strProvince, epi.strHomePhone, epi.strEmail, rdt.dblEolFeeTax, rdt.dblDonationSubTotal AS aTotal, rdt.dblEolFee, rdt.dblRemit, et.dttDateCreated FROM relDonationTransaction AS rdt
				INNER JOIN lkpCharity AS lc ON lc.intCharityID = rdt.intCharityID
				INNER JOIN lkpDonationTransactionType AS ldtt ON ldtt.intDonationTransactionTypeID = rdt.intDonationTransactionTypeID
				INNER JOIN lkpEvent AS le ON le.intEventID = rdt.intEventID
				INNER JOIN entTransaction AS et ON et.intTransactionID = rdt.intTransactionID
				INNER JOIN relAccountPersonalInformation AS rapi ON rapi.intAccountID = rdt.intAccountID
				INNER JOIN entPersonalInformation AS epi ON epi.intPersonalInformationID = rapi.intPersonalInformationID
				ORDER BY et.intCartID";
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			$retArray = $result;
		}
				
		return $retArray;
	}
	
	public function getAllAddonData() {
		$retArray = array();
			
		try {
			$sql = "SELECT et.intCartID, le.strEvent, latt.strAddonTransactionType, epi.strFirstName, epi.strLastName, epi.strCity, epi.strProvince, epi.strHomePhone, epi.strEmail, la.strAddon, rat.intQty, las.strAddonShirt, rat.dblAddonSubTotal, et.dttDateCreated FROM relAddonTransaction AS rat				
				INNER JOIN lkpAddonTransactionType AS latt ON latt.intAddonTransactionTypeID = rat.intAddonTransactionTypeID
				INNER JOIN lkpAddon AS la ON la.intAddonID = rat.intAddonID 
				INNER JOIN lkpEvent AS le ON le.intEventID = rat.intEventID
				INNER JOIN entTransaction AS et ON et.intTransactionID = rat.intTransactionID				
				INNER JOIN entPersonalInformation AS epi ON epi.intPersonalInformationID = rat.intPersonalInformationID
				LEFT JOIN lkpAddonShirt AS las ON las.intAddonShirtID = rat.intAddonShirtID
				ORDER BY et.intCartID";
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			$retArray = $result;
		}
				
		return $retArray;
	}
}
