<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pledge_model extends CI_Model {

  private $insertIDs;
  public $personalInformationID;

  public function get_profile_details($id)
  {
    $sql = "SELECT epi.strFirstName AS firstName, epi.strLastName AS lastName, relpp.intEventID AS eventID, lkpc.intCharityID AS charityID, entpp.intPledgeProfileID AS profileID, entpp.dblPledgeGoal AS goal, entpp.strPledgeMessage AS message, entpp.strPledgeProfilePicture AS picture, lkpc.strCharity AS charity FROM entPledgeProfile AS entpp
    INNER JOIN relPledgeProfile AS relpp
    ON relpp.intPledgeProfileID = entpp.intPledgeProfileID
    INNER JOIN lkpCharity AS lkpc
    ON relpp.intCharityID = lkpc.intCharityID
    INNER JOIN entAccount AS enta
    ON relpp.intAccountID = enta.intAccountID
    INNER JOIN relAccountPersonalInformation AS relapi
    ON relapi.intAccountID = enta.intAccountID
    INNER JOIN entPersonalInformation AS epi
    ON relapi.intPersonalInformationID = epi.intPersonalInformationID
    WHERE entpp.intPledgeProfileID = :profileID
    LIMIT 1;";

    $stmt = $this->db->prepare($sql);
    $dataToExecute = array(
      "profileID" => $id
      );
    if($stmt->execute($dataToExecute))
    {
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if($results)
      {
        return $results[0];
      }
    }
  }

  public function get_profile_pledges($pledgeProfileID) {
    $retArray = array();
    
    $aSession = $this->session->userdata('participant');

    try {
      $sql = "SELECT epi.strFirstName, epi.strLastName, rdt.dblDonationSubTotal, rdt.strPledgingMessage FROM relDonationTransaction AS rdt
        INNER JOIN relAccountPersonalInformation AS rapi ON rapi.intAccountID = rdt.intAccountID
        INNER JOIN entPersonalInformation AS epi ON epi.intPersonalInformationID = rapi.intPersonalInformationID
        WHERE rdt.strRefundStatus IS NULL AND rdt.intPledgeProfileID = :profileID";
        
      $stmt = $this->db->prepare($sql);

      $dataToExecute = array(
        "profileID" => $pledgeProfileID
        );
      if($stmt->execute($dataToExecute))
      {
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
      }
      else
      {
        throw new Exception();
      }
    }
    catch (Exception $e) {  
      //Function located in general_helper.php
      processError($sql);
    }
    
    if (isset($result) && count($result) > 0) {
      $retArray = $result;  
    }
    
    return $retArray;
  }
  

  public function insert_transaction()
  {
    $sqlTransactionStatusID = "SELECT intTransactionPaidStatusID AS statusID FROM lkpTransactionPaidStatus WHERE strTransactionPaidStatus = 'Paid' LIMIT 1;";
    $stmt = $this->db->prepare($sqlTransactionStatusID);

    if($stmt->execute())
    {
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

      if($results)
      {
        $transactionPaidStatusID = $results[0]['statusID'];

        $sqlInsertTransaction = "INSERT INTO entTransaction (intTransactionPaidStatusID, dblActivityCostTotal, dblAddonTotal, dblDutTotal, dblTaxTotal, dblEolFee, dblDonationTotal, dblFinalTotal, dblRemit, dttDateCreated, dttDateModified, intCartID)
          VALUES (:transactionPaidStatusID, 0, 0, 0, :tax, :eolFee, :donation, :total, :remit, NOW(), NOW(), :cartID);";

        $eolFee = calculate_eol_fee($_SESSION['pledge']['donation']);
        $eolFee = $eolFee * (1 * EOL_FEE_TAX);

        $stmt1 = $this->db->prepare($sqlInsertTransaction);

        $dataToExecute = array(
          "transactionPaidStatusID" => $transactionPaidStatusID,
          "tax" => 0,
          "eolFee" => number_format($eolFee, 2, '.', ''),
          "donation" => number_format($_SESSION['pledge']['donation'], 2, '.', ''),
          "total" => number_format($_SESSION['pledge']['donation'], 2, '.', ''),
          "remit" => number_format($_SESSION['pledge']['donation'] - $eolFee, 2, '.', ''),
          "cartID" => $_SESSION['cartID']
          );

        if($stmt1->execute($dataToExecute))
        {
          $this->insertIDs['pledge-transaction'] = $this->db->lastInsertId();
          return TRUE;
        }
        else
        {
        echo "FAILED 4<BR>";
          //TODO: Alert - failed to insert into transaction from a pledge to a participant
        }
      }
      else
      {
        echo "FAILED 4<BR>";
        //TODO: Alert - failed to retrieve result of transaction paid status from lkpTransactionPaidStatus
      }
    }
  }

  public function insert_account()
  {
    $sql = "SELECT intActiveStatusID AS accountStatusID FROM lkpActiveStatus WHERE strActiveStatus = 'Active' LIMIT 1;";
    $stmt1 = $this->db->prepare($sql);
    
    if($stmt1->execute())
    {
      $results = $stmt1->fetchAll(PDO::FETCH_ASSOC);
      if($results)
      {
        $accountStatusID = $results[0]['accountStatusID'];
        $sqlInsertAcc = "INSERT INTO entAccount (intActiveStatusID, strUserName, strPassword, dttDateCreated, dttDateModified)
        VALUES (:accountStatusID, NULL, NULL, NOW(), NOW());";

        $stmt2 = $this->db->prepare($sqlInsertAcc);

        $dataToExecute2 = array(
          "accountStatusID" => $accountStatusID
          );

        if($stmt2->execute($dataToExecute2))
        {
          $this->insertIDs['pledge-account'] = $this->db->lastInsertId();
          return TRUE;
        }
        else
        {
        echo "FAILED 3<BR>";
          //TODO: Alert - invalid query when trying to insert a new donation account in entAccount
        }
      }
      else
      {
        echo "FAILED 2<BR>";
        //TODO: Alert - failed to retrieve result for active status account
      }
    }
    else
    {
        echo "FAILED 1<BR>";
      //TODO: Alert - invalid query when trying to select account status ID from lkpActiveStatus
    }
  }

  public function insert_donation_transaction()
  {
    $sqlGetDonationTransType = "SELECT intDonationTransactionTypeID AS donationTransactionTypeID FROM lkpDonationTransactionType WHERE strDonationTransactionType = 'pledge' LIMIT 1;";
    $stmt = $this->db->prepare($sqlGetDonationTransType);
    if($stmt->execute())
    {
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
      $donationTransactionTypeID = $results[0]['donationTransactionTypeID'];

      $sqlDonationTransaction = "INSERT INTO relDonationTransaction (intTransactionID, intAccountID, intDonationTransactionTypeID, intEventID, intCharityID, intPledgeProfileID, dblDonationSubTotal, dblEolFee, dblEolFeeTax, dblRemit, strPledgingMessage, dblTotal)
      VALUES (:transactionID, :accountID, :donationTransactionTypeID, :eventID, :charityID, :profileID, :donationSubTotal, :eolFee, :eolFeeTax, :remit, :message, :total);";

      $eolFee = number_format($_SESSION['pledge']['donation'], 2, '.', '');
      $eolFeeTax = number_format($eolFee * EOL_FEE_TAX, 2, '.', '');

      $totalEolFee = $eolFee + $eolFeeTax;

      $this->load->helper("security");

      $message = $this->security->xss_clean($_SESSION['pledge']['message']);

      $dataToExecute = array(
        "transactionID" => $this->insertIDs['pledge-transaction'],
        "accountID" => $this->insertIDs['pledge-account'],
        "donationTransactionTypeID" => $donationTransactionTypeID,
        "eventID" => $_SESSION['pledge']['athleteEventID'],
        "charityID" => $_SESSION['pledge']['athleteCharityID'],
        "profileID" => $_SESSION['pledge']['athleteID'],
        "donationSubTotal" => number_format($_SESSION['pledge']['donation'], 2, '.', ''),
        "eolFee" => number_format($totalEolFee, 2, '.', ''),
        "eolFeeTax" => number_format($eolFeeTax, 2, '.', ''),
        "remit" => number_format($_SESSION['pledge']['donation'] - $totalEolFee, 2, '.', ''),
        "message" => $_SESSION['pledge']['message'],
        "total" => number_format($_SESSION['pledge']['donation'], 2, '.', '')
        );

      $stmt1 = $this->db->prepare($sqlDonationTransaction);

      if($stmt1->execute($dataToExecute))
      {
        return TRUE;
      }
      else
      {
        echo "FAILED relDonationTransaction<BR>";
        //TODO: Alert - invalid query, failed to insert into relDonationTransaction
      }
    }
    else
    {
        echo "FAILED lkpDonationTransactionType<BR>";
      //TODO: Alert - failed to retrieve donation transaction type from lkpDonationTransactionType 
    }
  }

  public function insert_personal_informatino()
  {
    $sql = "INSERT INTO entPersonalInformation (strFirstName, strLastName, strAddress, strCity, strProvince, strCountry, strPostalCode, strHomePhone, strEmail)
    VALUES (:firstName, :lastName, :address, :city, :province, :country, :postal, :homephone, :email);";

    $stmt = $this->db->prepare($sql);

    $dataToExecute = array(
      "firstName" => $_SESSION['pledge']['first_name'],
      "lastName" => $_SESSION['pledge']['last_name'],
      "address" => $_SESSION['pledge']['address'],
      "city" => $_SESSION['pledge']['city'],
      "province" => $_SESSION['pledge']['province'],
      "country" => $_SESSION['pledge']['country'],
      "postal" => $_SESSION['pledge']['postal_code'],
      "homephone" => $_SESSION['pledge']['home_phone'],
      "email" => $_SESSION['pledge']['email']
      );

    if($stmt->execute($dataToExecute))
    {
      $this->personalInformationID = $this->db->lastInsertId();
      return TRUE;
    }
    else
    {
      echo "FAILED personal information<BR>";
      //TODO: Alert - failed to insert into entpersonal
    }
  }

  public function insert_account_personal_relation()
  {
    $sql1 = "SELECT intPersonalInformationTypeID AS typeID FROM lkpPersonalInformationType WHERE strPersonalInformationType = 'oneTimePledge' LIMIT 1;";
    $stmt1 = $this->db->prepare($sql1);
    if($stmt1->execute())
    {
      $result1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
      if($result1)
      {
        $this->personalInformationTypeID = $result1[0]['typeID'];
      }
      else
      {
        //TODO: Failed to find anything, defaulting
        $this->personalInformationTypeID = 1;
      }
    }

    $sql2 = "INSERT INTO relAccountPersonalInformation (intPersonalInformationID, intAccountID, intPersonalInformationTypeID)
    VALUES (:personalInformationID, :accountID, :personalInformationTypeID);";

    $stmt2 = $this->db->prepare($sql2);

    $dataToExecute = array(
      "personalInformationID" => $this->personalInformationID,
      "accountID" => $this->insertIDs['pledge-account'],
      "personalInformationTypeID" => $this->personalInformationTypeID
      );

    if($stmt2->execute($dataToExecute))
    {
      return TRUE;
    }
    else
    {
      //TODO: Failed to insert account - personal relation
      echo "Failed account personal relation<BR>";
    }
  }

  public function insert_account_transaction()
  {
    $sqlGetTransactionType = "SELECT intTransactionTypeID AS transactionTypeID FROM lkpTransactionType WHERE strTransactionType = 'oneTimePledge' LIMIT 1;";
    $stmt1 = $this->db->prepare($sqlGetTransactionType);
    if($stmt1->execute())
    {
      $results = $stmt1->fetchAll(PDO::FETCH_ASSOC);
      $transactionTypeID = $results[0]['transactionTypeID'];

      $sqlInsertAccountTransaction = "INSERT INTO relAccountTransaction (intAccountID, intTransactionID, intTransactionTypeID)
      VALUES (:accountID, :transactionID, :transactionTypeID);";

      $dataToExecute = array(
        "accountID" => $this->insertIDs['pledge-account'],
        "transactionID" => $this->insertIDs['pledge-transaction'],
        "transactionTypeID" => $transactionTypeID
        );
      $stmt2 = $this->db->prepare($sqlInsertAccountTransaction);

      if($stmt2->execute($dataToExecute))
      {
        return TRUE;
      }
      else
      {
        echo "FAILED relAccountTransaction<BR>";

        //TODO: Alert - invalid query, failed to insert into relAccountTransaction
      }
    }
    else
    {
        echo "FAILED lkpTransactionType<BR>";
      //TODO: Alert - failed to retrieve transaction type from lkpTransactionType
    }
  }

  public function get_form_sections()
  {
    $donationAmount = array();
    for($i = 10; $i <= 1000; $i += 5)
    {
      $donationAmount[$i] = $i;
    }

    $sections = array(
      "Personal Information" => array(
        "First Name" => array(
          "type" => "text",
          "name" => "pledge_first_name",
          "id" => "inpPledgeFirstName",
          "label" => "First Name",
          "required" => TRUE
          ),
        "Last Name" => array(
          "type" => "text",
          "name" => "pledge_last_name",
          "id" => "inpPledgeLastName",
          "label" => "Last Name",
          "required" => TRUE
        ),
        "email" => array(
          "type" => "email",
          "name" => "pledge_email",
          "id" => "inpEmail",
          "label" => "Email",
          "required" => TRUE
          ),
        "email verify" => array(
          "type" => "email",
          "name" => "pledge_email_verify",
          "id" => "inpEmailVerify",
          "label" => "Email Verification",
          "required" => TRUE
          ),
        "Address" => array(
          "type" => "text",
          "name" => "pledge_address",
          "id" => "inpPledgeAddress",
          "label" => "Address",
          "required" => TRUE
          ),
        "City" => array(
          "type" => "text",
          "name" => "pledge_city",
          "id" => "inpPledgeCity",
          "label" => "City",
          "required" => TRUE
          ),
          "province" => array(
            "type" => "select",
            "name" => "pledge_province",
            "id" => "inpProvince",
            "label" => "Province/State",
            "data" => getProvinceState(),
            "required" => TRUE
            ),
          "country" => array(
            "type" => "select",
            "name" => "pledge_country",
            "id" => "selCountry",
            "label" => "Country",
            "data" => getCountry(),
            "required" => TRUE
            ),
          "postal" => array(
            "type" => "text",
            "name" => "pledge_postal_code",
            "id" => "inpPostalCode",
            "label" => "Postal/Zip Code",
            "required" => TRUE
            ),
          "home phone" => array(
            "type" => "text",
            "name" => "pledge_home_phone",
            "id" => "inpHomePhone",
            "label" => "Home Phone",
            "required" => TRUE
            ),
          "donation amount" => array(
            "type" => "select",
            "name" => "pledge_donation",
            "id" => "inpPledgeDonation",
            "label" => "Donation Amount",
            "data" => $donationAmount,
            "required" => TRUE
            ),
          "pledge message" => array(
            "type" => "textarea",
            "name" => "pledge_message",
            "id" => "inpPledgeMessage",
            "label" => "Message"
            )
        )
      );

    return $sections;
  }
}

/* End of file pledge_model.php */
/* Location: ./application/model/pledge_model.php */