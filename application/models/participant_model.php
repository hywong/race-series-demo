<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Participant_model extends CI_Model {
	
	//Get all events
	public function getEventsByAccountID() {
		$retArray = array();
		
		$aSession = $this->session->userdata('participant');
		$anAccountID = ((isset($aSession['participantAccountID']))?$aSession['participantAccountID']:'0');
		
		try {
			$sql = "SELECT le.intEventID, le.strEvent FROM lkpEvent AS le
				INNER JOIN relAccountActivity AS raa ON raa.intEventID = le.intEventID AND raa.strStatusChange IS NULL AND raa.intAccountID = " . $anAccountID;
		
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
			
		if (isset($result) && count($result) > 0) {
			$retArray = $result;
		}
		
		return $retArray;
	}

	//Takes a username and password and verifies against the particpant accounts in the database
    public function verifyLogin($username,$password) {
		$retValue = "notValid";
		
		try {
			$sql = "SELECT a.intAccountID, a.strUserName, a.strPassword, b.intEventID, d.intPersonalInformationID, e.strPersonalInformationType FROM entAccount AS a
				INNER JOIN relAccountActivity AS b ON b.intAccountID = a.intAccountID
				INNER JOIN lkpActiveStatus AS c ON c.intActiveStatusID = a.intActiveStatusID
				INNER JOIN relAccountPersonalInformation AS d ON d.intAccountID = a.intAccountID
				INNER JOIN lkpPersonalInformationType AS e ON e.intPersonalInformationTypeID = d.intPersonalInformationTypeID
				INNER JOIN entPersonalInformation AS f ON f.intPersonalInformationID = d.intPersonalInformationID			
				WHERE a.strUserName = '" . $username . "' AND lower(c.strActiveStatus) = 'active' AND b.strStatusChange IS NULL AND (e.strPersonalInformationType = 'individualBillingMember' OR e.strPersonalInformationType = 'individual')";

			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
				
		//Check if any results were returned
		if (isset($result[0]) && count($result[0]) > 0) {
			$aPassword = $result[0]['strPassword'];
			//This is how to check for blowfish encryption
			if (crypt($password, $aPassword) == $aPassword) { 
				$retValue = "valid";
				$sessionEventID = 1;
				if (count($result) == 1) {
					$sessionEventID = $result[0]['intEventID'];
				}
				$sessionData['participant'] = array(
					"participantAccountID" => $result[0]['intAccountID'],
					"participantUserName" => $result[0]['strUserName'],
					"participantPersonalInformationID" => $result[0]['intPersonalInformationID'],
					"participantType" => $result[0]['strPersonalInformationType'],
					"participantEventID" => $sessionEventID);
				$this->session->set_userdata($sessionData);
			}
		}
		
        return $retValue;
    }
	
	public function getEolCharityEvents() {
		$retArray = array();
				
		//HARDCODED... 
		try {
			$sql = "SELECT intEventID FROM relEventCharity AS rec
				INNER JOIN lkpCharity AS lc ON lc.intCharityID = rec.intCharityID WHERE strCharity <> 'Charity 1' AND strCharity <> 'Princess Margaret Cancer Foundation'";
		
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
			
		if (isset($result) && count($result) > 0) {
			$countResult = count($result);
			for ($i = 0; $i < $countResult; $i++) {
				$retArray[$i] = $result[$i]['intEventID'];
			}
		}
		
		return $retArray;
	}
	
	public function checkTransferToolAvailability() {
		$retValue = "available";
		
		$aSession = $this->session->userdata('participant');
		
		try {
			$sql = "SELECT lta.dttTransferStartDate, lta.dttTransferEndDate FROM lkpTransferAvailability AS lta
				INNER JOIN relTransferAvailability AS rta ON rta.intTransferAvailabilityID = lta.intTransferAvailabilityID
				WHERE rta.intEventID = " . $aSession['participantEventID'];
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
				
		//Check if any results were returned
		if (isset($result[0]) && count($result[0]) > 0) {
			date_default_timezone_set("America/Toronto");
			
			$nowDate = strtotime("now");
			$transferStartDate = strtotime($result[0]['dttTransferStartDate']);
			$transferEndDate = strtotime($result[0]['dttTransferEndDate']);
			/*
			echo "CLOSE DATE:" . date("m/d/Y h:i:s A T",$transferEndDate) . "<br>";
			echo "OPEN DATE:" . date("m/d/Y h:i:s A T",$transferStartDate) . "<br>";
			echo "CURRENT DATE:" . date("m/d/Y h:i:s A T",$nowDate) . "<br>";
			echo "NOWDATE:" . $nowDate . " || STARTDATE:" . $transferStartDate . " || ENDATE:" . $transferEndDate; exit;
			*/
			
			if ($nowDate < $transferStartDate || $nowDate > $transferEndDate) {
				$retValue = "notAvailable";
			}
		}
		
		return $retValue;
	}
	
	public function checkModifyToolAvailability() {
		$retValue = "available";
		
		$aSession = $this->session->userdata('participant');
		
		try {
			$sql = "SELECT lpma.dttPublicModifyStartDate, lpma.dttPublicModifyEndDate FROM lkpPublicModifyAvailability AS lpma
				INNER JOIN relPublicModifyAvailability AS rpma ON rpma.intPublicModifyAvailabilityID = lpma.intPublicModifyAvailabilityID
				WHERE rpma.intEventID = " . $aSession['participantEventID'];
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
				
		//Check if any results were returned
		if (isset($result[0]) && count($result[0]) > 0) {
			date_default_timezone_set("America/Toronto");
			
			$nowDate = strtotime("now");
			$publicModifyStartDate = strtotime($result[0]['dttPublicModifyStartDate']);
			$publicModifyEndDate = strtotime($result[0]['dttPublicModifyEndDate']);
			/*
			echo "CLOSE DATE:" . date("m/d/Y h:i:s A T",$publicModifyEndDate) . "<br>";
			echo "OPEN DATE:" . date("m/d/Y h:i:s A T",$publicModifyStartDate) . "<br>";
			echo "CURRENT DATE:" . date("m/d/Y h:i:s A T",$nowDate) . "<br>";
			echo "NOWDATE:" . $nowDate . " || STARTDATE:" . $publicModifyStartDate . " || ENDATE:" . $publicModifyEndDate; exit;
			*/
			
			if ($nowDate < $publicModifyStartDate || $nowDate > $publicModifyEndDate) {
				$retValue = "notAvailable";
			}
		}
		
		return $retValue;
	}
	
	public function checkForgotPasswordUsername() {
		$retArray = array();
		
		$aPost = $this->input->sanitizeForDbPost();
		
		try {
			$sql = "SELECT a.intAccountID, c.strEmail FROM entAccount AS a
				INNER JOIN relAccountPersonalInformation AS b ON b.intAccountID = a.intAccountID
				INNER JOIN entPersonalInformation AS c ON c.intPersonalInformationID = b.intPersonalInformationID
				INNER JOIN lkpPersonalInformationType AS d ON d.intPersonalInformationTypeID = b.intPersonalInformationTypeID
				WHERE a.strUserName = '" . $aPost['username'] . "' AND (d.strPersonalInformationType = 'individualBillingMember' OR d.strPersonalInformationType = 'individual')";
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}	
		
		if (isset($result) && count($result) > 0) {
			$retArray['accountID'] = $result[0]['intAccountID'];
			$retArray['email'] = $result[0]['strEmail'];			
		}
		
		return $retArray;
	}
	
	public function addForgotPasswordTokenToDb($arrDetails) {
		$retValue = "goodtogo";
				
		try {
			$sql = "INSERT INTO entPasswordChangeRequest (intActiveStatusID,intAccountID,strToken,dttDateCreated) VALUES (1," . $arrDetails['accountID'] . ",'" . $arrDetails['token'] . "',NOW())";
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}		

		return $retValue;
	}
	
	public function buildSendResetPasswordEmail($arrDetails) {
		$this->load->helper('url');
	
		
		$buildUrl = base_url() . "participant/reset_password?id=" . $arrDetails['token'];
		//$content= $getDomain . 'user/retrieve.php?id=' . $rand;

		//$boundary = md5(rand());
		/*
		$rn = "\r\n";
		$boundary = md5(rand());
		$boundary_content = md5(rand());
		$to= '"' . mb_encode_mimeheader('User') . '" <' . $headers[$emailKey] . '>';
		$from = '"' . mb_encode_mimeheader('core') . '" <fwang@bump-network.com>';

		$mailheaders = "MIME-Version: 1.0\r\n";
		$mailheaders .= "From: Eventsonline.ca \r\n";
		$mailheaders .= "To: " . $to . "\r\n";
		$mailheaders .= "Content-Type: multipart/alternative;boundary=" . $boundary . "\r\n";
		*/


		//here is the content body
		/*
		$message = "This is a MIME encoded message.";
		$message .= "\r\n\r\n--" . $boundary . "\r\n";
		$message .= "Content-type: text/plain;charset=utf-8\r\n\r\n";

		//Plain text body
		$message .= $buildUrl;
		$message .= "\r\n\r\n--" . $boundary . "\r\n";
		$message .= "Content-type: text/html;charset=utf-8\r\n\r\n";
		*/
		//Html body
		/*
		$message .= '<div>' . translate("Please go to the following link to reset your password.") . '                            
		<a href="' . $buildUrl . '">' . $buildUrl . '</a>
		</div>
		';
		$message .= "\r\n\r\n--" . $boundary . "--";
		*/
		$message = '<div>' . translate("Please go to the following link to reset your password.") . '                            
		<a href="' . $buildUrl . '">' . $buildUrl . '</a>
		</div>';

		$this->load->library('email');

		$config['mailtype'] = "html";
		$config['charset'] = "utf-8";
		$this->email->initialize($config);
		$this->email->from('Eventsonline.ca');
		$this->email->to($arrDetails['email']);

		$this->email->subject("Series Demo - " . translate("Reset Password"));
		$this->email->message($message);

		$this->email->send();
	}
	
	public function checkResetAccountPasswordToken() {
		$retValue = "invalid";
		
		$aGet = $this->input->get();
		
		if (!empty($aGet)) {
			try {		
				$sql = "SELECT intAccountID FROM entPasswordChangeRequest AS epcr
					INNER JOIN lkpActiveStatus AS las ON las.intActiveStatusID = epcr.intActiveStatusID 
					WHERE LOWER(las.strActiveStatus) = 'active' AND epcr.strToken = '" . $aGet['id'] . "'";
				
				$query = $this->db->query($sql);
				if (!$query) {
					throw new Exception();
				}
				$result = $query->fetchAll(PDO::FETCH_ASSOC);
			}
			catch (Exception $e) {	
				//Function located in general_helper.php
				processError($sql);
			}		
			
			if (isset($result) && count($result) > 0) {
				$retValue = "valid";
			}
		}
		
		return $retValue;
	}
	
	public function resetAccountPassword() {
		$retValue = "goodtogo";
		
		$aPost = $this->input->sanitizeForDbPost();
		$aGet = $this->input->get();
		
		try {		
			$sql = "SELECT intAccountID FROM entPasswordChangeRequest WHERE strToken = '" . $aGet['id'] . "'";
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}		
		
		if (isset($result) && count($result) > 0) {
			$accountID = $result[0]['intAccountID'];
			$newPassword = b_encrypt($aPost['password']);
			
			try {
				$sql = "START TRANSACTION;
					UPDATE entAccount SET strPassword = '" . $newPassword . "' WHERE intAccountID = " . $accountID . ";
					UPDATE entPasswordChangeRequest SET intActiveStatusID = 2 WHERE strToken = '" . $aGet['id'] . "';
					COMMIT;";
				
				$query = $this->db->query($sql);
				if (!$query) {
					throw new Exception();
				}
			}
			catch (Exception $e) {	
				//Function located in general_helper.php
				processError($sql);
			}
		}
		else {
			$retValue = "notFound";
		}

		return $retValue;
	}
	
	public function getParticipantDetailsForDashboard() {
		$retArray = array();
		
		$aSession = $this->session->userdata('participant');

		try {
			$sql = "SELECT epi.strFirstName, epi.strLastName, epi.strAddress, epi.strEmail, epi.dteDateOfBirth, et.strTeamName, rpp.intPledgeProfileID FROM entPersonalInformation AS epi
				INNER JOIN relAccountPersonalInformation AS rapi ON rapi.intPersonalInformationID = epi.intPersonalInformationID 
				INNER JOIN entAccount AS ea ON ea.intAccountID = rapi.intAccountID AND ea.intAccountID = " . $aSession['participantAccountID'] . "
				LEFT JOIN relTeam AS rt ON rt.intPersonalInformationID = epi.intPersonalInformationID AND rt.intEventID = " . $aSession['participantEventID'] . "
				LEFT JOIN entTeam AS et ON et.intTeamID = rt.intTeamID
				LEFT JOIN relPledgeProfile AS rpp ON rpp.intAccountID = ea.intAccountID AND rpp.intEventID = " . $aSession['participantEventID'];
				
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			$retArray = $result[0];	
		}
		
		return $retArray;
	}
	
	public function getTeamsByEventID() {
		$retArray = array();
		
		$aSession = $this->session->userdata('participant');

		try {
			$sql = "SELECT et.intTeamID, et.strTeamName FROM entTeam AS et
				INNER JOIN relTeam AS rt ON rt.intTeamID = et.intTeamID AND rt.intEventID = " . $aSession['participantEventID'] . " GROUP BY et.intTeamID";
				
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			$retArray = $result;	
		}
		
		return $retArray;
	}
	
	public function joinTeam($teamID,$aPassword) {
		$retValue = "incorrectPassword";
		
		$aSession = $this->session->userdata('participant');
			
		try {
			$sql = "SELECT strTeamName, strTeamPassword FROM entTeam WHERE intTeamID = " . $teamID;
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
				
		//Check if any results were returned
		if (isset($result[0]) && count($result[0]) > 0) {
			$teamName = $result[0]['strTeamName'];
			$teamPassword = $result[0]['strTeamPassword'];
			//This is how to check for blowfish encryption
			if (crypt($aPassword, $teamPassword) == $teamPassword) { 
				$retValue = "valid";
				
				try {
					$sql = "INSERT INTO relTeam (intTeamID, intPersonalInformationID, intEventID, strIsCaptain) VALUES (" . $teamID . "," . $aSession['participantPersonalInformationID'] . "," . $aSession['participantEventID'] . ",'no')";
					$query = $this->db->query($sql);
					if (!$query) {
						throw new Exception();
					}
				}
				catch (Exception $e) {	
					//Function located in general_helper.php
					processError($sql);
				}
						
				$aSession['participantTeamID'] = $teamID;
				$aSession['participantTeamName'] = $teamName;
				$this->session->set_userdata("participant",$aSession);
			}
		}
		
        return $retValue;
	}
	
	public function getTeamMembersByAccountID() {
		$retArray = array();
		
		$aSession = $this->session->userdata('participant');

		try {
			$sql = "SELECT epi.strFirstName, epi.strLastName, epi.strEmail, la.strActivity FROM relTeam AS rt
				INNER JOIN entPersonalInformation AS epi ON epi.intPersonalInformationID = rt.intPersonalInformationID
				INNER JOIN relAccountPersonalInformation AS rapi ON rapi.intPersonalInformationID = epi.intPersonalInformationID
				INNER JOIN relAccountActivity AS raa ON raa.intAccountID = rapi.intAccountID AND raa.strStatusChange IS NULL AND raa.intEventID = " . $aSession['participantEventID'] . "
				INNER JOIN lkpActivity AS la ON la.intActivityID = raa.intActivityID	
				WHERE rt.intTeamID = 
				(SELECT intTeamID FROM relTeam AS rt2 WHERE rt2.intPersonalInformationID = " . $aSession['participantPersonalInformationID'] . " AND rt2.intEventID = " . $aSession['participantEventID'] . ")";
				
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
			
		if (isset($result) && count($result) > 0) {
			$retArray = $result;	
		}
		
		return $retArray;
	}
	
	public function getCharitiesForPledgeProfile() {
		$retArray = array();
		
		$aSession = $this->session->userdata('participant');

		try {
			$sql = "SELECT lc.intCharityID, lc.strCharity FROM lkpCharity AS lc 
				INNER JOIN relEventCharity AS rec ON rec.intCharityID = lc.intCharityID AND rec.intEventID = " . $aSession['participantEventID'] . "
				WHERE strCharity <> 'Charity 1' AND strCharity <> 'Princess Margaret Cancer Foundation'";
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			$retArray = $result;	
		}
		
		return $retArray;
	}
	
	public function createPledgeProfile() {
		$retValue = "goodtogo";
		
		$aSession = $this->session->userdata('participant');
		$aPost = $this->input->sanitizeForDbPost();
		
		try {
			$sql = "START TRANSACTION;
				INSERT INTO entPledgeProfile (dblPledgeGoal, strPledgeMessage, strPledgeProfilePicture, dttDateCreated) VALUES (" . $aPost['profile_pledge_goal'] . ",'" . $aPost['profile_pledge_message'] . "','" . $aPost['profile_pic'] . "',NOW());
				SET @intPledgeProfileID = last_insert_id();
				INSERT INTO relPledgeProfile (intAccountID, intEventID, intCharityID, intPledgeProfileID) VALUES (" . $aSession['participantAccountID'] . "," . $aSession['participantEventID'] . "," . $aPost['charity'] . ",@intPledgeProfileID);
				COMMIT;";

			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		return $retValue;
	}
	
	public function getPledgeProfileByAccountID() {
		$retArray = array();
		
		$aSession = $this->session->userdata('participant');

		try {
			$sql = "SELECT epi.strFirstName, epi.strLastName, epp.intPledgeProfileID, epp.dblPledgeGoal, epp.strPledgeMessage, epp.strPledgeProfilePicture, lc.strCharity FROM entPledgeProfile AS epp
				INNER JOIN relPledgeProfile AS rpp ON rpp.intPledgeProfileID = epp.intPledgeProfileID
				INNER JOIN lkpCharity AS lc ON lc.intCharityID = rpp.intCharityID
				INNER JOIN relAccountPersonalInformation AS rapi ON rapi.intAccountID = rpp.intAccountID
				INNER JOIN entPersonalInformation AS epi ON epi.intPersonalInformationID = rapi.intPersonalInformationID
				WHERE rpp.intEventID = " . $aSession['participantEventID'] . " AND rpp.intAccountID = " . $aSession['participantAccountID'];

			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
			
		if (isset($result) && count($result) > 0) {
			$retArray = $result[0];	
		}
		
		return $retArray;
	}
	
	public function getPledgesForPledgeProfile($pledgeProfileID) {
		$retArray = array();
		
		$aSession = $this->session->userdata('participant');

		try {
			$sql = "SELECT epi.strFirstName, epi.strLastName, rdt.dblDonationSubTotal, rdt.strPledgingMessage FROM relDonationTransaction AS rdt
				INNER JOIN relAccountPersonalInformation AS rapi ON rapi.intAccountID = rdt.intAccountID
				INNER JOIN entPersonalInformation AS epi ON epi.intPersonalInformationID = rapi.intPersonalInformationID
				WHERE rdt.strRefundStatus IS NULL AND rdt.intPledgeProfileID = " . $pledgeProfileID . " AND (rdt.intEventID = 1 OR rdt.intEventID = " . $aSession['participantEventID'] . ")";
				
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			$retArray = $result;	
		}
		
		return $retArray;
	}
	
	public function getPledgeProfileDetails($pledgeID) {
		$retArray = array();
		
		$aSession = $this->session->userdata('participant');

		try {
			$sql = "SELECT epi.strFirstName, epi.strLastName FROM relPledgeProfile AS rpp
				INNER JOIN relAccountPersonalInformation AS rapi ON rapi.intAccountID = rpp.intAccountID
				INNER JOIN entPersonalInformation AS epi ON epi.intPersonalInformationID = rapi.intPersonalInformationID
				WHERE rpp.intPledgeProfileID = " . $pledgeID . " AND rpp.intEventID = " . $aSession['participantEventID'];

			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}			
			
		if (isset($result) && count($result) > 0) {
			$retArray = $result[0];	
		}
		
		return $retArray;
	}
	
	public function editPledgeProfile() {
		$retValue = "goodtogo";
		
		$aPost = $this->input->sanitizeForDbPost();
			
		try {
			$sql = "UPDATE entPledgeProfile SET 
				dblPledgeGoal = " . $aPost['profile_pledge_goal'] . ",
				strPledgeMessage = '" . $aPost['profile_pledge_message'] . "',
				dttDateModified = NOW()
				WHERE intPledgeProfileID = " . $aPost['pid'];

			$query = $this->db->query($sql);	
			if (!$query) {
				throw new Exception();
			}
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}

		return $retValue;
	}
	
	public function buildIndividualParticipantArray() {
		$retArray = array();						
		
		$aSession = $this->session->userdata('participant');
			
		try {
			$sql = "SELECT a.strFirstName, a.strLastName, a.strAddress, a.strCity, a.strProvince, a.strCountry, a.strPostalCode, a.strHomePhone, a.strMobilePhone, a.strEmail, a.strGender, a.dteDateOfBirth, a.intAgeOnRegistration, a.strCategory, a.strSeriesNewsletter, a.strNationality, a.strNationalityDuration, a.strIndustryType, a.strIndustryTypeOther, a.strYearlySalary, a.strEmergencyContactName, a.strEmergencyContactNumber, a.strMedicalCondition FROM entPersonalInformation AS a			
				WHERE a.intPersonalInformationID = " . $aSession['participantPersonalInformationID'];
					
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}	
			
		if (isset($result) && count($result) > 0) {
			$retArray = array(		
				"first_name" => array(
					"type" => "onlytext",                
					"label" => "First Name",
					"value" => $result[0]['strFirstName'],
					"required" => ""
				),
				"last_name" => array(
					"type" => "onlytext",                
					"label" => "Last Name",
					"value" => $result[0]['strLastName'],
					"required" => ""
				),
				"address" => array(
					"type" => "text",                
					"label" => "Address",
					"value" => $result[0]['strAddress'],
					"required" => "required"
				),
				"city" => array(
					"type" => "text",                
					"label" => "City",
					"value" => $result[0]['strCity'],
					"required" => "required"
				),
				"province" => array(
					"type" => "select",                
					"label" => "Province/State",
					"value" => $result[0]['strProvince'],
					"required" => "required",
					"data" => getProvinceState()
				),
				"country" => array(
					"type" => "select",                
					"label" => "Country",
					"value" => $result[0]['strCountry'],
					"required" => "required",
					"data" => getCountry()
				),
				"postal" => array(
					"type" => "text",                
					"label" => "Postal Code/Zip Code",
					"value" => $result[0]['strPostalCode'],
					"required" => "required"
				),
				"home_phone" => array(
					"type" => "text",                
					"label" => "Home Phone",
					"value" => $result[0]['strHomePhone'],
					"required" => "required"
				),
				"mobile_phone" => array(
					"type" => "text",                
					"label" => "Mobile Phone",
					"value" => $result[0]['strMobilePhone'],
					"required" => "required"
				),
				"email" => array(
					"type" => "text",                
					"label" => "Email",
					"value" => $result[0]['strEmail'],
					"required" => "required"
				),				
				"gender" => array(
					"type" => "select",                
					"label" => "Gender",
					"value" => $result[0]['strGender'],
					"required" => "required",
					"data" => array(
						"M" => "Male",
						"F" => "Female"
					)
				)		
			);
		}		
		
		return $retArray;
	}
	
	public function getIndividualShirtInfo() {
		$retArray = array();
		
		$aSession = $this->session->userdata('participant');

		try {
			$sql = "SELECT epi.strFirstName, epi.strLastName, le.intEventID, le.strEvent, la.intActivityID, la.strActivity, rps.intShirtID, rps.intRelParticipantShirtID, ls.strShirtDescription FROM relParticipantShirt AS rps
				INNER JOIN lkpEvent AS le ON le.intEventID = rps.intEventID
				INNER JOIN lkpActivity AS la ON la.intActivityID = rps.intActivityID
				INNER JOIN entPersonalInformation AS epi ON epi.intPersonalInformationID = rps.intPersonalInformationID
				INNER JOIN lkpShirt AS ls ON ls.intShirtID = rps.intShirtID
				WHERE rps.intPersonalInformationID = " . $aSession['participantPersonalInformationID'];

			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}	
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			$retArray = $result;	
		}
		
		return $retArray;
	}
	
	public function drawIndividualShirtSection($arrShirtDetails) {
		//echo "<pre>"; print_r($arrShirtDetails); echo "</pre>"; exit;
		$retString = "";
		$countShirtDetails = count($arrShirtDetails);
		for ($i = 0; $i < $countShirtDetails; $i++) {
			$retString .= "
			<tr>
				<td>" . $arrShirtDetails[$i]['strEvent'] . " - " . $arrShirtDetails[$i]['strActivity'] . " " . translate("Technical T-shirt") . "</td>
				<td>";
			$retString .= $this->drawShirtDropdown($arrShirtDetails[$i]['intEventID'],$arrShirtDetails[$i]['intActivityID'],$arrShirtDetails[$i]['intShirtID'],$arrShirtDetails[$i]['intRelParticipantShirtID']);	
			$retString .= "
				</td>
				<td>" . $arrShirtDetails[$i]['strShirtDescription'] . "</td>
			</tr>";
		}
		
		return $retString;
	}
	
	public function drawShirtDropdown($eventID,$activityID,$shirtID,$relParticipantShirtID) {
		$arrShirtCount = array();
		try {
			//Get the count of all participant shirts that have been taken so far based on event and activity
			$sql = "SELECT COUNT(intShirtID) AS aCount, intShirtID FROM relParticipantShirt WHERE intEventID = " . $eventID . " and intActivityID = " . $activityID . " GROUP BY intShirtID";
									
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}

		if (isset($result) && count($result) > 0) {			
			$countResult = count($result);
			for ($i = 0; $i < $countResult; $i++) {
				$arrShirtCount[$result[$i]['intShirtID']] = $result[$i]['aCount'];
			}
		}
	
		$retString = "<select name=\"shirt_" . $relParticipantShirtID . "\" id=\"shirt_" . $relParticipantShirtID . "\" class=\"form-control required valid\">";
		
		try {			
			//Get all shirts and their caps based on event and activity
			$sql = "SELECT ls.intShirtID, ls.strShirtDescription, rs.intShirtCap FROM relShirt AS rs
				INNER JOIN lkpShirt AS ls ON ls.intShirtID = rs.intShirtID 
				WHERE rs.intEventID = " . $eventID . " AND rs.intActivityID = " . $activityID . " ORDER BY ls.intShirtID";

			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}	
			
		if (isset($result) && count($result) > 0) {			
			$countResult = count($result);
			for ($i = 0; $i < $countResult; $i++) {
				$getShirtID = $result[$i]['intShirtID'];
				$shirtCap = $result[$i]['intShirtCap'];
				
				$numShirtTaken = ((isset($arrShirtCount[$getShirtID]))?$arrShirtCount[$getShirtID]:0);
				if (($shirtCap - $numShirtTaken) > 0 || $getShirtID == $shirtID) {
					$retString .= "<option value=\"" . $getShirtID . "\" " . (($getShirtID == $shirtID)?'selected':'') . ">" . $result[$i]['strShirtDescription'] . "</option>";
				}
			}
		}
		$retString .= "</select>";
		
		return $retString;
	}
	
	public function getNumGroupMembers() {
		$retNumber = 0;
		
		$aSession = $this->session->userdata('participant');

		try {
			$sql = "SELECT COUNT(rapi.intPersonalInformationID) AS 'aCount' FROM relAccountPersonalInformation AS rapi
				INNER JOIN lkpPersonalInformationType AS lpit ON lpit.intPersonalInformationTypeID = rapi.intPersonalInformationTypeID
				WHERE rapi.intAccountID = " . $aSession['participantPersonalInformationID'] . " AND LOWER(lpit.strPersonalInformationType) = 'groupmember'";
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			$retNumber = $result[0]['aCount'];
		}
		
		return $retNumber;
	}
	
	public function updateParticipantDetails() {
		$retValue = "goodtogo";
		$sql = "";
		
		$aPost = $this->input->sanitizeForDbPost();
		$aSession = $this->session->userdata('participant');
		//echo "<pre>"; print_r($aPost); echo "</pre>"; exit;
		
		$sql = "START TRANSACTION;
			UPDATE entPersonalInformation SET 
				strAddress = '" . $aPost['address'] . "',
				strCity = '" . $aPost['city'] . "',
				strProvince = '" . $aPost['province'] . "',
				strCountry = '" . $aPost['country'] . "',
				strPostalCode = '" . $aPost['postal'] . "',
				strHomePhone = '" . $aPost['home_phone'] . "',
				strMobilePhone = '" . $aPost['mobile_phone'] . "',
				strEmail = '" . $aPost['email'] . "',
				strGender = '" . $aPost['gender'] . "'
				WHERE intPersonalInformationID = " . $aSession['participantPersonalInformationID'] . ";";
				
		if ($aSession['participantType'] == "individualBillingMember" || $aSession['participantType'] == "individual") {		
			foreach ($aPost as $key => $value) {
				if (stripos($key,"shirt") === 0) {
					//The second index of this array holds the intRelParticipantShirtID
					$shirtKey = explode("_",$key);
					$sql .= " UPDATE relParticipantShirt SET intShirtID = " . $value . " WHERE intRelParticipantShirtID = " . $shirtKey[1] . ";";
				}
			}
			$sql .= "COMMIT;";
		}
		/*
		else if (strtolower($aSession['participantType']) == "groupbillingmember") {						
			for ($i = 1; $i <= $aPost['memberCount']; $i++) {
				$sql .= " UPDATE entPersonalInformation SET 
						strEmail = '" . $aPost['member_email_' . $i] . "',
						strGender = '" . $aPost['member_gender_' . $i] . "'
						WHERE intPersonalInformationID = " . $aPost['member_pid_' . $i] . ";
					UPDATE relParticipantShirt SET intShirtID = " . $aPost['member_shirt_' . $i] . " WHERE intPersonalInformationID = " . $aPost['member_pid_' . $i] . " AND intEventID = " . $aSession['participantEventID'] . ";";
			}
			$sql .= " COMMIT;";
		}
		*/
		
		try {
			$query = $this->db->query($sql);	
			if (!$query) {
				throw new Exception();
			}
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}

		return $retValue;
	}
	
	public function buildBillingMemberArray() {
		$retArray = array();						
		
		$aSession = $this->session->userdata('participant');
		
		try {
			$sql = "SELECT epi.strFirstName, epi.strLastName, epi.strAddress, epi.strCity, epi.strProvince, epi.strCountry, epi.strPostalCode, epi.strHomePhone, epi.strMobilePhone, epi.strEmail, epi.strGender  FROM entPersonalInformation AS epi							
				WHERE epi.intPersonalInformationID = " . $aSession['participantPersonalInformationID'];
					
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {						
			$retArray = array(		
				"heading" => array(
					"label" => "Billing Member",
				),
				"first_name" => array(
					"type" => "onlytext",                
					"label" => "First Name",
					"value" => $result[0]['strFirstName'],
					"required" => ""
				),
				"last_name" => array(
					"type" => "onlytext",                
					"label" => "Last Name",
					"value" => $result[0]['strLastName'],
					"required" => ""
				),
				"address" => array(
					"type" => "text",                
					"label" => "Address",
					"value" => $result[0]['strAddress'],
					"required" => "required"
				),
				"city" => array(
					"type" => "text",                
					"label" => "City",
					"value" => $result[0]['strCity'],
					"required" => "required"
				),
				"province" => array(
					"type" => "select",                
					"label" => "Province/State",
					"value" => $result[0]['strProvince'],
					"required" => "required",
					"data" => getProvinceState()
				),
				"country" => array(
					"type" => "select",                
					"label" => "Country",
					"value" => $result[0]['strCountry'],
					"required" => "required",
					"data" => getCountry()
				),
				"postal" => array(
					"type" => "text",                
					"label" => "Postal Code/Zip Code",
					"value" => $result[0]['strPostalCode'],
					"required" => "required"
				),
				"home_phone" => array(
					"type" => "text",                
					"label" => "Home Phone",
					"value" => $result[0]['strHomePhone'],
					"required" => "required"
				),
				"mobile_phone" => array(
					"type" => "text",                
					"label" => "Mobile Phone",
					"value" => $result[0]['strMobilePhone'],
					"required" => "required"
				),
				"email" => array(
					"type" => "text",                
					"label" => "Email",
					"value" => $result[0]['strEmail'],
					"required" => "required"
				),				
				"gender" => array(
					"type" => "select",                
					"label" => "Gender",
					"value" => $result[0]['strGender'],
					"required" => "required",
					"data" => array(
						"M" => "Male",
						"F" => "Female"
					)
				)				
			);
						
			//$retArray = array_merge($retArray,$retExtraFieldArray);
		}		
		
		return $retArray;
	}
	
	public function buildShirtArray($activityID) {
		$retArray = array();
		
		$aSession = $this->session->userdata('participant');

		try {
			$sql = "SELECT ls.intShirtID, ls.strShirtDescription FROM relShirt AS rs
				INNER JOIN lkpShirt AS ls ON ls.intShirtID = rs.intShirtID
				WHERE rs.intEventID = " . $aSession['participantEventID'] . " AND rs.intActivityID = " . $activityID . " AND intShirtCap > 0";

			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			$countResult = count($result);
			for ($i = 0; $i < $countResult; $i++) {
					$retArray[$result[$i]['intShirtID']] = $result[$i]['strShirtDescription'];
			}			
		}
		
		return $retArray;
	}
	
	public function buildGroupMembersArray() {				
		$retArray = array();						
		
		$aSession = $this->session->userdata('participant');
		
		try {
			$sql = "SELECT epi.intPersonalInformationID, epi.strFirstName, epi.strLastName, epi.strEmail, epi.strGender, rps.intShirtID, rps.intActivityID, ls.strShirtDescription FROM entPersonalInformation AS epi	
				INNER JOIN relAccountPersonalInformation AS rapi ON rapi.intPersonalInformationID = epi.intPersonalInformationID
				INNER JOIN lkpPersonalInformationType AS lpi ON lpi.intPersonalInformationTypeID = rapi.intPersonalInformationTypeID
				INNER JOIN relParticipantShirt AS rps ON rps.intPersonalInformationID = epi.intPersonalInformationID
				INNER JOIN lkpShirt AS ls ON ls.intShirtID = rps.intShirtID
				WHERE rapi.intAccountID = " . $aSession['participantAccountID'] . " AND LOWER(lpi.strPersonalInformationType) = 'groupmember'";
										
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			$activityID = $result[0]['intActivityID'];
			$arrShirt = $this->buildShirtArray($activityID);
			
			$countResult = count($result);
			for ($i = 0; $i < $countResult; $i++) {
				$retArray["heading" . $i] = array("label" => "Group Member " . ($i + 1));
				$retArray["member_pid_" . ($i + 1)] = array(
					"type" => "hidden",                
					"label" => "",
					"value" => $result[$i]['intPersonalInformationID'],
					"previousValue" => "",
					"required" => ""
				);
				$retArray["member_firstName_" . ($i + 1)] = array(
					"type" => "onlytext",                
					"label" => "First Name",
					"value" => $result[$i]['strFirstName'],
					"required" => ""
				);
				$retArray["member_lastName_" . ($i + 1)] = array(
					"type" => "onlytext",                
					"label" => "Last Name",
					"value" => $result[$i]['strLastName'],
					"required" => ""
				);
				$retArray["member_email_" . ($i + 1)] = array(
					"type" => "text",                
					"label" => "Email",
					"value" => $result[$i]['strEmail'],
					"required" => "required"
				);
				$retArray["member_gender_" . ($i + 1)] = array(
					"type" => "select",                
					"label" => "Gender",
					"value" => $result[$i]['strGender'],
					"required" => "required",
					"data" => array(
						"M" => "Male",
						"F" => "Female"
					)
				);
				$retArray["member_shirt_" . ($i + 1)] = array(
					"type" => "select",                
					"label" => "Technical T-shirt",
					"value" => $result[$i]['intShirtID'],
					"previousValue" => $result[$i]['strShirtDescription'],
					"required" => "required",
					"data" => $arrShirt
				);
			}
		}
		
		return $retArray;
	}
	
	public function swapFirstLastName() {
		$retValue = "goodtogo";
		
		$aSession = $this->session->userdata('participant');
			
		try {
			$sql = "START TRANSACTION;
				SELECT @strFirstName := strFirstName, @strLastName := strLastName FROM entPersonalInformation WHERE intPersonalInformationID = " . $aSession['participantPersonalInformationID'] . ";
				UPDATE entPersonalInformation SET strFirstName = @strLastName,
					strLastName = @strFirstName
					WHERE intPersonalInformationID = " . $aSession['participantPersonalInformationID'] . ";
				COMMIT;";
					
			$query = $this->db->query($sql);	
			if (!$query) {
				throw new Exception();
			}
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}

		return $retValue;
	}
	
	public function getCurrentActivity() {
		$retArray = array();
		
		$aSession = $this->session->userdata('participant');
		
		try {
			/*
			$sql = "SELECT rrt.dblRegistrationSubTotal, raa.intActivityID FROM relRegistrationTransaction AS rrt
				INNER JOIN relAccountActivity AS raa ON raa.intAccountID = rrt.intAccountID
				WHERE rrt.intAccountID = " . $aSession['participantAccountID'] . " AND raa.strStatusChange IS NULL AND intEventID = " . $aSession['participantEventID'];
			*/
			$sql = "SELECT raa.dblActivityPrice, raa.intActivityID, la.strActivity FROM relAccountActivity AS raa
			INNER JOIN lkpActivity AS la ON la.intActivityID = raa.intActivityID
			WHERE raa.intAccountID = " . $aSession['participantAccountID'] . " AND raa.strStatusChange IS NULL AND raa.intEventID = " . $aSession['participantEventID'];
		
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
			
		if (isset($result) && count($result) > 0) {
			$retArray = $result[0];			
		}
		
		return $retArray;
	}
	
	public function getActivityList($activityID) {
		$retArray = array();
		
		$aSession = $this->session->userdata('participant');
		
		try {
			$sql = "SELECT rpcd.intActivityID, la.strActivity, rpcd.dblPrice, reac.intCap FROM relPriceChangeDate AS rpcd
				INNER JOIN lkpActiveStatus AS las ON las.intActiveStatusID = rpcd.intActiveStatusID
				INNER JOIN lkpActivity AS la ON la.intActivityID = rpcd.intActivityID
				INNER JOIN relEventActivityCap AS reac ON reac.intActivityID = rpcd.intActivityID AND reac.intEventID = rpcd.intEventID
				WHERE rpcd.intEventID = " . $aSession['participantEventID'] . " AND rpcd.intActivityID <> " . $activityID . " AND rpcd.dttDateTill > NOW() AND LOWER(las.strActiveStatus) = 'active' GROUP BY rpcd.intEventID, rpcd.intActivityID ORDER BY rpcd.dttDateTill ASC";

			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}	
			
		if (isset($result) && count($result) > 0) {
			$retArray = $result;		
		}
		
		return $retArray;
	}
	
	public function getRegisteredCountForActivity() {
		$retArray = array();
		
		$aSession = $this->session->userdata('participant');
		
		try {
			$sql = "SELECT intActivityID, COUNT(intActivityID) AS 'aCount' FROM relAccountActivity WHERE intEventID = " . $aSession['participantEventID'] . " AND strStatusChange IS NULL GROUP BY intActivityID";
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
			
		if (isset($result) && count($result) > 0) {
			$tempArray = $result;
			$countTempArray = count($tempArray);
			for ($i = 0; $i < $countTempArray; $i++) {
				$retArray[$tempArray[$i]['intActivityID']] = $tempArray[$i]['aCount'];
			}
		}
		
		return $retArray;
	}
	
	public function getTransferCode() {
		$retValue = "";
		
		$aSession = $this->session->userdata('participant');
		
		try {
			$sql = "SELECT strTransferCode FROM relAccountActivity WHERE intAccountID = " . $aSession['participantAccountID'] . " AND intEventID = " . $aSession['participantEventID'] . " AND strStatusChange IS NULL";
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
			
		if (isset($result) && count($result) > 0) {
			$retValue = $result[0]['strTransferCode'];		
		}
		
		return $retValue;
	}
	
	public function drawPaymentBillingInformation() {
		$retString = "";
		
		$aSession = $this->session->userdata('participant');
		
		try {
			$sql = "SELECT epi.strFirstName, epi.strLastName, epi.strAddress, epi.strCity, epi.strPostalCode, epi.strCountry, epi.strHomePhone, epi.strEmail FROM entPersonalInformation AS epi
				INNER JOIN relAccountPersonalInformation AS rapi ON rapi.intPersonalInformationID = epi.intPersonalInformationID AND rapi.intAccountID = " . $aSession['participantAccountID'];
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			$retString .= '
				<div class="col-sm-6">
					<div class="well" id="billinginfo">
						<h4 class="well-title">' . translate("K293") . '</h4>
						<table class="table">
							<tr><td>' . translate("First Name") . ': </td><td align="right"><label>' . $result[0]['strFirstName'] . '</label></td></tr>
							<tr><td>' . translate("Last Name") . ': </td><td align="right"><label>' . $result[0]['strLastName'] . '</label></td></tr>
							<tr><td>' . translate("Address") . ': </td><td align="right"><label>' . $result[0]['strAddress'] . '</label></td></tr>
							<tr><td>' . translate("City") . ': </td><td align="right"><label>' . $result[0]['strCity'] . '</label></td></tr>
							<tr><td>' . translate("Postal Code") . ': </td><td align="right"><label>' . $result[0]['strPostalCode'] . '</label></td></tr>
							<tr><td>' . translate("Country") . ': </td><td align="right"><label>' . $result[0]['strCountry'] . '</label></td></tr>
							<tr><td>' . translate("Home Phone") . ': </td><td align="right"><label>' . $result[0]['strHomePhone'] . '</label></td></tr>
							<tr><td>' . translate("Email") . ': </td><td align="right"><label>' . $result[0]['strEmail'] . '</label></td></tr>
						</table>
					</div>
				</div>';		
		}
		
		return $retString;
	}
	
	public function getEventTax($aType) {
		$retValue = "available";
		
		$aSession = $this->session->userdata('participant');
		
		try {
			$sql = "SELECT dblTax FROM lkpEventTax WHERE strType = '" . $aType . "' AND intEventID = " . $aSession['participantEventID'];
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
				
		//Check if any results were returned
		if (isset($result[0]) && count($result[0]) > 0) {
			$retValue = $result[0]['dblTax'];
		}
		
		return $retValue;
	}
	
	public function drawChangeActivityPaymentCostTotals() {
		$aSession = $this->session->userdata('participant');
		//echo "<pre>"; print_r($aSession); echo "</pre>"; 
		
		$activityChangeFee = "";
		$dutType = $aSession['cart']['changeActivity']['dutType'];
		if ($dutType == "downgrade") {
			$activityChangeFee = number_format(DOWNGRADE_FEE,2);
		}
		else if ($dutType == "upgrade") {
			$activityChangeFee = number_format(UPGRADE_FEE,2);
		}
		$eventTax = $aSession['cart']['changeActivity']['eventTax'];
		$activityChangeFeeTax = number_format(($activityChangeFee * $eventTax),2);
		$priceDiffFee = number_format($aSession['cart']['changeActivity']['priceDifference'],2);
		$priceDiffFeeTax = number_format(($priceDiffFee * $eventTax),2);
		$subTotal = number_format(($activityChangeFee + $priceDiffFee),2);
		$tax = number_format(($activityChangeFeeTax + $priceDiffFeeTax),2);
		$eolFee = number_format(calculate_eol_fee($subTotal + $tax),2);
		$eolFeeTax = number_format(($eolFee * EOL_FEE_TAX),2);
		$total = number_format($subTotal + $tax + $eolFee + $eolFeeTax,2);
		$remit = number_format($subTotal + $tax,2);
		
		$aSession['cart']['changeActivity']['activityChangeFee'] = $activityChangeFee;
		$aSession['cart']['changeActivity']['activityChangeFeeTax'] = $activityChangeFeeTax;
		$aSession['cart']['changeActivity']['priceDifference'] = $priceDiffFee;
		$aSession['cart']['changeActivity']['priceDifferenceTax'] = $priceDiffFeeTax;
		$aSession['cart']['changeActivity']['subTotal'] = $subTotal;
		$aSession['cart']['changeActivity']['tax'] = $tax;
		$aSession['cart']['changeActivity']['eolFee'] = $eolFee;
		$aSession['cart']['changeActivity']['eolFeeTax'] = $eolFeeTax;
		$aSession['cart']['changeActivity']['total'] = $total;
		$aSession['cart']['changeActivity']['remit'] = $remit;
			
		$this->session->set_userdata("participant",$aSession);
		
		$retString = '
			<div class="col-sm-6">
				<div class="well">
					<h4 class="well-title">' . translate("K828") . '</h4>
					<table class="table">
						<tr>
							<td>' . translate("Activity Change Fee") . ':</td>
							<td></td>
							<td align="right">$' . $activityChangeFee . '</td>
						</tr>
						<tr>
							<td>' . translate("Price Difference") . ':</td>
							<td></td>
							<td align="right">$' . $priceDiffFee . '</td>
						</tr>
						<tr>
							<td><b>' . translate("K1086") . '</b></td>
							<td></td>
							<td align="right">$' . $subTotal . '</td>
						</tr>
						<tr>
							<td>' . translate("Tax") . ' (' . ($eventTax * 100) . '%):</td>
							<td></td>
							<td align="right">$' . $tax . '</td>
						</tr>
						<tr>
							<td><a href="' . base_url("register/fee") . '" target="_blank" style="text-decoration:underline">' . translate("K1087") . ':</a></td>
							<td></td>
							<td align="right">$' . ($eolFee + $eolFeeTax) . '</td>
						</tr>
						<tr>
							<td><h4><b>' . translate("K1088") . ':</b></h4></td>
							<td></td>
							<td align="right"><h4><b> $' . $total . ' CA</b></h4></td>
						</tr>
					</table>
				</div>
			</div>';
		
		return $retString;
	}
	
	public function drawExtraPurchasePaymentCostTotals() {
		$aSession = $this->session->userdata('participant');
		//echo "<pre>"; print_r($aSession); echo "</pre>"; 
		/*
		$activityChangeFee = "";
		$dutType = $aSession['cart']['changeActivity']['dutType'];
		if ($dutType == "downgrade") {
			$activityChangeFee = number_format(DOWNGRADE_FEE,2);
		}
		else if ($dutType == "upgrade") {
			$activityChangeFee = number_format(UPGRADE_FEE,2);
		}
		$eventTax = $aSession['cart']['changeActivity']['eventTax'];
		$activityChangeFeeTax = number_format(($activityChangeFee * $eventTax),2);
		$priceDiffFee = number_format($aSession['cart']['changeActivity']['priceDifference'],2);
		$priceDiffFeeTax = number_format(($priceDiffFee * $eventTax),2);
		$subTotal = number_format(($activityChangeFee + $priceDiffFee),2);
		$tax = number_format(($activityChangeFeeTax + $priceDiffFeeTax),2);
		$eolFee = number_format(calculate_eol_fee($subTotal + $tax),2);
		$total = number_format($subTotal + $tax + $eolFee,2);
		$remit = number_format($subTotal + $tax,2);
		
		$aSession['cart']['changeActivity']['activityChangeFee'] = $activityChangeFee;
		$aSession['cart']['changeActivity']['activityChangeFeeTax'] = $activityChangeFeeTax;
		$aSession['cart']['changeActivity']['priceDifference'] = $priceDiffFee;
		$aSession['cart']['changeActivity']['priceDifferenceTax'] = $priceDiffFeeTax;
		$aSession['cart']['changeActivity']['subTotal'] = $subTotal;
		$aSession['cart']['changeActivity']['tax'] = $tax;
		$aSession['cart']['changeActivity']['eolFee'] = $eolFee;
		$aSession['cart']['changeActivity']['total'] = $total;
		$aSession['cart']['changeActivity']['remit'] = $remit;
			
		$this->session->set_userdata("participant",$aSession);
		*/
		//echo "<pre>"; print_r($aSession); echo "</pre>"; exit;
		//echo "EVENT TAX" . $eventTax; exit;
		
		$addonSessionData = $aSession['cart']['addons'][$aSession['participantEventID']];
		
		$retString = '
			<div class="col-sm-6">
				<div class="well">
					<h4 class="well-title">' . translate("K828") . '</h4>
					<table class="table">';
						$subTotal = 0;
						foreach ($addonSessionData as $addonID => $addonDetails) {
							if ($addonID != "cartID" && $addonID != "total") {
								$countAddonDetails = count($addonDetails);
								for ($i = 0; $i < $countAddonDetails; $i++) {
									$subTotal += $addonDetails[$i]['price'];
		$retString .= '<tr>
								<td>' . $addonDetails[$i]['name'] . ':</td>
								<td></td>
								<td align="right">$' . number_format($addonDetails[$i]['price'],2) . '</td>
							</tr>';
								}
							}
						}
						$eventTax = $this->getEventTax('addon');
						$subTotal = number_format($subTotal,2);
						$tax = number_format(($subTotal * $eventTax),2);
						$eolFee = number_format(calculate_eol_fee($subTotal + $tax),2);
						$eolFeeTax = number_format(($eolFee * EOL_FEE_TAX),2);
						$total = number_format($subTotal + $tax + $eolFee + $eolFeeTax,2);
						$remit = number_format($subTotal + $tax,2);
						
						/*
						$aSession['cart']['extraPurchase']['subTotal'] = $subTotal;
						$aSession['cart']['extraPurchase']['tax'] = $tax;
						$aSession['cart']['extraPurchase']['eolFee'] = $eolFee;
						$aSession['cart']['extraPurchase']['total'] = $total;
						$aSession['cart']['addons']['remit'] = $remit;
						*/
						
						$aSession['cart']['addons'][$aSession['participantEventID']]['total'] = $total;
						$this->session->set_userdata("participant",$aSession);
						
		$retString .= '						
						<tr>
							<td><b>' . translate("K1086") . '</b></td>
							<td></td>
							<td align="right">$' . $subTotal . '</td>
						</tr>
						<tr>
							<td>' . translate("Tax") . ' (' . ($eventTax * 100) . '%):</td>
							<td></td>
							<td align="right">$' . $tax . '</td>
						</tr>
						<tr>
							<td><a href="' . base_url("register/fee") . '" target="_blank" style="text-decoration:underline">' . translate("K1087") . ':</a></td>
							<td></td>
							<td align="right">$' . ($eolFee + $eolFeeTax) . '</td>
						</tr>
						<tr>
							<td><h4><b>' . translate("K1088") . ':</b></h4></td>
							<td></td>
							<td align="right"><h4><b> $' . $total . ' CA</b></h4></td>
						</tr>
					</table>
				</div>
			</div>';
		
		return $retString;
	}
	
	public function drawPaymentCreditCard() {
		$retString = '
			<div class="well well-sm">
				<div class="card-details">
					<h3>' . translate("Accepted Credit Cards") . '</h3>
					<div class="row">
						<img src="' . base_url("assets/img/visa_48.png") . '" title="Visa" alt="Visa" />
						<img src="' . base_url("assets/img/mastercard_48.png") . '" title="Master Card" alt="Master Card" />
						<img src="' . base_url("assets/img/amex_48.png") . '" title="American Express" alt="American Express" />
					</div>
					<br />
					  <div class="form-group">
						<label for="cc_name">' . translate("K542") . '</label>
						<input type="text" name="cc_name" class="form-control required" id="cc_name" placeholder="Name on Credit Card:">
					  </div>
					  <div class="form-group">
						<label for="cc_num">' . translate("K543") . '</label>
						<input type="text" name="cc_num" class="form-control required" id="cc_num" placeholder="XXXXXXXXXXXX1234">
					  </div>
					  <div class="form-group">
						<label>' . translate("K544") . '</label>
						<br />
						<div class="row">
							<div class="col-sm-6">
								<small>' . translate("K39") . '</small><br />
								<select class="form-control" name="exp_month">';
								for ($i = 1; $i <= 12; $i++) {
									$aValue = $i;
									if ($i < 10) {
										$aValue = "0" . $i;
									}
									$retString .= '<option value="' . $aValue . '">' . $aValue . '</option>';
								}
			$retString .= '
								</select>
							</div>
							<div class="col-sm-6">
								<small>' . translate("K311") . '</small><br />
								<select name="exp_year" class="form-control" required>';
								$startYear = date('Y');
								for ($i = $startYear; $i < ($startYear + 20); $i++) {
									$retString .= '<option value="' . substr($i,2,2) . '">' . $i . '</option>';
								}
			$retString .= '					
								</select>
							</div>
						</div>						
					  </div>
						<div class="form-group">
							<label for="cvv">' . translate("CVV") . '</label>
							<input type="text" name="cvv" class="form-control required" id="cvv">
						</div>
					  <div class="form-group">
						<button class="btn btn-block btn-success"><h5><span class="glyphicon glyphicon-credit-card"></span> ' . translate("Purchase") . '</h5></button>
					  </div>
				  </div>
			</div>';
		return $retString;
	}
	
	public function insertCartSession() {
		$retValue = "";
		$jsonData = json_encode($_SESSION['participant']);
				
		try {
			$sql = "INSERT INTO entActiveCart (strActiveCartData, dttDateCreated, dttDateModified) VALUES ('" . $jsonData . "', NOW(), NOW());";
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$retValue = $this->db->lastInsertId();
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
						
		return $retValue;
	}
	
	public function updateSessionToCart($cartID) {
		$jsonData = json_encode($_SESSION['participant']);
				
		try {
			$sql = "UPDATE entActiveCart SET 
				strActiveCartData = '" . $jsonData . "', 
				dttDateModified = NOW() 
				WHERE intActiveCartID = " . $cartID;
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}		
	}
	
	public function processChangeActivity() {
		$aSession = $this->session->userdata('participant');
		//echo "<pre>"; print_r($aSession); echo "</pre>"; exit;
		
		try {
			$sql = "START TRANSACTION;
				UPDATE relParticipantShirt SET intShirtID = " . $aSession['cart']['changeActivity']['newShirtID'] . ", intActivityID = " . $aSession['cart']['changeActivity']['newActivityID'] . " WHERE intPersonalInformationID = " . $aSession['participantPersonalInformationID'] . " AND intEventID = " . $aSession['participantEventID'] . " AND intActivityID = " . $aSession['cart']['changeActivity']['oldActivityID'] . ";
				
				UPDATE relAccountActivity SET strStatusChange = 'changedActivity', dttDateModified = NOW() WHERE strStatusChange IS NULL AND intAccountID = " . $aSession['participantAccountID'] . " AND intEventID = " . $aSession['participantEventID'] . " AND intActivityID = " . $aSession['cart']['changeActivity']['oldActivityID'] . ";
				
				INSERT INTO relAccountActivity (intActivityID, intEventID, intAccountID, dblActivityPrice, dttDateCreated) VALUES (" . $aSession['cart']['changeActivity']['newActivityID'] . ", " . $aSession['participantEventID'] . ", " . $aSession['participantAccountID'] . ", " . $aSession['cart']['changeActivity']['actualEventActivityPrice'] . ", NOW());
				
				SELECT @intTransactionPaidStatusID := intTransactionPaidStatusID FROM lkpTransactionPaidStatus WHERE LOWER(strTransactionPaidStatus) = 'paid';
				
				INSERT INTO entTransaction (intTransactionPaidStatusID, dblDutTotal, dblTaxTotal, dblEolFee, dblFinalTotal, dblRemit, dttDateCreated, intCartID) VALUES (@intTransactionPaidStatusID, " . $aSession['cart']['changeActivity']['subTotal'] . ", " . $aSession['cart']['changeActivity']['tax'] . ", " . ($aSession['cart']['changeActivity']['eolFee'] + $aSession['cart']['changeActivity']['eolFeeTax']) . ", " . $aSession['cart']['changeActivity']['total'] . ", " . $aSession['cart']['changeActivity']['remit'] . ", NOW(), " . $aSession['cart']['changeActivity']['cartID'] . ");
				
				SELECT @intTransactionID := LAST_INSERT_ID();
				
				SELECT @intDutTransactionTypeID := intDutTransactionTypeID FROM lkpDutTransactionType WHERE LOWER(strDutTransactionType) = '" . $aSession['cart']['changeActivity']['dutType'] . "';
				
				INSERT INTO relDutTransaction (intTransactionID, intAccountID, intDutTransactionTypeID, intEventID, intActivityID, intOldActivityID, dblDutFee, dblDutFeeTax, dblPriceDifference, dblPriceDifferenceTax, dblEolFee, dblEolFeeTax, dblTotal) VALUES (@intTransactionID, " . $aSession['participantAccountID'] . ", @intDutTransactionTypeID, " . $aSession['participantEventID'] . ", " . $aSession['cart']['changeActivity']['newActivityID'] . ", " . $aSession['cart']['changeActivity']['oldActivityID'] . ", " . $aSession['cart']['changeActivity']['activityChangeFee'] . ", " . $aSession['cart']['changeActivity']['activityChangeFeeTax'] . ", " . $aSession['cart']['changeActivity']['priceDifference'] . ", " . $aSession['cart']['changeActivity']['priceDifferenceTax'] . ", " . $aSession['cart']['changeActivity']['eolFee'] . ", " . $aSession['cart']['changeActivity']['eolFeeTax'] . ", " . $aSession['cart']['changeActivity']['total'] . ");
				
				SELECT @intTransactionTypeID := intTransactionTypeID FROM lkpTransactionType WHERE LOWER(strTransactionType) = '" . $aSession['cart']['changeActivity']['dutType'] . "';
				
				INSERT INTO relAccountTransaction (intAccountID, intTransactionID, intTransactionTypeID) VALUES (" . $aSession['participantAccountID'] . ", @intTransactionID, @intTransactionTypeID);
				
				COMMIT;";
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}			
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
	}
	
	public function processAddonPurchase() {
		$aSession = $this->session->userdata('participant');
		//echo "<pre>"; print_r($aSession); echo "</pre>"; exit;
		$buildSql = "";
		$eventTax = $this->getEventTax('addon');
		$allAddonTotal = 0;
		$allAddonTaxTotal = 0;
		
		
		$addonSessionData = $aSession['cart']['addons'][$aSession['participantEventID']];
		foreach ($addonSessionData as $addonID => $addonDetails) {
			if ($addonID != "cartID" && $addonID != "total") {
				$countAddonDetails = count($addonDetails);
				for ($i = 0; $i < $countAddonDetails; $i++) {	
					$shirtID = ((isset($addonDetails[$i]['tshirtID']))?$addonDetails[$i]['tshirtID']:'NULL');
					$subTotal = number_format($addonDetails[$i]['price'],2);
					$subTotalTax = number_format(($subTotal * $eventTax),2);
					$total = number_format(($subTotal + $subTotalTax),2);
					
					$allAddonTotal += $subTotal;
					$allAddonTaxTotal += $subTotalTax;
					
					$buildSql .= " INSERT INTO relAddonTransaction (intTransactionID, intAccountID, intAddonTransactionTypeID, intEventID, intAddonID, intPersonalInformationID, intQty, intAddonShirtID, dblAddonSubTotal, dblAddonSubTotalTax, dblTotal) VALUES (@intTransactionID, " . $aSession['participantAccountID'] . ", @intAddonTransactionTypeID, " . $aSession['participantEventID'] . ", " . $addonID . ", " . $aSession['participantPersonalInformationID'] . ", " . $addonDetails[$i]['qty'] . ", " . $shirtID . ", " . $subTotal . ", " . $subTotalTax . ", " . $total . ");";
				}
			}
		}	

		$eolFee = number_format(calculate_eol_fee($allAddonTotal + $allAddonTaxTotal),2);
		$eolFeeTax = number_format(($eolFee * EOL_FEE_TAX),2);
		$eolFee += $eolFeeTax;
		$finalTotal = number_format(($allAddonTotal + $allAddonTaxTotal + $eolFee),2);
		$remit = number_format(($allAddonTotal + $allAddonTaxTotal),2);
		
		try {
			$sql = "START TRANSACTION;
								
				SELECT @intTransactionPaidStatusID := intTransactionPaidStatusID FROM lkpTransactionPaidStatus WHERE LOWER(strTransactionPaidStatus) = 'paid';
				
				INSERT INTO entTransaction (intTransactionPaidStatusID, dblAddonTotal, dblTaxTotal, dblEolFee, dblFinalTotal, dblRemit, dttDateCreated, intCartID) VALUES (@intTransactionPaidStatusID, " . $allAddonTotal . ", " . $allAddonTaxTotal . ", " . $eolFee . ", " . $finalTotal . ", " . $remit . ", NOW(), " . $addonSessionData['cartID'] . ");
				
				SELECT @intTransactionID := LAST_INSERT_ID();
				
				SELECT @intAddonTransactionTypeID := intAddonTransactionTypeID FROM lkpAddonTransactionType WHERE LOWER(strAddonTransactionType) = 'userdashboard';";
			
			$sql .= $buildSql;
				/*
				INSERT INTO relDutTransaction (intTransactionID, intAccountID, intDutTransactionTypeID, intEventID, intActivityID, intOldActivityID, dblDutFee, dblDutFeeTax, dblPriceDifference, dblPriceDifferenceTax, dblEolFee, dblTotal) VALUES (@intTransactionID, " . $aSession['participantAccountID'] . ", @intDutTransactionTypeID, " . $aSession['participantEventID'] . ", " . $aSession['cart']['changeActivity']['newActivityID'] . ", " . $aSession['cart']['changeActivity']['oldActivityID'] . ", " . $aSession['cart']['changeActivity']['activityChangeFee'] . ", " . $aSession['cart']['changeActivity']['activityChangeFeeTax'] . ", " . $aSession['cart']['changeActivity']['priceDifference'] . ", " . $aSession['cart']['changeActivity']['priceDifferenceTax'] . ", " . $aSession['cart']['changeActivity']['eolFee'] . ", " . $aSession['cart']['changeActivity']['total'] . ");
				*/
			$sql .= "	
				SELECT @intTransactionTypeID := intTransactionTypeID FROM lkpTransactionType WHERE LOWER(strTransactionType) = 'extraPurchase';
				
				INSERT INTO relAccountTransaction (intAccountID, intTransactionID, intTransactionTypeID) VALUES (" . $aSession['participantAccountID'] . ", @intTransactionID, @intTransactionTypeID);
				
				COMMIT;";
			//echo $sql; exit;
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}			
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
	}
	
	public function getActualEventActivityPrice($activityID) {
		$retValue = "";
		
		$aSession = $this->session->userdata('participant');
		
		try {
			$sql = "SELECT dblPrice FROM relPriceChangeDate WHERE intEventID = " . $aSession['participantEventID'] . " AND intActivityID = " . $activityID . " AND dttDateTill > NOW()
				ORDER BY dttDateTill
				LIMIT 1";
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
				
		//Check if any results were returned
		if (isset($result[0]) && count($result[0]) > 0) {
			$retValue = $result[0]['dblPrice'];
		}
		
        return $retValue;
	}
	
	public function getParticipantAgeOnRaceDay() {
		$retValue = "0";
		$dob = "";
		$eventDate = "";
		
		$aSession = $this->session->userdata('participant');
		
		try {
			$sql = "SELECT dttEventDate FROM lkpEvent WHERE intEventID = " . $aSession['participantEventID'];
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
				
		//Check if any results were returned
		if (isset($result[0]) && count($result[0]) > 0) {
			$eventDate = $result[0]['dttEventDate'];
		}
		
		try {
			$sql = "SELECT dteDateOfBirth FROM entPersonalInformation WHERE intPersonalInformationID = " . $aSession['participantPersonalInformationID'];
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
				
		//Check if any results were returned
		if (isset($result[0]) && count($result[0]) > 0) {
			$dob = $result[0]['dteDateOfBirth'];
		}
		
		$retValue = calculateAge($eventDate,$dob);
				
        return $retValue;
	}
	
	public function getParticipantAddons() {
		$retArray = array();
		
		$aSession = $this->session->userdata('participant');
		
		try {
			$sql = "SELECT SUM(intQty) AS aSum, intAddonID FROM relAddonTransaction WHERE strRefundStatus IS NULL AND intAccountID = " . $aSession['participantAccountID'] . " AND intEventID = " . $aSession['participantEventID'] . " GROUP BY intAddonID";
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
				
		//Check if any results were returned
		if (isset($result[0]) && count($result[0]) > 0) {
			$countResult = count($result);
			for ($i = 0; $i < $countResult; $i++) {
				$retArray[$result[$i]['intAddonID']] = $result[$i]['aSum'];
			}
		}
		
        return $retArray;
	}
	
	public function getAllPurchasedAddons() {
		$retArray = array();
		
		$aSession = $this->session->userdata('participant');
		
		try {
			$sql = "SELECT SUM(intQty) AS aSum, intAddonID FROM relAddonTransaction WHERE strRefundStatus IS NULL AND intEventID = " . $aSession['participantEventID'] . " GROUP BY intAddonID";
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
				
		//Check if any results were returned
		if (isset($result[0]) && count($result[0]) > 0) {
			$countResult = count($result);
			for ($i = 0; $i < $countResult; $i++) {
				$retArray[$result[$i]['intAddonID']] = $result[$i]['aSum'];
			}
		}
		
        return $retArray;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */