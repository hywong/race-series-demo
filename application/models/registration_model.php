<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registration_model extends CI_Model 
{
  public $insertIDs = array();
  public $createdTeamIDs = array();
  public $billingMemberPersonalID;
  public $personalInsertsCount;
  
  private $statements = array();
  private $transactionStatusID;
  private $registrationTransactionTypeID;
  private $addonTransactionTypeID;
  private $donationTransactionTypeID;
  private $personalInformationType;
  private $profilesCreated;
  private $transferredAccountID;
  private $accountTransactionTypeID;

  public function insert_account($id = NULL, $saveID = FALSE)
  {
    //Get active status ID from database
    $sqlAccount = "INSERT INTO entAccount (intActiveStatusID, strUserName, strPassword, dttDateCreated, dttDateModified)
      VALUES (1, :username, :password, NOW(), NOW());";

    if(!isset($this->statements['account']))
    {
      $this->statements['account'] = $this->db->prepare($sqlAccount);
    }

    $data = array(
      "username" => $_SESSION['registration'][$id]['personal']['username'],
      "password" => b_encrypt($_SESSION['registration'][$id]['personal']['password']),
      );
    
    if($this->_execute("account", $data))
    {
      if($saveID === TRUE) $this->insertIDs['account'] = $this->db->lastInsertId();
      return TRUE;
    }
    else
    {
      sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO entAccount with the following: <br />'.json_encode($data));
      return FALSE;
    }
  }

  public function insert_account_group($id = NULL, $saveID = FALSE)
  {
    //Get active status ID from database
    $sqlAccount = "INSERT INTO entAccount (intActiveStatusID, dttDateCreated, dttDateModified)
      VALUES (1, NOW(), NOW());";

    if(!isset($this->statements['account']))
    {
      $this->statements['account'] = $this->db->prepare($sqlAccount);
    }

    if($this->_execute("account", array()))
    {
      if($saveID === TRUE) $this->insertIDs['account'] = $this->db->lastInsertId();
      return TRUE;
    }
    else
    {
      sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO entAccount as a group.');
      return FALSE;
    }
  }

  public function insert_personal($id = NULL, $saveID = FALSE)
  {
    $sqlPersonal = "INSERT INTO entPersonalInformation (strFirstName, strLastName, strAddress, strCity, strProvince, strCountry, strPostalCode, strHomePhone, strMobilePhone, strEmail, strGender, dteDateOfBirth, intAgeOnRegistration, strCategory, strSeriesNewsletter, strNationality, strNationalityDuration, strIndustryType, strIndustryTypeOther, strYearlySalary, strEmergencyContactName, strEmergencyContactNumber, strMedicalCondition)
      VALUES (:first_name, :last_name, :address, :city, :province, :country, :postal_code, :home_phone, :mobile_phone, :email, :gender, :date_of_birth,TIMESTAMPDIFF(YEAR, :date_of_birth, CURDATE()), :category, :newsletter, :nationality, :nationalityDuration, :industryType, :industryTypeOther, :yearlySalary, :emergencyName, :emergencyNumber, :medicalCondition);";
    
    if(!isset($this->statements['personal']))
    {
      $this->statements['personal'] = $this->db->prepare($sqlPersonal);
    }

    $data["first_name"] = (isset($_SESSION['registration'][$id]['personal']["first_name"]) ? $_SESSION['registration'][$id]['personal']["first_name"] : NULL);
    $data["last_name"] = (isset($_SESSION['registration'][$id]['personal']["last_name"]) ? $_SESSION['registration'][$id]['personal']["last_name"] : NULL);
    $data["address"] = (isset($_SESSION['registration'][$id]['personal']["address"]) ? $_SESSION['registration'][$id]['personal']["address"] : NULL);
    $data["city"] = (isset($_SESSION['registration'][$id]['personal']["city"]) ? $_SESSION['registration'][$id]['personal']["city"] : NULL);
    $data["province"] = (isset($_SESSION['registration'][$id]['personal']["province"]) ? $_SESSION['registration'][$id]['personal']["province"] : NULL);
    $data["country"] = (isset($_SESSION['registration'][$id]['personal']["country"]) ? $_SESSION['registration'][$id]['personal']["country"] : NULL);
    $data["postal_code"] = (isset($_SESSION['registration'][$id]['personal']["postal_code"]) ? $_SESSION['registration'][$id]['personal']["postal_code"] : NULL);
    $data["home_phone"] = (isset($_SESSION['registration'][$id]['personal']["home_phone"]) ? $_SESSION['registration'][$id]['personal']["home_phone"] : NULL);
    $data["mobile_phone"] = (isset($_SESSION['registration'][$id]['personal']["mobile_phone"]) ? $_SESSION['registration'][$id]['personal']["mobile_phone"] : NULL);
    $data["email"] = (isset($_SESSION['registration'][$id]['personal']["email"]) ? $_SESSION['registration'][$id]['personal']["email"] : NULL);
    $data["gender"] = (isset($_SESSION['registration'][$id]['personal']["gender"]) ? $_SESSION['registration'][$id]['personal']["gender"] : NULL);
    $data["date_of_birth"] = (isset($_SESSION['registration'][$id]['personal']["date_of_birth"]) ? $_SESSION['registration'][$id]['personal']["date_of_birth"] : NULL);
    $data["category"] = (isset($_SESSION['registration'][$id]['personal']['categories']) ? (is_array($_SESSION['registration'][$id]['personal']['categories']) ? implode("|", $_SESSION['registration'][$id]['personal']['categories']) : $_SESSION['registration'][$id]['personal']['categories']) : NULL);
    $data["newsletter"] = (isset($_SESSION['registration'][$id]['personal']['newsletter']) ? $_SESSION['registration'][$id]['personal']['newsletter'] : NULL);
    $data["nationality"] = (isset($_SESSION['registration'][$id]['personal']['nationality']) ? $_SESSION['registration'][$id]['personal']['nationality'] : NULL);
    $data["nationalityDuration"] = (isset($_SESSION['registration'][$id]['personal']['nationality_duration']) ? $_SESSION['registration'][$id]['personal']['nationality_duration'] : NULL);
    $data["industryType"] = (isset($_SESSION['registration'][$id]['personal']['type_of_industry']) ? $_SESSION['registration'][$id]['personal']['type_of_industry'] : NULL);
    $data["industryTypeOther"] = (isset($_SESSION['registration'][$id]['personal']['type_of_industry_other']) ? $_SESSION['registration'][$id]['personal']['type_of_industry_other'] : NULL);
    $data["yearlySalary"] = (isset($_SESSION['registration'][$id]['personal']['yearly_salary']) ? $_SESSION['registration'][$id]['personal']['yearly_salary'] : NULL);
    $data["emergencyName"] = (isset($_SESSION['registration'][$id]['personal']['emergency_name']) ? $_SESSION['registration'][$id]['personal']['emergency_name'] : NULL);
    $data["emergencyNumber"] = (isset($_SESSION['registration'][$id]['personal']['emergency_number']) ? $_SESSION['registration'][$id]['personal']['emergency_number'] : NULL);
    $data["medicalCondition"] = (isset($_SESSION['registration'][$id]['personal']['medical_conditions']) ? $_SESSION['registration'][$id]['personal']['medical_conditions'] : NULL);


    if($this->_execute("personal", $data))
    {
      if($saveID === TRUE) $this->insertIDs['personal'] = $this->db->lastInsertID();
      if($saveID === TRUE) $this->insertIDs['all-personal-id'][] = $this->db->lastInsertID();
      if($saveID === TRUE) $this->insertIDs['personal-by-id'][$id] = $this->db->lastInsertID();
      if($this->input->post('billing_member') === $id) $this->billingMemberPersonalID = $this->db->lastInsertID();

      return TRUE;
    }
    else
    {
      sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO entPersonalInformation with the following: '.json_encode($data).'<br /><br /> Query: '.$sqlPersonal);
      return FALSE;
    }
  }

  public function insert_personal_group($id = NULL, $saveID = FALSE)
  {
    $sqlPersonal = "INSERT INTO entPersonalInformation (strFirstName, strLastName, strAddress, strCity, strProvince, strCountry, strPostalCode, strHomePhone, strMobilePhone, strEmail, strGender, dteDateOfBirth, intAgeOnRegistration, strEmergencyContactName, strEmergencyContactNumber, strMedicalCondition)
      VALUES ";
    
    if(!isset($this->statements['personal-group']))
    {
      $this->statements['personal-group'] = $this->db->prepare($sqlPersonal);
    }
    $groupCart = $_SESSION['registration'][$id];
    $personalInserts = array();

    //This is the billing member
    $numOfInserts = 1;
    $personalInserts[] = "(:firstName0,:lastName0,:address0,:city0,:province0, :country0, :postalCode0,:homePhone0,:mobilePhone0,:email0,:gender0,:dob0, :age0, :emergencyName0,:emergencyNumber0,:medicalCondition0)";
    $personalInsertData = array(
      "firstName0" => $groupCart['personal']['first_name'],
      "lastName0" => $groupCart['personal']['last_name'],
      "address0" => $groupCart['personal']['address'],
      "city0" => $groupCart['personal']['city'],
      "province0" => $groupCart['personal']['province'],
      "country0" => $groupCart['personal']['country'],
      "postalCode0" => $groupCart['personal']['postal_code'],
      "homePhone0" => $groupCart['personal']['home_phone'],
      "mobilePhone0" => $groupCart['personal']['mobile_phone'],
      "email0" => $groupCart['personal']['email'],
      "gender0" => NULL,
      "dob0" => NULL,
      "age0" => NULL,
      "emergencyName0" => NULL,
      "emergencyNumber0" => NULL,
      "medicalCondition0" => NULL
      );

    if(isset($groupCart['group_details']))
    {
      $c = 1;
      foreach($groupCart['group_details'] as $memberID => $memberDetails)
      {
        $numOfInserts++;
        $personalInserts[] = "(:firstName".$c.",:lastName".$c.",:address".$c.",:city".$c.",:province".$c.", :country".$c.", :postalCode".$c.",:homePhone".$c.",:mobilePhone".$c.",:email".$c.",:gender".$c.",:dob".$c.",TIMESTAMPDIFF(YEAR, :dob".$c.", CURDATE()),:emergencyName".$c.",:emergencyNumber".$c.",:medicalCondition".$c.")";
        $personalInsertData["firstName".$c] = $memberDetails["first_name"];
        $personalInsertData["lastName".$c] = $memberDetails["last_name"];
        $personalInsertData["address".$c] = $groupCart['personal']["address"];
        $personalInsertData["city".$c] = $groupCart['personal']["city"];
        $personalInsertData["province".$c] = $groupCart['personal']["province"];
        $personalInsertData["country".$c] = $groupCart['personal']["country"];
        $personalInsertData["postalCode".$c] = $groupCart['personal']["postal_code"];
        $personalInsertData["homePhone".$c] = $groupCart['personal']["home_phone"];
        $personalInsertData["mobilePhone".$c] = $groupCart['personal']["mobile_phone"];
        $personalInsertData["gender".$c] = $memberDetails["gender"];
        $personalInsertData["email".$c] = $memberDetails["email"];
        $personalInsertData["dob".$c] = $memberDetails["date_of_birth"];
        $personalInsertData["emergencyName".$c] = $memberDetails["emergency_name"];
        $personalInsertData["emergencyNumber".$c] = $memberDetails["emergency_number"];
        $personalInsertData["medicalCondition".$c] = $memberDetails["medical_conditions"];
        $c++;
      } 

      $sqlAppended = $sqlPersonal . implode(",", $personalInserts);
      $this->statements['personal-group'] = $this->db->prepare($sqlAppended);
    }

    if($this->_execute("personal-group", $personalInsertData))
    {
      $this->personalInsertsCount = $numOfInserts;
      if($saveID === TRUE) $this->insertIDs['personal-group'] = $this->db->lastInsertID();
      return TRUE;
    }
    else
    {
      sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO entPersonalInformation as a group with the following: '.json_encode($personalInsertData).'<br /><br /> Query: '.$sqlAppended);
      return FALSE;
    }
  }

  public function insert_activities($id, $prices, $saveID = FALSE)
  {
    $sqlActivity = "INSERT INTO relAccountActivity (intActivityID, intEventID, intAccountID, dblActivityPrice, dttDateCreated, dttDateModified)
      VALUES ";

    $count = 0;
    foreach($_SESSION['registration'][$id]['activities'] as $eventActivity)
    {
      $exploded = explode("_", $eventActivity);
      $eventID = reset($exploded);
      $activityID = end($exploded);

      $activityInserts[] = "(:activityID".$count.", :eventID".$count.", :accountID, :activityPrice".$count.", NOW(), NOW())";

      $activityInsertData['activityID'.$count] = $activityID;
      $activityInsertData['eventID'.$count] = $eventID;   
      $activityInsertData['activityPrice'.$count] = $prices[$eventID][$activityID];
      $count++;   
    }

    $activityInsertData['accountID'] = $this->insertIDs['account'];

    //Prepare and execute activity bulk insert
    $sqlActivityAppended = $sqlActivity.implode(",", $activityInserts);
    $this->statements['activity'] = $this->db->prepare($sqlActivityAppended);
    
    if($this->_execute("activity", $activityInsertData))
    {
      if($saveID === TRUE) $this->insertIDs['activity'] = $this->db->lastInsertID();
      return TRUE;
    }
    else
    {
      sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO relAccountActivity with the following: '.json_encode($activityInsertData).'<br /><br /> Query: '.$sqlActivityAppended);
      return FALSE;
    }
  }

  public function insert_finishtime($id, $saveID = FALSE)
  {
    $sqlParticipantFinish = "INSERT INTO relParticipantFinishTime (intPersonalInformationID, intEventID, intFinishTimeHour, intFinishTimeMinute)
      VALUES ";

    $count = 0;
    foreach($_SESSION['registration'][$id]['activities'] as $eventActivity)
    {
      $exploded = explode("_", $eventActivity);
      $eventID = reset($exploded);
      $activityID = end($exploded);

      $finishtimeInserts[] = "(:personalInformationID, :eventID".$count.", :finishTimeHour".$count.", :finishTimeMinute".$count.")";

      if(isset($_SESSION['registration'][$id]['eventextras'][$eventID]['finish_time_hour']))
        $finishtimeInsertData['finishTimeHour'.$count] = $_SESSION['registration'][$id]['eventextras'][$eventID]['finish_time_hour'];
      else
        $finishtimeInsertData['finishTimeHour'.$count] = NULL;

      if(isset($_SESSION['registration'][$id]['eventextras'][$eventID]['finish_time_minute']))
        $finishtimeInsertData['finishTimeMinute'.$count] = $_SESSION['registration'][$id]['eventextras'][$eventID]['finish_time_minute'];
      else
        $finishtimeInsertData['finishTimeMinute'.$count] = NULL;

      $finishtimeInsertData['eventID'.$count] = $eventID;
      $count++;
    }

    $formattedInserts = $sqlParticipantFinish . implode(",", $finishtimeInserts);

    $this->statements['finishtime'] = $this->db->prepare($formattedInserts);

    $mergedData = array_merge($finishtimeInsertData, array(
      "personalInformationID" => $this->insertIDs['personal']
      ));
    
    if($this->_execute("finishtime", $mergedData))
    {
      if($saveID === TRUE) $this->insertIDs['finishtime'] = $this->db->lastInsertID();
      return TRUE;
    }
    else
    {
      sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO relParticipantFinishTime with the following: '.json_encode($mergedData).'<br /><br /> Query: '.$formattedInserts);
      return FALSE;
    }
  }

  public function insert_finishtime_group($id, $saveID = FALSE)
  {
    $sqlParticipantFinish = "INSERT INTO relParticipantFinishTime (intPersonalInformationID, intEventID, intFinishTimeHour, intFinishTimeMinute)
      VALUES ";

    foreach($_SESSION['registration'][$id]['activities'] as $eventActivity)
    {
      $exploded = explode("_", $eventActivity);
      $eventID = reset($exploded);
      $activityID = end($exploded);

      //Starts at one to skip billing member
      for($i = 1; $i < $this->personalInsertsCount; $i++)
      {
        $finishtimeInserts[] = "(:personalInformationID".$i.", :eventID".$i.", :finishTimeHour".$i.", :finishTimeMinute".$i.")";

        if(isset($_SESSION['registration'][$id]['group_details'][$i]['eventextras']['finish_time_hour']))
          $finishtimeInsertData['finishTimeHour'.$i] = $_SESSION['registration'][$id]['group_details'][$i]['eventextras']['finish_time_hour'];
        else
          $finishtimeInsertData['finishTimeHour'.$i] = NULL;

        if(isset($_SESSION['registration'][$id]['group_details'][$i]['eventextras']['finish_time_minute']))
          $finishtimeInsertData['finishTimeMinute'.$i] = $_SESSION['registration'][$id]['group_details'][$i]['eventextras']['finish_time_minute'];
        else
          $finishtimeInsertData['finishTimeMinute'.$i] = NULL;

        $finishtimeInsertData['eventID'.$i] = $eventID;

        $finishtimeInsertData['personalInformationID'.$i] = $this->insertIDs['personal-group']+$i;
      }
    }

    $formattedInserts = $sqlParticipantFinish . implode(",", $finishtimeInserts);

    $this->statements['finishtime-group'] = $this->db->prepare($formattedInserts);
    
    if($this->_execute("finishtime-group", $finishtimeInsertData))
    {
      if($saveID === TRUE) $this->insertIDs['finishtime-group'] = $this->db->lastInsertID();
      return TRUE;
    }
    else
    {
      sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO relParticipantFinishTime with the following: '.json_encode($finishtimeInsertData).'<br /><br /> Query: '.$formattedInserts);
      return FALSE;
    }
  }

  public function insert_account_personal_relation($id, $saveID = FALSE)
  {
    $sqlAccountPersonalRel = "INSERT INTO relAccountPersonalInformation (intPersonalInformationID, intAccountID, intPersonalInformationTypeID)
      VALUES (:personalInformationID, :accountID, :personalInformationTypeID);";

    $this->statements['account-personal-rel'] = $this->db->prepare($sqlAccountPersonalRel);

    if($this->input->post('billing_member') === $id)
    {
      $personalInformationTypeID = $this->personalInformationType['individualBillingMember'];
    }
    else
    {
      $personalInformationTypeID = $this->personalInformationType['individual'];
    }

    $dataToExecute = array(
      "personalInformationID" => $this->insertIDs['personal'],
      "accountID" => $this->insertIDs['account'],
      "personalInformationTypeID" => $personalInformationTypeID
      );

    if($this->_execute("account-personal-rel", $dataToExecute))
    {
      if($saveID === TRUE) $this->insertIDs['account-personal-rel'] = $this->db->lastInsertID();
      return TRUE;
    }
    else
    {
      sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO relAccountPersonalInformation with the following: '.json_encode($dataToExecute).'<br /><br /> Query: '.$sqlAccountPersonalRel);
      return FALSE;
    }
  }

  public function insert_account_personal_relation_group($id, $saveID = FALSE)
  {
    $sqlAccountPersonalRel = "INSERT INTO relAccountPersonalInformation (intPersonalInformationID, intAccountID, intPersonalInformationTypeID)
      VALUES ";

    for($i = 0; $i < $this->personalInsertsCount; $i++)
    {
      $accountPersonalRelInserts[] = "(:personalID".$i.", :accountID, :personalInformationTypeID".$i.")";

      if($i === 0)
        $accountPersonalRelData["personalInformationTypeID".$i] = $this->personalInformationType["groupBillingMember"];
      else
        $accountPersonalRelData["personalInformationTypeID".$i] = $this->personalInformationType["groupMember"];

      $accountPersonalRelData["personalID".$i] = $this->insertIDs['personal-group']+$i;
    }

    $accountPersonalRelData['accountID'] = $this->insertIDs['account'];
    $sqlAppended = $sqlAccountPersonalRel . implode(",", $accountPersonalRelInserts);

    $this->statements['account-personal-rel'] = $this->db->prepare($sqlAppended);

    if($this->_execute("account-personal-rel", $accountPersonalRelData))
    {
      if($saveID === TRUE) $this->insertIDs['account-personal-rel'] = $this->db->lastInsertID();
      return TRUE;
    }
    else
    { 
      sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO relAccountPersonalInformation with the following: '.json_encode($accountPersonalRelData).'<br /><br /> Query: '.$sqlAppended);
      return FALSE;
    }
  }

  public function insert_participant_shirt($id, $saveID = FALSE)
  {
    $sqlParticipantShirt = "INSERT INTO relParticipantShirt (intPersonalInformationID, intShirtID, intEventID, intActivityID)
      VALUES ";

    $count = 0;
    foreach($_SESSION['registration'][$id]['activities'] as $eventActivity)
    {
      $exploded = explode("_", $eventActivity);
      $eventID = reset($exploded);
      $activityID = end($exploded);

      $participantShirtInserts[] = "(:personalInformationID, :shirtID".$count.", :eventID".$count.", :activityID".$count.")";
      $participantShirtInsertData['shirtID'.$count] = $_SESSION['registration'][$id]['eventextras'][$eventID]['tshirt'];
      $participantShirtInsertData['activityID'.$count] = $activityID;
      $participantShirtInsertData['eventID'.$count] = $eventID;
      $count++;
    }

    $formattedInserts = $sqlParticipantShirt . implode(",", $participantShirtInserts);

    $this->statements['participant-shirt'] = $this->db->prepare($formattedInserts);

    $mergedData = array_merge($participantShirtInsertData, array(
      "personalInformationID" => $this->insertIDs['personal']
      ));

    if($this->_execute("participant-shirt", $mergedData))
    {
      if($saveID === TRUE) $this->insertIDs['participant-shirt'] = $this->db->lastInsertID();
      return TRUE;
    }
    else
    {
      sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO relParticipantShirt with the following: '.json_encode($mergedData).'<br /><br /> Query: '.$formattedInserts);
      return FALSE;
    }
  }

  public function insert_participant_shirt_group($id, $saveID = FALSE)
  {
    $sqlParticipantShirt = "INSERT INTO relParticipantShirt (intPersonalInformationID, intShirtID, intEventID, intActivityID)
      VALUES ";

    foreach($_SESSION['registration'][$id]['activities'] as $eventActivity)
    {
      $exploded = explode("_", $eventActivity);
      $eventID = reset($exploded);
      $activityID = end($exploded);

      for($i = 1; $i < $this->personalInsertsCount; $i++)
      {
        $participantShirtInserts[] = "(:personalInformationID".$i.", :shirtID".$i.", :eventID".$i.", :activityID".$i.")";

        $participantShirtInsertData['shirtID'.$i] = $_SESSION['registration'][$id]['group_details'][$i]['eventextras']['tshirt'];
        $participantShirtInsertData['activityID'.$i] = $activityID;
        $participantShirtInsertData['eventID'.$i] = $eventID;
        $participantShirtInsertData['personalInformationID'.$i] = $this->insertIDs['personal-group'] + $i;
      }
    }

    $formattedInserts = $sqlParticipantShirt . implode(",", $participantShirtInserts);

    $this->statements['participant-shirt'] = $this->db->prepare($formattedInserts);

    if($this->_execute("participant-shirt", $participantShirtInsertData))
    {
      if($saveID === TRUE) $this->insertIDs['participant-shirt'] = $this->db->lastInsertID();
      return TRUE;
    }
    else
    {
      sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO relParticipantShirt with the following: '.json_encode($participantShirtInsertData).'<br /><br /> Query: '.$formattedInserts);
      return FALSE;
    }
  }

  public function insert_transaction_transfer($financialData = array(), $saveID = FALSE)
  {
    $sqlTransaction = "INSERT INTO entTransaction (intTransactionPaidStatusID, dblActivityCostTotal, dblAddonTotal, dblDutTotal, dblDiscountTotal, dblTaxTotal, dblEolFee, dblDonationTotal, dblFinalTotal, dblRemit, dttDateCreated, dttDateModified, intCartID)
      VALUES (:transactionPaidStatusID, :activityCostTotal, :addonTotal, :dutTotal, :discountTotal, :taxTotal, :eolFee, :donationTotal, :finalTotal, :remit, NOW(), NOW(), :cartID);";

    $this->statements['transaction'] = $this->db->prepare($sqlTransaction);
    $dataToExecute = array(
      "activityCostTotal" => 0,
      "addonTotal" => $financialData['addonTotal'],
      "dutTotal" => $financialData['activityCostTotal'],
      "discountTotal" => 0,
      "taxTotal" => $financialData['taxTotal'],
      "eolFee" => $financialData['eolFee'] + $financialData['eolFeeTax'],
      "donationTotal" => $financialData['donationTotal'],
      "finalTotal" => $financialData['finalTotal'],
      "remit" => $financialData['remit'],
      "transactionPaidStatusID" => $this->transactionStatusID,
      "cartID" => $_SESSION['cartID']
      );

    if($this->_execute("transaction", $dataToExecute))
    {
      if($saveID === TRUE) $this->insertIDs['transaction'] = $this->db->lastInsertID();
      return TRUE;
    }
    else
    {
      sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO entTransaction with the following: '.json_encode($dataToExecute).'<br /><br /> Query: '.$sqlTransaction);
      return FALSE;
    }
  }

  public function insert_transaction($financialData = array(), $saveID = FALSE)
  {
    $sqlTransaction = "INSERT INTO entTransaction (intTransactionPaidStatusID, dblActivityCostTotal, dblAddonTotal, dblDiscountTotal, dblTaxTotal, dblEolFee, dblDonationTotal, dblFinalTotal, dblRemit, dttDateCreated, dttDateModified, intCartID)
      VALUES (:transactionPaidStatusID, :activityCostTotal, :addonTotal, :discountTotal, :taxTotal, :eolFee, :donationTotal, :finalTotal, :remit, NOW(), NOW(), :cartID);";

    $this->statements['transaction'] = $this->db->prepare($sqlTransaction);
    $financialDataNew = $financialData;
    $financialDataNew['eolFee'] += $financialDataNew['eolFeeTax'];

    unset($financialDataNew['eolFeeTax']);
    $dataToExecute = array_merge(
      $financialDataNew, 
      array(
      "transactionPaidStatusID" => $this->transactionStatusID,
      "cartID" => $_SESSION['cartID']
      )
      );

    if($this->_execute("transaction", $dataToExecute))
    {
      if($saveID === TRUE) $this->insertIDs['transaction'] = $this->db->lastInsertID();
      return TRUE;
    }
    else
    {
      sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO entTransaction with the following: '.json_encode($dataToExecute).'<br /><br /> Query: '.$sqlTransaction);
      return FALSE;
    }
  }

  public function insert_aka($id, $saveID = FALSE)
  {
    $sqlAka = "INSERT INTO relAkaCharityTeamAccount (intEventID, strAkaCharityTeam, intAccountID, strAkaFundraisingOption, strLearnAboutProstateCancer)
    VALUES ";

    $c = 0;
    foreach($_SESSION['registration'][$id]['aka'] as $eventID => $akaDetails)
    {
      $akaInserts[] = "(:eventID".$c.", :akaCharityTeam".$c.", :accountID, :fundraisingOption".$c.", :learnAbout".$c.")";
      $akaData['eventID'.$c] = $eventID;
      $akaData['akaCharityTeam'.$c] = $akaDetails['aka_team_name'];
      $akaData['fundraisingOption'.$c] = $akaDetails['aka_fundraising_option'];
      (isset($akaDetails['aka_prostate_cancer_info']) ? $akaData['learnAbout'.$c] = $akaDetails['aka_prostate_cancer_info'] : $akaData['learnAbout'.$c] = NULL);
    }
    $akaData['accountID'] = $this->insertIDs['account'];

    $sqlAppended = $sqlAka . implode(",", $akaInserts);

    $this->statements['aka-account'] = $this->db->prepare($sqlAppended);

    if($this->_execute("aka-account", $akaData))
    {
      if($saveID === TRUE) $this->insertIDs['aka-account'] = $this->db->lastInsertID();
      return TRUE;
    }
    else
    {
      sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO relAkaCharityTeamAccount with the following: '.json_encode($akaData).'<br /><br /> Query: '.$sqlAppended);
      return FALSE;
    }
  }

  public function insert_artez_account_relation($id, $artezDetails, $saveID = FALSE)
  {
    $sqlArtez = "INSERT INTO relArtezCharityAccount (intAccountID, intArtezCharityID, strUsedPin, dblDiscountedAmount, dttDateCreated)
    VALUES ";

    $c = 0;

    foreach($artezDetails['artez'] as $eID => $artez)
    {
      $artezInserts[] = "(:accountID, :artezCharityID".$c.", :usedPin".$c.", :discountedAmount".$c.", NOW())";
      $artezInsertData['artezCharityID'.$c] = $artez['artez_details']['artezCharityID'];
      if(isset($artez['artez_details']['pin']))
      {
        $artezInsertData['usedPin'.$c] = 'y';
      }
      else
      {
        $artezInsertData['usedPin'.$c] = NULL;
      }
      $artezInsertData['discountedAmount'.$c] = $artez['artez_details']['discount'];
      $c++;
    }

    $artezInsertData['accountID'] = $this->insertIDs['account'];

    $sqlAppended = $sqlArtez . implode(',', $artezInserts);
    $this->statements['artez-account'] = $this->db->prepare($sqlAppended);

    if($this->_execute("artez-account", $artezInsertData))
    {
      if($saveID === TRUE) $this->insertIDs['artez-account'] = $this->db->lastInsertID();
      return TRUE;
    }
    else
    {
      sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO relArtezCharityAccount with the following: '.json_encode($artezInsertData).'<br /><br /> Query: '.$sqlAppended);
      return FALSE;
    }
  }

  public function insert_dut_transaction_relation($id, $additionalData, $saveID = FALSE)
  {
    $sqlSelectDutType = "SELECT intDutTransactionTypeID AS dutTypeID FROM lkpDutTransactionType WHERE strDutTransactionType = 'transfer' LIMIT 1";
    $stmt = $this->db->prepare($sqlSelectDutType);
    if($stmt->execute())
    {
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
      $typeID = $results[0]['dutTypeID'];

      $eventActivity = $_SESSION['registration'][$id]['activities'][0];
      $expl = explode("_", $eventActivity);
      $activityID = end($expl);
      $eventID = reset($expl);

      $sqlInsertDutRelation = "INSERT INTO relDutTransaction (intTransactionID, intAccountID, intOldAccountID, intDutTransactionTypeID, intEventID, intActivityID, intOldActivityID, dblDutFee, dblDutFeeTax, dblPriceDifference, dblPriceDifferenceTax, dblEolFee, dblEolFeeTax, dblTotal)
      VALUES (:transactionID, :accountID, :origAccountID, :dutType, :eventID, :activityID, :oldActivityID, :dutFee, :dutFeeTax, :priceDifference, :priceDifferenceTax, :eolFee, :eolFeeTax, :total);";

      $this->statements['dut-transaction-relation'] = $this->db->prepare($sqlInsertDutRelation);

      $dataToExecute = array(
        "transactionID" => $this->insertIDs['transaction'],
        "accountID" => $this->insertIDs['account'],
        "origAccountID" => $this->transferredAccountID,
        "dutType" => $typeID,
        "oldActivityID" => $activityID,
        "dutFee" => $additionalData['dutFee'],
        "dutFeeTax" => $additionalData['dutFeeTax'],
        "priceDifference" => 0,
        "priceDifferenceTax" => 0,
        "eolFee" => $additionalData['eolFee'],
        "eolFeeTax" => $additionalData['eolFeeTax'],
        "total" => $additionalData['total'],
        "eventID" => $eventID,
        "activityID" => $activityID
        );
      
      if($this->_execute("dut-transaction-relation", $dataToExecute))
      {
        if($saveID === TRUE) $this->insertIDs['dut-transaction-relation'] = $this->db->lastInsertID();
        return TRUE;
      }
      else
      {
        sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO relArtezCharityAccount with the following: '.json_encode($dataToExecute).'<br /><br /> Query: '.$sqlInsertDutRelation);
        return FALSE;
      }
    }
    else
    {
      sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO relArtezCharityAccount with the following: '.json_encode($dataToExecute).'<br /><br /> Query: '.$sqlInsertDutRelation);
      return FALSE;
    }
  }

  public function insert_registration_transaction_relation($id, $additionalData, $saveID = FALSE)
  {
    $sqlRegistrationTransaction = "INSERT INTO relRegistrationTransaction (intTransactionID, intAccountID, intRegistrationTransactionTypeID, intDiscountCodeID, dblRegistrationSubTotal, dblRegistrationSubTotalTax, dblDiscount, strDiscountType, dblArtezDiscount, dblTotal)
      VALUES (:transactionID, :accountID, :registrationTransactionTypeID, :discountCodeID, :registrationSubTotal, :registrationSubTotalTax, :discount, :discountType, :artezDiscount, :total);";

    $dataToExecute = array(
      "transactionID" => $this->insertIDs['transaction'],
      "accountID" => $this->insertIDs['account'],
      "registrationTransactionTypeID" => $this->registrationTransactionTypeID,
      "registrationSubTotal" => $additionalData['registrationSubTotal'],
      "registrationSubTotalTax" => $additionalData['registrationSubTotalTax'],
      "total" => $additionalData['registrationSubTotal'] + $additionalData['registrationSubTotalTax'] - $additionalData['artezDiscount'] - $additionalData['discount'],
      "discount" => $additionalData['discount'],
      "artezDiscount" => $additionalData['artezDiscount'],
      "discountCodeID" => $additionalData['discountCodeID']
      );

    $mergedData = array_merge($dataToExecute, $additionalData);

    $this->statements['registration-transaction-rel'] = $this->db->prepare($sqlRegistrationTransaction);
    if($this->_execute("registration-transaction-rel", $mergedData))
    {
      if($saveID === TRUE) $this->insertIDs['registration-transaction-rel'] = $this->db->lastInsertID();
      return TRUE;
    }
    else
    {
      sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO relArtezCharityAccount with the following: '.json_encode($mergedData).'<br /><br /> Query: '.$sqlRegistrationTransaction);
      return FALSE;
    }
  }

  public function insert_addons($id, $addonFinancialData, $saveID = FALSE)
  {
    $sqlAddonTransaction = "INSERT INTO relAddonTransaction (intTransactionID, intAccountID, intAddonTransactionTypeID, intEventID, intAddonID, intPersonalInformationID, intQty, intAddonShirtID, dblAddonSubTotal, dblAddonSubTotalTax, dblTotal)
      VALUES ";

    $groupedItems = array();

    $addonCounter = 0;
    $groupedCount = 0;
    foreach($_SESSION['registration'][$id]['addons'] as $eventID => $addonSet)
    {
      foreach($addonSet as $addonID => $addonGrouped)
      {
        foreach($addonGrouped as $k => $addonDetails)
        {
          $addonInserts[] = "(:transactionID, :accountID, :addonTransactionTypeID, :eventID".$addonCounter.", :addonID".$addonCounter.", :personalInformationID, :qty".$addonCounter.", :addonShirtID".$addonCounter.", :addonSubTotal".$addonCounter.", :addonSubTotalTax".$addonCounter.", :addonTotal".$addonCounter.")";
          $addonInsertData["addonID".$addonCounter] = $addonID;
          $addonInsertData["eventID".$addonCounter] = $eventID;
          (isset($addonDetails['tshirt']) ? $addonInsertData["addonShirtID".$addonCounter] = $addonDetails['tshirt'] : $addonInsertData["addonShirtID".$addonCounter] = NULL);
          (isset($addonDetails['qty']) ? $addonInsertData["qty".$addonCounter] = $addonDetails['qty'] : $addonInsertData['qty'.$addonCounter] = 1);
          $addonInsertData["addonSubTotal".$addonCounter] = $addonFinancialData[$eventID][$addonID]["price"] * $addonDetails['qty'];
          $addonInsertData["addonTotal".$addonCounter] = $addonFinancialData[$eventID][$addonID]["price"] * $addonDetails['qty'];
          $addonInsertData["addonSubTotalTax".$addonCounter] = 0;
          $addonCounter++;
        }
      }
    }

    $addonInsertData['addonTransactionTypeID'] = $this->addonTransactionTypeID;
    $addonInsertData['personalInformationID'] = $this->insertIDs['personal-by-id'][$id];
    $addonInsertData['transactionID'] = $this->insertIDs['transaction'];
    $addonInsertData['accountID'] = $this->insertIDs['account'];
    $sqlAppended = $sqlAddonTransaction . implode(",", $addonInserts);
    $this->statements["addon"] = $this->db->prepare($sqlAppended);

    if($this->_execute("addon", $addonInsertData))
    {
      if($saveID === TRUE) $this->insertIDs['addon'] = $this->db->lastInsertID();
      return TRUE;
    }
    else
    {
      sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO relAddonTransaction with the following: '.json_encode($addonInsertData).'<br /><br /> Query: '.$sqlAppended);
      return FALSE;
    }
  }

  public function insert_addons_group($id, $addonFinancialData, $saveID = FALSE)
  {
    $sqlAddonTransaction = "INSERT INTO relAddonTransaction (intTransactionID, intAccountID, intAddonTransactionTypeID, intEventID, intAddonID, intPersonalInformationID, intQty, dblAddonSubTotal, dblAddonSubTotalTax, dblTotal)
      VALUES ";

    $groupedItems = array();

    $addonCounter = 0;
    $groupedCount = 0;

    $eventActivity = $_SESSION['registration'][$id]['activities'][0];
    $expl = explode("_", $eventActivity);
    $geventID = reset($expl);
    unset($expl);

    $group = $_SESSION['registration'][$id]['group_details'];
    $itabID = NULL;
    $c = 0;
    for($i = 1; $i < $this->personalInsertsCount; $i++)
    {
      if(isset($group[$i]['addon']))
      {
        if(isset($group[$i]['addon']['itab']))
        {
          if($group[$i]['addon']['itab'] != "")
          {
            if($itabID === NULL)
              $itabID = $group[$i]['addon']['itab'];

            $addonInserts[] = "(:transactionID, :accountID, :addonTransactionTypeID, :eventID, :addonID, :personalInformationID".$c.", :qty, :addonSubTotal, :addonSubTotalTax, :addonTotal)";
            $addonData['personalInformationID'.$c] = $this->insertIDs['personal-group'] + $i;
          }
        }
      }
      $c++;
    }

    if(!empty($addonData))
    {
      $addonData['transactionID'] = $this->insertIDs['transaction'];
      $addonData['accountID'] = $this->insertIDs['account'];
      $addonData['addonTransactionTypeID'] = $this->addonTransactionTypeID;
      $addonData['eventID'] = $geventID;
      $addonData['addonID'] = $itabID;
      $addonData['qty'] = 1;
      $addonData['addonSubTotal'] = $addonFinancialData[$geventID][$itabID]["price"];
      $addonData['addonTotal'] = $addonFinancialData[$geventID][$itabID]["price"];
      $addonData['addonSubTotalTax'] = 0;

      $sqlAppended = $sqlAddonTransaction . implode(",", $addonInserts);

      $this->statements["addon-group"] = $this->db->prepare($sqlAppended);

      if($this->_execute("addon-group", $addonData))
      {
        if($saveID === TRUE) $this->insertIDs['addon-group'] = $this->db->lastInsertID();
        return TRUE;
      }
      else
      {
        sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO relAddonTransaction with the following: '.json_encode($addonData).'<br /><br /> Query: '.$sqlAppended);
        return FALSE;
      }
    }
  }

  public function insert_donations($id, $saveID = FALSE)
  {
    $sqlDonationTransaction = "INSERT INTO relDonationTransaction (intTransactionID, intAccountID, intDonationTransactionTypeID, intEventID, intCharityID, intPledgeProfileID, dblDonationSubTotal, dblEolFee, dblEolFeeTax, dblRemit, dblTotal)
      VALUES ";

    $count = 0;

    foreach($_SESSION['registration'][$id]['charities'] as $eventID => $charityDetails)
    {
      $donationInserts[] = "(:transactionID, :accountID, :donationTransactionTypeID, :eventID".$count.", :charityID".$count.", :pledgeProfileID, :donationSubTotal".$count.", :donationEolFee".$count.", :donationEolFeeTax".$count.", :donationRemit".$count.", :total".$count.")";
      $donationInsertData["eventID".$count] = $eventID;
      $donationInsertData["charityID".$count] = $charityDetails["charity"];

      //Financial calculations
      $donationSubTotal = $charityDetails["donation"];
      $donationEolFee = number_format($charityDetails['donation'] * 0.065, 2, '.', '');
      $donationEolFeeTax = number_format($donationEolFee * EOL_FEE_TAX, 2, '.', '');
      $donationTotalEolFee = $donationEolFee + $donationEolFeeTax;
      // $donationEolFee += $donationEolFeeTax;
      $donationRemit = number_format($donationSubTotal - $donationTotalEolFee, 2, '.', '');

      $donationInsertData["donationSubTotal".$count] = $donationSubTotal;
      $donationInsertData["donationEolFee".$count] = $donationEolFee;
      $donationInsertData["donationEolFeeTax".$count] = $donationEolFeeTax;
      $donationInsertData["donationRemit".$count] = $donationRemit;
      $donationInsertData["total".$count] = $donationSubTotal;
      $donationInsertData["pledgeProfileID"] = NULL;

      $count++;
    }

    $donationInsertData["transactionID"] = $this->insertIDs["transaction"];
    $donationInsertData["accountID"] = $this->insertIDs["account"];
    $donationInsertData["donationTransactionTypeID"] = $this->donationTransactionTypeID;

    $sqlDonationAppended = $sqlDonationTransaction . implode(",", $donationInserts);

    $this->statements["donations"] = $this->db->prepare($sqlDonationAppended);

    if($this->_execute("donations", $donationInsertData))
    {
      if($saveID === TRUE) $this->insertIDs['donations'] = $this->db->lastInsertID();
      return TRUE;
    }
    else
    {
      sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO relDonationTransaction with the following: '.json_encode($donationInsertData).'<br /><br /> Query: '.$sqlDonationAppended);
      return FALSE;
    }
  }

  public function insert_fundraising_profiles($id, $saveID = FALSE)
  {
    $sqlPledgeProfile = "INSERT INTO entPledgeProfile (dblPledgeGoal, strPledgeMessage, strPledgeProfilePicture, dttDateCreated) VALUES ";

    $sess = $_SESSION['registration'][$id]['charities'];
    $profileInserts = array();
    $profileInsertData = array();
    $c = 0;
    $this->profilesCreated = 0;
    foreach($sess as $eventID => $charityDetails)
    {
      if(isset($charityDetails['fundraising_profile']))
      {
        if($charityDetails['fundraising_profile'] == "Yes")
        {
          $profileInserts[] = "(:pledgeGoal".$c.", :pledgeMessage".$c.", :pledgeProfilePicture".$c.", NOW())";
          $profileInsertData["pledgeGoal".$c] = $charityDetails['fundraising_goal'];
          $profileInsertData["pledgeMessage".$c] = $charityDetails['fundraising_message'];
          $profileInsertData["pledgeProfilePicture".$c] = $charityDetails['fundraising_image'];
          $this->profilesCreated++;
          $c++;
        }
      }
    }

    $sqlAppended = $sqlPledgeProfile . implode(",", $profileInserts);

    $this->statements['fundraising-profile'] = $this->db->prepare($sqlAppended);

    if($this->_execute("fundraising-profile", $profileInsertData))
    {
      if($saveID === TRUE) $this->insertIDs['fundraising-profile'] = $this->db->lastInsertID();

      if($this->insert_fundraising_profiles_relation($id))
      {
        return TRUE;
      }
      else
      {
        sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO entPledgeProfile with the following: '.json_encode($profileInsertData).'<br /><br /> Query: '.$sqlAppended);
        return FALSE;
      }
    }
    else
    {
      return FALSE;
    }
  }

  public function insert_fundraising_profiles_relation($id, $saveID = FALSE)
  {
    $sqlPledgeProfileRel = "INSERT INTO relPledgeProfile (intAccountID, intEventID, intCharityID, intPledgeProfileID) VALUES ";

    $sess = $_SESSION['registration'][$id]['charities'];
    $c = 0;
    foreach($sess as $eID => $charityDetails)
    {
      if(isset($charityDetails['fundraising_profile']))
      {
        if($charityDetails['fundraising_profile'] == "Yes")
        {
          $profileRelInserts[] = "(:accountID".$c.", :eventID".$c.", :charityID".$c.", :profileID".$c.")";
          $profileRelInsertData["accountID".$c] = $this->insertIDs['account'];
          $profileRelInsertData["eventID".$c] = $eID;
          $profileRelInsertData["charityID".$c] = $charityDetails['charity'];
          $profileRelInsertData["profileID".$c] = $this->insertIDs['fundraising-profile']+$c;
          $c++;
        }
      }
    }

    $sqlAppended = $sqlPledgeProfileRel . implode(",", $profileRelInserts);

    $this->statements['fundraising-profile-rel'] = $this->db->prepare($sqlAppended);

    if($this->_execute("fundraising-profile-rel", $profileRelInsertData))
    {
      if($saveID === TRUE) $this->insertIDs['fundraising-profile-rel'] = $this->db->lastInsertID();
      return TRUE;
    }
    else
    {
      sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO relPledgeProfile with the following: '.json_encode($profileRelInsertData).'<br /><br /> Query: '.$sqlAppended);
      return FALSE;
    }
  }

  public function insert_teams($id, $saveID = FALSE)
  {
    $sqlCreateTeam = "INSERT INTO entTeam (strTeamName, strTeamPassword)
      VALUES (:teamName, :teamPassword);";
    $sqlJoinTeam = "INSERT INTO relTeam (intTeamID, intPersonalInformationID, intEventID, strIsCaptain)
      VALUES (:teamID, :personalInformationID, :eventID, :isCaptain);";

    foreach($_SESSION['registration'][$id]['teams'] as $thisEventID => $teamDetails)
    {
      //We're joining a team, only insert to relation
      if($teamDetails['team_action'] == "join")
      {
        if(!isset($this->statements['team-join']))
        {
          $this->statements['team-join'] = $this->db->prepare($sqlJoinTeam);
        }

        if(stripos($teamDetails['team_name'], "TEMPTEAM_") === 0)
        {
          $sessionCaptainID = str_replace("TEMPTEAM_", '', $teamDetails['team_name']);
          $teamID = $this->createdTeamIDs[$sessionCaptainID];
        }
        else
        {
          $teamID = $teamDetails['team_name'];
        }

        $dataToExecute = array(
          "teamID" => $teamID,
          "personalInformationID" => $this->insertIDs['personal'],
          "eventID" => $thisEventID,
          "isCaptain" => "no"
          );
        if(!$this->_execute("team-join", $dataToExecute))
        {
          sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO relTeam with the following: '.json_encode($dataToExecute).'<br /><br /> Query: '.$sqlJoinTeam);
        }
      }

      if($teamDetails['team_action'] == "create")
      {
        if(!isset($this->statements['team-create']))
        {
          $this->statements['team-create'] = $this->db->prepare($sqlCreateTeam);
        }

        $dataToExecute = array(
          "teamName" => $teamDetails['team_name_create'],
          "teamPassword" => b_encrypt($teamDetails['team_password_create'])
          );

        if(!$this->_execute("team-create", $dataToExecute))
        {
          sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO entTeam with the following: '.json_encode($dataToExecute).'<br /><br /> Query: '.$sqlCreateTeam);
        }

        $thisTeamID = $this->db->lastInsertId();
        $this->createdTeamIDs[$id] = $thisTeamID;

        if(!isset($this->statements['team-join']))
        {
          $this->statements['team-join'] = $this->db->prepare($sqlJoinTeam);
        }

        $dataToExecute = array(
          "teamID" => $thisTeamID,
          "personalInformationID" => $this->insertIDs['personal'],
          "eventID" => $thisEventID,
          "isCaptain" => "yes"
          );

        if(!$this->_execute("team-join", $dataToExecute))
        {
          sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO relTeam (after creating the team) with the following: '.json_encode($dataToExecute).'<br /><br /> Query: '.$sqlJoinTeam);
        }
      }
    }
  }

  public function insert_teams_group($id, $saveID = FALSE)
  {
    $sqlCreateTeam = "INSERT INTO entTeam (strTeamName, strTeamPassword, strGroupName)
    VALUES (:teamName, :teamPassword, :groupName);";
    $sqlJoinTeam = "INSERT INTO relTeam (intTeamID, intPersonalInformationID, intEventID, strIsCaptain)
    VALUES ";

    $this->statements['group-team-create'] = $this->db->prepare($sqlCreateTeam);
    if(isset($_SESSION['registration'][$id]['personal']['group_name']))
    {
      if($_SESSION['registration'][$id]['personal']['group_name'] != "")
      {
        $groupName = $_SESSION['registration'][$id]['personal']['group_name'];
      }
      else
      {
        $groupName = NULL;
      }
    }
    else
    {
      $groupName = NULL;
    }

    $createData = array(
      "teamName" => $_SESSION['registration'][$id]['personal']['team_name_create'],
      "teamPassword" => b_encrypt($_SESSION['registration'][$id]['personal']['team_password_create']),
      "groupName" => $groupName
      );
    if(!$this->_execute("group-team-create", $createData))
    {
      sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO entTeam with the following: '.json_encode($createData).'<br /><br /> Query: '.$sqlCreateTeam);
    }

    $thisCreatedTeam = $this->db->lastInsertId();

    $c = 0;
    for($i = 1; $i < $this->personalInsertsCount; $i++)
    {
      $relTeamInserts[] = "(:teamID, :personalInformationID".$c.", :eventID, :isCaptain".$c.")";
      $relTeamData['personalInformationID'.$c] = $this->insertIDs['personal-group'] + $i;
      if($i == 1)
      {
        $relTeamData['isCaptain'.$c] = "yes";
      }
      else
      {
        $relTeamData['isCaptain'.$c] = "no";
      }
      $c++;
    }
    $expl = explode("_", $_SESSION['registration'][$id]['activities'][0]);
    $eventID = reset($expl);

    $relTeamData['eventID'] = $eventID;
    $relTeamData['teamID'] = $thisCreatedTeam;

    $sqlJoinTeamAppended = $sqlJoinTeam . implode(',', $relTeamInserts);

    $this->statements['group-team-create-join'] = $this->db->prepare($sqlJoinTeamAppended);

    if(!$this->_execute("group-team-create-join", $relTeamData))
    {
      sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO relTeam with the following: '.json_encode($relTeamData).'<br /><br /> Query: '.$sqlJoinTeamAppended);
    }

    return TRUE;
  }

  public function insert_extras($id, $saveID = FALSE)
  {
    $sqlExtraField = "INSERT INTO entExtraField (strLearnAboutEvent, strLearnAboutEventOther, strFirstTimeParticipating, strHowManyTimesParticipated, strWhyParticipating)
      VALUES (:learnAboutEvent, :learnAboutEventOther, :firstTimeParticipating, :howManyTimes, :whyParticipating)";
    $sqlExtraFieldRelation = "INSERT INTO relExtraField (intAccountID, intEventID, intExtraFieldID)
      VALUES (:accountID, :eventID, :extraFieldID);";

    if(isset($_SESSION['registration'][$id]['eventextras']))
    {
      if(!isset($this->statements['extras']))
      {
        $this->statements['extras'] = $this->db->prepare($sqlExtraField);
      }

      if(!isset($this->statements['account-extras-rel']))
      {
        $this->statements['account-extras-rel'] = $this->db->prepare($sqlExtraFieldRelation);
      }

      foreach($_SESSION['registration'][$id]['eventextras'] as $thisEventID => $extraDetails)
      {
        (isset($extraDetails['learn_about']) ? $extraInsertData["learnAboutEvent"] = $extraDetails['learn_about'] : $extraInsertData["learnAboutEvent"] = NULL);
        (isset($extraDetails["learn_about_other"]) ? $extraInsertData["learnAboutEventOther"] = $extraDetails['learn_about_other'] : $extraInsertData["learnAboutEventOther"] = NULL);
        (isset($extraDetails["first_time"]) ? $extraInsertData["firstTimeParticipating"] = $extraDetails['first_time'] : $extraInsertData["firstTimeParticipating"] = NULL);
        (isset($extraDetails["times_participated"]) ? $extraInsertData["howManyTimes"] = $extraDetails['times_participated'] : $extraInsertData["howManyTimes"] = NULL);
        (isset($extraDetails["why_participating"]) ? $extraInsertData["whyParticipating"] = $extraDetails['why_participating'] : $extraInsertData["whyParticipating"] = NULL);

        if($this->_execute("extras", $extraInsertData))
        {
          $thisExtraInsertID = $this->db->lastInsertID();
          $dataForRelation = array(
            "accountID" => $this->insertIDs['account'],
            "eventID" => $thisEventID,
            "extraFieldID" => $thisExtraInsertID
            );
          if($this->statements['account-extras-rel']->execute($dataForRelation))
          {
            //Success
          }
          else
          {
            sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO relTeam with the following: '.json_encode($dataForRelation).'<br /><br /> Query: '.$extraInsertData);
          }
        }
        else
        {
            sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO relTeam with the following: '.json_encode($dataForRelation).'<br /><br /> Query: '.$sqlExtraFieldRelation);
        }
      }
    }
  }

  public function insert_seriesdonation($id)
  {
    $sqlGetCharityID = "SELECT intCharityID AS charity FROM lkpCharity WHERE strCharity = 'Charity 1' LIMIT 1;";
    $stmt1 = $this->db->prepare($sqlGetCharityID);
    if($stmt1->execute())
    {
      $results = $stmt1->fetchAll(PDO::FETCH_ASSOC);
      $seriesfoundationID = $results[0]['charity'];

      $sqlGetAllEventID = "SELECT intEventID AS event FROM lkpEvent WHERE strEvent = 'All' LIMIT 1;";
      $stmt2 = $this->db->prepare($sqlGetAllEventID);
      if($stmt2->execute())
      {
        $results1 = $stmt2->fetchAll(PDO::FETCH_ASSOC);
        $allEventID = $results1[0]['event'];

        $sqlDonationTrans = "INSERT INTO relDonationTransaction (intTransactionID, intAccountID, intDonationTransactionTypeID, intEventID, intCharityID, dblDonationSubTotal, dblEolFee, dblEolFeeTax, dblRemit, dblTotal)
        VALUES (:transactionID, :accountID, :donationTransactionTypeID, :eventID, :charityID, :donationSubTotal, :eolFee, :eolFeeTax, :remit, :total);";
        $donationSubTotal = $_SESSION['registration'][$id]['seriesfoundation']['donation'];
        $eolFee = number_format($donationSubTotal * 0.065, 2, '.', '');
        $eolFeeTax = number_format(($eolFee * EOL_FEE_TAX), 2, '.', '');
        $totalEolFee = $eolFee + $eolFeeTax;

        $dataToExecute = array(
          "transactionID" => $this->insertIDs['transaction'],
          "accountID" => $this->insertIDs['account'],
          "donationTransactionTypeID" => $this->donationTransactionTypeID,
          "eventID" => $allEventID,
          "charityID" => $seriesfoundationID,
          "donationSubTotal" => number_format($donationSubTotal, 2, '.', ''),
          "eolFee" => number_format($eolFee, 2, '.', ''),
          "eolFeeTax" => number_format($eolFeeTax, 2, '.', ''),
          "remit" => number_format($donationSubTotal - $totalEolFee, 2, '.', ''),
          "total" => number_format($donationSubTotal, 2, '.', '')
          );

        $stmt3 = $this->db->prepare($sqlDonationTrans);

        if($stmt3->execute($dataToExecute))
        {
          return TRUE;
        }
        else
        {
          sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO relDonationTransaction (Series Foundation) with the following: '.json_encode($dataToExecute).'<br /><br /> Query: '.$sqlDonationTrans);
          return FALSE;
        }
      }
      else
      {
        sendErrorEmail('Series Demo - Failed query', 'Failed SELECT lkpEvent (Series Foundation) with the following: <br /><br /> Query: '.$sqlGetAllEventID);
        return FALSE;
      }
    }
    else
    {
        sendErrorEmail('Series Demo - Failed query', 'Failed SELECT lkpCharity (Series Foundation) with the following: <br /><br /> Query: '.$sqlGetCharityID);
        return FALSE;
    }

  }

  public function insert_account_transaction()
  {
    $sql = "INSERT INTO relAccountTransaction (intAccountID, intTransactionID, intTransactionTypeID)
    VALUES (:accountID, :transactionID, :transactionTypeID);";

    $dataToExecute = array(
      "accountID" => $this->insertIDs['account'],
      "transactionID" => $this->insertIDs['transaction'],
      "transactionTypeID" => $this->accountTransactionTypeID
      );

    $this->statements['account-transaction-type'] = $this->db->prepare($sql);

    if($this->_execute('account-transaction-type', $dataToExecute))
    {
      return TRUE;
    }
    else
    {
      sendErrorEmail('Series Demo - Failed query', 'Failed INSERT INTO relAccountTransaction (Series Foundation) with the following: '.json_encode($dataToExecute).'<br /><br /> Query: '.$sql);
      return FALSE;
    }
  }

  private function _execute($statement, $data)
  {
      // echo "<PRE>";
      // print_r($data);
      // print_r($this->statements[$statement]);
      // echo "</PRE>";
    if(!$this->statements[$statement]->execute($data))
    {
      // echo "<PRE>";
      // print_r($data);
      // print_r($this->statements[$statement]);
      // echo "<h4>".$statement." execution failed.</h4>";
      // echo "</PRE>";
      return FALSE;
    }
    else
    {
      return TRUE;
    }
  }

  public function get_transfer_costs()
  {
    $this->load->library("registration");
    $taxRates = $this->registration->get_tax_rates();
    $transferFee = TRANSFER_FEE;

    foreach($_SESSION['registration'] as $id => $participant)
    {
      if($id != "last_modified" && $id != "active_user")
      {
        if(isset($participant['activities']))
        {
          if(count($participant['activities']) == 1 && $_SESSION['type'] == "transfer")
          {
            $expl = explode("_", $participant['activities'][0]);
            $activity = end($expl);
            $event = reset($expl);
            $thisID = $id;
          }
          else
          {
            //TODO: Error, the session is out of whack and the transfer was terminated
          }
        }
      }
    }

    $tax = $transferFee * $taxRates[$event]['registration'];

    $fixedTotals = $transferFee + $tax;
    $transferTotal = $fixedTotals;

    $data = array();
    $data[$thisID] = array(
      "activityCostTotal" => $transferFee,
      "taxTotal" => $tax,
      "remit" => $transferFee + $tax,
      "total" => $transferFee + $tax
      );

    return $data;
  }

  public function deactivate_transfer($code)
  {
    $sqlDeactivate = "UPDATE relAccountActivity SET strStatusChange = 'transferred', dttDateModified = NOW() WHERE strTransferCode = :transferCode AND strStatusChange IS NULL LIMIT 1";
    $stmt = $this->db->prepare($sqlDeactivate);
    $transferData = array(
      "transferCode" => $_SESSION['transfer_code']
      );
    if($stmt->execute($transferData))
    {
      $this->verify_active_account($code);
      $this->remove_shirt($code);
      return TRUE;
    }
    else
    {
      sendErrorEmail('Series Demo - Failed query', 'Failed UPDATE relAccountActivity with the following: '.json_encode($transferData).'<br /><br /> Query: '.$sqlDeactivate);
      //TODO Alert - failed to deactivate transfer code related account
    }
  }

  public function remove_shirt()
  {
    $sqlRelShirt = "SELECT relapi.intPersonalInformationID AS personalID FROM relAccountPersonalInformation AS relapi
    WHERE relapi.intAccountID = :accountID LIMIT 1;";

    $stmt = $this->db->prepare($sqlRelShirt);

    $dataToExecute = array(
      "accountID" => $this->transferredAccountID
      );

    if($stmt->execute($dataToExecute))
    {
      $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
      $personalInformationID = $result[0]['personalID'];

      $sqlRemoveShirt = "DELETE FROM relParticipantShirt WHERE intPersonalInformationID = :personalID AND intEventID = :eventID AND intActivityID = :activityID;";
      $stmt2 = $this->db->prepare($sqlRemoveShirt);

      $dataToExecute = array(
        "personalID" => $personalInformationID,
        "activityID" => $this->transferredActivityID,
        "eventID" => $this->transferredEventID
        );
      if($stmt2->execute($dataToExecute))
      {
        //Success
      }
      else
      {
        sendErrorEmail('Series Demo - Failed query', 'Failed DELETE FROM relParticipantShirt with the following: '.json_encode($dataToExecute).'<br /><br /> Query: '.$sqlRemoveShirt);
      }
    }
    else
    {
      sendErrorEmail('Series Demo - Failed query', 'Failed SELECT relAccountPersonalInformation with the following: '.json_encode($dataToExecute).'<br /><br /> Query: '.$sqlRelShirt);
    }
  }

  public function verify_active_account($code)
  {
	//CHANGED BY HYW
    //$sqlFindAccount = "SELECT intAccountID AS accountID, intActivityID AS activityID, intEventID AS eventID FROM relAccountActivity WHERE strTransferCode = :transferCode LIMIT 1;";
	$sqlFindAccount = "SELECT intAccountID AS accountID, intActivityID AS activityID, intEventID AS eventID FROM relAccountActivity WHERE strStatusChange IS NULL AND strTransferCode = :transferCode LIMIT 1;";
    $stmt = $this->db->prepare($sqlFindAccount);
    $transferCodeData = array(
      "transferCode" => $_SESSION['transfer_code']
      );
    if($stmt->execute($transferCodeData))
    {
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
      $accountID = $results[0]['accountID'];
      $eventID = $results[0]['eventID'];
      $activityID = $results[0]['activityID'];
      $this->transferredAccountID = $accountID;
      $this->transferredEventID = $eventID;
      $this->transferredActivityID = $activityID;

      $sqlFindActivities = "SELECT count(*) AS active_activities FROM relAccountActivity WHERE intAccountID = :accountID AND strStatusChange IS NULL;";
      
      $stmt1 = $this->db->prepare($sqlFindActivities);
      $findActivitiesData = array(
        "accountID" => $accountID
        );
      if($stmt1->execute($findActivitiesData))
      {
        $results1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
        if($results1[0]['active_activities'] == 0)
        {
          $sqlGetStatusID = "SELECT intActiveStatusID AS inactiveStatusID FROM lkpActiveStatus WHERE strActiveStatus = 'Not Active';";
          $stmt2 = $this->db->prepare($sqlGetStatusID);
          if($stmt2->execute())
          {
            $results2 = $stmt2->fetchAll(PDO::FETCH_ASSOC);
            $inactiveID = $results2[0]['inactiveStatusID'];

            $sqlDeactivateAccount = "UPDATE entAccount SET intActiveStatusID = :inactiveID, dttDateModified = NOW() WHERE intAccountID = :accountID";
            $stmt3 = $this->db->prepare($sqlDeactivateAccount);

            $dataToExecute = array(
              "accountID" => $accountID,
              "inactiveID" => $inactiveID
              );

            if($stmt3->execute($dataToExecute))
            {
              return TRUE;
            }
            else
            {
              sendErrorEmail('Series Demo - Failed query', 'Failed UPDATE entAccount with the following: '.json_encode($dataToExecute).'<br /><br /> Query: '.$sqlDeactivateAccount);
              return FALSE;
            }
          }
          else
          {
            sendErrorEmail('Series Demo - Failed query', 'Failed SELECT lkpActiveStatus with the following: <br /><br /> Query: '.$sqlGetStatusID);
            return FALSE;
          }
        }
        else
        {
          return TRUE;
        }
      }
      else
      {
        sendErrorEmail('Series Demo - Failed query', 'Failed SELECT entAccount with the following: '.json_encode($findActivitiesData).'<br /><br /> Query: '.$sqlFindActivities);
        return FALSE;
      }
    }
    else
    {
      sendErrorEmail('Series Demo - Failed query', 'Failed SELECT relAccountActivity with the following: '.json_encode($transferCodeData).'<br /><br /> Query: '.$sqlFindAccount);
      return FALSE;
    }
  }  

  public function set_account_transaction_type($type)
  {
    switch($type)
    {
      case "individual":
        $transactionType = "individualRegistration";
      break;

      case "transfer":
        $transactionType = "transfer";
      break;

      case "staffEntry":
        $transactionType = "staffRegistration";
      break;

      case "group":
        $transactionType = "groupRegistration";
      break;

      default:
        $transactionType = "individualRegistration";
      break;
    }

    $sql = "SELECT intTransactionTypeID AS transTypeID FROM lkpTransactionType AS lkpTT
    WHERE strTransactionType = :transactionType LIMIT 1;";

    $stmt = $this->db->prepare($sql);
    $dataToExecute = array(
      "transactionType" => $transactionType
      );
    if($stmt->execute($dataToExecute))
    {
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
      $this->accountTransactionTypeID = $results[0]['transTypeID'];
      return TRUE;
    }
    else
    {
      sendErrorEmail('Series Demo - Failed query', 'Failed SELECT intTransactionTypeID with the following: '.json_encode($dataToExecute).'<br /><br /> Query: '.$sql);
      return FALSE;
    }
  }

  public function set_account_personal_rel_type()
  {
    $sqlAccountPersonalRelType = "SELECT intPersonalInformationTypeID as id, strPersonalInformationType as type FROM lkpPersonalInformationType;";
    $statement = $this->db->prepare($sqlAccountPersonalRelType);
    if($statement->execute())
    {
      $result = $statement->fetchAll(PDO::FETCH_ASSOC);

      $parsed = array();
      foreach($result as $row)
      {
        $parsed[$row['type']] = $row['id'];
      }

      $this->personalInformationType = $parsed;
      return TRUE;      
    }
    else
    {
      $this->personalInformationType["individual"] = 1;
      $this->personalInformationType["groupMember"] = 1;
      $this->personalInformationType["groupBillingMember"] = 1;
      $this->personalInformationType["oneTimeDonation"] = 1;
      sendErrorEmail('Series Demo - Failed query', 'Failed SELECT lkpPersonalInformationType with the following: <br /><br /> Query: '.$sqlAccountPersonalRelType);
      return FALSE;
    }

  }

  public function set_addon_transaction_type($type = 'registration')
  {
    switch($type)
    {
      case 'staff':
        $type = 'staffEntry';
      break;
    }

    $sqlAddonTransactionType = "SELECT intAddonTransactionTypeID as addon_type FROM lkpAddonTransactionType WHERE strAddonTransactionType = \"".$type."\" LIMIT 1";

    $statement = $this->db->prepare($sqlAddonTransactionType);
    if($statement->execute())
    {
      $results = reset($statement->fetchAll(PDO::FETCH_ASSOC));
      $id = $results['addon_type'];
      $this->addonTransactionTypeID = $id;
      return TRUE;
    }
    else
    {
      sendErrorEmail('Series Demo - Failed query', 'Failed SELECT lkpAddonTransactionType with the following: <br /><br /> Query: '.$sqlAddonTransactionType);
      $this->addonTransactionTypeID = 1;
      return FALSE;
    }
  }

  public function set_transaction_status()
  {    
    $sqlTransactionPrep = "SELECT intTransactionPaidStatusID AS status FROM lkpTransactionPaidStatus WHERE strTransactionPaidStatus = 'Paid' LIMIT 1;";

    $statement = $this->db->prepare($sqlTransactionPrep);

    if($statement->execute())
    {
      $rows = $statement->fetch(PDO::FETCH_ASSOC);
      $statusID = $rows['status'];
      $this->transactionStatusID = $statusID;
      return TRUE;
    }
    else
    {
      //TODO: Send alert - database call has failed to fetch a transaction status but has continued processing
      $this->transactionStatusID = 0;
      return FALSE;
    }
  }

  public function set_registration_transaction_type($type = 'individual')
  {    
    $sqlRegistrationTransactionPrep = "SELECT intRegistrationTransactionTypeID AS type FROM lkpRegistrationTransactionType WHERE strRegistrationTransactionType = :type LIMIT 1;";

    $statement = $this->db->prepare($sqlRegistrationTransactionPrep);

    $dataToExecute = array(
      "type" => $type
      );

    if($statement->execute($dataToExecute))
    {
      $rows = $statement->fetch(PDO::FETCH_ASSOC);
      $statusID = $rows['type'];
      $this->registrationTransactionTypeID = $statusID;
      return TRUE;
    }
    else
    {
      //TODO: Send alert - database call has failed to fetch a registration transaction type but has continued processing
      $this->registrationTransactionTypeID = 1;
      return FALSE;
    }
  }

  public function set_donation_transaction_type($type = 'registration')
  {
    $sqlDonationTypePrep = "SELECT intDonationTransactionTypeID as donation_type FROM lkpDonationTransactionType WHERE
      strDonationTransactionType = \"".$type."\";";
    $statement = $this->db->prepare($sqlDonationTypePrep);
    if($statement->execute())
    {
      $rows = $statement->fetch(PDO::FETCH_ASSOC);
      $statusID = $rows['donation_type'];
      $this->donationTransactionTypeID = $statusID;
      return TRUE;
    }
    else
    {
      //TODO: Send alert - database call has failed to fetch a registration transaction type but has continued processing
      $this->donationTransactionTypeID = 1;
      return FALSE;
    }
  }

  public function get_discount_data($id, $prices)
  {
    $sqlDiscountData = "SELECT entdc.dblDiscountAmount AS discount, entdc.strDiscountCode AS code, lkpdct.strDiscountCodeType AS type, lkpa.strActivity AS activity, lkpe.strEvent AS event, reldc.intEventID AS eventID, reldc.intActivityID AS activityID  FROM relDiscountCode AS reldc
    INNER JOIN entDiscountCode AS entdc
    ON entdc.intDiscountCodeID = reldc.intDiscountCodeID
    INNER JOIN lkpDiscountCodeType AS lkpdct
    ON lkpdct.intDiscountCodeTypeID = reldc.intDiscountCodeTypeID
    INNER JOIN lkpActivity AS lkpa
    ON lkpa.intActivityID = reldc.intActivityID
    INNER JOIN lkpEvent AS lkpe
    ON lkpe.intEventID = reldc.intEventID
    WHERE reldc.intDiscountCodeID = :code
    LIMIT 1;
    ";

    $stmt = $this->db->prepare($sqlDiscountData);
    $data = array(
      "code" => $_SESSION['registration'][$id]['discount_code']
      );

    if($stmt->execute($data))
    {
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

      $thisDiscount = $results[0];

      if($thisDiscount['activity'] == "All")
      {
        foreach($_SESSION['registration'][$id]['activities'] as $eventActivity)
        {
          $expl = explode("_", $eventActivity);
          $activityID = end($expl);
          $eventID = reset($expl);

          if($eventID == $thisDiscount['eventID'])
          {
            $amountToBeDiscountedOn = $prices[$eventID][$activityID];
          }
        }
      }
      else
      {
        if(in_array($thisDiscount['eventID'].'_'.$thisDiscount['activityID'], $_SESSION['registration'][$id]['activities']))
        {
          $amountToBeDiscountedOn = $prices[$thisDiscount['eventID']][$thisDiscount['activityID']];
        }
      }
    }

    $discountAmount = 0;
    switch($thisDiscount['type'])
    {
      case "Percent":
        $discountAmount = (($thisDiscount['discount'] / 100) * $amountToBeDiscountedOn);
      break;

      case "Dollar":
        $discountAmount = $amountToBeDiscountedOn - $thisDiscount['discount'];
      break;
    }

    return array(
      "discountID" => $_SESSION['registration'][$id]['discount_code'],
      "discount" => $discountAmount,
      "codeUsed" => $thisDiscount['code'],
      "event" => $thisDiscount['event'],
      "eventID" => $thisDiscount['eventID']
      );
  }

  public function validate_transfer_code($code)
  {
	//MODIFIED BY HYW
    //$sql = "SELECT count(*) AS valid, intEventID AS eventID, intActivityID AS activityID FROM relAccountActivity AS relAA WHERE strTransferCode = :code LIMIT 1";
	$sql = "SELECT count(*) AS valid, intEventID AS eventID, intActivityID AS activityID FROM relAccountActivity AS relAA WHERE strStatusChange IS NULL AND strTransferCode = :code LIMIT 1";
	
    $stmt = $this->db->prepare($sql);

    if($stmt->execute(array("code" => $code)))
    {
      $results = reset($stmt->fetchAll(PDO::FETCH_ASSOC));

      if($results['valid'] == 1)
      {
        return $results;
      }
      else
      {
        return FALSE;
      }
    }
    else
    {
      //TODO: Alert - failed to query relAccountActivity to retrieve a transfer code
    }
  }

  public function get_addon_prices()
  {
    $sql = "SELECT lkpAd.strAddon AS addon, lkpAd.intAddonID AS addon_id, lkpE.strEvent AS event, lkpE.intEventID AS event_id, relA.dblPrice AS price FROM
    reladdon AS relA
    INNER JOIN lkpEvent AS lkpE
    ON lkpE.intEventID = relA.intEventID
    INNER JOIN lkpAddon AS lkpAd
    ON relA.intAddonID = lkpAd.intAddonID
    WHERE dttExpiration > NOW();";

    $prices = array();
    try
    {
      $statement = $this->db->prepare($sql);
      $statement->execute();
      $rows = $statement->fetchAll(PDO::FETCH_ASSOC);

      foreach($rows as $row)
      {
        $array = array(
          "price" => $row['price'],
          "addon" => $row['addon']
          );
        $prices[$row['event_id']][$row['addon_id']] = $array;
      }
    }
    catch(Exception $e)
    {
      //TODO: Proper catching
    }

    return $prices;
  }
  // public function get_addon_prices($addons)
  // {
  //   $allAddonsToFind = '';
  //   $c = 0;
  //   $sqlAddons = "SELECT relA.dblPrice AS price, relA.intEventID AS eventID, relA.intAddonID AS addonID FROM relAddon AS relA WHERE ";
  //   foreach($addons as $eID => $addonDetails)
  //   {
  //     foreach($addonDetails as $aID => $addonList)
  //     {
  //       if($allAddonsToFind == '')
  //       {
  //         $allAddonsToFind .= "(:eventID".$c." = relA.intEventID AND :addonID".$c." = relA.intAddonID)";
  //       }
  //       else
  //       {
  //         $allAddonsToFind .= "OR (:eventID".$c." = relA.intEventID AND :addonID".$c." = relA.intAddonID)";
  //       }
  //       $addonsSelData["eventID".$c] = $eID;
  //       $addonsSelData["addonID".$c] = $aID;
  //       $c++;
  //     }
  //   }
  //   $sqlAppended = $sqlAddons . $allAddonsToFind;
  //   $this->statements['addon-select'] = $this->db->prepare($sqlAppended);
  //   if($this->_execute("addon-select", $addonsSelData))
  //   {
  //     $results = $this->statements['addon-select']->fetchAll(PDO::FETCH_ASSOC);
  //     $parsedData = array();
  //     foreach($results as $row)
  //     {
  //       $parsedData[$row['eventID']][$row['addonID']] = $row['price'];
  //     }

  //     return $parsedData;
  //   }
  //   else
  //   {
  //     //TODO: Send alert - database call has failed 
  //     return FALSE;
  //   }
  // }

  public function get_group_itab($eventID)
  {
    $sql = "SELECT a.intAddonID AS addonID, b.strAddon AS addon FROM relAddon AS a 
    INNER JOIN lkpAddon AS b
    ON a.intAddonID = b.intAddonID
    WHERE a.dttExpiration > NOW()
    AND a.intEventID = :eventID;";

    $stmt = $this->db->prepare($sql);
    $data = array(
      "eventID" => $eventID
      );

    $itabID = false;

    if($stmt->execute($data))
    {
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if($results)
      {
        foreach($results as $row)
        {
          $itabID = $row['addonID'];
        }
      }
    }
    else
    {
      //TODO: Alert - failed to select valid addon
    }

    return $itabID;
  }

  public function get_group_fields()
  {
    $this->load->library("registration");
    $tshirts = $this->registration->get_tshirts();

    $shirtsArray = array("" => "");
	
	

    foreach($_SESSION['registration'][$_SESSION['registration']['active_user']]['activities'] as $eventActivity)
    {
      $expl = explode("_", $eventActivity);
      $eventID = reset($expl);
      $activityID = end($expl);
    }

	///Add event finish time limits here - first key is location id second key is activity id
		$finishTimeNotes = array(
		"2"=>array(
				"2"=>"Time limit to complete the course is 3 hours",
				"3"=>"Time limit to complete the course is 1 hour 15 minutes"
			),
		"3"=>array(
				"5"=>"Time limit to complete the course is 1 hour 15 minutes",
				"6"=>"Time limit to complete the course is 1 hour 15 minutes"
			),
		"4"=>array(
				"8"=>"Time limit to complete the course is 1 hour 40 minutes"
			)
		);

    $itabAddon = $this->get_group_itab($eventID);

    foreach($tshirts[$eventID] as $shirtID => $description)
    {
      $shirtsArray[$shirtID] = $description;
    }

    $blankArray = array(
      "" => ""
      );
    $timeHours = array_merge($blankArray, array_keys(array_fill(0, 24, "")));
    $timeMinutes = array_merge($blankArray, array_keys(array_fill(0, 60, "")));
	
	

    $fields = array(
      "first_name" => array(
          "type" => "text",
          "name" => "first_name",
          "id" => "inpFirstName",
          "label" => "First Name",
          "required" => TRUE,
        ),
      "last_name" => array(
          "type" => "text",
          "name" => "last_name",
          "id" => "inpLastName",
          "label" => "Last Name",
          "required" => TRUE
        ),
        "email" => array(
          "type" => "email",
          "name" => "email",
          "id" => "inpEmail",
          "label" => "Email",
          "class" => "verify-email",
          "add-member" => TRUE,
          "required" => TRUE
          ),
        "email verify" => array(
          "type" => "email",
          "name" => "email_verify",
          "id" => "inpEmailVerify",
          "add-member" => TRUE,
          "class" => "verify-email-confirm",
          "label" => "Email Verification",
          "required" => TRUE
          ),
      "date of birth" => array(
          "type" => "text",
          "name" => "date_of_birth",
          "id" => "inpDob",
          "class" => "date-picker",
          "label" => "Date of Birth",
          "readonly" => TRUE,
          "required" => TRUE
        ),
        "gender" => array(
          "type" => "radio",
          "name" => "gender",
          "id" => "radGender",
          "label" => "Gender",
          "data" => array(
            "M" => "Male",
            "F" => "Female"
            ),
          "required" => TRUE
          ),
        "Emergency Contact Name" => array(
          "type" => "text",
          "label" => "Emergency Contact Name",
          "name" => "emergency_name",
          "id" => "inpEmergencyName",
          "required" => TRUE
          ),
        "Emergency Contact Number" => array(
          "type" => "text",
          "label" => "Emergency Contact Number",
          "name" => "emergency_number",
          "id" => "EmergencyNumber",
          "required" => TRUE
          ),
        "Medical Conditions" => array(
          "type" => "text",
          "label" => "List any medical conditions",
          "name" => "medical_conditions",
          "id" => "inpMedicalConditions"
          ),
        "Tshirt" => array(
          "type" => "select",
          "name" => "extrainfo_tshirt",
          "id" => "selTshirt",
          "label" => "Technical T-shirt Size",
          "class" => "validate-shirt",
          "data" => $shirtsArray,
          "required" => TRUE
          ),
        "Expected finish time hours" => array(
          "type" => "select",
          "name" => "extrainfo_finish_time_hour",
          "id" => "inpFinishTime",
          "label" => "Expected finish time hours",
          "data" => $timeHours,
          "required" => TRUE
          ),
        "Expected finish time minutes" => array(
          "type" => "select",
          "name" => "extrainfo_finish_time_minute",
          "id" => "inpFinishTime",
          "label" => "Expected finish time minutes",
		  "helper-text" => translate($finishTimeNotes[$eventID][$activityID]),
          "data" => $timeMinutes,
          "required" => TRUE
          )
      );
  
    if($itabAddon)
    {

      $fields['itab'] = array(
        "type" => "select",
        "name" => "gaddon_itab",
        "id" => "selGroupiTab",
        "label" => "iTab ($10)",
        "data" => array(
          "" => "",
          $itabAddon => "Yes"
          )
        );
    }

    return $fields;
  }

  public function get_form_group_activities()
  {
    $this->load->library("registration");
    $validActivities = $this->registration->get_valid_activities();
    $caps = $this->registration->get_activity_caps();

    foreach($validActivities as $event => $activities)
    {
      $eventName = "";
      unset($rebuiltActivities);
      $soldOut = array();
      foreach($activities as $activity => $details)
      {
        //Set the input name once
        if($eventName == "") $eventName = "group_event";
        if(isset($caps[$details['event_id']][$details['activity_id']]))
        {
          if($caps[$details['event_id']][$details['activity_id']] < $details['cap'])
          {
            $rebuiltActivities[$details['event_id'].'_'.$details['activity_id']] = $activity;
          }
          else
          {
            $rebuiltActivities[$details['event_id'].'_'.$details['activity_id']] = $activity;
            $soldOut[] = $details['event_id'].'_'.$details['activity_id'];
          }
        }
        else
        {
          $rebuiltActivities[$details['event_id'].'_'.$details['activity_id']] = $activity;
        }
      }

      $sections["Select a race"][] = array(
        "type" => "radio",
        "name" =>$eventName,
        "data" => $rebuiltActivities,
        "soldout" => $soldOut,
        "label" => $event
        );
    }

    return $sections;
  }

	public function get_form_activities()
	{
    //Load the registration library and get activities
    $this->load->library("registration");
    $validActivities = $this->registration->get_valid_activities();
    $caps = $this->registration->get_activity_caps();
    $priceChart = $this->registration->get_all_priceflips();

    $notes['Banque Scotia 21K'] =  "<ul>
    <li>".translate("Online registration for this event will close at Midnight on Sunday, April 19th, 2015 unless limit is reached")."</li>
    <li><a href=\"http://www.canadarunningseries.com/monthalf_en/montREG.htm#fees\" target=\"_blank\">".translate("Click here for Event Registration Fees and Distance Caps")."</a></li>
    <li><a href=\"http://www.canadarunningseries.com/monthalf_en/montREG.htm#stroller\" target=\"_blank\">".translate("Click here for more single stroller information")."</a></li>
    </ul>
    ";
    $notes["Harry's Spring Run Off"] =  "<ul>
    <li>".translate("Online registration for this event will close at Midnight on Sunday, March 29th, 2015 unless limit is reached")."</li>
    <li><a href=\"http://www.canadarunningseries.com/springrunoff/csroREG.htm#fees\" target=\"_blank\">".translate("Click here for Event Registration Fees and Distance Caps")."</a></li>
    <li><a href=\"http://www.canadarunningseries.com/springrunoff/csroREG.htm#stroller\" target=\"_blank\">".translate("Click here for more single stroller information")."</a></li>
    </ul>
    ";
    $notes["Toronto Yonge Street 10K"] =  "<ul>
    <li>".translate("Online registration for this event will close at Midnight on Sunday, April 13th, 2015 unless limit is reached")."</li>
    <li><a href=\"http://www.canadarunningseries.com/toronto10k/tys10kREG.htm#fees\" target=\"_blank\">".translate("Click here for Event Registration Fees and Distance Caps")."</a></li>
    <li><a href=\"http://www.canadarunningseries.com/toronto10k/tys10kREG.htm#stroller\" target=\"_blank\">".translate("Click here for more single stroller information")."</a></li>
    <li>Aug 12 - Oct 20, 2014 - Includes Souvenir Tech Shirt, Goodie Bag, Virtual Goodie Bag pluschoice of BONUS COOL-MAX® Runner's Cap OR Toronto Yonge Street 10k Neon Sunglasses</li>
    <li>Oct 21, 2014 Jan 27, 2015 - Includes Souvenir Tech Shirt, Goodie Bag, Virtual Goodie Bag pluschoice of BONUS COOL-MAX® Runner's Cap OR Toronto Yonge Street 10k Neon Sunglasses</li>
    <li>Jan 28 - Apr 13 2015 - Includes Souvenir Tech Shirt, Goodie Bag, Virtual Goodie Bag plusBONUS Toronto Yonge Street 10k Neon Sunglasses</li>
    <li>
      Walk-up Apr 17–18 (unless limit is reached)
    </li>
    </ul>
    ";

    foreach($validActivities as $event => $activities)
    {
        $eventName = "";
        unset($rebuiltActivities);
        $soldOut = array();
        $tableHead = "<table class=\"table table-condensed table-striped pricing-table\"><thead><tr>";
        $first = TRUE;
        $tableBody = '';
        foreach($priceChart[$event] as $activity => $arr)
        {
          if($first)
            $tableHead .= "<tr><th></th>";
          $tableBody .= "<tr><td>".$activity."</td>";
          foreach($arr as $date => $price)
          {
            if($first === TRUE)
            {
              $tableHead .= "<th class=\"text-center\">".$date."</th>";
            }
            $tableBody .= "<td class=\"text-center\">".$price."</td>";
          }
          if($first)
          {
              $tableHead .= "</tr></thead>";
              $first = FALSE;
          }
          $tableBody .= "</tr>";
        }
        $tableBody .= "<tr><td colspan='100'>".$notes[$event]."</td></tr>";
        $tableBody .= "</tbody></table>";
        $finalTable = $tableHead.$tableBody;
        $thisEventDate = '';
        foreach($activities as $activity => $details)
        {
          //Set the input name once
          if($eventName == "") $eventName = "race_event_".$details['event_id'];
          if($thisEventDate == "") $thisEventDate = $details['event_date'];

          $rebuiltActivities[""] = "None";

          if(isset($caps[$details['event_id']][$details['activity_id']]))
          {
            if($caps[$details['event_id']][$details['activity_id']] < $details['cap'])
            {
              $rebuiltActivities[$details['activity_id']] = $activity;
            }
            else
            {
              $rebuiltActivities[$details['activity_id']] = $activity;
              $soldOut[] = $details['activity_id'];
            }
          }
          else
          {
            $rebuiltActivities[$details['activity_id']] = $activity;
          }
        }

        $sections[translate($event).' - '.date("M d, Y", strtotime($thisEventDate))][$activity] = array(
          "type" => "radio",
          "name" =>$eventName,
          "info" => "<div class=\"col-sm-offset-3\"><button type=\"button\" data-state=\"\" class=\"btn btn-sm btn-info view-pricing\"><i class=\"glyphicon glyphicon-plus\"></i> ".translate('View Pricing and Info')."</button></div>".$finalTable,
          "data" => $rebuiltActivities,
          "soldout" => $soldOut
          );
    }
    
    return $sections;
	}

  public function get_form_sections()
  {
      $this->load->library("registration");
      //Retrieve estimated finish times
      // $finishTimes = $this->get_finish_times();

      $tshirts = $this->registration->get_tshirts();

      $blankArray = array(
        "" => ""
        );
      $timeHours = array_merge($blankArray, array_keys(array_fill(0, 24, "")));
      $timeMinutes = array_merge($blankArray, array_keys(array_fill(0, 60, "")));

      $provinceState = getProvinceState();
      $countries = getCountry();

      $sections = array(
        translate("Discounts") => array(
          "Discount Code" => array(
            "type" => "text",
            "name" => "discount_code",
            "id" => "inpDiscountCode",
            "label" => "Discount Code",
            "parent-class" => "discount-row",
            "input-group-right" => "<button id=\"validate-discount\" type=\"button\" class=\"btn btn-success\">Validate</button>"
            )
          ),
        "Category" => array(
          "categories" => array(
              "type" => "checkbox",
              "name" => "categories",
              "id" => "chkFirstName",
              "label" => "Categories",
              "data" => array(
                "Guide" => "I will be participating with a guide",
                "Nordic Poles" => "Nordic Poles",
                "Recreational Wheelchair" => "Recreational Wheelchair",
                )
            ),
          ),
          translate("Personal Information") => array(
          "first_name" => array(
            "type" => "text",
            "name" => "first_name",
            "id" => "inpFirstName",
            "label" => "First Name",
            "required" => TRUE
            ),
          "last_name" => array(
            "type" => "text",
            "name" => "last_name",
            "id" => "inpLastName",
            "label" => "Last Name",
            "required" => TRUE
            ),
          "address" => array(
            "type" => "text",
            "name" => "address",
            "id" => "inpAddress",
            "label" => "Address",
            "required" => TRUE
            ),
          "city" => array(
            "type" => "text",
            "name" => "city",
            "id" => "inpCity",
            "label" => "City",
            "required" => TRUE
            ),
          "province" => array(
            "type" => "select",
            "name" => "province",
            "id" => "inpProvince",
            "label" => "Province/State",
            "data" => $provinceState,
            "required" => TRUE
            ),
          "country" => array(
            "type" => "select",
            "name" => "country",
            "id" => "selCountry",
            "label" => "Country",
            "data" => $countries,
            "required" => TRUE
            ),
          "postal" => array(
            "type" => "text",
            "name" => "postal_code",
            "id" => "inpPostalCode",
            "label" => "Postal/Zip Code",
            "required" => TRUE
            ),
          "home phone" => array(
            "type" => "text",
            "name" => "home_phone",
            "id" => "inpHomePhone",
            "label" => "Home Phone",
            "required" => TRUE
            ),
          "mobile phone" => array(
            "type" => "text",
            "name" => "mobile_phone",
            "id" => "inpMobilePhone",
            "label" => "Mobile Phone"
            ),
          "email" => array(
            "type" => "email",
            "name" => "email",
            "id" => "inpEmail",
            "label" => "Email",
            "required" => TRUE
            ),
          "email verify" => array(
            "type" => "email",
            "name" => "email_verify",
            "id" => "inpEmailVerify",
            "label" => "Email Verification",
            "required" => TRUE
            ),
          "username" => array(
            "type" => "text",
            "name" => "username",
            "id" => "inpUsername",
            "label" => "Username",
            "class" => "validate-username",
            "parent-class" => "has-feedback",
            "helper-text" => translate("Note").": ".translate("Your username and password are needed to login to the")." <a href=\"".base_url("participant")."\" target=\"_blank\"><b>".translate("Participant Dashboard")."</b></a>. ".translate("Username and password must include only numbers and letters, no special characters. Passwords are case sensitive.")."</br>",
            "required" => TRUE
            ),
          "password" => array(
            "type" => "password",
            "name" => "password",
            "id" => "inpPassword",
            "label" => "Password",
            "required" => TRUE
            ),
          "password verify" => array(
            "type" => "password",
            "name" => "password_verify",
            "id" => "inpPasswordVerify",
            "label" => "Password verification",
            "required" => TRUE
            ),
          "date of birth" => array(
            "type" => "date",
            "name" => "date_of_birth",
            "id" => "datDateOfBirth",
            "label" => "Date of Birth",
            "class" => "date-picker",
            "readonly" => TRUE,
            "required" => TRUE
            ),
          "gender" => array(
            "type" => "radio",
            "name" => "gender",
            "id" => "radGender",
            "label" => "Gender",
            "data" => array(
              "M" => "Male",
              "F" => "Female"
              ),
            "required" => TRUE
            ),
          ),
          translate("Additional Information") => array(
            "Series Newsletter" => array(
              "type" => "select",
              "name" => "newsletter",
              "id" => "selNewsletter",
              "label" => "I would like to receive newsletters",
              "data" => array(
                "" => "",
                "Yes" => "Yes",
                "No" => "No"
                )
              )
            ),
          translate("Survey Information") => array(
            "nationality" => array(
              "type" => "select",
              "name" => "nationality",
              "label" => "Nationality",
              "id" => "selNationality",              
              "data" => array(
                "" => "",
                "Canadian" => "Canadian",
                "Landed immigrant" => "Landed immigrant",
                "Refugee" => "Refugee",
                "Other" => "Other"
                )
              ),
            "Nationality Duration" => array(
              "type" => "select",
              "name" => "nationality_duration",
              "id" => "selNationalityDuration",
              "label" => "Please specify how many months you have been in Canada",
              "data" => array(
                "" => "",
                "Less than 1 month" => ">1 month",
                "1" => "1",
                "2" => "2",
                "3" => "3",
                "4" => "4",
                "5" => "5",
                "6" => "6",
                "7" => "7",
                "8" => "8",
                "9" => "9",
                "10" => "10",
                "11" => "11",
                "12+" => "12+",
                )
              ),
            "Type of Industry" => array(
              "type" => "select",
              "name" => "type_of_industry",
              "id" => "selTypeOfIndustry",
              "label" => "In what type of industry do you work?",
              "data" => array(
                  "" => "",
                  "Administration" => "Administration",
                  "Banking/Finance" => "Banking/Finance",
                  "Communications/Marketing" => "Communications/Marketing",
                  "Education (k-12)" => "Education (k-12)",
                  "Education (post-secondary)" => "Education (post-secondary)",
                  "Event Planning" => "Event Planning",
                  "Government" => "Government",
                  "Health Care" => "Health Care",
                  "Hospitality" => "Hospitality",
                  "Insurance" => "Insurance",
                  "Manufacturing" => "Manufacturing",
                  "Retail" => "Retail",
                  "Transportation" => "Transportation",
                  "Other" => "Other"
                )
              ),
            "Type of Industry (Other)" => array(
              "type" => "text",
              "name" => "type_of_industry_other",
              "id" => "inpTypeOfIndustry",
              "label" => "Please specify"
              ),
            "Yearly salary" => array(
              "type" => "select",
              "name" => "yearly_salary",
              "label" => "What is your yearly salary?",
              "id" => "selYearlySalary",
              "data" => array(
                "" => "",
                "Less than 30,000" => ">30,000",
                "30,001 - 50,000" => "30,001 - 50,000",
                "50,001 - 70,000" => "50,001 - 70,000",
                "70,001 - 90,000" => "70,001 - 90,000",
                "90,001 - 110,000" => "90,001 - 110,000",
                "110,001+" => "110,001+"
                )
              )
          ),
        translate("Emergency Contact Information") => array(
          "Emergency Contact Name" => array(
            "type" => "text",
            "label" => "Emergency Contact Name",
            "name" => "emergency_name",
            "id" => "inpEmergencyName",
            "required" => TRUE
            ),
          "Emergency Contact Number" => array(
            "type" => "text",
            "label" => "Emergency Contact Number",
            "name" => "emergency_number",
            "id" => "EmergencyNumber",
            "required" => TRUE
            ),
          "Medical Conditions" => array(
            "type" => "text",
            "label" => "List any medical conditions",
            "name" => "medical_conditions",
            "id" => "inpMedicalConditions",
            "required" => TRUE
            )
          )
        );

      $sessionActivities = $_SESSION['registration'][$_SESSION['registration']['active_user']]['activities'];
      foreach($sessionActivities as $eventActivity)
      { 
        $eventIDs[] = reset(explode("_", $eventActivity));
		$activityIDs[reset(explode("_", $eventActivity))] = end(explode("_", $eventActivity));
      }

      foreach($eventIDs as $id)
      {
        //Parse shirts for each event
        $shirtsArray = array(
          "" => ""
          );

        if(isset($tshirts[$id]))
        {
          foreach($tshirts[$id] as $shirtID => $description)
          {
            $shirtsArray[$shirtID] = $description;
          }
        }
		
		///Add event finish time limits here - first key is location id second key is activity id
		$finishTimeNotes = array(
		"2"=>array(
				"2"=>"Time limit to complete the course is 3 hours",
				"3"=>"Time limit to complete the course is 1 hour 15 minutes"
			),
		"3"=>array(
				"5"=>"Time limit to complete the course is 1 hour 15 minutes",
				"6"=>"Time limit to complete the course is 1 hour 15 minutes"
			),
		"4"=>array(
				"8"=>"Time limit to complete the course is 1 hour 40 minutes"
			)
		);

        $sections[translate($_SESSION['general'][$id]['display'])." - Information"] = array(
          "Learn about this event" => array(
            "type" => "select",
            "name" => "extrainfo_learn_about_".$id,
            "id" => "selLearnAbout".$id,
            "label" => "How did you learn about this event?",
            "class" => "learn-about-event",
            "data" => array(
              "" => "",
              "Affiliated Charity" => "Affiliated Charity",
              "Expo at another race" => "Expo at another race",
              "Friends & Family" => "Friends & Family",
              "Insert in Race Kit" => "Insert in Race Kit",
              "Magazine" => "Magazine",
              "Metro Newspaper" => "Metro Newspaper",
              "Running Store" => "Running Store",
              "Social Media" => "Social Media",
              "Website" => "Website",
              "Other" => "Other (if other, please specify)"
              ),
            "required" => TRUE
            ),
          "Learn about other" => array(
            "type" => "text",
            "name" => "extrainfo_learn_about_other_".$id,
            "id" => "inpLearnAboutOther".$id,
            "class" => "learn-about-other",
            "label" => "Please specify"
          ),
          "First time participanting" => array(
            "type" => "select",
            "name" => "extrainfo_first_time_".$id,
            "id" => "selFirstTime".$id,
            "label" => "Is this your first time participating in this event?",
            "class" => "first-time-participating",
            "data" => array(
              "" => "",
              "Yes" => "Yes",
              "No" => "No"
              ),
            "required" => TRUE
            ),
          "How many times participated" => array(
            "type" => "select",
            "name" => "extrainfo_times_participated_".$id,
            "id" => "selTimesParticipated".$id,
            "label" => "How many times have you participated before?",
            "class" => "times-participated",
            "parent-class" => "times-participated-section",
            "data" => array(
              "" => "",
              1 => 1,
              2 => 2,
              3 => 3,
              4 => 4,
              "5+" => "5+"
              )
            ),
          "Why are you participating" => array(
            "type" => "text",
            "name" => "extrainfo_why_participating_".$id,
            "id" => "inpWhyParticipating".$id,
            "label" => "What is your inspiration to run/walk? Why are you participating in this event?",
            ),
          "Expected finish time hours" => array(
            "type" => "select",
            "name" => "extrainfo_finish_time_hour_".$id,
            "id" => "inpFinishTime".$id,
            "label" => "Expected finish time hours",
            "required" => TRUE,
            "data" => $timeHours
            ),
          "Expected finish time minutes" => array(
            "type" => "select",
            "name" => "extrainfo_finish_time_minute_".$id,
            "id" => "inpFinishTime".$id,
            "label" => "Expected finish time minutes",
			"helper-text" => translate($finishTimeNotes[$id][$activityIDs[$id]]),
            "required" => TRUE,
            "data" => $timeMinutes
            ),
          "Tshirt" => array(
            "type" => "select",
            "name" => "extrainfo_tshirt_".$id,
            "id" => "selTshirt".$id,
            "label" => "Technical T-shirt Size",
            "data" => $shirtsArray,
            "required" => TRUE
            )
        );
      }
      $sections[translate("Release Waiver And Indemnity")] = array(
        "waiver" => array(
          "type" => "textarea",
          "id" => "txtWaiver",
          "readonly" => TRUE,
          "class" => "txt-waiver",
          "label" => "Release Waiver And Indemnity",
          "data" => translate("Waiver Text")
          ),
        "no refunds" => array(
          "type" => "checkbox",
          "id" => "noRefundAgreement",
          "required" => TRUE,
          "data" => array(
            "Yes" => "I understand that my entry is non-refundable and non-transferrable to another Series Demo event or to the following year's event."
            ),
          "isArray" => FALSE
          ),
        "details permission" => array(
          "type" => "checkbox",
          "id" => "permissionAgreement",
          "required" => TRUE,
          "data" => array(
            "Yes" => "The event has permission to send me details on event-specific items (race-day details, race photos, race results, official race hotels, etc.)"
            ),
          "isArray" => FALSE
          ),
        "charityPermission" => array(
          "type" => "checkbox",
          "id" => "charityAgreement",
          "required" => TRUE,
          "data" => array(
            "Yes" => "I understand that if I select to donate to or fundraise for a participating charity, the charity may contact me with more information on fundraising and race participation."
            ),
          "isArray" => FALSE
          ),
        "transaction understanding" => array(
          "type" => "checkbox",
          "id" => "transactionUnderstanding",
          "required" => TRUE,
          "data" => array(
            "Yes" => "I understand that the company listed on my credit card statement for this event/transaction will be www.eventsonline.ca"
            ),
          "isArray" => FALSE
          ),
        "waiver agreement" => array(
          "type" => "checkbox",
          "id" => "waiverAgreement",
          "required" => TRUE,
          "data" => array(
            "Yes" => "I ACKNOWLEDGE HAVING READ, UNDERSTOOD, AND AGREE TO THE ABOVE WAIVER, RELEASE and INDEMNITY"
            ),
          "isArray" => FALSE
          )
          );

      return $sections;
  }

  public function get_charity_names()
  {
    $sql = "SELECT intCharityID AS charityID, strCharity AS charity FROM lkpCharity;";
    $stmt = $this->db->prepare($sql);
    $parsedArray = array();

    if($stmt->execute())
    {
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if($results)
      {
        foreach($results as $row)
        {
          $parsedArray[$row['charityID']] = $row['charity'];
        }
      }
    }
    else
    {

    }

    return $parsedArray;
  }

  public function get_team_names()
  {
    $sql = "SELECT intTeamID AS teamID, strTeamName AS team FROM entTeam;";
    $stmt = $this->db->prepare($sql);
    $parsedArray = array();

    if($stmt->execute())
    {
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if($results)
      {
        foreach($results as $row)
        {
          $parsedArray[$row['teamID']] = $row['team'];
        }
      }
    }
    else
    {
      //TODO: Alert - failed to select teams
    }

    $tempSession = $_SESSION['registration'];
    unset($tempSession['active_user']);
    unset($tempSession['last_modified']);

    foreach($tempSession as $id => $details)
    {
      if(isset($details['teams']))
      {
        foreach($details['teams'] as $eID => $teamDetails)
        {
          if(isset($teamDetails['team_action']))
          {
            if($teamDetails['team_action'] == 'create')
            {
              $parsedArray['TEMPTEAM_'.$id] = $teamDetails['team_name_create'];
            }
          }
        }        
      }
    }

    return $parsedArray;
  }


  public function set_general_session()
  {
      if(!isset($_SESSION['general']))
      {
          $this->load->library("registration", "registration");
          $eventIDs = $this->registration->get_event_ids();
      }
  }

  public function initialize_session()
  {
      $_SESSION['registration']['last_modified'] = strtotime("now");
      if(isset($_SESSION['registration']['was_expired']))
      {
          unset($_SESSION['registration']['was_expired']);
          return TRUE;
      }
      else
      {
          return FALSE;
      }
  }

  public function reinitialize_expired_session()
  {
      $_SESSION['registration']['was_expired'] = TRUE;
      $_SESSION['registration']['last_modified'] = strtotime("now");
  }

  public function get_shirt_names()
  {
    $sql = "SELECT strShirtDescription AS shirt, intShirtID AS shirtID FROM lkpShirt;";
    $stmt = $this->db->prepare($sql);
    $shirtNames = array();

    if($stmt->execute())
    {
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
      foreach($results as $row)
      {
        $shirtNames[$row['shirtID']] = $row['shirt'];
      }
    }

    return $shirtNames;
  }

  public function get_addon_names()
  {
    $sql = "SELECT strAddon AS addon, intAddonID AS addonID FROM lkpAddon;";
    $stmt = $this->db->prepare($sql);
    if($stmt->execute())
    {
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
      $addonNames = array();
      foreach($results as $row)
      {
        $addonNames[$row['addonID']] = $row['addon'];
      }
    }

    $shirts = $this->get_shirt_names();
    $addonNames['shirtSizes'] = $shirts;

    return $addonNames;
  }

  public function get_group_setup()
  {
    $activities = $this->get_form_group_activities();

    $numberOfMembers = array_merge(array("" => ""), range(0, 50));
    unset($numberOfMembers[0]);
    // unset($numberOfMembers[1]);
    // unset($numberOfMembers[2]);
    // unset($numberOfMembers[3]);
    // unset($numberOfMembers[4]);

    $provinceState = getProvinceState();
    $countries = getCountry();

    $sections = array();
    $sections["Company/Group Information"] = array(
      "Discount Code" => array(
        "type" => "text",
        "name" => "discount_code",
        "id" => "inpDiscountCode",
        "label" => "Discount Code",
        "parent-class" => "discount-row",
        "input-group-right" => "<button id=\"validate-discount\" type=\"button\" class=\"btn btn-success\">Validate</button>"
        ),
      "Company Group Name" => array(
        "type" => "text",
        "name" => "group_name",
        "id" => "inpGroupName",
        "label" => "Company or Group Name"
        ),
      "Team name" => array(
        "type" => "text",
        "name" => "team_name_create",
        "id" => "inpCorporateTeam",
        "class" => "team-create-field",
        "label" => "Team Name",
        "required" => TRUE
        ),
      "Team pass" => array(
        "type" => "password",
        "name" => "team_password_create",
        "id" => "inpCorporatePass",
        "class" => "team-create-field",
        "label" => "Team Password",
        "required" => TRUE
        )
      );
    $sections["Billing Member"] = array(
      "number of members" => array(
        "type" => "select",
        "name" => "members",
        "id" => "selMembers",
        "label" => "Number of members registering",
        "data" => $numberOfMembers,
        "required" => TRUE
        ),
      "first_name" => array(
        "type" => "text",
        "name" => "first_name",
        "id" => "inpFirstName",
        "label" => "First Name",
        "required" => TRUE
        ),
      "last_name" => array(
        "type" => "text",
        "name" => "last_name",
        "id" => "inpLastName",
        "label" => "Last Name",
        "required" => TRUE
        ),
      "address" => array(
        "type" => "text",
        "name" => "address",
        "id" => "inpAddress",
        "label" => "Address",
        "required" => TRUE
        ),
      "city" => array(
        "type" => "text",
        "name" => "city",
        "id" => "inpCity",
        "label" => "City",
        "required" => TRUE
        ),
      "province" => array(
        "type" => "select",
        "name" => "province",
        "id" => "inpProvince",
        "label" => "Province/State",
        "required" => TRUE,
        "data" => $provinceState
        ),
      "country" => array(
        "type" => "select",
        "name" => "country",
        "id" => "selCountry",
        "label" => "Country",
        "required" => TRUE,
        "data" => $countries
        ),
      "postal" => array(
        "type" => "text",
        "name" => "postal_code",
        "id" => "inpPostalCode",
        "label" => "Postal/Zip Code",
        "required" => TRUE
        ),
      "home phone" => array(
        "type" => "text",
        "name" => "home_phone",
        "id" => "inpHomePhone",
        "label" => "Home Phone",
        "required" => TRUE
        ),
      "mobile phone" => array(
        "type" => "text",
        "name" => "mobile_phone",
        "id" => "inpMobilePhone",
        "label" => "Mobile Phone"
        ),
      "email" => array(
        "type" => "email",
        "name" => "email",
        "id" => "inpEmail",
        "label" => "Email",
        "required" => TRUE
        ),
      "email verify" => array(
        "type" => "email",
        "name" => "email_verify",
        "id" => "inpEmailVerify",
        "label" => "Email Verification",
        "required" => TRUE
        ),
      "waiver" => array(
        "type" => "textarea",
        "id" => "txtWaiver",
        "readonly" => TRUE,
        "class" => "txt-waiver",
        "label" => "Release Waiver And Indemnity",
        "data" => translate("Waiver Text")
        ),
        "no refunds" => array(
          "type" => "checkbox",
          "id" => "noRefundAgreement",
          "required" => TRUE,
          "data" => array(
            "Yes" => "I understand that my entry is non-refundable and non-transferrable to another Series Demo event or to the following year's event."
            ),
          "isArray" => FALSE
          ),
        "details permission" => array(
          "type" => "checkbox",
          "id" => "permissionAgreement",
          "required" => TRUE,
          "data" => array(
            "Yes" => "The event has permission to send me details on event-specific items (race-day details, race photos, race results, official race hotels, etc.)"
            ),
          "isArray" => FALSE
          ),
        "charityPermission" => array(
          "type" => "checkbox",
          "id" => "charityAgreement",
          "required" => TRUE,
          "data" => array(
            "Yes" => "I understand that if I select to donate or fundraise for the charity challenge the charity may contact me with more information on fundraising and race participation"
            ),
          "isArray" => FALSE
          ),
        "transaction understanding" => array(
          "type" => "checkbox",
          "id" => "transactionUnderstanding",
          "required" => TRUE,
          "data" => array(
            "Yes" => "I understand that the company listed on my credit card statement for this event/transaction will be www.eventsonline.ca"
            ),
          "isArray" => FALSE
          ),
        "waiver agreement" => array(
          "type" => "checkbox",
          "id" => "waiverAgreement",
          "required" => TRUE,
          "data" => array(
            "Yes" => "I ACKNOWLEDGE HAVING READ, UNDERSTOOD, AND AGREE TO THE ABOVE WAIVER, RELEASE and INDEMNITY"
            ),
          "isArray" => FALSE
          )
      );
  
    $sections = array_merge($activities, $sections);

    return $sections;
  }

  public function get_addon_shirt_names()
  {
    $sql = "SELECT relAS.intAddonShirtID AS shirtID, lkpS.strAddonShirt AS shirt, relAS.intEventID AS eventID FROM relAddonShirt AS relAS 
    INNER JOIN lkpAddonShirt AS lkpS
    ON relAS.intAddonShirtID = lkpS.intAddonShirtID;";

    $parsedArray = array();

    $stmt = $this->db->prepare($sql);
    if($stmt->execute())
    {
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

      foreach($results as $row)
      {
        $parsedArray[$row['eventID']][$row['shirtID']] = $row['shirt'];
      }
    }

    return $parsedArray;
  }

  public function get_cart_summary($type = "registration")
  {
    $sections = array();
    $addonNames = $this->get_addon_names();
    $addonShirtNames = $this->get_addon_shirt_names();
    $charityNames = $this->get_charity_names();
    $teamNames = $this->get_team_names();

    $participant = $_SESSION[$type][$_SESSION[$type]['active_user']];
       if(isset($participant['activities']))
        {
          foreach($participant['activities'] as $id)
          {
            $explodedID = explode("_", $id);
            $thisEventID = reset($explodedID);
            $thisActivityID = end($explodedID);
            $sections["Activities"][$_SESSION['general'][$thisEventID]['display']][] = $thisActivityID;
          }
        }

        if(isset($participant['artez']))
        {
          foreach($participant['artez'] as $eventID => $artezDetails)
          {
            foreach($artezDetails as $field => $value)
            {
              if($value != "")
              {
                $sections[$_SESSION['general'][$eventID]['display']]["Artez Fundraising"][$field] = $value;
              }
            }
          }
        }

        if(isset($participant['group_event']))
        {
          $explodedID = explode("_", $participant['group_event']);
          $thisEventID = reset($explodedID);
          $thisActivityID = end($explodedID);
          $sections["Activities"][$_SESSION['general'][$thisEventID]['display']][] = $thisActivityID;
        }

        if(isset($participant["group_details"]))
        {
          foreach($participant["group_details"] as $memberNumber => $member)
          {
            foreach($member as $key => $value)
            {
              if($key == "eventextras")
              {
                foreach($value as $k1 => $v2)
                {
                  $sections["Group Members"]["Member #".$memberNumber][$k1] = $v2;
                }
              }
              else if($key == "addon")
              {
                foreach($value as $k1 => $v2)
                {
                  $sections["Group Members"]["Member #".$memberNumber][$k1] = $v2;
                }
              }
              else
              {
                $newField = str_replace(array("g_", "_".$memberNumber), "", $key);
                $sections["Group Members"]["Member #".$memberNumber][$newField] = $value;
              }
            }
          }
        }

        if(isset($participant['charities']))
        {
          foreach($participant['charities'] as $thisEventID => $charityDetails)
          {
            if(!empty($charityDetails))
            {
              foreach($charityDetails as $field => $value)
              {
                if(stripos($field, "fundraising_image") === FALSE)
                {
                  $newField = str_replace("charitydetails_", "", $field);
                  $newField = str_replace("_".$thisEventID, "", $newField);
                  if($value != "")
                  {
                    if($newField == "charity" && array_key_exists($value, $charityNames))
                    {
                      $sections[$_SESSION['general'][$thisEventID]['display']]["Charity Info"][$newField] = $charityNames[$value];
                    }
                    else
                    {
                      $sections[$_SESSION['general'][$thisEventID]['display']]["Charity Info"][$newField] = $value;
                    }
                  }
                }
              }
            }
          }
        }

        if(isset($participant['seriesfoundation']))
        {
          $sections["Series"]["Series Foundation"]['donation'] = $participant['seriesfoundation']['donation'];
        }

        if(isset($participant['teams']))
        {
          foreach($participant['teams'] as $thisEventID => $teamDetails)
          {
            if(!empty($teamDetails))
            {
              foreach($teamDetails as $field => $value)
              {
                $newField = str_replace("eventteam_", "", $field);
                $newField = str_replace("_".$thisEventID, "", $newField);
                if($value != "")
                {
                  if($newField == "team_name")
                  {
                    $sections[$_SESSION['general'][$thisEventID]['display']]["Team Info"][$newField] = $teamNames[$value];
                  }
                  else
                  {
                    $sections[$_SESSION['general'][$thisEventID]['display']]["Team Info"][$newField] = $value;
                  }
                }
              }
            }
          }
        }

        if(isset($participant['addons']))
        {
          foreach($participant['addons'] as $eventID => $addons)
          {
            foreach($addons as $addonID => $addonList)
            {
              foreach($addonList as $addonDetails)
              {
                if(isset($addonDetails['tshirt']))
                {
                  $sections[$_SESSION['general'][$eventID]['display']]["Event Info"][$addonNames[$addonID]] = $addonShirtNames[$eventID][$addonDetails['tshirt']];
                }
                else
                {
                  $sections[$_SESSION['general'][$eventID]['display']]["Event Info"][$addonNames[$addonID]] = $addonDetails['qty'];
                }
              }
            }
          }
        }
        
        if(isset($participant['eventextras']))
        {
          foreach($participant['eventextras'] as $thisEventID => $extras)
          {
            if(!empty($extras))
            {
              foreach($extras as $field => $value)
              {
                $newField = str_replace("eventextra_", "", $field);
                $newField = str_replace("_".$thisEventID, "", $newField);
                if($value != "")
                {
                  if($field == "tshirt")
                  {
                    $sections[$_SESSION['general'][$thisEventID]['display']]["Event Info"][$newField] = $addonNames['shirtSizes'][$value];
                  }
                  else
                  {
                    $sections[$_SESSION['general'][$thisEventID]['display']]["Event Info"][$newField] = $value;
                  }
                }
              }
            }
          }
        }

        if(isset($participant['personal']))
        {
          foreach($participant['personal'] as $field => $value)
          {
              $sections['Personal Info'][$field] = $value;
          }
        }

    return $sections;
  }
  
	public function searchParticipant() {
		$retArray = array();
		
		$aPost = $this->input->sanitizeForDbPost();
		$searchType = $aPost['searchtype'];
		$searchValue = $aPost['search'];
		
		try {
			$sql = "SELECT epi.strFirstName, epi.strLastName, la.strActivity, rpp.intPledgeProfileID FROM entPersonalInformation AS epi
				INNER JOIN relAccountPersonalInformation AS rapi ON rapi.intPersonalInformationID = epi.intPersonalInformationID
				INNER JOIN entAccount AS ea ON ea.intAccountID = rapi.intAccountID
				INNER JOIN lkpActiveStatus AS las ON las.intActiveStatusID = ea.intActiveStatusID AND LOWER(las.strActiveStatus) = 'active'
				INNER JOIN relAccountActivity AS raa ON raa.intAccountID = ea.intAccountID AND raa.strStatusChange IS NULL
				INNER JOIN lkpActivity AS la ON la.intActivityID = raa.intActivityID
				LEFT JOIN relPledgeProfile AS rpp ON rpp.intAccountID = ea.intAccountID AND rpp.intEventID = raa.intEventID";
			
			if ($searchType == "first_name") {
				$sql .= " WHERE epi.strFirstName LIKE '%" . $searchValue . "%'";
			}
			else if ($searchType == "last_name") {
				$sql .= " WHERE epi.strLastName LIKE '%" . $searchValue . "%'";
			}
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError();
		}
		
		if (isset($result) && count($result) > 0) {
			$retArray = $result;					
		}
		
		return $retArray;
	}
	
	public function processResendPostData() {
		$retArray = array();
		
		$aPost = $this->input->sanitizeForDbPost();
		
		$dob = $aPost['year_of_birth'] . "-" . $aPost['month_of_birth'] . "-" . $aPost['date_of_birth'];
		
		try {
			$sql = "SELECT intPersonalInformationID, strFirstName, strLastName, strAddress, strCity, strProvince, strCountry, strPostalCode, strHomePhone, strMobilePhone, strEmail, strGender, dteDateOfBirth, strCategory, strSeriesNewsletter, strNationality, strNationalityDuration, strIndustryType, strIndustryTypeOther, strYearlySalary, strEmergencyContactName, strEmergencyContactNumber, strMedicalCondition FROM entPersonalInformation WHERE strEmail = '" . $aPost['email'] . "' AND dteDateOfBirth = '" . $dob . "'";
			
			
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError();
		}
		
		if (isset($result) && count($result) > 0) {
			$retArray = $result[0];					
		}
		
		return $retArray;
	}
}

/* End of file registration_model.php */
/* Location: ./application/models/registration_model.php */