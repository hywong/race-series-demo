<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Addons_model extends CI_Model {

  public function get_addons($eventIDs = array())
  {
  	if (empty($eventIDs)) {
  		$eventIDs = get_session_events();
  	}
	
    $sqlShirt = "SELECT lkpAS.strAddonShirt AS shirt, lkpAS.intAddonShirtID AS shirtID, relAS.intEventID AS eventID, relAS.intAddonID AS addonID FROM relAddonShirt AS relAS
    INNER JOIN lkpAddonShirt AS lkpAS
    ON lkpAS.intAddonShirtID = relAS.intAddonShirtID;";
    $stmt = $this->db->prepare($sqlShirt);
    $results = array();
    
    if($stmt->execute())
    {
      $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
      $rCount = count($rows);
      $shirtsArray = array();

      for($i = 0; $i < $rCount; $i++)
      {
        $shirtsArray[$rows[$i]['eventID']][$rows[$i]['addonID']][$rows[$i]['shirtID']] = $rows[$i]['shirt'];
      }

      $sql = "SELECT relA.intEventID AS eventID, lkpA.intAddonID AS addonID, lkpA.strAddon AS addon, relA.strFields AS fields, relA.dblPrice AS price, relA.intCap AS cap, relA.intPurchaseLimit AS purchase_limit, 
      relA.dttExpiration AS expiration, relA.strDescription AS description, relA.strTitle AS title, relA.intEventID AS eventID FROM relAddon AS relA
  INNER JOIN lkpAddon AS lkpA
  ON relA.intAddonID = lkpA.intAddonID
  WHERE relA.intEventID IN (".implode(",", $eventIDs).");";      

      try
      {
        $statement = $this->db->prepare($sql);
        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);

        $cResults = count($results);
        for($i = 0; $i < $cResults; $i++)
        {
          $results[$i]['fields'] = json_decode($results[$i]['fields'], TRUE);
          if(is_array($results[$i]['fields']))
          {
            foreach($results[$i]['fields'] as $k => $field)
            {
              if(isset($field['name']))
              {
                if($field['name'] == "tshirt")
                  $results[$i]['fields'][$k]['data'] = $shirtsArray[$results[$i]['eventID']][$results[$i]['addonID']];
              }
            }
          }
        }
      }
      catch (Exception $e)
      {
        //TODO: Proper catching
      }      
    }
    return $results;
  }

  public function get_addon_counts()
  {
    $sql = "SELECT count(intAddonID) AS count, intAddonID AS addonID, intEventID AS eventID FROM relAddonTransaction WHERE strRefundStatus IS NULL GROUP BY intEventID, intAddonID;";
    $stmt = $this->db->prepare($sql);
    $parsedArray = array();
    if($stmt->execute())
    {
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if($results)
      {
        foreach($results as $row)
        {
          $parsedArray[$row['eventID']][$row['addonID']] = $row['count'];
        }
      }
    }

    return $parsedArray;
  }

  public function get_addon_shirt_cap()
  {
    $sql = "SELECT relat.intAddonID AS addonID, relat.intEventID AS eventID, relat.intAddonShirtID AS addonShirtID, count(relat.intAddonShirtID) AS count, relas.intCap AS cap FROM relAddonTransaction AS relat
    INNER JOIN lkpAddonShirt AS lkpas
    ON relat.intAddonShirtID = lkpas.intAddonShirtID
    INNER JOIN relAddonShirt as relas
    ON lkpas.intAddonShirtID = relas.intAddonShirtID
	WHERE relat.strRefundStatus IS NULL
    ";

    $stmt = $this->db->prepare($sql);
    $parsedArray = array();
    if($stmt->execute())
    {
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
      foreach($results as $row)
      {
        $parsedArray[$row['eventID']][$row['addonID']][$row['addonShirtID']] = array(
          "count" => $row['count'],
          "cap" => $row['cap']
          );
      }
    }

    return $parsedArray;
  }
}

/* End of file addons_model.php */
/* Location: ./application/models/addons_model.php */