<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment_model extends CI_Model {

  public function get_form_sections()
  {
    $expYears = array();
    for($i = date("Y"); $i <= date("Y")+25; $i++)
    {
      $expYears[substr($i, -2)] = $i;
    }

    $expMonths = array();
    for($i = 1; $i <= 12; $i++)
    {
      $padMonth = str_pad($i, 2, '0', STR_PAD_LEFT);
      $expMonths[$padMonth] = date("F", strtotime(date("Y")."-".$i));
    }

    $billingMembers = array();
    if(isset($_SESSION['registration']))
    {
      if(count($_SESSION['registration']) >= 3)
      {
        foreach($_SESSION['registration'] as $id => $participant)
        {
          if($id != "active_user" && $id != "last_modified")
            $billingMembers[$id] = $participant['personal']['first_name'] . " " . $participant['personal']['last_name'];
        }
      }
    }

    $formitForm = array(
        "method" => "POST",
        "action" => "payment/process",
        "sections" => array(
          translate("Payment Information") => array(
            "cc_name" => array(
              "type" => "text",
              "label" => "Name on Credit Card",
              "name" =>"cc_name",
              "id" => "inpCcName"
              ),
            "cc_num" => array(
              "type" => "text",
              "label" => "Credit Card Number",
              "name" => "cc_num",
              "id" => "inpCcNum"
              ),
            "cvv" => array(
              "type" => "text",
              "label" => "CVV",
              "name" => "cvv",
              "id" => "inpCvv"
              ),
            "exp_year" => array(
              "type" => "select",
              "label" => "Expiration Year",
              "name" => "exp_year",
              "id" => "selExpYear",
              "data" => $expYears
              ),
            "exp_month" => array(
              "type" => "select",
              "label" => "Expiration Month",
              "name" => "exp_month",
              "id" => "selExpMonth",
              "data" => $expMonths
              )
            )
          )
      );
      
      if(isset($_SESSION['registration']))
      {
        if(count($_SESSION['registration']) >= 3)
        {
          $formitForm['sections']['Payment Information']["billing_member"] = array(
            "type" => "select",
            "name" => "billing_member",
            "label" => "Billing Member",
            "id" => "selBillingMember",
            "data" => $billingMembers
            );
        }
      }

      return $formitForm;
  }

  // public function calculate_discounts($prices)
  // {
  //   $eventDiscount = 0;
  //   $codeDiscount = 0;
  //   $groupDiscount = 0;
  //   $totalGroupMembers = 0;
  //   $totalDiscount = 0;
  //   $member = 0;
  //   $this->load->model("registration_model", "registrationm");

  //   foreach($_SESSION['registration'] as $id => $p)
  //   {
  //     if($id != "active_user" && $id != "last_modified")
  //     {
  //       if($_SESSION['type'] == "group")
  //       {
  //         $totalGroupMembers += count($p['group_details']);
  //       }

  //       if(isset($p['discount_code']))
  //       {
  //         $thisDiscount[$id] = $this->registrationm->get_discount_data($id, $prices);
  //         $codeDiscount += $thisDiscount[$id]['discount'];
  //       }

  //       $activityCount = count($p['activities']);
  //       if($activityCount == 2)
  //       {
  //         $eventDiscount += 5;
  //       }
  //       if($activityCount == 3)
  //       {
  //         $eventDiscount += 10;
  //       }
  //       if($activityCount >= 4)
  //       {
  //         $eventDiscount += 20;
  //       }
  //       $member++;
  //     }
  //   }

  //   if($member >= 10 && !$eventDiscount && !$codeDiscount)
  //   {
  //     $groupDiscount = $member * 5;
  //   }

  //   if($totalGroupMembers >= 10 && !$eventDiscount && !$codeDiscount)
  //   {
  //     $groupDiscount = $totalGroupMembers * 5;
  //   }

  //   $paymentSummary = array();
  //   if($codeDiscount && !$eventDiscount)
  //   {
  //     $paymentSummary["Discounts"] = array(
  //       "Discount" => array(
  //         "Discount Code" => array($codeDiscount)
  //         )
  //       );

  //     $totalDiscount += $codeDiscount;
  //   }

  //   if($eventDiscount)
  //   {
  //     $paymentSummary["Discounts"] = array(
  //       "Discount" => array(
  //         "Multi-Event Discount" => array($eventDiscount)
  //         )
  //       );

  //     $totalDiscount += $eventDiscount;
  //   }

  //   if($groupDiscount)
  //   {
  //     $paymentSummary["Discounts"] = array(
  //       "Discount" => array(
  //         "Multi-Event Discount" => array($groupDiscount)
  //         )
  //       );

  //     $totalDiscount += $groupDiscount;
  //   }

  //   $return = array(
  //     "summaryArray" => $paymentSummary,
  //     "totalDiscount" => $totalDiscount
  //     );
    
  //   return $return;
  // }

  public function calculate_cart_discounts($prices)
  {
    $applyArtez = FALSE;
    $applyEvent = FALSE;
    $applyGroup = FALSE;
    $applyCode = FALSE;

    $eventDiscount = array();
    $codeDiscount = array();
    $artezDiscountDetails = array();
    $cartCount = 0;

    $this->load->model("registration_model", "registrationm");
    $this->load->library("artez");
    $artezCharities = $this->artez->get_valid_live_charities();
    $earlyPrices = $this->get_early_bird_prices();

    foreach($_SESSION['registration'] as $id => $participant)
    {
      if($id != "active_user" && $id != "last_modified")
      {
        if(isset($participant['artez']))
        {
          //Parse events/activities to easily find the activity ID
          foreach($participant['activities'] as $eventActivity)
          {
            $expl = explode("_", $eventActivity);
            $activityID = end($expl);
            $eventID = reset($expl);
            $currentActivities[$eventID] = $activityID;
          }

          $artezDiscount = array();
          if(!isset($artezDiscount[$id]))
            $artezDiscount[$id] = 0;

          foreach($participant['artez'] as $eID => $charitySelections)
          {
            if($charitySelections['artez_charity_action'] == "fundraising")
            {
              if(array_key_exists($charitySelections['artez_charity'], $artezCharities))
              {
                //Check if the pin matches
                if(isset($charitySelections['artez_charity_pin']))
                {
                  if($charitySelections['artez_charity_pin'] == $artezCharities[$charitySelections['artez_charity']]['pin'])
                  {
                    $applyArtez = TRUE;
                    $artezDiscountDetails[$id]['artez'][$eID] = array(
                      "pin" => $charitySelections['artez_charity_pin'],
                      "artezCharityID" => $charitySelections['artez_charity'],
                      'discount' => $prices[$eID][$currentActivities[$eID]]
                      );
                    $artezDiscount[$id] += $prices[$eID][$currentActivities[$eID]];
                  }
                  else
                  {
                    $applyArtez = TRUE;
                    $artezDiscountDetails[$id]['artez'][$eID] = array(
                      "artezCharityID" => $charitySelections['artez_charity'],
                      'reduced' => $earlyPrices[$eID][$currentActivities[$eID]],
                      'discount' => 0,
                      );
                    $artezDiscount[$id] += $earlyPrices[$eID][$currentActivities[$eID]];
                  }                  
                }
                else
                {
                  $applyArtez = TRUE;
                  $artezDiscountDetails[$id]['artez'][$eID] = array(
                    "artezCharityID" => $charitySelections['artez_charity'],
                    'reduced' => $earlyPrices[$eID][$currentActivities[$eID]],
                    'discount' => 0,
                    );
                  $artezDiscount[$id] += $earlyPrices[$eID][$currentActivities[$eID]];
                }
              }
              else
              {
                $applyArtez = TRUE;
                $artezDiscountDetails[$id]['artez'][$eID] = array(
                  "artezCharityID" => $charitySelections['artez_charity'],
                  'reduced' => $earlyPrices[$eID][$currentActivities[$eID]],
                  'discount' => 0
                  );
                $artezDiscount[$id] += $earlyPrices[$eID][$currentActivities[$eID]];
              }            
            }
          }
        }

        $activityCount = count($participant['activities']);
        if($activityCount == 2)
        {
          $applyEvent = TRUE;
          $eventDiscount[$id] = 5;
        }
        else if($activityCount == 3)
        {
          $applyEvent = TRUE;
          $eventDiscount[$id] = 10;
        }
        else if($activityCount >= 4)
        {
          $applyEvent = TRUE;
          $eventDiscount[$id] = 20;
        }

        if($_SESSION['type'] == "group")
        {
          if(isset($participant['personal']['members']))
          {
            $cartCount += $participant['personal']['members'];
          }        
        }
        else
        {
          $cartCount++;
        }

        if(isset($participant['discount_code']))
        {
          $thisDiscountCode = $this->registrationm->get_discount_data($id, $prices);
          if($_SESSION['type'] == 'group')
          {
            if(isset($_SESSION['registration'][$id]['personal']['members']))
            {
              $thisDiscountCode['discount'] = $thisDiscountCode['discount'] * $_SESSION['registration'][$id]['personal']['members'];
              $codeDiscount[$id] = $thisDiscountCode;
              $applyCode = TRUE;
            }
            else
            {
              $codeDiscount[$id] = $thisDiscountCode;
              $applyCode = TRUE;
            }
          }
          else
          {
            $codeDiscount[$id] = $thisDiscountCode;
            $applyCode = TRUE;
          }
        }
      }
    }

    $summaryArray = array();
    $parsedArray = array();
    $artezDiscountApplied = FALSE;

    if($applyArtez === TRUE)
    {
      $artezDiscountApplied = TRUE;

      foreach($artezDiscountDetails as $id => $artezCategory)
      {
        foreach($artezCategory['artez'] as $eID => $dets)
        {
          $diff = $prices[$eID][$currentActivities[$eID]] - $earlyPrices[$eID][$currentActivities[$eID]];
          if($dets['discount'] > 0)
          {
            $parsedArray[$id]['artez'][$eID]['artez_discount'] = $dets['discount'];
            $parsedArray[$id]['artez'][$eID]['artez_details'] = $dets;
            $summaryArray[translate("Artez Fundraising Discount")."<br />".$_SESSION['registration'][$id]['personal']['first_name']. ' '.$_SESSION['registration'][$id]['personal']['last_name']." - ".$_SESSION['general'][$eID]['display']] = array($dets['discount']);
          }
          else if(isset($dets['reduced']) && $diff > 0)
          {
            $parsedArray[$id]['artez'][$eID]['artez_discount'] = $diff;
            $parsedArray[$id]['artez'][$eID]['artez_details'] = $dets;
            $summaryArray[translate("Artez Fundraising Discount")."<br />".$_SESSION['registration'][$id]['personal']['first_name']. ' '.$_SESSION['registration'][$id]['personal']['last_name']." - ".$_SESSION['general'][$eID]['display']] = array($dets['discount']);
          }
        }
      }
    }

    if($applyEvent === TRUE)
    {
      foreach($eventDiscount as $id => $amount)
      {
        $parsedArray[$id]['discount'] = $amount;
        $summaryArray[translate("Multi-Event Discount")."<br />".$_SESSION['registration'][$id]['personal']['first_name']. ' '.$_SESSION['registration'][$id]['personal']['last_name']." - ".count($_SESSION['registration'][$id]['activities'])." ".translate("Registrations") . "<br />( ".translate("Discount is split evenly per registration")." )"] = array($amount);
      }

      $data = array(
        "type" => "event",
        "data" => $parsedArray,
        "summary" => $summaryArray
        );
      return $data;
    }

    if($artezDiscountApplied === FALSE)
    {      
      if($cartCount >= 10)
      {
        $totalGroupDiscount = 5 * $cartCount;

        $data = array(
          "type" => "group",
          "data" => array(
            "discount" => 5
            ),
          "summary" => array(
            translate("Group Discount")."<br />(".translate("Total of discounts applied - 5$/participant").")" => array($totalGroupDiscount)
            )
          );
        return $data;
      }

      if($applyCode === TRUE)
      {
        $summaryArray = array();

        foreach($codeDiscount as $codeDetails)
        {
          $summaryArray[$codeDetails['event'].' - Discount Code [ '.$codeDetails['codeUsed'].' ]'] = array($codeDetails['discount']);
        }
        $data = array(
          "type" => "code",
          "data" => $codeDiscount,
          "summary" => $summaryArray
          );
        return $data;
      }
    }

    return array();
  }

  public function get_early_bird_prices()
  {
    $sql = "SELECT * FROM (SELECT lkpE.intEventID AS event_id, lkpA.intActivityID AS activity_id, relPCD.dblPrice AS price FROM relPriceChangeDate AS relPCD
            INNER JOIN lkpEvent AS lkpE
            ON relPCD.intEventID = lkpE.intEventID
            INNER JOIN lkpActivity AS lkpA
            ON relPCD.intActivityID = lkpA.intActivityID
            INNER JOIN relEventActivityCap AS relEAC
            ON lkpE.intEventID = relEAC.intEventID AND lkpA.intActivityID = relEAC.intActivityID
            ORDER BY DATE(relPCD.dttDateTill) ASC) AS validPrices
            GROUP BY event_id,activity_id;";
    $statement = $this->db->prepare($sql);
    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);

    $parsedArray = array();
    $count = count($results);
    for($i = 0; $i < $count; $i++)
    {
      $parsedArray[$results[$i]["event_id"]][$results[$i]["activity_id"]] = $results[$i]["price"];
    }

    return $parsedArray;
  }

  public function get_activity_total_costs($activityPrices, $cartDiscount, $addonPrices)
  {
    //Get tax rates
    $this->load->library("registration");
    $taxRates = $this->registration->get_tax_rates();

    $grandTotal = 0;
    $addonTotal = 0;
    $activityTotal = 0;
    $activityCalculatedTotal = 0;
    $charityTotal = 0;
    $taxTotal = 0;
    $codeDiscount = 0;
    $groupDiscount = 5;
    $totalDiscount = 0;
    $totalDiscountIssued = 0;
    $totalRegularPrice = 0;
    $discountApplied = FALSE;
    $participantData = array();

    foreach($_SESSION['registration'] as $id => $participant)
    {
      $activitySubtotalTotals = 0;
      $charitySubtotal = 0;

      if($id != "active_user" && $id != "last_modified")
      {
        //Check if we have activities
        if(isset($participant['activities']) && is_array($participant))
        {
          $activityCount = count($participant['activities']);
          $activityTotalPerParticipant = 0;
          $taxTotalPerParticipant = 0;
          foreach($participant['activities'] as $eventActivity)
          {
            $activitySubtotal = 0;
            $exploded = explode("_", $eventActivity);
            $eventID = reset($exploded);
            $activityID = end($exploded);
            if(isset($activityPrices[$eventID][$activityID]))
            {
              $thisPrice = 0;

              if(isset($participant['personal']['members']))
              {
                $thisPrice = number_format(($activityPrices[$eventID][$activityID] * $participant['personal']['members']), 2, '.', '');
                $regularPrice = number_format(($activityPrices[$eventID][$activityID] * $participant['personal']['members']), 2, '.', '');
              }
              else
              {
                if($_SESSION['type'] == 'transfer')
                {
                  $thisPrice = TRANSFER_FEE;
                  $regularPrice = TRANSFER_FEE;
                }
                else
                {
                  $thisPrice = $activityPrices[$eventID][$activityID];
                  $regularPrice = $activityPrices[$eventID][$activityID];
                }
              }

              $activityTotal += $regularPrice;
              $activityTotalPerParticipant += $regularPrice;

              if(isset($cartDiscount['type']))
              {
                if(isset($cartDiscount['data']))
                {
                  if(isset($cartDiscount['data'][$id]['artez'][$eventID]['artez_discount']))
                  {
                    if(isset($participant['artez'][$eventID]))
                    {
                      if($participant['artez'][$eventID]['artez_charity_action'] == "fundraising")
                      {
                        $artezDiscountAmount = $cartDiscount['data'][$id]['artez'][$eventID]['artez_discount'];
                        $thisPrice -= $artezDiscountAmount;
                        $totalDiscountIssued += $artezDiscountAmount;
                      }
                    }
                  }
                }

                if(!isset($participantData[$id]['discount']))
                  $participantData[$id]['discount'] = 0;
                
                if($cartDiscount['type'] === 'event')
                {
                  if(isset($cartDiscount['data'][$id]))
                  {
                    if($thisPrice > 0)
                    {
                      $eventDiscountAmount = $cartDiscount['data'][$id]['discount'];
                      $discountPerEvent = number_format($eventDiscountAmount / $activityCount, 2, '.', '');
                      $thisPrice -= $discountPerEvent;   
                      $totalDiscountIssued += $discountPerEvent;

                      $participantData[$id]['discount'] += $discountPerEvent;
                    }
                    if($thisPrice < 0 )
                    {
                      $thisPrice = 0;
                    }  
                  }
                }

                if($cartDiscount['type'] === 'group')
                {
                  $thisPrice -= ($cartDiscount['data']['discount'] * $participant['personal']['members']);
                  $totalDiscountIssued += ($cartDiscount['data']['discount'] * $participant['personal']['members']);
                  $participantData[$id]['discount'] += $cartDiscount['data']['discount'];
                }

                if($cartDiscount['type'] === 'code')
                {
                  if(isset($cartDiscount['data'][$id]))
                  {
                    if($cartDiscount['data'][$id]['eventID'] == $eventID)
                    {
                      $thisPrice -= $cartDiscount['data'][$id]['discount'];
                      $totalDiscountIssued += $cartDiscount['data'][$id]['discount'];
                      $participantData[$id]['discount'] += $cartDiscount['data'][$id]['discount'];
                      if($thisPrice < 0 )
                      {
                        $thisPrice = 0;
                      }                        
                    }
                  }
                }
              }
            }

            if($thisPrice > 0)
            {
              if(!isset($taxes[$eventID]["registrations"]))
                $taxes[$eventID]["registrations"][$taxRates[$eventID]["registration"]] = 0;

              if($_SESSION['type'] != "transfer")
              {
                $tax = number_format(($thisPrice * $taxRates[$eventID]["registration"]), 2, '.' ,'');
                $taxes[$eventID]["registrations"][$taxRates[$eventID]["registration"]] += $tax;
                $taxTotal += $tax;
                $taxTotalPerParticipant += $tax;
              }
            }

            $activityCalculatedTotal += $thisPrice;
          }
        }

        $addonSubtotal = 0;

        //Check if we have group addons
        if(isset($participant['group_details']))
        {
          //Get event ID
          $eventActivity = $participant['activities'][0];
          $expl = explode("_", $eventActivity);
          $geventID = reset($expl);
          unset($expl);
          $groupitabqty = 0;
          foreach($participant['group_details'] as $member => $memberDetails)
          {
            if(isset($memberDetails['addon']))
            {
              if(isset($memberDetails['addon']['itab']))
              {
                if($memberDetails['addon']['itab'] != "")
                {
                  $itabID = $memberDetails['addon']['itab'];
                  $addonSubtotal += $addonPrices[$geventID][$itabID]['price'];
                  $groupitabqty++;
                }
              }
            }
          }
        }

        //Check if we have addons
        if(isset($participant['addons']) && is_array($participant))
        {
          foreach($participant['addons'] as $eID => $addonsPerEvent)
          {
            foreach($addonsPerEvent as $thisAddon => $addonArray)
            {
              $addonsCount = count($addonArray);
              for($i = 0; $i < $addonsCount; $i++)
              {
                if(isset($addonArray[$i]['qty']))
                {
                  $amountToAdd = ($addonPrices[$eID][$thisAddon]['price'] * $addonArray[$i]['qty']);
                  $addonSubtotal += $amountToAdd;
                }
                else
                {
                  $amountToAdd = $addonPrices[$eID][$thisAddon]['price'];
                  $addonSubtotal += $amountToAdd;
                }
              }
            }
          }
        }

        if(isset($participant['seriesfoundation']))
        {
          if($participant['seriesfoundation']['donation'] > 0)
          {
            $charitySubtotal += $participant['seriesfoundation']['donation'];
          }
        }

        if(isset($participant['charities']) && is_array($participant))
        {
          foreach($participant['charities'] as $eID => $charityDetails)
          {
            if(isset($charityDetails['donation']))
            {
              $charitySubtotal += $charityDetails["donation"];
            }
          }
        }

        if(isset($participant['artez']))
        {
          if(isset($participant['artez']))
          {
            $this->load->library('artez');
            $artezCharities = $this->artez->get_charities();

            foreach($participant['artez'] as $eID => $artezDetails)
            {
              if(isset($artezDetails['qty_cap']) || isset($artezDetails['dollar_cap']))
              {
                $theseRules = array();
                (isset($artezDetails['qty_cap']) ? $theseRules['qty_cap'] = $artezDetails['qty_cap'] :NULL);
                (isset($artezDetails['dollar_cap']) ? $theseRules['dollar_cap'] = $artezDetails['dollar_cap'] :NULL);
                $this->artez->set_rules($theseRules);

                $this->artez->validate_charity_rules();
              }
            }
          }
        }

        if($activityTotalPerParticipant > 0) 
        {
          if(!isset($participantData[$id]['activityCostTotal']))
            $participantData[$id]['activityCostTotal'] = 0;

          $participantData[$id]['activityCostTotal'] += $activityTotalPerParticipant;
        }

        if($addonSubtotal > 0) $addonTotal += $addonSubtotal;

        if($taxTotal > 0)
        {
          if($_SESSION['type'] != "transfer")
          {
            foreach($taxes as $eID => $taxDetails)
            {
              foreach ($taxDetails["registrations"] as $rate => $amount)
              {
                $grandTotal += $amount;
              }
            }
          }
        }

        if($taxTotalPerParticipant > 0)
        {
          $participantData[$id]['taxTotal'] = $taxTotalPerParticipant;
        }
        else
        {
          $participantData[$id]['taxTotal'] = 0;
        }

        if($charitySubtotal > 0) $charityTotal += $charitySubtotal;
      }
    }

    $eolFee = number_format(calculate_eol_fee($taxTotal + $addonTotal + $activityCalculatedTotal), 2, '.', '');
    $eolFeeTax = number_format(($eolFee  * EOL_FEE_TAX), 2, '.', '');
    $totalEolFee = number_format(($eolFee + $eolFeeTax), 2, '.', '');

    if($_SESSION['type'] == "transfer")
    {
      $eolTransferAddons = number_format(calculate_eol_fee($addonTotal), 2, '.', '');
      $eolTransferAddonsTax = number_format($eolTransferAddons * EOL_FEE_TAX, 2, '.', '');
      $eolFeeTax = 5 * EOL_FEE_TAX;
      $eolFee = 5 - $eolFeeTax;
      $activityTotal = $activityTotal - 5;

      $eolFee += $eolTransferAddons;
      $eolFeeTax += $eolTransferAddonsTax;

      $grandTotal = TRANSFER_FEE;
      $remit = 5;
    }

    $grandTotal = $addonTotal + $activityCalculatedTotal + $totalEolFee + $charityTotal + $taxTotal;

    if($_SESSION['type'] == 'transfer')
    {
      $remit = $addonTotal + ($activityCalculatedTotal - 5) + $taxTotal;
    }
    else
    {
      $remit = $addonTotal + $activityCalculatedTotal + $taxTotal;
    }

    // $totalDiscount = 0;
    // if(isset($cartDiscount['data']))
    // {
    //   foreach($cartDiscount['data'] as $id => $arr)
    //   {
    //     if(isset($arr['discount']))
    //       $totalDiscount += $arr['discount'];

    //     if(isset($arr['artez_discount']))
    //       $totalDiscount += $arr['artez_discount'];
    //   }
    // }

    $registrationSummary = array(
      "addonTotal" => $addonTotal,
      "finalTotal" => $grandTotal,
      "eolFee" => $eolFee,
      "eolFeeTax" => $eolFeeTax,
      "remit" => $remit,
      "discountTotal" => $totalDiscountIssued,
      "donationTotal" => $charityTotal,
      "activityCostTotal" => $activityTotal,
      "taxTotal" => $taxTotal
      );
    
    $returnArray = array(
      "participants" => $participantData,
      "summary" => $registrationSummary
    );

    return $returnArray;
  }

  public function get_charity_names()
  {
    $sql = "SELECT intCharityID AS charityID, strCharity AS charity FROM lkpCharity;";
    $stmt = $this->db->prepare($sql);
    $parsedArray = array();

    if($stmt->execute())
    {
      $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if($results)
      {
        foreach($results as $row)
        {
          $parsedArray[$row['charityID']] = $row['charity'];
        }
      }
    }
    else
    {

    }

    return $parsedArray;
  }

  public function get_registration_summary($prices)
  {
    //Get activity prices
    $activityPrices = $this->get_activity_prices();

    //Get addon prices
    $addonPrices = $this->get_addon_prices();

    $charityNames = $this->get_charity_names();

    //Get tax rates
    $this->load->library("registration");
    $taxRates = $this->registration->get_tax_rates();

    $grandTotal = 0;
    $addonTotal = 0;
    $activityTotal = 0;
    $charityTotal = 0;
    $charitySubtotal = 0;
    $taxTotal = 0;
    $codeDiscount = 0;
    $groupDiscount = 5;
    $totalDiscount = 0;
    $discountApplied = FALSE;

    $cartDiscount = $this->calculate_cart_discounts($prices);

    foreach($_SESSION['registration'] as $id => $participant)
    {
      $activitySubtotalTotals = 0;
      $charitySubtotal = 0;
      if($id != "active_user" && $id != "last_modified")
      {
        //Check if we have activities
        if(isset($participant['activities']) && is_array($participant))
        {
          $activityCount = count($participant['activities']);
          foreach($participant['activities'] as $eventActivity)
          {
            $activitySubtotal = 0;
            $exploded = explode("_", $eventActivity);
            $eventID = reset($exploded);
            $activityID = end($exploded);
            if(isset($activityPrices[$eventID][$activityID]))
            {
              $thisPrice = 0;
              if(isset($participant['personal']['members']))
              {
                $regularPrice = $activityPrices[$eventID][$activityID]['price'];
                $regularPriceTotal = $regularPrice * $participant['personal']['members'];

                if(isset($cartDiscount))
                {
                  if(isset($cartDiscount['type']))
                  {
                    if($cartDiscount['type'] === 'group' && isset($cartDiscount['data']))
                    {
                      $thisPrice = number_format(($regularPrice - $cartDiscount['data']['discount']) * $participant['personal']['members'], 2, '.', '');        
                    }
                    else if ($cartDiscount['type'] == "code" && isset($cartDiscount['data']))
                    {
                      $thisPrice = number_format(($regularPrice * $participant['personal']['members']) - $cartDiscount['data'][$id]['discount'], 2, '.', '');
                    }
                    else
                    {
                      $thisPrice = number_format(($regularPrice * $participant['personal']['members']), 2, '.', '');
                    }                    
                  }
                  else
                  {
                    $thisPrice = number_format(($regularPrice * $participant['personal']['members']), 2, '.', '');
                  }
                }
                else
                {
                  $thisPrice = number_format(($regularPrice * $participant['personal']['members']), 2, '.', '');
                }

                $summary[$_SESSION['general'][$eventID]['display']]["Registrations"][$activityPrices[$eventID][$activityID]['activity']. " ( $".$regularPrice." x".$participant['personal']['members']." )"][] = $regularPriceTotal;
              }
              else
              {
                if($_SESSION['type'] == 'transfer')
                {
                  $thisPrice = TRANSFER_FEE;
                }
                else
                {
                  $thisPrice = $activityPrices[$eventID][$activityID]['price'];
                }

                $regularPrice = $thisPrice;

                if(isset($cartDiscount['type']))
                {
                  if(isset($cartDiscount['data']))
                  {
                    if(isset($cartDiscount['data'][$id]['artez'][$eventID]['artez_discount']))
                    {
                      if(isset($participant['artez'][$eventID]))
                      {
                        if($participant['artez'][$eventID]['artez_charity_action'] == "fundraising")
                        {
                          if($cartDiscount['data'][$id]['artez'][$eventID]['artez_discount'] > 0)
                          {
                            $artezDiscountAmount = $cartDiscount['data'][$id]['artez'][$eventID]['artez_discount'];
                            $thisPrice -= $artezDiscountAmount;                            
                          }
                          else if(isset($cartDiscount['data'][$id]['artez'][$eventID]['artez_details']['reduced']))
                          {
                            $thisPrice = $cartDiscount['data'][$id]['artez'][$eventID]['artez_details']['reduced'];
                          }
                        }
                      }
                    }
                  }

                  if($cartDiscount['type'] === 'event')
                  {
                    if(isset($cartDiscount['data'][$id]))
                    {
                      $eventDiscountAmount = $cartDiscount['data'][$id]['discount'];
                      $discountPerEvent = number_format($eventDiscountAmount / $activityCount, 2, '.', '');
                      $thisPrice -= $discountPerEvent;
                      if($thisPrice < 0 )
                      {
                        $thisPrice = 0;
                      }      
                    }
                  }

                  if($cartDiscount['type'] === 'code')
                  {
                    if(isset($cartDiscount['data'][$id]))
                    {
                      if($cartDiscount['data'][$id]['eventID'] == $eventID)
                      {
                        $thisPrice -= $cartDiscount['data'][$id]['discount'];

                        if($thisPrice < 0 )
                        {
                          $thisPrice = 0;
                        }                        
                      }
                    }
                  }
                }



                $summary[$_SESSION['general'][$eventID]['display']]["Registrations"][$activityPrices[$eventID][$activityID]['activity']][] = $regularPrice;
              }
              $activitySubtotal += $thisPrice;
            }

            if($activitySubtotal > 0)
            {
              if($_SESSION['type'] != "transfer")
              {
                if(!isset($taxes[$eventID]["registrations"]))
                  $taxes[$eventID]["registrations"][$taxRates[$eventID]["registration"]] = 0;

                $tax = number_format(($activitySubtotal * $taxRates[$eventID]["registration"]), 2, '.' ,'');
                $taxes[$eventID]["registrations"][$taxRates[$eventID]["registration"]] += $tax;
                $taxTotal += $tax;

              }

              $activitySubtotalTotals += number_format($activitySubtotal, 2, '.', '');
            }
          }
        }

        $addonSubtotal = 0;

        //Check if we have group addons
        if(isset($participant['group_details']))
        {
          //Get event ID
          $eventActivity = $participant['activities'][0];
          $expl = explode("_", $eventActivity);
          $geventID = reset($expl);
          unset($expl);
          $groupitabqty = 0;
          foreach($participant['group_details'] as $member => $memberDetails)
          {
            if(isset($memberDetails['addon']))
            {
              if(isset($memberDetails['addon']['itab']))
              {
                if($memberDetails['addon']['itab'] != "")
                {
                  $itabID = $memberDetails['addon']['itab'];
                  $addonSubtotal += $addonPrices[$geventID][$itabID]['price'];
                  $groupitabqty++;
                }
              }
            }
          }

          if($groupitabqty > 0)
          {
            $summary[$_SESSION['general'][$geventID]['display']]['Addons'][$addonPrices[$geventID][$itabID]['addon']. " ( $".$addonPrices[$geventID][$itabID]['price']." x ".$groupitabqty." )"][] = $groupitabqty * $addonPrices[$geventID][$itabID]['price'];
          }
        }

        //Check if we have addons
        if(isset($participant['addons']) && is_array($participant))
        {
          foreach($participant['addons'] as $eID => $addonsPerEvent)
          {
            foreach($addonsPerEvent as $thisAddon => $addonArray)
            {
              $addonsCount = count($addonArray);
              for($i = 0; $i < $addonsCount; $i++)
              {
                if(isset($addonArray[$i]['qty']))
                {
                  $amountToAdd = ($addonPrices[$eID][$thisAddon]['price'] * $addonArray[$i]['qty']);
                  $addonSubtotal += $amountToAdd;
                }
                else
                {
                  $amountToAdd = $addonPrices[$eID][$thisAddon]['price'];
                  $addonSubtotal += $amountToAdd;
                }
                $summary[$_SESSION['general'][$eID]['display']]['Addons'][$addonPrices[$eID][$thisAddon]['addon']. " ( ".$addonPrices[$eID][$thisAddon]['price']." x ".$addonArray[$i]['qty']." )"][] = $amountToAdd;
              }
            }
          }
        }

        if(isset($participant['seriesfoundation']))
        {
          if($participant['seriesfoundation']['donation'] > 0)
          {
            $summary["Series Foundation"]["Donation"]['Series Foundation'][] = $participant['seriesfoundation']['donation'];
            $charitySubtotal += $participant['seriesfoundation']['donation'];
          }
        }

        if(isset($participant['charities']) && is_array($participant))
        {
          foreach($participant['charities'] as $eID => $charityDetails)
          {
            if(isset($charityDetails['donation']))
            {
              $charitySubtotal += $charityDetails["donation"];
              $summary[$_SESSION['general'][$eID]['display']]['Donation'][$charityNames[$charityDetails['charity']]][] = $charityDetails["donation"];
            }
          }
        }

        if(isset($cartDiscount['summary']))
        {
          $summary['Summary']['Discounts'] = $cartDiscount['summary'];
        }

        if($activitySubtotalTotals > 0)
        {
          if(!isset($summary["Summary"]["Total Registrations"][" "][0]))
            $summary["Summary"]["Total Registrations"][" "][0] = 0;

          $summary["Summary"]["Total Registrations"][" "][0] += $activitySubtotalTotals;

          $activityTotal += number_format($activitySubtotalTotals, 2, '.', '');
          // echo $activityTotal.'<BR>';
        }

        if($addonSubtotal > 0)
        {
          if(!isset($summary["Summary"]["Total Addons"][" "][0]))
            $summary["Summary"]["Total Addons"][" "][0] = 0;

          $summary["Summary"]["Total Addons"][" "][0] += $addonSubtotal;
          $addonTotal += $addonSubtotal;
        }

        if($taxTotal > 0)
        {
          foreach($taxes as $eID => $taxDetails)
          {
            foreach ($taxDetails["registrations"] as $rate => $amount)
            {
              $summary["Summary"]["Taxes"]["(".$_SESSION['general'][$eID]['display']." Tax - ".($rate * 100)."% )"][0] = $amount;
              $grandTotal += $amount;
            }
          }
        }


        if($charitySubtotal > 0)
        {
          if(!isset($summary["Summary"]["Total Donations"][" "][0]))
            $summary["Summary"]["Total Donations"][" "][0] = 0;

          $summary["Summary"]["Total Donations"][" "][0] += $charitySubtotal;
          $charityTotal += $charitySubtotal;
        }
      }
    }

    $eolFee = 0;
    if($_SESSION['type'] == 'transfer')
    {
      $eolFee = calculate_eol_fee($addonTotal);
    }
    else
    {
      $eolFee = calculate_eol_fee($taxTotal + $addonTotal + $activityTotal);
    }

    $eolFee += $eolFee * EOL_FEE_TAX;
    $summary["Summary"]["<a href=\"".base_url('register/fee')."\" target=\"_blank\">".translate("Online Fee")."</a>"][" "][0] = $eolFee;

    $grandTotal = number_format($addonTotal + $activityTotal + $eolFee + $charityTotal + $taxTotal, 2, '.', '');

    $summary["Summary"]["Grand Total"][" "][0] = $grandTotal;

    $returnData = array(
      "summary" => $summary,
      "values" => array(
        "total" => $grandTotal
        )
      );
    return $returnData;
  }

  public function get_activity_prices()
  {
    //TODO: Have this re-verified with test data and price flips
    $sql = "SELECT * FROM 
    (
      SELECT relPCD.dblPrice AS price, lkpA.strActivity AS activity, lkpE.strEvent AS event, relPCD.dttDateTill AS date,
      lkpE.intEventID AS event_id, lkpA.intActivityID AS activity_id
      FROM relPriceChangeDate AS relPCD
      INNER JOIN lkpEvent AS lkpE 
      ON relPCD.intEventID = lkpE.intEventID
      INNER JOIN lkpActivity AS lkpA 
      ON relPCD.intActivityID = lkpA.intActivityID
      WHERE relPCD.dttDateTill > NOW()
      AND relPCD.intActiveStatusID = 1
      ORDER BY DATE(relPCD.dttDateTill) ASC
    ) AS newestDates 
    GROUP BY newestDates.activity, newestDates.event;";

    $prices = array();
    try
    {
      $statement = $this->db->prepare($sql);
      $statement->execute();
      $rows = $statement->fetchAll(PDO::FETCH_ASSOC);

      foreach($rows as $row)
      {
        $array = array(
          "price" => $row['price'],
          "activity" => $row['activity'],
          "event" => $row['event']
          );
        $prices[$row['event_id']][$row['activity_id']] = $array;
      }
    }
    catch(Exception $e)
    {
      //TODO: Proper catching
    }

    return $prices;
  }

  public function get_addon_prices()
  {
    $sql = "SELECT lkpAd.strAddon AS addon, lkpAd.intAddonID AS addon_id, lkpE.strEvent AS event, lkpE.intEventID AS event_id, relA.dblPrice AS price FROM
relAddon AS relA
INNER JOIN lkpEvent AS lkpE
ON lkpE.intEventID = relA.intEventID
INNER JOIN lkpAddon AS lkpAd
ON relA.intAddonID = lkpAd.intAddonID
WHERE dttExpiration > NOW();";

    $prices = array();
    try
    {
      $statement = $this->db->prepare($sql);
      $statement->execute();
      $rows = $statement->fetchAll(PDO::FETCH_ASSOC);

      foreach($rows as $row)
      {
        $array = array(
          "price" => $row['price'],
          "addon" => $row['addon']
          );
        $prices[$row['event_id']][$row['addon_id']] = $array;
      }
    }
    catch(Exception $e)
    {
      //TODO: Proper catching
    }

    return $prices;
  }
}

/* End of file payment_model.php */
/* Location: ./application/models/payment_model.php */