<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Donation_model extends CI_Model {

    public function get_charities()
    {
        //Get all relevant event IDs in our active user's cart
        $allCharities = array();
        foreach($_SESSION['registration'][$_SESSION['registration']['active_user']]['activities'] as $eventActivity)
        {
            $eventIDs[] = reset(explode("_", $eventActivity));
        }

        //Check for artez charities
        $this->load->library("artez");
        $artezCharities = $this->artez->get_charities($eventIDs);

        foreach($artezCharities as $array)
        {
            $allCharities['artez'][$array['event_id']][] = array(
              "display" => $array['artez_charity_display'],
              "id" => $array['artez_charity_id']
              );
        }

        $this->load->library("aka");  
        $akaTeams = array();
        $akaTeams = $this->aka->get_teams($eventIDs);

        foreach($akaTeams as $eventID => $rows)
        {
          foreach($rows as $teamName)
          {
            $data = array(
              "display" => $teamName,
              "id" => $teamName
              );
            $allCharities['aka'][$eventID][] = $data;            
          }
        }


        //Check for our charities
        $this->load->library("donation");
        $eolCharities = $this->donation->get_charities($eventIDs);

        foreach($eolCharities as $array)
        {
          if($array['charity'] != "Princess Margaret Cancer Foundation")
          {
            $data = array(
              "display" => $array['charity'],
              "id" => $array['charity_id']
              );
            $allCharities['eol'][$array['event_id']][] = $data;
          }
        }

        return $allCharities;
    }

    public function get_form_sections($charities)
    {   
      $sqlprincessMargaret = "SELECT intCharityID AS charityID FROM lkpCharity WHERE strCharity = 'Princess Margaret Cancer Foundation';";
      $stmt = $this->db->prepare($sqlprincessMargaret);

      if($stmt->execute())
      {
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if($results)
        {
          $princessMargaretID = $results[0]['charityID'];
        }
      }
      else
      {

      }
        foreach($_SESSION['registration'][$_SESSION['registration']['active_user']]['activities'] as $eventActivity)
        {
          $eventID = reset(explode("_", $eventActivity));
          if(isset($charities['artez'][$eventID]))
          {
            if(array_key_exists($eventID, $charities['artez']))
            {
              $allCharities[$eventID]['artez'] = $charities['artez'][$eventID];
            }
          }

          if(isset($charities['eol']))
          {
            if(array_key_exists($eventID, $charities['eol']))
            {
              $allCharities[$eventID]['eol'] = $charities['eol'][$eventID];
            }                
          }

          if(isset($charities['aka']))
          {
            if(array_key_exists($eventID, $charities['aka']))
            {
              $allCharities[$eventID]['aka'] = $charities['aka'][$eventID];
            }              
          }
        }

		$tooniesCinqos = array(
			"" => "",
			2 => 2
		);

		for($i = 5; $i <= 200; $i += 5)
		{
			$tooniesCinqos[$i] = $i;
		}
		  
        if(isset($allCharities))
        {

          foreach($allCharities as $eventID => $charityTypes)
          {
              foreach($charityTypes as $type => $theseCharities)
              {
                $parsedArray = array("" => "");
                foreach($theseCharities as $id => $arr)
                {
                  $parsedArray[$arr["id"]] = $arr["display"];
                }

                switch($type)
                {
                  case "artez":
						        $display = translate("Scotiabank Charity Challenge");
                    $sections[$_SESSION['general'][$eventID]['display']." - ".$display] = array(
                      "Charity Pin Code" => array(
                        "type" => "text",
                        "name" => "artezdetails_artez_charity_pin_".$eventID,
                        "id" => "txtArtezCharityPin".$eventID,
                        "label" => "Charity Pin Code"
                        ),
                      "I would like to" => array(
                        "type" => "select",
                        "name" => "artezdetails_artez_charity_action_".$eventID,
                        "id" => "selArtezCharityAction".$eventID,
                        "label" => "I would like to",
                        "data" => array(
                          "" => "",
                          "fundraising" => "Create a fundraising account",
                          "donation" => "Make a one-time donation"
                          )
                        ),
                      "Charities" => array(
                        "type" => "select",
                        "name" => "artezdetails_artez_charity_".$eventID,
                        "id" => "inpArtezCharity".$eventID,
                        "label" => "Charities",
                        "data" => $parsedArray
                        )
                      );
                  break;

                  case "eol":
                    //Following code was taken from Core 1.6
                    $characters = array("1", "2", "3", "4", "5", "6", "7", "8", "9");
                    $passcodeValue = "";
                    for ($i = 0; $i < 5; $i++) {
                      $randomIndex = rand(0, 8);
                      $passcodeValue .= $characters[$randomIndex];
                      $randomIndex = "";
                    }
                    $profilePicFileName = $passcodeValue . "_" . microtime(true);

                    $sections[$_SESSION['general'][$eventID]['display']." - EventsOnline"] = array(
                      "Charity" => array(
                        "type" => "select",
                        "name" => "charitydetails_charity_".$eventID,
                        "id" => "inpCharity".$eventID,
                        'class' => 'donation-charity',
                        "label" => "Charity",
                        "data" => $parsedArray
                        ),
                      "Donation" => array(
                        "type" => "select",
                        "name" => "charitydetails_donation_".$eventID,
                        "id" => "inpDonation".$eventID,
                        "label" => "Donation",
                        'class' => 'donation-charity-amount',
                        "data" => $tooniesCinqos
                        ),
                      "Fundraising account" => array(
                        "type" => "select",
                        "name" => "charitydetails_fundraising_profile_".$eventID,
                        "id" => "selFundraisingProfile".$eventID,
                        "label" => "Create a fundraising profile?",
                        "class" => "eol-fundraising-account",
                        "data" => array(
                          "" => "No",
                          "Yes" => "Yes"
                          )
                        ),
                      "Fundraising image" => array(
                        "type" => "hidden",
                        "name" => "charitydetails_fundraising_image_".$eventID,
                        "data" => $profilePicFileName
                        ),
                      "Fundraising image upload" => array(
                        "type" => 'iframe',
                        'name' => 'iframe',
                        "id" => "iframeProfileImg_".$eventID,
                        'url' => "https://secure.eventsonline.ca/profile_uploads/pledge_profile/default/_upload.php?filename=".$profilePicFileName."&btnTitle=Profile"
                        ),
                      "Fundraising Goal" => array(
                        "type" => "text",
                        "name" => "charitydetails_fundraising_goal_".$eventID,
                        "id" => "inpFundraisingGoal".$eventID,
                        "label" => "Fundraising Goal",
                        ),
                      "Fundraising Message" => array(
                        "type" => "textarea",
                        "name" => "charitydetails_fundraising_message_".$eventID,
                        "id" => "txtFundraisingMessage".$eventID,
                        "label" => "Fundraising Message"
                        )
                      );
                  break;

                  case "aka":
                    $sections[$_SESSION['general'][$eventID]['display']." - Princess Margaret Cancer Foundation"] = array(
                      "Donation Option 0" => array(
                        "type" => "radio",
                        "name" => "aka_fundraising_option_".$eventID,
                        "id" => "inpAkaFundOpt0",
                        "class" => "aka-selection",
                        "row-before" => "<img class=\"img-responsive\" src=\"".base_url('assets/img/princessmarg.png')."\" title=\"Princess Margaret Cancer Foundation\" />",
                        "data" => array(
                          "" => "I don't want to raise funds."
                          )
                        ),
                      "Donation Option 1" => array(
                        "type" => "radio",
                        "name" => "aka_fundraising_option_".$eventID,
                        "id" => "inpAkaFundOpt1",
                        "class" => "aka-selection",
                        "data" => array(
                          "Run For Free" => "Joining the 'Run For Free' program and raising $300 by a specific date to receive a refund on my race entry from The Princess Margaret *Restrictions apply, please see <a href=\"http://www.canadarunningseries.com/springrunoff/csroCHARITY.htm#run-for-free\" target=\"_blank\">Run For Free</a> contest for details."
                          )
                        ),
                      "Donation Option 2" => array(
                        "type" => "radio",
                        "name" => "aka_fundraising_option_".$eventID,
                        "id" => "inpAkaFundOpt2",
                        "class" => "aka-selection",
                        "data" => array(
                          "Fundraising" => "Fundraising with no minimums for The Princess Margaret and supporting Personalized Prostate Cancer Medicine"
                          )
                        ),
                      "Teams" => array(
                        "type" => "select",
                        "name" => "aka_team_name_".$eventID,
                        "id" => "selAkaTeamName",
                        "label" => "Fundraising Team Name",
                        "class" => "aka-teams",
                        "data" => $parsedArray
                        ),
                      "Learn more" => array(
                        "type" => "checkbox",
                        "name" => "aka_prostate_cancer_info_".$eventID,
                        "id" => "rdoLearnProstate",
                        "data" => array(
                          "yes" => "Learning more about Prostate Cancer breakthroughs and The Princess Margaret"
                          ),
                        "isArray" => FALSE
                        ),
                      "OneTime Donation Charity" => array(
                        "type" => "hidden",
                        "name" => "charitydetails_charity_".$eventID,
                        "class" => "aka-donation-charity",
                        "data" => $princessMargaretID
                        ),
                      "OneTime Donation" => array(
                        "type" => "select",
                        "name" => "charitydetails_donation_".$eventID,
                        "id" => "selAkaDonation".$eventID,
                        "label" => "Donating to The Princess Margaret Cancer",
                        "class" => "aka-donation",
                        "data" => $tooniesCinqos
                        )
                      );
                  break;
                }
              }
          }
        }

        $sections["Series Foundation"] = array(
          "Donation Amount Toonies Cinqos" => array(
            "type" => "select",
            "name" => "seriesfoundation_donation",
            "id" => "selTooniesCinqos",
            "label" => "Donation Amount",
            "row-before" => "<img class=\"img-responsive\" src=\"".base_url('assets/img/eol_logo_sm.png')."\" title=\"EOL Foundation\" /><br /><p>".translate("Help us promote healthy lifestyles and give back to the communities we run through. Tax receipts will be issued for donations of $25 or more."),
            "data" => $tooniesCinqos
            )
          );

        return $sections;
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */