<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax_model extends CI_Model {

	public function changeAdminPassword($adminAccountID,$newPassword) {
		$retValue = true;
		
		$encryptNewPassword = b_encrypt($newPassword);
		
		try {
			$sql = "UPDATE entAdminAccount SET strAdminPassword = '" . $encryptNewPassword . "'	WHERE intAdminAccountID = " . $adminAccountID;
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		return $retValue;
	}

	public function validate_count_group_shirts()
	{

		//Get the count of all participant shirts that have been taken so far based on event and activity
	  $sql1 = "SELECT COUNT(intShirtID) AS aCount, intShirtID, intEventID, intActivityID FROM relParticipantShirt GROUP BY intShirtID, intEventID, intActivityID";
		$sql2 = "SELECT ls.intShirtID, ls.strShirtDescription, rs.intShirtCap, intEventID, intActivityID FROM relShirt AS rs
    INNER JOIN lkpShirt AS ls ON ls.intShirtID = rs.intShirtID ORDER BY ls.intShirtID";

	  $parsedArray = array();
	  $stmt1 = $this->db->prepare($sql1);
	  if($stmt1->execute())
	  {
	  	$results1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
	  	foreach($results1 as $row)
	  	{
	  		$parsedArray[$row['intEventID']][$row['intActivityID']][$row['intShirtID']] = $row['aCount'];
	  	}
	  }

	  $parsedArray2 = array();
	  $stmt2 = $this->db->prepare($sql2);
	  if($stmt2->execute())
	  {
	  	$results2 = $stmt2->fetchAll(PDO::FETCH_ASSOC);
	  	foreach($results2 as $row)
	  	{
	  		$parsedArray2[$row['intEventID']][$row['intActivityID']][$row['intShirtID']]['cap'] = $row['intShirtCap'];
	  		$parsedArray2[$row['intEventID']][$row['intActivityID']][$row['intShirtID']]['description'] = $row['strShirtDescription'];
	  	}
	  }

	  $shirtsToValidate = $this->input->post('shirts');

	  //Get counts of shirts within session
	  $tempSess = $_SESSION['registration'];
	  unset($tempSess[$tempSess['active_user']]);
	  unset($tempSess['active_user']);
	  unset($tempSess['last_modified']);

	  $thisSessShirts = array();
	  if($_SESSION['type'] == 'group')
	  {
		  foreach($tempSess as $id => $participant)
		  {
		  	if(isset($participant['activities']))
		  	{
		  		$thisParticipantsEvents = array();
			  	foreach($participant['activities'] as $eA)
			  	{
			  		$expl1 = explode('_', $eA);
			  		$thisParticipantsEvents[$expl1[0]] = $expl1[1];
			  		$eID = $expl1[0];
			  		$aID = $expl1[1];
			  	}

			  	if(isset($participant['group_details']))
			  	{
			  		foreach($participant['group_details'] as $member)
			  		{
					  	if(isset($member['eventextras']))
					  	{
					  		if(isset($member['eventextras']['tshirt']))
					  		{
					  			if(!isset($thisSessShirts[$eID][$aID][$member['eventextras']['tshirt']]))
					  				$thisSessShirts[$eID][$aID][$member['eventextras']['tshirt']] = 0;

					  			$thisSessShirts[$eID][$aID][$member['eventextras']['tshirt']]++;
					  		}
					  	}			  			
			  		}
			  	}
		  	}
		  }
	  }

		$invalidShirts = array();

		foreach($_SESSION['registration'][$_SESSION['registration']['active_user']]['activities'] AS $eventActivity)
		{
			$expl = explode('_', $eventActivity);
			$thisAID = end($expl);
			$thisEID = reset($expl);
			$currentParticipantsEvents[$thisEID] = $thisAID;

			foreach($shirtsToValidate as $shirtID => $qty)
			{
				$sessionShirts = 0;
				if(isset($thisSessShirts[$thisEID][$thisAID][$shirtID]))
				{
					$sessionShirts = $thisSessShirts[$thisEID][$thisAID][$shirtID];
				}

				$purchasedShirts = 0;
				if(isset($parsedArray[$thisEID][$thisAID][$shirtID]))
				{
					$purchasedShirts = $parsedArray[$thisEID][$thisAID][$shirtID];
				}

				if($parsedArray2[$thisEID][$thisAID][$shirtID]['cap'] >= 0)
				{
					if(($sessionShirts + $purchasedShirts + $qty) > $parsedArray2[$thisEID][$thisAID][$shirtID]['cap'])
					{
						$diff = ($sessionShirts + $purchasedShirts + $qty) - $parsedArray2[$thisEID][$thisAID][$shirtID]['cap'];

						if(!isset($invalidShirts[$parsedArray2[$thisEID][$thisAID][$shirtID]['description']]))
						{
							$invalidShirts[$parsedArray2[$thisEID][$thisAID][$shirtID]['description']] = 0;
						}

						$invalidShirts[$parsedArray2[$thisEID][$thisAID][$shirtID]['description']] += $diff;
					}
				}
			}
		}

		if($invalidShirts)
		{
			echo json_encode($invalidShirts);
			exit;
		}
		else
		{
			echo TRUE;
			exit;
		}
	}

	public function validate_discount_code($code = NULL)
	{
		if($code === NULL) return FALSE;

		$sql = "SELECT entdc.intDiscountCodeID AS codeID, reldc.intEventID, reldc.intActivityID, entdc.intQuantity AS qty, lkpa.strActivity AS activity FROM entDiscountCode AS entdc
		INNER JOIN relDiscountCode AS reldc
		ON entdc.intDiscountCodeID = reldc.intDiscountCodeID
		INNER JOIN lkpActiveStatus AS lkpas
		ON lkpas.intActiveStatusID = reldc.intActiveStatusID
		INNER JOIN lkpActivity AS lkpa
		ON reldc.intActivityID = lkpa.intActivityID
		WHERE DATE (entdc.dttExpiration) > DATE(NOW()) AND
		lkpas.strActiveStatus = 'Active' AND 
		entdc.strDiscountCode = :code;";

		$stmt = $this->db->prepare($sql);

		$dataToExecute = array(
			"code" => $code
			);

		if($stmt->execute($dataToExecute))
		{
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

			if($results)
			{
				$discountCode = $results[0];

				$sql1 = "SELECT COUNT(*) AS count FROM relRegistrationTransaction AS rrt
				INNER JOIN entDiscountCode AS ed 
				ON ed.intDiscountCodeID = rrt.intDiscountCodeID 
				WHERE rrt.strRefundStatus IS NULL AND ed.strDiscountCode = :code";

				$stmt1 = $this->db->prepare($sql1);
				if($stmt1->execute($dataToExecute))
				{
					$results1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
					$codeCount = $results1[0];

					//Add session discount codes to count if applicable
					$tempSess = $_SESSION['registration'];
					unset($tempSess[$tempSess['active_user']]);
					unset($tempSess['active_user']);
					unset($tempSess['last_modified']);

					foreach($tempSess as $id => $participant)
					{
						if(isset($participant['discount_code']))
						{
							if($participant['discount_code'] == $discountCode['codeID'])
							{
								$codeCount['count']++;
							}
						}
					}

					if($codeCount['count'] < $discountCode['qty'])
					{
						//Make sure we selected the event/activity corresponding to the discount
						$eventsOfUser = array();
						foreach($_SESSION['registration'][$_SESSION['registration']['active_user']]['activities'] as $eA)
						{
							$expl1 = explode('_', $eA);
							$eventsOfUser[] = reset($expl1);
						}

						if($discountCode['activity'] == "All" && in_array($discountCode['intEventID'], $eventsOfUser))
						{
							$_SESSION['registration'][$_SESSION['registration']['active_user']]['discount_code'] = $discountCode['codeID'];
							echo TRUE;
							exit;
						}

						if(in_array($discountCode['intEventID']."_".$discountCode['intActivityID'], $_SESSION['registration'][$_SESSION['registration']['active_user']]['activities']))
						{
							$_SESSION['registration'][$_SESSION['registration']['active_user']]['discount_code'] = $discountCode['codeID'];
							echo TRUE;
							exit;
						}
						else
						{
							echo FALSE;
							exit;
						}
					}
					else
					{
						echo FALSE;
						exit;
					}
				}
				else
				{
					//TODO: Alert - database query failed
					echo FALSE;
				}

				if($discountCode)
				$_SESSION['registration'][$_SESSION['registration']['active_user']]['discount_code'] = $results[0]['codeID'];
				echo TRUE;
				exit;
			}
		}
		else
		{
			//TODO: Alert - database query failed
			echo FALSE;
			exit;			
		}

	}
	
	public function changeAdminAccountDetails() {
		$retValue = true;
		
		$aPost = $this->input->sanitizeForDbPost();
		
		try {
			$sql = "UPDATE entAdminAccount SET strAdminName = '" . $aPost['name'] . "', strAdminUserName = '" . $aPost['email'] . "', dttDateModified = NOW() WHERE intAdminAccountID = " . $aPost['id'];
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
			
		return $retValue;
	}
	
	public function getActivityByPID() {
		$retArray = array();
		
		$aPost = $this->input->sanitizeForDbPost();
		$aSession = $this->session->userdata('admin');
		
		try {
			$sql = "SELECT b.intActivityID, c.strActivity FROM relAccountPersonalInformation AS a
				INNER JOIN relAccountActivity AS b ON b.intAccountID = a.intAccountID AND b.intEventID = " . $aSession['adminEventID'] . "
				INNER JOIN lkpActivity AS c ON c.intActivityID = b.intActivityID
				WHERE a.intPersonalInformationID = " . $aPost['aPID'] . " AND b.strStatusChange IS NULL";
				
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			$retArray = $result;
		}
		
		return $retArray;
	}
	
	public function getActivityByEID($eventID) {
		$retArray = array();
		
		if ($eventID == "") {
			$eventID = 0;
		}
				
		try {
			$sql = "SELECT la.intActivityID, la.strActivity FROM relEventActivityCap AS reac
				INNER JOIN lkpActivity AS la ON la.intActivityID = reac.intActivityID
				WHERE reac.intEventID = " . $eventID;
			
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			$retArray = $result;
		}
		
		return $retArray;
	}
	
	public function setTransferCode($aCode) {
		$aSession = $this->session->userdata('participant');
		
		try {
			$sql = "UPDATE relAccountActivity SET 
				strTransferCode = '" . $aCode . "',
				dttDateModified = NOW()	
				WHERE intAccountID = " . $aSession['participantAccountID'] . " AND intEventID = " . $aSession['participantEventID'] . " AND strStatusChange IS NULL";
							
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
	}
	
	//Generates the shirt dropdown for change_activity in the participant dashboard
	public function generateShirtDropdown() {
		$retString = "<label for=\"new_shirt\">" . translate("Technical T-shirt") . "</label>
			<select id='new_shirt' name='new_shirt' class=\"form-control required\">
				<option value=\"\">" . translate("Select") . "</option>";
		
		$aSession = $this->session->userdata('participant');
		$aPost = $this->input->sanitizeForDbPost();
		
		$getShirts = $this->getShirtByEventActivity($aSession['participantEventID'],$aPost['activityID']);
		foreach ($getShirts as $shirtID => $shirtDesc) {
			$retString .= "<option value=\"" . $shirtID . "\">" . $shirtDesc . "</option>";
		}
	
		$retString .= "</select>";
		
		echo $retString;
	}
	
	//Gets the count of the number of addons already purchased.  Used in the participant dashboard "Purchase extra items" feature
	public function getAddonPurchasedCount($addonID) {
		$retValue = array();
		
		$aSession = $this->session->userdata('participant');
		
		try {
			$sql = "SELECT SUM(intQty) AS aSum, intAddonID FROM relAddonTransaction WHERE strRefundStatus IS NULL AND intEventID = " . $aSession['participantEventID'] . " AND intAddonID = " . $addonID . " AND intAccountID = " . $aSession['participantAccountID'] . " GROUP BY intAddonID";
	
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			$retValue = ((isset($result[0]['aSum']))?$result[0]['aSum']:0);
		}
		
		return $retValue;
	}
	
	//Gets the count of all addons already purchased by all participants.  Used in the participant dashboard "Purchase extra items" feature
	public function getAllAddonPurchasedCount($addonID) {
		$retValue = array();
		
		$aSession = $this->session->userdata('participant');
		
		try {
			$sql = "SELECT SUM(intQty) AS aSum, intAddonID FROM relAddonTransaction WHERE strRefundStatus IS NULL AND intEventID = " . $aSession['participantEventID'] . " AND intAddonID = " . $addonID . " GROUP BY intAddonID";
	
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		if (isset($result) && count($result) > 0) {
			$retValue = ((isset($result[0]['aSum']))?$result[0]['aSum']:0);
		}
		
		return $retValue;
	}
	
	public function generateShirtOptions() {		
		$retString = "<option value=\"\">" . translate("Select") . "</option>";
		
		$aSession = $this->session->userdata('admin');
		$aPost = $this->input->sanitizeForDbPost();
		
		$getShirts = $this->getShirtByEventActivity($aSession['adminEventID'],$aPost['activityID']);
		foreach ($getShirts as $shirtID => $shirtDesc) {
			$retString .= "<option value=\"" . $shirtID . "\">" . $shirtDesc . "</option>";
		}
			
		echo $retString;
	}
	
	public function getShirtByEventActivity($eventID,$activityID) {
		$arrShirtCount = array();
		$retArray = array();
		
		//Check for all shirts that have already been chosen
		try {
			$sql = "SELECT COUNT(intShirtID) AS aCount, intShirtID FROM relParticipantShirt WHERE intEventID = " . $eventID . " and intActivityID = " . $activityID . " GROUP BY intShirtID";
									
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}

		if (isset($result) && count($result) > 0) {			
			$countResult = count($result);
			for ($i = 0; $i < $countResult; $i++) {
				$arrShirtCount[$result[$i]['intShirtID']] = $result[$i]['aCount'];
			}
		}
		
		//Get all shirt information including caps
		try {
			$sql = "SELECT ls.intShirtID, ls.strShirtDescription, rs.intShirtCap FROM relShirt AS rs
				INNER JOIN lkpShirt AS ls ON ls.intShirtID = rs.intShirtID WHERE rs.intEventID = " . $eventID . " AND rs.intActivityID = " . $activityID . " ORDER BY ls.intShirtID";
									
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		//Calculate if shirt should be output based on cap check
		if (isset($result) && count($result) > 0) {			
			$countResult = count($result);
			for ($i = 0; $i < $countResult; $i++) {
				$shirtID = $result[$i]['intShirtID'];
				$shirtCap = $result[$i]['intShirtCap'];
				
				$numShirtTaken = ((isset($arrShirtCount[$shirtID]))?$arrShirtCount[$shirtID]:0);
				if (($shirtCap - $numShirtTaken) > 0) {					
					$retArray[$shirtID] = $result[$i]['strShirtDescription'];
				}
			}
		}
		
		return $retArray;
	}
}
