<?php 

if (!defined('BASEPATH')) exit('No direct script access allowed');

// Original PHP code by Chirp Internet: www.chirp.com.au 
// Blowfish Encryption
function b_encrypt($input, $rounds = 7) { 
	$salt = ""; 
	$salt_chars = array_merge(range('A','Z'), range('a','z'), range(0,9)); 
	for($i=0; $i < 22; $i++) { 
		$salt .= $salt_chars[array_rand($salt_chars)]; 
	} 
	
	return crypt($input, sprintf('$2a$%02d$', $rounds) . $salt); 
}

if(!function_exists("in_array_r"))
{
  function in_array_r($needle, $haystack, $strict = false) 
  {
    foreach ($haystack as $item) 
    {
      if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) 
      {
        return true;
      }
    }

    return false;
  }
}

if(!function_exists("array_exists_recursive"))
{
	function array_exists_recursive($key, $value, $haystack, $strict = false)
	{
		foreach($haystack as $k => $v)
		{
			if(($strict ? $key === $k && $value === $v : $key == $k && $value == $v) || is_array($v) && array_exists_recursive($key, $value, $v, $strict))
			{
				return TRUE;
			}
		}
		return FALSE;
	}
}

if(!function_exists("get_session_events"))
{
  function get_session_events()
  {
    $eventIDs = array();

    foreach($_SESSION['registration'][$_SESSION['registration']['active_user']]['activities'] as $eventActivity)
    {
        $eventIDs[] = reset(explode("_", $eventActivity));
    }
    
    return $eventIDs;
  }
}

function drawHiddenField($fieldName,$fieldValue) {
	$anElement =  "<input type=\"hidden\" id=\"" . $fieldName . "\" name=\"" . $fieldName . "\" value=\"" . $fieldValue . "\" />";
	
	return $anElement;
}

if(!function_exists("calculate_eol_fee"))
{
  function calculate_eol_fee($amount = NULL)
  {
  	if($amount === NULL) $fee = NULL;
    if($amount == 0) $fee = 0;
  	if($amount > 0 && $amount <= 20) $fee = 2.50;
  	if($amount > 20 && $amount <= 30) $fee = 3;
  	if($amount > 30 && $amount <= 40) $fee = 3.5;
  	if($amount > 40 && $amount <= 50) $fee = 4;
  	if($amount > 50 && $amount <= 60) $fee = 4.5;
  	if($amount > 60 && $amount <= 70) $fee = 5;
  	if($amount > 70 && $amount <= 80) $fee = 5.5;
  	if($amount > 80 && $amount <= 90) $fee = 6;
  	if($amount > 90 && $amount <= 100) $fee = 6.5;
  	if($amount > 100) $fee = ($amount * 0.065);

  	return $fee;
  }
}

function drawTextField($fieldName,$fieldValue,$required,$readOnlyFlag) {
	$anElement =  "<input class=\"form-control " . $required . "\" type=\"text\" id=\"" . $fieldName . "\" name=\"" . $fieldName . "\" value=\"" . $fieldValue . "\" " . $readOnlyFlag . " />";
	
	return $anElement;
}

function drawSelectField($fieldName,$fieldValue,$required,$optionArray) {
	$anElement =  "<select class=\"form-control " . $required . "\" id=\"" . $fieldName . "\" name=\"" . $fieldName . "\">";
	foreach ($optionArray as $optionValue => $optionDisplay) {
		$anElement .= "<option value=\"" . $optionValue . "\" " . ((strtolower($optionValue) == strtolower($fieldValue))?'selected':'') . ">" . $optionDisplay . "</option>";
	}
	$anElement .= "</select>";
	
	return $anElement;
}

function drawCheckboxField($fieldName,$fieldValue,$required,$optionArray) {
	$arrValues = explode("|",$fieldValue);
	$anElement = "";
	foreach ($optionArray as $optionValue => $optionDisplay) {
		$anElement .= "<input type=\"checkbox\" id=\"" . $fieldName . "[]\" name=\"" . $fieldName . "[]\" class=\"" . $required . "\" value=\"" . $optionValue . "\" " . ((in_array($optionValue,$arrValues))?'checked=checked':'') . ">&nbsp;" . $optionDisplay . "<br />";
	}
	
	return $anElement;
}

function drawEditForm($anArray) {
	foreach ($anArray as $fieldName => $fieldDetails) {
		if (stripos($fieldName,"heading") === 0) {
			echo "</table><table class=\"table table-striped table-hover\"><tr><td colspan=\"3\" class=\"well well-sm text-center\">" . translate($fieldDetails['label']) . "</td></tr>";
		}
		else {
			echo "
			<tr>
				<td>" . ((trim($fieldDetails['label']) == "")?'':translate($fieldDetails['label'])) . "</td>
				<td>";
				if ($fieldDetails['type'] == "onlytext") {
					echo $fieldDetails['value'];
				}
				else if ($fieldDetails['type'] == "hidden") {
					echo drawHiddenField($fieldName, $fieldDetails['value']);
				}
				else if ($fieldDetails['type'] == "text") {
					$readOnlyFlag = ((isset($fieldDetails['readonly']))?$fieldDetails['readonly']:"");
					echo drawTextField($fieldName, $fieldDetails['value'], $fieldDetails['required'], $readOnlyFlag);
				}
				else if ($fieldDetails['type'] == "select") {
					echo drawSelectField($fieldName, $fieldDetails['value'], $fieldDetails['required'], $fieldDetails['data']);
				}
				else if ($fieldDetails['type'] == "checkbox") {
					echo drawCheckboxField($fieldName, $fieldDetails['value'], $fieldDetails['required'], $fieldDetails['data']);
				}
			echo "
				</td>
				<td>" . ((isset($fieldDetails['previousValue']))?$fieldDetails['previousValue']:$fieldDetails['value']) . "</td>
			</tr>";
		}
	}
}

function escapeString($aValue) {
	if (!is_array($aValue)) {
		$aValue = @mysql_escape_string($aValue);
	}
	return $aValue;
}

function getProvinceState() {	
	$retArray = array(
		"Ontario" => "Ontario",
		"Alberta" => "Alberta",
		"British Columbia" => "British Columbia",
		"Manitoba" => "Manitoba",
		"New Brunswick" => "New Brunswick",
		"Newfoundland And Labrador" => "Newfoundland And Labrador",
		"Northwest Territories" => "Northwest Territories",
		"Nova Scotia" => "Nova Scotia",
		"Nunavut" => "Nunavut",
		"Prince Edward Island" => "Prince Edward Island",
		"Quebec" => "Quebec",
		"Saskatchewan" => "Saskatchewan",
		"Yukon" => "Yukon", 
		"Alabama" => "Alabama",
		"Alaska" => "Alaska",
		"Arizona" => "Arizona",
		"Arkansas" => "Arkansas",
		"California" => "California",
		"Colorado" => "Colorado",
		"Connecticut" => "Connecticut",
		"Delaware" => "Delaware",
		"Florida" => "Florida",
		"Georgia" => "Georgia",
		"Hawaii" => "Hawaii",
		"Idaho" => "Idaho",
		"Illinois" => "Illinois",
		"Indiana" => "Indiana",
		"Iowa" => "Iowa",
		"Kansas" => "Kansas",
		"Kentucky" => "Kentucky",
		"Louisiana" => "Louisiana",
		"Maine" => "Maine",
		"Maryland" => "Maryland",
		"Massachusetts" => "Massachusetts",
		"Michigan" => "Michigan",
		"Minnesota" => "Minnesota",
		"Mississippi" => "Mississippi",
		"Missouri" => "Missouri",
		"Montana" => "Montana",
		"Nebraska" => "Nebraska",
		"Nevada" => "Nevada",
		"New Hampshire" => "New Hampshire",
		"New Jersey" => "New Jersey",
		"New Mexico" => "New Mexico",
		"New York" => "New York",
		"North Carolina" => "North Carolina",
		"North Dakota" => "North Dakota",
		"Ohio" => "Ohio",
		"Oklahoma" => "Oklahoma",
		"Oregon" => "Oregon",
		"Pennsylvania" => "Pennsylvania",
		"Puerto Rico " => "Puerto Rico ",
		"Rhode Island" => "Rhode Island",
		"South Carolina" => "South Carolina",
		"South Dakota" => "South Dakota",
		"Tennessee" => "Tennessee",
		"Texas" => "Texas",
		"Utah" => "Utah",
		"Vermont" => "Vermont",
		"Virginia" => "Virginia",
		"Washington" => "Washington",
		"West Virginia" => "West Virginia",
		"Wisconsin" => "Wisconsin",
		"Wyoming" => "Wyoming"
	);
	
	return $retArray;
}

function getCountry() {
	//$retArray = array("CAN" => "Canada", "USA" => "United States of America");
	$retArray = array("CAN" => "Canada", 
				"USA" => "United States", 
				"ALB" => "Albania", 
				"DZA" => "Algeria", 
				"AND" => "Andorra", 
				"AGO" => "Angola", 
				"AIA" => "Anguilla", 
				"ATG" => "Antigua and Barbuda", 
				"ARG" => "Argentina", 
				"ARM" => "Armenia", 
				"ABW" => "Aruba", 
				"AUT" => "Austria", 
				"AUS" => "Australia", 
				"AZE" => "Azerbaijan Republic", 
				"BHS" => "Bahamas", 
				"BHR" => "Bahrain", 
				"BRB" => "Barbados", 
				"BEL" => "Belgium", 
				"BLZ" => "Belize", 
				"BEN" => "Benin", 
				"BMU" => "Bermuda", 
				"BTN" => "Bhutan", 
				"BOL" => "Bolivia", 
				"BIH" => "Bosnia and Herzegovina", 
				"BWA" => "Botswana", 
				"BRA" => "Brazil", 
				"VGB" => "British Virgin Islands", 
				"BRN" => "Brunei", 
				"BGR" => "Bulgaria", 
				"BFA" => "Burkina Faso", 
				"BDI" => "Burundi", 
				"KHM" => "Cambodia", 
				"CPV" => "Cape Verde", 
				"CYM" => "Cayman Islands", 
				"TCD" => "Chad", 
				"CHL" => "Chile", 
				"CHN" => "China Worldwide", 
				"COL" => "Colombia", 
				"COM" => "Comoros", 
				"COK" => "Cook Islands", 
				"CRI" => "Costa Rica", 
				"HRV" => "Croatia", 
				"CYP" => "Cyprus", 
				"CZE" => "Czech Republic", 
				"COD" => "Democratic Republic of the Congo", 
				"DNK" => "Denmark", 
				"DJI" => "Djibouti", 
				"DMA" => "Dominica", 
				"DOM" => "Dominican Republic", 
				"ECU" => "Ecuador", 
				"SLV" => "El Salvador", 
				"ERI" => "Eritrea", 
				"EST" => "Estonia", 
				"ETH" => "Ethiopia", 
				"FLK" => "Falkland Islands", 
				"FRO" => "Faroe Islands", 
				"FSM" => "Federated States of Micronesia", 
				"FJI" => "Fiji", 
				"FIN" => "Finland", 
				"FRA" => "France", 
				"GUF" => "French Guiana", 
				"PYF" => "French Polynesia", 
				"GAB" => "Gabon Republic", 
				"GMB" => "Gambia", 
				"DEU" => "Germany", 
				"GIB" => "Gibraltar", 
				"GRC" => "Greece", 
				"GRL" => "Greenland", 
				"GRD" => "Grenada", 
				"GLP" => "Guadeloupe", 
				"GTM" => "Guatemala", 
				"GIN" => "Guinea", 
				"GNB" => "Guinea Bissau", 
				"GUY" => "Guyana", 
				"HND" => "Honduras", 
				"HKG" => "Hong Kong", 
				"HUN" => "Hungary", 
				"ISL" => "Iceland", 
				"IDN" => "Indonesia", 
				"IRL" => "Ireland", 
				"ISR" => "Israel", 
				"ITA" => "Italy", 
				"JAM" => "Jamaica", 
				"JPN" => "Japan", 
				"JOR" => "Jordan", 
				"KAZ" => "Kazakhstan", 
				"KEN" => "Kenya", 
				"KIR" => "Kiribati", 
				"KWT" => "Kuwait", 
				"KGZ" => "Kyrgyzstan", 
				"LAO" => "Laos", 
				"LVA" => "Latvia", 
				"LSO" => "Lesotho", 
				"LIE" => "Liechtenstein", 
				"LTU" => "Lithuania", 
				"LUX" => "Luxembourg", 
				"MDG" => "Madagascar", 
				"MWI" => "Malawi", 
				"MYS" => "Malaysia", 
				"MDV" => "Maldives", 
				"MLI" => "Mali", 
				"MLT" => "Malta", 
				"MHL" => "Marshall Islands", 
				"MTQ" => "Martinique", 
				"MRT" => "Mauritania", 
				"MUS" => "Mauritius", 
				"MYT" => "Mayotte", 
				"MEX" => "Mexico", 
				"MNG" => "Mongolia", 
				"MSR" => "Montserrat", 
				"MAR" => "Morocco", 
				"MOZ" => "Mozambique", 
				"NAM" => "Namibia", 
				"NRU" => "Nauru", 
				"NPL" => "Nepal", 
				"NLD" => "Netherlands", 
				"ANT" => "Netherlands Antilles", 
				"NCL" => "New Caledonia", 
				"NZL" => "New Zealand", 
				"NIC" => "Nicaragua", 
				"NER" => "Niger", 
				"NIU" => "Niue", 
				"NFK" => "Norfolk Island", 
				"NOR" => "Norway", 
				"OMN" => "Oman", 
				"PLW" => "Palau", 
				"PAN" => "Panama", 
				"PNG" => "Papua New Guinea", 
				"PER" => "Peru", 
				"PHL" => "Philippines", 
				"PCN" => "Pitcairn Islands", 
				"POL" => "Poland", 
				"PRT" => "Portugal", 
				"QAT" => "Qatar", 
				"COD" => "Republic of the Congo", 
				"REU" => "Reunion", 
				"ROM" => "Romania", 
				"RUS" => "Russia", 
				"RWA" => "Rwanda", 
				"VCT" => "Saint Vincent and the Grenadines", 
				"WSM" => "Samoa", 
				"SMR" => "San Marino", 
				"STP" => "S?o Tom? and Pr?ncipe", 
				"SAU" => "Saudi Arabia", 
				"SEN" => "Senegal", 
				"SYC" => "Seychelles", 
				"SLE" => "Sierra Leone", 
				"SGP" => "Singapore", 
				"SVK" => "Slovakia", 
				"SVN" => "Slovenia", 
				"SLB" => "Solomon Islands", 
				"SOM" => "Somalia", 
				"ZAF" => "South Africa", 
				"KOR" => "South Korea", 
				"ESP" => "Spain", 
				"LKA" => "Sri Lanka", 
				"SHN" => "St. Helena", 
				"KNA" => "St. Kitts and Nevis", 
				"LCA" => "St. Lucia", 
				"SPM" => "St. Pierre and Miquelon", 
				"SUR" => "Suriname", 
				"SJM" => "Svalbard and Jan Mayen Islands", 
				"SWZ" => "Swaziland", 
				"SWE" => "Sweden", 
				"CHE" => "Switzerland", 
				"TWN" => "Taiwan", 
				"TJK" => "Tajikistan", 
				"TZA" => "Tanzania", 
				"THA" => "Thailand", 
				"TGO" => "Togo", 
				"TON" => "Tonga", 
				"TTO" => "Trinidad and Tobago", 
				"TUN" => "Tunisia", 
				"TUR" => "Turkey", 
				"TKM" => "Turkmenistan", 
				"TCA" => "Turks and Caicos Islands", 
				"TUV" => "Tuvalu", 
				"UGA" => "Uganda", 
				"UKR" => "Ukraine", 
				"GBR" => "United Kingdom", 
				"ARE" => "United Arab Emirates", 
				"URY" => "Uruguay", 
				"VUT" => "Vanuatu", 
				"VAT" => "Vatican City State", 
				"VEN" => "Venezuela", 
				"VNM" => "Vietnam", 
				"WLF" => "Wallis and Futuna Islands", 
				"YEM" => "Yemen", 
				"ZMB" => "Zambia");
	
	return $retArray;
}

function processError($sql = "") {
	$subject = "Series Fatal Error";
	$message = "A Fatal Error has occured at " . site_url() . uri_string() . " \r\n\r\n";
	if ($sql != "") {
		$message .= "<br /><br />QUERY:<br />" . $sql;
	}
	sendErrorEmail($subject,$message);
	
	$errorMsg = translate(ERROR_MSG);
	show_error($errorMsg);
	exit;
}

function sendErrorEmail($subject,$message) {
	$toEmail = EMAIL_ERROR_RECIPIENT;

	$CI =& get_instance();
    $CI->load->library('email');

	$config['mailtype'] = "html";
	$config['charset'] = "utf-8";
	$CI->email->initialize($config);
	$CI->email->from('Eventsonline.ca');
	$CI->email->to($toEmail);
	$CI->email->cc('hwong@eventsonline.ca'); 

	$CI->email->subject($subject);
	$CI->email->message($message);

	$CI->email->send();
}

//Both dates must be in the format of Y-m-d (e.g. 1975-12-25)
function calculateAge($aDate,$birthDate) {
	$age = 0;
	$aDate = explode('-', $aDate);
	$birthDate = explode('-', $birthDate);
    
    $aYear = $aDate[0];
    $aMonth = $aDate[1];
    $aDay = $aDate[2];
	
	$birthYear = $birthDate[0];
	$birthMonth = $birthDate[1];
	$birthDay = $birthDate[2];
	
    $age = $aYear - $birthYear;
    if (($aMonth < $birthMonth) || ($aMonth == $birthMonth && $aDay < $birthDay)) {
        $age--;
    }
	
    return $age;
}

function displayJsNoScript() {
    echo '
			<br /><br />
			<noscript>
				<center>
				<div style="border: 2px solid red; width: 600px; background-color:#FF8181; padding: 5px">
					<br />
					<img src="' . base_url("assets/img/small_warning.png") . '" />
					<br /><b>
					<h4 style="text-decoration: underline;">' . translate("K359") . '</h4>' . translate("K360") . '<span style="text-decoration: underline;">' . translate("K361") . '</span>. <br />' . translate("K362") . '
					please <a href="mailto:techsupport@eventsonline.ca?subject=Series Demo - Javascript Disabled Help">' . translate("K363") . '</a>.  
					<br />
					' . translate("K364") . '<br />
					<ul style="list-style-type: none;">
					<li style="display:inline;"> &nbsp &nbsp <a href="https://www.google.com/intl/en/chrome/browser/" target="_blank">Google Chrome</a> &nbsp &nbsp </li>
					<li style="display:inline;"> &nbsp &nbsp <a href="http://www.mozilla.org/en-US/firefox/new/" target="_blank">Mozilla FireFox</a> &nbsp &nbsp </li>
					<li style="display:inline;"> &nbsp &nbsp <a href="http://www.microsoft.com/en-us/download/details.aspx?id=13950" target="_blank">Internet Explorer 9</a> &nbsp &nbsp</li>
					</ul></b>
				</div>
				</center>
			</noscript>';

}
