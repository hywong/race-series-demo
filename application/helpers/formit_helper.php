<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
* Form builder
*
* @category   	EventsOnline
* @author				Eric Roy
* @dependencies	CodeIgniter Error Function (show_error)
* @copyright 		Copyright (c) 2014-End of time Events Online (http://www.eventsonline.ca)
* @license   		Apache License 2.0 http://opensource.org/licenses/Apache-2.0
* @version   		Release: 1.0
* @since    		Available since Release 1.0
* @deprecated 	
*/

/**
 * Creates a form's HTML based on parameters
 *
 * @param  Form    	$form  Array containing all of the form details EX:
 *									array
 *										'action'  // Where we're posting (page name)
 *										'method' 	// GET or POST
 *										'sections' // Array containing all of the form sections
 *											'section_mysection'
 *												'field1'
 *													'type'
 *													'name'
 *													'class'
 *													'id'
 * @param 	Specs 	$specs 	Array containing additional form specs
 *									array
 *										'label_columns'	
 *										'field_columns'
 *										'class'
 *										'id'
 * @return 	boolean	On success (will show an error if essentials are missing)
 */ 
if ( ! function_exists('formit') )
{
	function formit($form=NULL, $specs=NULL)
	{
		if($form === NULL) show_error("You must pass an array of data to formit.");
		if(!isset($specs['skip'])){ $specs['skip'] = false; }
		//Setup columns
		$labelCols = '';
		$fieldCols = '';
		if(isset($specs['label_columns']) && isset($specs['field_columns']))
		{
			$labelCols = $specs['label_columns'];
			$fieldCols = $specs['field_columns'];
		}

		//Setup classes
		$class = "";
		if(isset($specs['type']))
		{
			$class .= "class=\"form-".$specs['type'];
		}
		else
		{
			$class .= "class=\"";
		}

		if(isset($specs['class']))
		{
			$class .= " ".$specs['class'];
		}

		$class .= "\" ";

		//Setup id
		if(isset($specs['id']))
		{
			$id = "id=\"".$specs['id']."\" ";
		}

		echo "
			<form autocomplete=\"off\" ".$class."".$id."action=\"".$form['action']."\" method=\"".$form['method']."\">";
				if($specs['skip'] == TRUE){
					echo "
					<div class=\"row\">
						<div class=\"col-sm-offset-3 col-sm-5\">
							<button class=\"btn btn-block btn-success\">".translate("Skip")."</button>
						</div>
					</div><br />";
				}
				foreach($form['sections'] as $section => $fields)
				{
					echo "
					<div class=\"row\">
						<div class=\"col-xs-12\">";
					echo "<fieldset>
					<legend>".$section."</legend>";
					
					foreach($fields as $field => $fieldData)
					{
						if(isset($fieldData['section-hint']))
						{
							echo "<div class=\"row\"><div class=\"col-xs-12\"><em class=\"text-muted section-hint\">".$fieldData['section-hint']."</em></div></div>";
						}

						if(isset($fieldData['info']))
						{
							echo "<div class=\"row\">".$fieldData['info']."</div>";
						}

						if(isset($specs['type']))
						{
							formit_build_field($fieldData, $labelCols, $fieldCols, $specs['type']);
						}
						else
						{
							formit_build_field($fieldData, $labelCols, $fieldCols);
						}
					}
					echo "</fieldset>
						</div>
					</div>";
				}
				echo "
				<div class=\"row\">
					<div class=\"col-sm-offset-3 col-sm-5\">
						<button class=\"btn btn-block btn-success\">".translate("Submit")."</button>
					</div>
				</div>
			</form>";
	}
}

/**
 * Used in pair with function formit, creates the individual types of fields and returns their html
 *
 * @param  Field 		$field  Array containing all of the field details EX:
 *									array
 *										'name'  // Name for the field
 *										'type' 	// Type of field (text, select, checkbox etc...)
 *										'data' 	// Data to populate (if input, value; if select, options; if radio, multiple radios; if checkbox, multiple checkboxes)
 *										'label'	// Label for the field that will link to the ID 
 *										'class'
 *										'id'
 * @return 	string	$html 	Return string of the html for the field
 */ 
if( ! function_exists('formit_build_field') )
{
	function formit_build_field($field, $labelCols, $fieldCols, $formType=NULL, $helperCols=NULL)
	{
		$columns = TRUE;
		//Initialize variable if session exists
		$saveValue = '';
		$checked = '';
		if(isset($_SESSION['registration']['active_user']))
		{
			$activeUser = $_SESSION['registration']['active_user'];
		}
		else
		{
			$activeUser = FALSE;
		}

		//Check if the form is horizontal, if columns aren't defined, default them.
		if($formType == "horizontal")
		{
			($labelCols == "" ? $labelCols = "col-sm-4" : NULL);
			($fieldCols == "" ? $fieldCols = "col-sm-8" : NULL);
		}
		else
		{
			$columns = FALSE;
		}

		//Make sure we can create a valid field
		if(isset($field['type']))
		{
			$labelClass = "class=\"";
			$fieldClass = "class=\"form-control";

			//If we have columns (horizontal form) add the col classes
			//Labels need the control-label class for horizontal forms
			if($columns === TRUE)
			{
				$labelClass .= "control-label ".$labelCols;
			}

			//Add any custom classes
			if(isset($field['class']))
			{
				$fieldClass .= " ".$field['class'];
			}

			//Close class strings
			$labelClass .= "\"";
			$fieldClass .= "\"";

			$labelFor = '';
			$fieldId = '';
			if(isset($field['id']))
			{
				$labelFor = "for=\"".$field['id']."\"";
				$fieldId = "id=\"".$field['id']."\"";
			}

			if(isset($field['parent-class']))
			{
				$parentClass = " ".$field['parent-class'];
			}
			else
			{
				$parentClass = "";
			}

			$isRequired = FALSE;
			if(isset($field['required']))
			{
				if($field['required'] === TRUE)
				{
					$isRequired = TRUE;
				}
			}

			//Check if the field exists in our session
			if(isset($field['name']))
			{
				if(isset($field['data']))
				{
					$exists = _field_exists($field['name'], $field['data']);	
				}
				else 
				{
					$exists = _field_exists($field['name']);
				}
			}
			else
			{
				$exists = false;
			}

			if(isset($field['row-before']))
			{
				$cols = substr($fieldCols, -1);
				$toOffset = 12 - $cols;
				echo "
				<div class=\"form-group\">
					<div class=\"row\">
						<div class=\"".$fieldCols." col-sm-offset-".$toOffset."\">
						".$field['row-before']."
						</div>
					</div>
				</div>
				<br />
				";
			}

			//Find and create the type of field we need
			switch($field['type'])
			{
				case "text":
					//Session restore
					if($exists)
					{
						$saveValue = " value=\"".$exists."\" ";
					}

					echo "
					<div class=\"form-group".$parentClass."\">";
					if(isset($field['label']))
					{
						if(isset($_SESSION['registration']['active_user']) && $field['name'] == "discount_code")
						{
							if(!isset($_SESSION['registration'][$_SESSION['registration']['active_user']]['discount_code']))
							{
								echo "<label ".$labelClass." ".$labelFor.">".translate($field['label']).($isRequired ? "<span class=\"required-text\">*</span>" : NULL) . "</label>";							
							}
						}
						else
						{
								echo "<label ".$labelClass." ".$labelFor.">".translate($field['label']).($isRequired ? "<span class=\"required-text\">*</span>" : NULL)."</label>";		
						}
					}
					else
					{
						echo "<div ".$labelClass."></div>";
					}

					$readOnly = ' ';
					if(isset($field['readonly']))
					{
						if($field['readonly'] == TRUE)
						{
							$readOnly = ' readonly ';
						}
					}

					if(isset($_SESSION['registration']['active_user']))
					{
						if($field['name'] == "discount_code" && isset($_SESSION['registration'][$_SESSION['registration']['active_user']]['discount_code']))
						{
							echo "<div class=\"row discount-applied\"><div class=\"col-xs-12\"><div class=\"alert alert-success text-center\"><i class=\"glyphicon glyphicon-ok\"></i> ".translate("Discount code has been applied to your registration"). "</div></div></div>
							<div class=\"row discount-removal\"><div class=\"col-xs-12 text-center\">
								<button class=\"btn btn-danger btn-sm remove-discount\" type=\"button\">".translate("Remove Discount Code")."</button>
							</div></div>";
						}
						else
						{
							if($columns === TRUE) echo "<div class=\"".$fieldCols."\">";

							if(isset($field['input-group-right']) || isset($field['input-group-left']))
							{
								echo "<div class=\"input-group\">";
							}

							if(isset($field['input-group-left'])) echo "<span class=\"input-group-btn\">".$field['input-group-left']."</span>";

							echo "<input type=\"text\"".$readOnly."name=\"".$field['name']."\" ".$fieldClass." ".$fieldId.$saveValue.($isRequired ? ' required' : NULL)." />";

							if(isset($field['input-group-right'])) echo "<span class=\"input-group-btn\">".$field['input-group-right']."</span>";

							if(isset($field['input-group-right']) || isset($field['input-group-left']))
							{
								echo "</div>";
							}

							if($columns === TRUE) echo "</div>";
						}
					}
					else
					{
						if($columns === TRUE) echo "<div class=\"".$fieldCols."\">";

						if(isset($field['input-group-right']) || isset($field['input-group-left']))
						{
							echo "<div class=\"input-group\">";
						}

						if(isset($field['input-group-left'])) echo "<span class=\"input-group-btn\">".$field['input-group-left']."</span>";

						echo "<input type=\"text\" name=\"".$field['name']."\" ".$fieldClass." ".$fieldId.$saveValue.($isRequired ? ' required' : NULL)." />";

						if(isset($field['input-group-right'])) echo "<span class=\"input-group-btn\">".$field['input-group-right']."</span>";

						if(isset($field['input-group-right']) || isset($field['input-group-left']))
						{
							echo "</div>";
						}

						if($columns === TRUE) echo "</div>";
					}

					echo "
					</div>";
				break;

				case "select":
					echo "
					<div class=\"form-group".$parentClass."\">";
						if(isset($field['label']))
						{
							echo "<label ".$labelClass." ".$labelFor.">".translate($field['label']).($isRequired ? "<span class=\"required-text\">*</span>" : NULL)."</label>";
						}
						else
						{
							echo "<div ".$labelClass."></div>";
						}

						if($columns === TRUE) echo "<div class=\"".$fieldCols."\">";

						echo "<select name=\"".$field['name']."\" ".$fieldClass." ".$fieldId.($isRequired ? ' required' : NULL).">";
						foreach($field['data'] as $value => $display)
						{
							//Session restore
							// echo $exists . " : " . $value.'<BR>';
							if((string)$exists === (string)$value)
							{
								echo "<option selected=\"selected\" value=\"".$value."\">".$display."</option>";
							}
							else
							{
								echo "<option value=\"".$value."\">".$display."</option>";
							}
						}

						echo "</select>";
						if($columns === TRUE) echo "</div>";
					echo "
					</div>";
				break;

				case "textarea":
					//Session restore
					if($exists)
					{
						$saveValue = $exists;
					}
					else
					{
						if(isset($field['data']))
						{
							$saveValue = $field['data'];
						}
					}

					if(isset($field['readonly']))
					{
						$readOnly = " readonly ";
					}
					else
					{
						$readOnly = " ";
					}

					echo "
					<div class=\"form-group".$parentClass."\">";
						if(isset($field['label']))
						{
							echo "<label ".$labelClass." ".$labelFor.">".translate($field['label']).($isRequired ? "<span class=\"required-text\">*</span>" : NULL)."</label>";
						}
						else
						{
							echo "<div ".$labelClass."></div>";
						}
						if(isset($field['name']))
						{
							$thisName = "name=\"".$field['name']."\" ";
						}
						else
						{
							$thisName = '';
						}
						if($columns === TRUE) echo "<div class=\"".$fieldCols."\">";
						echo "<textarea".$readOnly.$thisName.$fieldClass." ".$fieldId.($isRequired ? ' required' : NULL).">".$saveValue."</textarea>";
						if($columns === TRUE) echo "</div>";
					echo "
					</div>";
				break;

				case "checkbox":
					if(isset($field['class']))
					{
						$checkClass = " class=\"".$field['class']."\" ";
					}
					else
					{
						$checkClass = "";
					}

					if(isset($field['name']))
					{
						$thisName = "name=\"".$field['name']."\" ";
					}
					else
					{
						$thisName = '';
					}

					echo "
					<div class=\"form-group".$parentClass."\">";
						if(isset($field['label']))
						{
							if($field['label'] != '')
								echo "<label ".$labelClass.">".translate($field['label']).($isRequired ? "<span class=\"required-text\">*</span>" : NULL)."</label>";
						}
						else
						{
							echo "<div ".$labelClass."></div>";
						}
						if($columns === TRUE) echo "<div class=\"".$fieldCols."\">";

						$newName = '';
						foreach($field['data'] as $value => $display)
						{
							//Session restore
							if($exists)
							{
								if(is_array($exists))
								{
									if(in_array($value, $exists))
									{
										$checked = " checked=\"checked\" ";
									}
									else
									{
										$checked = "";
									}									
								}
								else
								{
									if($exists == $value)
									{
										$checked = " checked=\"checked\"";
									}
									else
									{
										$checked = '';
									}
								}
							}

							if(isset($field['isArray']))
							{
								if($field['isArray'] === FALSE)
								{
									$newName = $thisName;
								}
							}
							else
							{
								if($newName == '')
								{									
									$newName = $thisName.='[]';
								}
							}

							echo "
							<div class=\"checkbox\">
								<label>
									<input type=\"checkbox\"".$checked."value=\"".$value."\" ".$newName." class=\"".$checkClass."\"".($isRequired ? ' required' : NULL)." />";

									if($display != "")
										echo nl2br(translate($display));

								echo "
								</label>
							</div>";
						}
						if($columns === TRUE) echo "</div>";
					echo "
					</div>";
				break;

				case "radio":
					if(isset($field['class']))
					{
						$radClass = " class=\"".$field['class']."\" ";
					}
					else
					{
						$radClass = "";
					}
					echo "
					<div class=\"form-group".$parentClass."\">";					
						if(isset($field['label']))
						{
							if($field['label'] != '')
								echo "<label ".$labelClass.">".translate($field['label']).($isRequired ? "<span class=\"required-text\">*</span>" : NULL)."</label>";
						}
						else
						{
							echo "<div ".$labelClass."></div>";
						}
						if($columns === TRUE) echo "<div class=\"".$fieldCols."\">";
						foreach($field['data'] as $value => $display)
						{
							//Reset checked string
							$checked = " ";

							//Session restore #1 (non-race relate)
							if($exists == $value)
							{
									$checked = " checked=\"checked\" ";
							}
							else
							{
								$checked = " ";
							}

							if(isset($_SESSION['registration'][$activeUser]['activities']))
							{
								if(stripos($field['name'], "race_event_") !== FALSE)
								{
									$eventID = str_replace("race_event_", "", $field['name']);
									if(in_array($eventID."_".$value, $_SESSION['registration'][$activeUser]['activities']))
									{
										$checked = " checked=\"checked\" ";
									}
								}
							}
							echo "
							<div class=\"radio\">
								<label>";
								if(isset($field['soldout']))
								{
									if(in_array($value, $field['soldout']))
									{
										echo "<label class=\"".$radClass."\" />".translate($display)." <span class=\"label label-danger\">Sold Out</span>";
									}
									else
									{
										echo "<input type=\"radio\"".$checked."value=\"".$value."\" name=\"".$field['name']."\" ".$radClass." ".($isRequired ? ' required' : NULL)." />";
										if($display != "")
											echo translate($display);
									}
								}
								else
								{
									echo "<input type=\"radio\"".$checked."value=\"".$value."\" name=\"".$field['name']."\" ".$radClass." ".($isRequired ? ' required' : NULL)." />";
									if($display != "")
										echo translate($display);
								}
							echo "
								</label>
							</div>";
						}
						if($columns === TRUE) echo "</div>";
					echo "
					</div>";
				break;

				case "email":
					//Session restore
					if($exists)
					{
							$saveValue = " value=\"".$exists."\" ";
					}

					echo "
					<div class=\"form-group".$parentClass."\">";
					if(isset($field['label']))
					{
						echo "<label ".$labelClass." ".$labelFor.">".translate($field['label']).($isRequired ? "<span class=\"required-text\">*</span>" : NULL)."</label>";
					}
					else
					{
						echo "<div ".$labelClass."></div>";
					}			

					$dataAttribute = ' ';
					if(isset($field['data-attribute']))
					{
						$dataAttribute = " ".$field['data-attribute']." ";
					}

					if($columns === TRUE) echo "<div class=\"".$fieldCols."\">";

					echo "<input type=\"email\"".$dataAttribute."name=\"".$field['name']."\" ".$fieldClass." ".$fieldId.$saveValue.($isRequired ? ' required' : NULL)." />";

					if($columns === TRUE) echo "</div>";
					echo "
					</div>";
				break;

				case "password":
					//Session restore
					if($exists)
					{
						$saveValue = " value=\"".$exists."\" ";
					}

					echo "
					<div class=\"form-group".$parentClass."\">";
					if(isset($field['label']))
					{
						echo "<label ".$labelClass." ".$labelFor.">".translate($field['label']).($isRequired ? "<span class=\"required-text\">*</span>" : NULL)."</label>";
					}
					else
					{
						echo "<div ".$labelClass."></div>";
					}
						if($columns === TRUE) echo "<div class=\"".$fieldCols."\">";
						echo "<input type=\"password\" name=\"".$field['name']."\" ".$fieldClass." ".$fieldId.$saveValue.($isRequired ? ' required' : NULL)." />";
						if($columns === TRUE) echo "</div>";
					echo "
					</div>";
				break;

				case "hidden":
					$thisClass = '';
					if(isset($field['class']))
					{
						$thisClass = "class=\"".$field['class']."\"";
					}
					echo "<input type=\"hidden\" name=\"".$field['name']."\" ".$thisClass." value=\"".$field['data']."\" />";
				break;

				case "iframe":
					echo "<div class=\"col-sm-offset-3 col-sm-8\"><iframe id=\"".$field['id']."\"
					src=\"".$field['url']."\" width=\"100%\" height=\"230\" frameBorder=\"0\"></iframe><br /><br /></div>";
				break;

				case "date":
					//Session restore
					if(isset($_SESSION['registration'][$activeUser]['personal'][$field['name']]))
					{
						$saveValue = " value=\"".$_SESSION['registration'][$activeUser]['personal'][$field['name']]."\" ";
					}

					echo "
					<div class=\"form-group".$parentClass."\">";
					if(isset($field['label']))
					{
						echo "<label ".$labelClass." ".$labelFor.">".translate($field['label']).($isRequired ? "<span class=\"required-text\">*</span>" : NULL)."</label>";
					}
					else
					{
						echo "<div ".$labelClass."></div>";
					}
					$readOnly = ' ';
					if(isset($field['readonly']))
					{
						if($field['readonly'] == TRUE)
						{
							$readOnly = ' readonly ';
						}
					}
						if($columns === TRUE) echo "<div class=\"".$fieldCols."\">";
						echo "<input type=\"text\"".$readOnly."name=\"".$field['name']."\" ".$fieldClass." ".$fieldId.$saveValue.($isRequired ? ' required' : NULL)." />";
						if($columns === TRUE) echo "</div>";
					echo "
					</div>";
				break;

				default:
					show_error($field['type']. " is not defined.");
				break;
			}
			if(isset($field['helper-text']) && $field['helper-text'] !="*TR" )
			{
				if($helperCols==NULL){ $helperCols = "col-sm-9"; }
				echo "<div class=\"form-group\">
					<label class=\"control-label col-sm-3\"></label>
					<div class=\"".$helperCols."\">
						<div class=\"alert alert-info\">
					".$field['helper-text']."
						</div>
					</div>
				</div>";
			}
		}
		else
		{
			show_error("Building a field requires both a name and a type.");
		}
	}
}

if(!function_exists("formit_dynamic"))
{
	function formit_dynamic($form, $fields, $repeats = NULL)
	{
		if($repeats === NULL) show_error("A variable must be passed to formit_dynamic and it must be numeric.");//Setup columns
		$labelCols = '';
		$fieldCols = '';
		if(isset($specs['label_columns']) && isset($specs['field_columns']))
		{
			$labelCols = $specs['label_columns'];
			$fieldCols = $specs['field_columns'];
		}

		//Setup classes
		$class = "";
		if(isset($specs['type']))
		{
			$class .= "class=\"form-".$specs['type'];
		}
		else
		{
			$class .= "class=\"";
		}

		if(isset($specs['class']))
		{
			$class .= " ".$specs['class'];
		}

		$class .= "\" ";

		//Setup id
		$id = "";
		if(isset($specs['id']))
		{
			$id = "id=\"".$specs['id']."\" ";
		}

		echo "
			<form autocomplete=\"off\" ".$class."".$id."action=\"".$form['action']."\" method=\"".$form['method']."\">";
		if(is_numeric($repeats))
		{
			for($i = 1; $i <= $repeats; $i++)
			{
				echo "<div class=\"clearfix\"></div><br /><div class=\"well\">";
				if($i === 1 )
					echo "<h4>Member #".$i." (Captain)</h4>";
				else
					echo "<h4>Member #".$i."</h4>";

				$c = 0;
				foreach($fields as $details)
				{
					if($c%2 === 0)
					{
						echo "<div class=\"row\">";
					}
					$details['name'] = "g_".$details["name"]."_".$i;
					$details['id'] .="_".$i;
					$details['parent-class'] = "col-sm-6";
					if(isset($details['add-member']))
					{
						$details['data-attribute'] = "data-member=\"".$i."\"";
					}
					formit_build_field($details, "col-sm-3", "col-sm-9", 'horizontal', "col-sm-12");

					if($c%2 === 1)
					{
						echo "</div>";
					}
					$c++;
				}
				echo "</div>";
				echo "<div class=\"clearfix\"></div><br /></div>";
			}			
				echo "
				<div class=\"row\">
					<div class=\"col-sm-offset-3 col-sm-6\">
						<button class=\"btn btn-block btn-success\">".translate("Submit")."</button>
					</div>
				</div>
			</form>";
		}
		else
		{
			show_error("A variable must be passed to formit_dynamic and it must be numeric.");			
		}
	}
}

if(!function_exists("_field_exists"))
{
	function _field_exists($name, $data = NULL)
	{
		if(isset($_SESSION['registration']['active_user']))
			if(isset($_SESSION['registration'][$_SESSION['registration']['active_user']]))
				$session = $_SESSION['registration'][$_SESSION['registration']['active_user']];

		if(isset($session[$name]))
		{
			return $session[$name];
		}

		$isValidated = FALSE;

		//Check activities
		if(stripos($name, "race_event_") !== FALSE && isset($session['activities']))
		{
			$isValidated = TRUE;
			$thisEventID = substr($name, strrpos($name, '_') +1);
			if($data !== NULL)
			{
				foreach($data as $value => $display)
				{
					if($value != "")
					{
						if(in_array($thisEventID."_".$value, $session['activities']))
						{
							return $value;
						}			
					}		
				}
			}
		}

		if(stripos($name, 'group_event') === 0 && isset($session['activities']))
		{
			$isValidated = TRUE;
			if($data !== NULL)
			{
				foreach($data as $value => $display)
				{
					if($value != "")
					{
						if(in_array($value, $session['activities']))
						{
							return $value;
						}
					}
				}
			}
		}
		if(stripos($name, "members") === 0)
		{
			if($data !== NULL)
			{
				foreach($data as $value => $display)
				{
					if(isset($session['personal']['members']))
					{
						if($value == $session['personal']['members'])
						{
							return $value;
						}
					}
				}
			}
		}

		if(stripos($name, "g_") === 0)
		{
			$isValidated = TRUE;
			$memberNumber = substr($name, strrpos($name, '_') +1);
			$fieldName = str_replace(array("g_", "_".$memberNumber), "", $name);
			if($data !== NULL)
			{
				foreach($data as $value => $display)
				{
					if(isset($session['group_details'][$memberNumber][$fieldName]))
					{
						if($session['group_details'][$memberNumber][$fieldName] === (string)$value)
						{
							return $value;
						}
					}

					if(isset($session['group_details'][$memberNumber]['addon']))
					{
						$fieldName = str_replace("gaddon_", "", $fieldName);
						foreach($session['group_details'][$memberNumber]['addon'] as $field => $value)
						{
							if($fieldName === $field)
							{
								return $value;
							}
						}
					}

					if(isset($session['group_details'][$memberNumber]['eventextras']))
					{
						$fieldName = str_replace("extrainfo_", "", $fieldName);
						foreach($session['group_details'][$memberNumber]['eventextras'] as $field => $value)
						{
							if($fieldName === $field)
							{
								return $value;
							}
						}
					}
				}
			}
			else
			{
				if(isset($session['group_details'][$memberNumber]))
				{
					if(array_key_exists($fieldName, $session['group_details'][$memberNumber]))
					{
						return $session['group_details'][$memberNumber][$fieldName];
					}
				}
			}
		}

		//Check charities
		if(stripos($name, "charitydetails_") !== FALSE)
		{
			$isValidated = TRUE;
			$thisEventID = substr($name, strrpos($name, '_') +1);
			if($data !== NULL)
			{
				$formattedName = str_replace(array("charitydetails_", "_".$thisEventID), "", $name);

				if(isset($session['charities'][$thisEventID]))
				{
					if(is_array($data))
					{
						foreach($data as $value => $display)
						{
							if($value != "")
							{
								if(isset($session['charities'][$thisEventID][$formattedName]))
								{
									return $value;
								}
							}
						}
					}
				}
			}
			else
			{
				$formattedName = str_replace(array("charitydetails_", "_".$thisEventID), "", $name);
				if(isset($session['charities'][$thisEventID][$formattedName]))
				{
					return $session['charities'][$thisEventID][$formattedName];
				}
			}
		}

		//Check teams
		if(stripos($name, "eventteam_") !== FALSE)
		{
			$isValidated = TRUE;
			$thisEventID = substr($name, strrpos($name, '_') +1);
			if($data !== NULL)
			{
				foreach($data as $value => $display)
				{
					if($value != "")
					{
						if(isset($session['teams'][$thisEventID]))
						{
							$formattedName = str_replace(array("eventteam_", "_".$thisEventID), "", $name);
							if($value == $session['teams'][$thisEventID][$formattedName])
							{
								return $value;
							}
						}
					}
				}
			}
			else
			{
				$formattedName = str_replace(array("eventteam_", "_".$thisEventID), "", $name);
				if(isset($session['teams'][$thisEventID][$formattedName]))
				{
					return $session['teams'][$thisEventID][$formattedName];
				}
			}
		}

		//Check extra info
		if(stripos($name, "extrainfo_") === 0)
		{
			$isValidated = TRUE;
			$thisEventID = substr($name, strrpos($name, '_') +1);
			if($data !== NULL)
			{

				foreach($data as $value => $display)
				{
					if($value !== "")
					{
						if(isset($session['eventextras'][$thisEventID]))
						{
							$formattedName = str_replace(array("extrainfo_", "_".$thisEventID), "", $name);
							if($value == $session['eventextras'][$thisEventID][$formattedName])
							{
								return $value;
							}
						}
					}
				}
			}
			else
			{
				$formattedName = str_replace(array("extrainfo_", "_".$thisEventID), "", $name);
				if(isset($session['eventextras'][$thisEventID][$formattedName]))
				{
					return $session['eventextras'][$thisEventID][$formattedName];
				}
			}
		}

		if(stripos($name, "seriesfoundation_") === 0)
		{
			$isValidated = TRUE;
			if($data !== NULL)
			{
				foreach($data as $value => $display)
				{
					if($value !== "")
					{
						$formattedName = str_replace(array("seriesfoundation_"), "", $name);
						if(isset($session['seriesfoundation'][$formattedName]))
						{
							return $session['seriesfoundation'][$formattedName];	
						}
					}
				}
			}
		}

		if(stripos($name, 'artezdetails_') === 0)
		{
			$isValidated = TRUE;
			$thisEventID = substr($name, strrpos($name, '_') +1);

			if($data !== NULL)
			{
				foreach($data as $value => $display)
				{
					if($value !== "")
					{
						if(isset($session['artez'][$thisEventID]))
						{
							if(in_array($value, $session['artez'][$thisEventID]))
							{
								return $value;
							}
						}
					}
				}
			}
			else
			{
				$formattedName = str_replace(array("artezdetails_", "_".$thisEventID), "", $name);
				if(isset($session['artez'][$thisEventID][$formattedName]))
				{
					return $session['artez'][$thisEventID][$formattedName];
				}
			}
		}

		if(stripos($name, 'aka_') === 0)
		{
			$isValidated = TRUE;
			$thisEventID = substr($name, strrpos($name, '_') +1);

			if($data !== NULL)
			{
				foreach($data as $value => $display)
				{
					if($value !== "")
					{
						if(isset($session['aka'][$thisEventID]))
						{
							if(in_array($value, $session['aka'][$thisEventID]))
							{
								return $value;
							}
						}
					}
				}
			}
			else
			{
				$formattedName = str_replace(array("artezdetails_", "_".$thisEventID), "", $name);
				if(isset($session['artez'][$thisEventID][$formattedName]))
				{
					return $session['artez'][$thisEventID][$formattedName];
				}
			}
		}

		if(isset($_SESSION['pledge']) && stripos($name, "pledge_") === 0)
		{
			if($data !== NULL)
			{
				if(is_array($data))
				{
					foreach($data as $value => $display)
					{
						if($value !== "")
						{
							if(in_array($value, $_SESSION['pledge']))
							{
								return $value;
							}
						}
					}
				}
			}
			else
			{
				$formattedName = str_replace(array("pledge_"), "", $name);
				if(isset($_SESSION['pledge'][$formattedName]))
				{
					return $_SESSION['pledge'][$formattedName];
				}
			}
		}

		if($isValidated === FALSE && isset($session['personal']))
		{
			if($data !== NULL)
			{
				$found = array();
				if(is_array($data))
				{
					foreach($data as $value => $display)
					{
						if($value !== "")
						{
							if(in_array($value, $session['personal'], TRUE))
							{
								return $value;
							}
							elseif(in_array_r($value, $session['personal'], TRUE))
							{
								$found[] = $value;
							}
						}
					}
				}
				if($found)
				{
					return $found;
				}
			}
			else
			{
				if(isset($session['personal'][$name]))
				{
					return $session['personal'][$name];
				}
			}
		}
		return false;
	}
}