<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
* Translation utility
*
* @category   	EventsOnline
* @author				Eric Roy
* @dependencies	
* @copyright 	Copyright (c) 2014-End of time Events Online (http://www.eventsonline.ca)
* @license   	Apache License 2.0 http://opensource.org/licenses/Apache-2.0
* @version   	Release: 1.0
* @since    	Available since Release 1.0
* @deprecated 	
*/

/**
 * Translates a string into the set session language. The XML library can be set in the application/config/constants.php file.
 *
 * @param  	string 	$string  The string to translate
 * @return 	string	The string translated to the session's selected language
 */ 
if ( ! function_exists('translate') )
{
	function translate($string)
	{
    $returnString = "";
    //THIS STATIC VARIABLE WILL PERSIST EVEN WHEN LEAVING THE FUNCTION
    static $arrDictionary;

    //Store the dictionary in an associative array.  It's apparently faster to pull data from an associative array than the simplexml object
    if (!isset($arrDictionary)) 
    {
        $arrDictionary = array();
        $filePath = "";		

        $aList = simplexml_load_file(DATA_XML_TRANSLATE);

        foreach ($aList->translate as $listing) 
        {
            $aKey = (string)$listing->attributes();
            $arrDictionary[$aKey] = array('english' => (string)$listing->english, 'french' => (string)$listing->french);
        }
    }

    if (!isset($_SESSION['language'])) 
    {
        $_SESSION['language'] = "en";
    }
    if (isset($_GET["lang"])) 
    {
        $_SESSION['language'] = $_GET["lang"];
    }

    if (array_key_exists($string, $arrDictionary)) 
    {
        if (strtolower($_SESSION['language']) == 'en') 
        {
            $returnString = $arrDictionary[$string]['english'];
        } 
        else if (strtolower($_SESSION['language']) == 'fr') 
        {
            $returnString = $arrDictionary[$string]['french'];
        }
    }

    //IF THE KEY IS NOT FOUND OR A TRANSLATION WAS NOT FOUND IT WILL APPEND *TR TO THE END OF THE WORD TO SIGNIFY THAT IT NEEDS TO BE ADDED TO THE XML FILE OR NEEDS TO BE TRANSLATED
    if (trim($returnString) == "") 
    {
        $returnString = $string;
        if (TRANSLATE_SUFFIX) 
        {
            $returnString .= "*TR";
        }
    }

    return $returnString;
	}
}
