<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Participant_Controller extends MY_Controller {
		function __construct() {
			parent::__construct();			

			/*
			//Checks if we have a valid user session
			$this->load->model("Participant_model", "participantm");
			$validSession = $this->participantm->is_logged();

			if($validSession == FALSE && $this->router->method !== "login")
			{
			redirect("participant/login");
			}
			*/
			
			$sessionParticipantData = $this->session->userdata("participant");
			$sessionExistsFlag = true;
			if(!$sessionParticipantData) {
				$sessionExistsFlag = false;
			}
			
			if (!$sessionExistsFlag && $this->router->method !== "login" && $this->router->method !== "retrieve" && $this->router->method !== "reset_password") {
				redirect("participant/login?msgDanger=sessionExpired");
			}
			//Add the custom includes (css/js)
			$this->template->write_view("_includes", "participant/includes");
			
			$this->load->model("participant_model");
			$data['navEvents'] = $this->participant_model->getEventsByAccountID();
			//Load navigation into view			
			$this->template->write_view("_navigation", "participant/navigation", $data);
		}
	}