<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Admin_Controller extends MY_Controller {
	
		function __construct() {
			parent::__construct();	
			
			$sessionAdminData = $this->session->userdata("admin");
			$sessionExistsFlag = true;
			if(!$sessionAdminData) {
				$sessionExistsFlag = false;
			}
			
			if (!$sessionExistsFlag && $this->router->method == "index") {
				redirect("admin/login");
			}
			else if (!$sessionExistsFlag && $this->router->method !== "login") {
				redirect("admin/login?msgDanger=sessionExpired");
			}
			//Add the custom includes (css/js)
			$this->template->write_view("_includes", "admin/includes");
			
			$this->load->model("admin_model");
			$data['navEvents'] = $this->admin_model->getAllEvent();


			$post = $this->input->post();
			$registrationArray = array(
				"staff_entry",
				"personal",
				"charities",
				"teams",
				"addons",
				"confirmation"
				);
			if((in_array($this->router->method, $registrationArray) || $this->router->method == "finalize") && $this->router->method != "staff_entry")
			{
				if(count($_SESSION['registration']) <= 1)
				{
					redirect('admin/staff_entry');
				}
			}

			if($post && in_array($this->router->method, $registrationArray))
			{
				//Check if we have a session started
				$sessionRegistration = $this->session->userdata("registration");

				//Count this as an action, refresh last_modified
				$_SESSION['registration']['last_modified'] = strtotime("now");

				//TODO: CSRF Validation
				//If we don't have an active user, we've posted so this is our new active participant
				if(!isset($sessionRegistration['active_user']))
				{
					$activeUserId = uniqid();
					$allActiveUserSession = array("registration" => array("active_user" => $activeUserId));
					$_SESSION['registration']['active_user'] = $activeUserId;

					$sessionRegistration = $this->session->userdata("registration");
				}
				else
				{
					$activeUserId = $sessionRegistration['active_user'];
				}

				//Find if we selected a race, parse and store into "activities"
				$activitiesRegistered = array();
				$charityDetails = array();
				$extraInfo = array();
				$teamInfo = array();
				$eventsToKeep = array();
				$artezDetails = array();
				$groupInfo = array();
				$seriesfoundation = array();

				foreach($post as $key => $value)
				{
					if(is_array($value))
					{
						$value = array_map("trim", $value);
					}
					else
					{
						$value = trim($value);							
					}

					if(stripos($key, "g_") === 0)
					{
						$memberNumber = substr($key, strrpos($key, '_') +1);
						if(stripos($key, "g_extrainfo_") === 0)
						{
							$field = str_replace(array("g_extrainfo_", "_".$memberNumber), "", $key);
							$groupInfo[$memberNumber]['eventextras'][$field] = $value;
							unset($post[$key]);
						}
						elseif(stripos($key, "g_gaddon_") === 0)
						{
							$field = str_replace(array("g_gaddon_", "_".$memberNumber), "", $key);
							$groupInfo[$memberNumber]['addon'][$field] = $value;
							unset($post[$key]);
						}
						else
						{
							$field = str_replace(array("g_", "_".$memberNumber), "", $key);
							$groupInfo[$memberNumber][$field] = $value;
							unset($post[$key]);
						}
					}



					if(stripos($key, "race_event") === 0)
					{
						if($value != "")
						{
							$eventID = substr($key, strrpos($key, '_') +1);
							$activitiesRegistered[] = $eventID."_".$value;
							$eventsToKeep[] = $eventID;
							unset($post[$key]);
						}
						else
						{
							unset($post[$key]);
						}
					}

					if(stripos($key, "group_event") === 0)
					{
						$activitiesRegistered[] = $value;
						unset($post[$key]);
					}

					if(stripos($key, "charitydetails_") === 0)
					{
						if($value != "")
						{
							$eventID = substr($key, strrpos($key, '_') +1);
							$field = str_replace(array("charitydetails_", "_".$eventID), "", $key);
							$charityDetails[$eventID][$field] = $value;
						}
						unset($post[$key]);
					}

					if(stripos($key, "extrainfo_") === 0)
					{
						$eventID = substr($key, strrpos($key, '_') +1);
						$field = str_replace(array("extrainfo_", "_".$eventID), "", $key);
						$extraInfo[$eventID][$field] = $value;
						unset($post[$key]);
					}

					if(stripos($key, "eventteam_") === 0)
					{
						if($value != "")
						{
							$eventID = substr($key, strrpos($key, '_') +1);
							$field = str_replace(array("eventteam_", "_".$eventID), "", $key);
							$teamInfo[$eventID][$field] = $value;
						}
						unset($post[$key]);
					}

					if(stripos($key, "artezdetails") === 0)
					{
						if($value != "")
						{
							$eventID = substr($key, strrpos($key, '_') +1);
							$field = str_replace(array("artezdetails_", "_".$eventID), "", $key);
							$artezDetails[$eventID][$field] = $value;
						}
						unset($post[$key]);
					}

					if(stripos($key, "seriesfoundation") === 0)
					{
						if($value != "")
						{
							$seriesfoundation['donation'] = $value;
						}
						unset($post[$key]);
					}

					if(stripos($key, "aka_") === 0)
					{	
						if($value != "")
						{
							$eventID = substr($key, strrpos($key, '_') +1);
							$field = str_replace(array("artezdetails_", "_".$eventID), "", $key);
							$akaDetails[$eventID][$field] = $value;
						}
						unset($post[$key]);
					}
				}

				//Scan addons, teams, charities and extra fields for events we no longer have
				if(isset($_SESSION['registration'][$_SESSION['registration']['active_user']]['addons']))
				foreach($_SESSION['registration'][$_SESSION['registration']['active_user']]['addons'] as $eID => $arr)
				{
					if(!in_array($eID, $eventsToKeep))
					{
						unset($_SESSION['registration'][$_SESSION['registration']['active_user']]['addons'][$eID]);
					}
				}

				if(isset($_SESSION['registration'][$_SESSION['registration']['active_user']]['charities']))
				foreach($_SESSION['registration'][$_SESSION['registration']['active_user']]['charities'] as $eID => $arr)
				{
					if(!in_array($eID, $eventsToKeep))
					{
						unset($_SESSION['registration'][$_SESSION['registration']['active_user']]['charities'][$eID]);
					}
				}

				if(isset($_SESSION['registration'][$_SESSION['registration']['active_user']]['artez']))
				foreach($_SESSION['registration'][$_SESSION['registration']['active_user']]['artez'] as $eID => $arr)
				{
					if(!in_array($eID, $eventsToKeep))
					{
						unset($_SESSION['registration'][$_SESSION['registration']['active_user']]['charities'][$eID]);
					}
				}

				if(isset($_SESSION['registration'][$_SESSION['registration']['active_user']]['eventextras']))
				foreach($_SESSION['registration'][$_SESSION['registration']['active_user']]['eventextras'] as $eID => $arr)
				{
					if(!in_array($eID, $eventsToKeep))
					{
						unset($_SESSION['registration'][$_SESSION['registration']['active_user']]['eventextras'][$eID]);
					}
				}

				if($post)
				{
					$personal = $post;
					unset($post);
					$post['personal'] = $personal;
				}

				if($artezDetails)
				{
					$post['artez'] = $artezDetails;
				}

				if($groupInfo)
				{
					$post["group_details"] = $groupInfo;
				}

				if($activitiesRegistered)
				{
					$post['activities'] = $activitiesRegistered;
				}

				if($charityDetails)
				{
					$post['charities'] = $charityDetails;
				}

				if($extraInfo)
				{
					$post['eventextras'] = $extraInfo;
				}

				if($teamInfo)
				{
					$post['teams'] = $teamInfo;
				}

				if($seriesfoundation)
				{
					$post['seriesfoundation'] = $seriesfoundation;
				}

				if($akaDetails)
				{
					$post['aka'] = $akaDetails;
				}

				//Make sure we only update the active user's array
				if(isset($sessionRegistration[$activeUserId]))
				{
					$newActiveUserSession = $post;
					$activeUserMergedSession = array_merge($sessionRegistration[$activeUserId], $newActiveUserSession);
					$allActiveUserSession[$activeUserId] = $activeUserMergedSession;
				}
				else
				{
					$allActiveUserSession = array_merge($sessionRegistration, array($activeUserId => $post));
				}

				//Save our new information with our previous information into session
				$_SESSION['registration'] = array_merge($_SESSION['registration'], $allActiveUserSession);

				$this->load->library("ecart");
				$this->ecart->save_cart();
				
				if($this->router->method != "finalize")
				{
					redirect($this->router->class.'/'.$this->router->method);					
				}
			}

			if(!isset($_SESSION['cart-state']))
				$_SESSION['cart-state'] = FALSE;
			
			//Load navigation into view			
			$this->template->write_view("_navigation", "admin/navigation", $data);
		}
	}