<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  class MY_Controller extends CI_Controller
  {   
    function __construct()
    {
      parent::__construct();
      unset($_SESSION['general']);
      if(!isset($_SESSION['general']))
      {
        $sessionData = array();
        try
        {
          $sql = "SELECT lkpE.intEventID as eventID, lkpE.strEvent AS event FROM lkpEvent as lkpE;";
          $results = $this->db->query($sql);

          if($results)
          {
            $rows = $results->fetchAll(PDO::FETCH_ASSOC);
            foreach($rows as $row)
            {
              $sessionData[$row['eventID']]['display'] = $row['event'];
            }
            $_SESSION['general'] = $sessionData;
          }
          else
          {
            throw new Exception("No event data found");
          }

          $sql = "SELECT lkpA.intActivityID as activityID, lkpA.strActivity AS activity FROM lkpActivity as lkpA;";
          $results = $this->db->query($sql);

          if($results)
          {
            $rows = $results->fetchAll(PDO::FETCH_ASSOC);
            unset($sessionData);
            foreach($rows as $row)
            {
              $sessionData[$row['activityID']] = $row['activity'];
            }
            $_SESSION['general']['activities'] = $sessionData;
          }
          else
          {
            throw new Exception("No event data found");
          }

        }
        catch(Exception $e)
        {
          //TODO: Proper catching
        }
      }
    }
  }