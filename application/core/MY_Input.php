<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class MY_Input extends CI_Input
	{   
		public function sanitizeForDbPost() {
			$aPost = $this->post();
			if (is_array($aPost)) {
				$aPost = array_map("escapeString",$aPost);
			}
			
			return $aPost;
		}	
	}