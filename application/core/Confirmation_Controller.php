<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Confirmation_Controller extends MY_Controller {		
		function __construct()
		{
			parent::__construct();
			// //Check if the user was inactive for more than 15 mins
			// if(isset($_SESSION['registration']['last_modified']))
			// {
			// 	if(strtotime("now") > ($_SESSION['registration']['last_modified'] + 900))
			// 	{
			// 		//Expired, reset session and redirect
			// 		$this->session->unset_userdata("registration");
			// 		$this->session->set_userdata(array("registration" => array("last_modified" => strtotime("now"))));
			// 		redirect("register/expired");
			// 	}
			// }
			// else
			// {
			// 	//Expired, reset session and redirect
			// 	$this->session->unset_userdata("registration");
			// 	$this->session->set_userdata(array("registration" => array("last_modified" => strtotime("now"))));
			// 	redirect("register/expired");
			// }

			//Did we post to ourself?

			//Add the custom includes (css/js)
			$this->template->write_view("_includes", "includes");
			//Load banner into view
			$this->template->write_view("_banner", "banner");
			//Load navigation into view
			$this->template->write_view("_navigation", "navigation");
			//Load footer into view
			$this->template->write_view("_footer", "footer");

			
			//TODO: Global session checks here if needed
			
		}
	}