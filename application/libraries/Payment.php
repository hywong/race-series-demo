<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CodeIgniter Payment Class
 *
 * This class processes payments from different payment gateway clients.
 * 
 * @category     EventsOnline
 * @author       Eric Roy
 * @package      Payment
 * @dependencies Translate Helper
 * @copyright  Copyright (c) 2014-End of time Events Online (http://www.eventsonline.ca)
 * @license    Apache License 2.0 http://opensource.org/licenses/Apache-2.0
 * @version    Release: 1.0
 * @since      Class available since Release 1.0
 * @deprecated   
 */
class CI_Payment {
   
   var $CI;
   var $gateway;
   var $currency;
   
   /**
	 * Set gateway
	 *
	 * Sets the specified gateway for the payment
	 *
	 * @access	public
	 */
   public function set_gateway($gateway=NULL)
   {
    if($gateway === NULL)
    {
      show_error("You must define a gateway when setting one.");
    }

    if($this->_validate_gateway($gateway) === FALSE)
    {
      show_error("The gateway specified is not supported.");
    }

    $this->gateway = $gateway;
   }

   /**
   * Validate gateway
   *
   * Verifies that the gateway specified is supported by EventsOnline
   *
   * @access  private
   * @return  Boolean
   */
   private function _validate_gateway($gateway)
   {
    switch($gateway)
    {
      case "litle":
        show_error("Litle is not currently supported");
      break;
      case "exact":
        return TRUE;
      break;
      default:
        return FALSE;
      break;
    }
   }

   /**
   * Process payment
   *
   * Sends the transaction details to the gateway and returns the response
   *
   * @access  public
   */
   public function process($transaction=NULL)
   {
    if($transaction === NULL)
    {
      show_error("Transaction details must be set.");
    }

    if(isset($transaction['invoice']) &&
       isset($transaction['cc_num']) &&
       isset($transaction['cc_name']) &&
       isset($transaction['exp_year']) &&
       isset($transaction['exp_month']) &&
       isset($transaction['total']) &&
       isset($transaction['cvv']))
    {
      //Call the appropriate processing function
      return $this->{"_process_payment_".$this->gateway}($transaction);
    }
    else
    {
      show_error("Missing arguments for the transaction, can't process the payment.");
    }
   }

   /**
   * Process payment exact
   *
   * Sends the transaction details E-XACT payment gateway and parses the response to render it generic.
   *
   * @access  private
   */
   private function _process_payment_exact($transaction)
   {
    $account = $this->_get_exact_account();

    if(LIVE_ENVIRONMENT === FALSE)
    {
      $transaction['cc_num'] = "4111111111111111";
    }

    $transactionRequest = array(
      "Secure_AuthResult" => "",
      "Ecommerce_Flag" => "",
      "XID" => "",
      "ExactID" => $account['id'], //LIVE
      "CAVV" => "",
      "Password" => $account['password'], //LIVE
      "CAVV_Algorithm" => "",
      "Transaction_Type" => "00",
      "Reference_No" => $transaction['invoice'], //LIVE
      "Customer_Ref" => "",
      "Reference_3" => "",
      "Client_IP" => "",
      "User_Name" => "",
      "Client_Email" => "",
      "Language" => "en",
      "Card_Number" => $transaction['cc_num'], //LIVE
      "Expiry_Date" => $transaction['exp_month'] . $transaction['exp_year'],
      "CardHoldersName" => $transaction['cc_name'],
      "Track1" => "",
      "Track2" => "",
      "Authorization_Num" => "",
      "Transaction_Tag" => "",
      "DollarAmount" => $transaction['total'],
      "VerificationStr1" => "",
      "VerificationStr2" => $transaction['cvv'],
      "CVD_Presence_Ind" => "1",
      "Secure_AuthRequired" => "",

      // Level 2 fields
      "ZipCode" => "",
      "Tax1Amount" => "",
      "Tax1Number" => "",
      "Tax2Amount" => "",
      "Tax2Number" => "",

      "SurchargeAmount" => "",
      "PAN" => ""
    );

    $transactionFinalized = array("Transaction" => $transactionRequest);

    $client = new SoapClient("https://secure2.e-xact.com/vplug-in/transaction/rpc-enc/service.asmx?wsdl");
    $response = $client->__soapCall('SendAndCommit', $transactionFinalized);

    //If the client failed to send the request
    if(isset($client->fault))
    {
      if ($client->fault) 
      {
        return array(
          "status" => FALSE,
          "gateway_message" => translate("Transaction Failed"),
          "gateway_error_message" => "The transaction has failed to be processed. The online payment services are temporarily unavailable. We apologize for this inconvenience, please try again shortly."
          );
      }
    }

    $parsedResponse = array();
    foreach($response as $key => $value)
    {
      $parsedResponse[$key] = nl2br($value);
    }

    if($parsedResponse['Transaction_Approved'] == 1)
    {
      $returnArray = array(
        "status" => TRUE,
        "receipt" => $parsedResponse['CTR'],
        "message" => translate("Transaction Approved"),
        'gateway_error_message' => $parsedResponse['EXact_Message'],
        'gateway_error_code' => $parsedResponse['EXact_Resp_Code'],
        'bank_error_message' => $parsedResponse['Bank_Message'],
        'bank_error_code1' => $parsedResponse['Bank_Resp_Code'],
        'bank_error_code2' => $parsedResponse['Bank_Resp_Code_2']
        );
    }
    else
    {
      $returnArray = array(
        "status" => FALSE,
        "message" => translate("Transaction Failed"),
        'gateway_error_message' => $parsedResponse['EXact_Message'],
        'gateway_error_code' => $parsedResponse['EXact_Resp_Code'],
        'bank_error_message' => $parsedResponse['Bank_Message'],
        'bank_error_code1' => $parsedResponse['Bank_Resp_Code'],
        'bank_error_code2' => $parsedResponse['Bank_Resp_Code_2']
        );
    }
    
    return $returnArray;
   }

   /**
   * Set currency
   *
   * Sets the currency variable used to send currency to the right account.
   *
   * @access  public
   * @param   String  Currency type to set
   */
   public function set_currency($currency=NULL)
   {
    if($currency === NULL)
    {
      show_error("You must specify a currency type to set.");
    }

    if($this->_validate_currency($currency) === FALSE)
    {
      show_error("The currency you're trying to set is not supported.");
    }

    $this->currency = $currency;
   }

   /**
   * Validate currency
   *
   * Validates the specified currency against the global list of supported currency types by EventsOnline
   *
   * @param   String    Currency type to validate
   * @access  private
   * @return  Boolean
   */
   private function _validate_currency($currency)
   {
    switch($currency)
    {
      case "CA":
        return TRUE;
      break;
      case "US":
        return TRUE;
      break;
      case "EU":
        return TRUE;
      break;
      default:
        return FALSE;
      break;
    }
   }

   /**
    * Get exact account
    *
    * @access  private
    * @return  array    E-XACT account details
    */
   private function _get_exact_account()
   {
    if(LIVE_ENVIRONMENT)
    {
      switch($this->currency)
      {
        //Set the canadian account
        case "CA":
          $account = array(
            "id" => "A00759-01",
            "password" => "league"
            );
        break;

        //Set the american account
        case "US":
          $account = array(
            "id" => "A09083-01",
            "password" => "thb18hxd"
            );
        break;
        default:
          show_error("The currency type set is not valid with the gateway selected.");
        break;
      }
    }
    else
    {
      //Test account, transactions are NOT charged
      //TODO: Add notifications when processing test accounts
      $account = array(
        "id" => "A06766-01",
        "password" => "7wq6iavp"
        );
    }

    return $account;
   }
}
// END Template Class

/* End of file Template.php */
/* Location: ./system/application/libraries/Template.php */