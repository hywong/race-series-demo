<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
* Bridge with the AKA charity system
*
* @category   	EventsOnline
* @author				Eric Roy
* @package    	AKA
* @dependencies	
* @copyright 		Copyright (c) 2014-End of time Events Online (http://www.eventsonline.ca)
* @license   		Apache License 2.0 http://opensource.org/licenses/Apache-2.0
* @version   		Release: 1.0
* @since    		Class available since Release 1.0
* @deprecated 	
*/
class Aka
{
	private $_CI;
	private $_user = "pmhfwsuser";
	private $_password = "PmhF%2010!";
	private $_orgID = 0;
	private $_subEventID = 9295;
	//private $_subEventID = 7623;
	private $_sortOrder = "TeamId_ASC";
	private $_limit = 50;
	private $_currentPage = 1;
	private $_statusID = "All";
	private $_namespace = "http://tempuri.org/";
	private $_action = "";
	// private $_action = "http://tempuri.org/RegisterParticipantAuto";
	private $_regionID = 45;

	public function __construct()
	{
			$this->_CI =& get_instance();
	}
	
	public function get_teams($eventIDs)
	{
		$sql = "SELECT intSubEventID AS subEventID, intEventID AS eventID FROM lkpAka WHERE intEventID IN (".implode(',',$eventIDs).");";

		$parsedResults = array();
		$stmt = $this->_CI->db->prepare($sql);
		if($stmt->execute())
		{
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach($results as $row)
			{
				$parsedResults[$row['eventID']] = $row['subEventID'];
			}
		}
		else
		{
			//TODO: Alert - failed to select sub event ID
		}

		require_once("Nusoap.php");
		$client = new nusoap_client('https://ws.akaraisin.com/RaisinRegistration.asmx');

		$hits = array();
		
		if($parsedResults)
		{
			foreach($parsedResults as $eventID => $subEventID)
			{
				$this->_action = "http://tempuri.org/GetTeamsList";
				$setup = array(
			    'loginUser' => $this->_user, 
			    'loginPassword' => $this->_password, 
			    'orgId' => $this->_orgID, 
			    'teamName' => "", 
			    'subEventId' => $subEventID, 
			    'sortOrder' => $this->_sortOrder, 
			    'pageSize' => $this->_limit, 
			    'currentPage' => $this->_currentPage, 
			    'statusId' => $this->_statusID, 
			    'recordsFound' => '', 
			    'errorMessage' => '' 
					);
				$result = $client->call("GetTeamsList", $setup, $this->_namespace, $this->_action);

				if($result['recordsFound'] == 0){
					$hits[$eventID][] = "";
				}else{
					for ($i=0; $i<$result['recordsFound']; $i++) 
					{			
						if ($result['recordsFound'] > 1) 
						{
							//loop throught the numbers elements of the array
							if (isset($result['GetTeamsListResult']['diffgram']['NewDataSet']['Table'][$i]['TeamName']) && $result['GetTeamsListResult']['diffgram']['NewDataSet']['Table'][$i]['TeamName'] != "") {
								$hits[$eventID][] = $result['GetTeamsListResult']['diffgram']['NewDataSet']['Table'][$i]['TeamName'];
							}
						} 
						else 
						{
							//loop through the one element
							if (isset($result['GetTeamsListResult']['diffgram']['NewDataSet']['Table']['TeamName']) && $result['GetTeamsListResult']['diffgram']['NewDataSet']['Table']['TeamName'] != "") {
								$hits[$eventID][] = $result['GetTeamsListResult']['diffgram']['NewDataSet']['Table']['TeamName'];
							}
						}
					}
				}
			}
		}
		$options = "";

		return $hits;
	}

	public function create_profile($fields = NULL)
	{
		if(isset($fields['first_name']) &&
			 isset($fields['last_name']) &&
			 isset($fields['email']) &&
			 isset($fields['home_phone']) &&
			 isset($fields['address']) &&
			 isset($fields['country']) &&
			 isset($fields['city']) &&
			 isset($fields['postal_code']) &&
			 isset($fields['gender']) &&
			 isset($fields['aka_fundraising_option']))
		{
			require_once("Nusoap.php");
			$client = new nusoap_client('https://ws.akaraisin.com/RaisinRegistration.asmx');

			$countryID = 2;
			if(strtolower($fields['country']) == 'CAN')
			{
				$countryID = 1;
			}

			$genderID = 2;
			if($fields['gender'] == 'M')
			{
				$genderID = 1;
			}

			$setup = array(
				'loginUser' => $this->_user,
				'loginPassword' => $this->_password,
				'subEventId' => $this->_subEventID,
				'firstName' => $fields['first_name'],
				'middleName' => '',
				'lastName' => $fields['last_name'],
				'eMail' => $fields['email'],
				'phoneNumber' => $fields['home_phone'],
				'addressLine1' => $fields['address'],
				'addressLine2' => '',
				'countryId' => $countryID,
				'regionId' => $this->_regionID,
				'city' => $fields['city'],
				'postalCode' => $fields['postal_code'],
				'genderId' => $genderID,
				'surveyResponse' => '',
				'isCaptain' => FALSE,
				'teamName' => $fields['aka_team_name'],
				
				'customField1' => "Fundraising",
				'customField2' => "",
				'customField3' => "",
				'customField4' => "",
				'customField5' => "",

				'errorMessage' => ''
				// 'loginUser' => $this->_user,
				// 'loginPassword' => $this->_password,
				// 'subEventId' => $this->_subEventID,
				// 'firstName' => $fields['first_name'],
				// 'middleName' => '',
				// 'lastName' => $fields['last_name'],
				// 'eMail' => $fields['email'],
				// 'phoneNumber' => $fields['home_phone'],
				// 'addressLine1' => $fields['address'],
				// 'addressLine2' => '',
				// 'contryId' => $countryID,
				// 'genderId' => $genderID,
				// 'surveyResponse' => '',
				// 'isCaptain' => FALSE,
				// 'teamName' => $fields['aka_team_name'],
				// 'customField1' => $fields['aka_fundraising_option'],
				// 'customField2' => '',
				// 'customField3' => '',
				// 'customField4' => '',
				// 'customField5' => '',
				// 'errorMessage' => ''
				);
			$result = $client->call('RegisterParticipantAuto', $setup, $this->_namespace, "http://tempuri.org/RegisterParticipantAuto");

			if ($client->fault) 
			{
		    //TODO: Log AKA profile create attempt
				return FALSE;
			} 
			else 
			{
		    $err = $client->getError();
		    if ($err) 
		    {
		    	//TODO: Log AKA profile create attempt
					return FALSE;
		    } 
		    else 
		    {
		    	return TRUE;
				}
			}
		}
	}
}