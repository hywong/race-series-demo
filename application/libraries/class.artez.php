<?php
/**
* This object allows you to create an Artez account and
* validate charities based on your event (rules are optional).
*
* @category   	EventsOnline
* @author		Eric Roy
* @subcategory 	Bridge
* @package    	Artez
* @copyright 	Copyright (c) 2013-End of time Events Online (http://www.eventsonline.ca)
* @license   	Apache License 2.0 http://opensource.org/licenses/Apache-2.0
* @version   	Release: 1.0
* @since    	Class available since Release 1.0
* @deprecated 	
*/
class EOL_Artez_Services
{

	public $qty_cap;
	public $dollar_cap;
	public $charity_id;
	public $discount_full;
	public $discount_partial;
	public $charity;
	public $pin;
	private $_artez_file;
	private $_file;
	
	private $_rules_set;
	
	public function __construct($artez_file=NULL, $artez_custom_file=NULL)
	{		
		if(is_string($artez_file))
		{
			$this->_artez_file = $artez_file;
		}
		
		if(is_string($artez_custom_file))
		{
			$this->_artez_custom_file = $artez_custom_file;
		}
		
		unset($this->pin);
		unset($this->charity);
		unset($this->charity_id);
	}
	
	public function getCustomCharities($artezCustomFile=NULL)
	{
		if(!isset($this->_artez_custom_file))
		{
			$this->_artez_custom_file = $artezCustomFile;
		}
		
		$return = array();
		
		//Get Custom
		$fh = new SplFileObject($this->_artez_custom_file);
		$fh->flock(LOCK_EX);
		$_headers = preg_replace('/^\s+|\n|\r|\s+$/m', '', $fh->current());
		$headers = explode('|', $_headers);
		$fh->seek(1);
		while(!$fh->eof())
		{
			$line = preg_replace('/^\s+|\n|\r|\s+$/m', '', $fh->current());
			if($line != "")
			{
				$expl = explode("|", $line);
				if(count($expl) === count($headers))
				{
					$id_location = array_search("locationID",$headers);
					$return[$expl[$id_location]] = array_combine($headers, $expl);
				}
				else
				{
					$arr = array();
					foreach($headers as $k => $header)
					{
						$arr[$header] = $expl[$k];
					}
					(!isset($arr['dollar_cap']) ? $arr['dollar_cap'] = 0 : null);
					(!isset($arr['qty_cap']) ? $arr['qty_cap'] = 0 : null);
					$return[] = $arr;
				}
			}
			$fh->next();
		}
		$fh->flock(LOCK_UN);
		
		return $return;
	}
	
	public function getArtezRules($charity)
	{
		//Make sure we have info
		if($charity === NULL && !isset($this->charity))
		{
			$return['status'] = FALSE;
			$return['error'][] = "No charity data set.";
			return $return;
		}
		
		$custom_artez = $this->getCustomCharities();		
		
		if(isset($this->_artez_file) || !empty($custom_artez) )
		{
		
			//Check if the charity is valid
			$hits = $this->read_data($this->_artez_file, $data, array('display', 'locationID' , 'qty_cap' , 'dollar_cap'));
			
			$found = 0;
			
			foreach($hits as $id=>$data){
				if($data['display'] == $charity){
					$found = 1;
					$return['qty_cap'] = $data['qty_cap'];
					$return['dollar_cap'] = $data['dollar_cap'];
					break;
				}
			}
			
			if($found == 1)
			{
				$return['status'] = TRUE;

			}
			else
			{
				foreach($custom_artez as $id=>$data){
					if($data['display'] == $charity){
						$found = 1;
						$return['qty_cap'] = $data['qty_cap'];
						$return['dollar_cap'] = $data['dollar_cap'];
						break;
					}
				}
				
				if($found == 1){
					$return['status'] = TRUE;
				}
				else
				{
					$return['status'] = FALSE;
					$return['error'][] = "No matches found.";
				}
			}
			
		}
		else
		{
			$return['status'] = FALSE;
			$return['error'][] = "Artez data not found.";
		}
		
		return $return;			
	}
	
	/**
	 *Sets object variables used in validate_charity()
	 *@param 	Array  	$rules 		The array we're using to set rules and file
	 *@return 	Array	$rules_set	Array of the rules that were set
	 */
	public function set_rules($rules=NULL, $file=NULL)
	{
		$this->_rules_set = array();
		
		if(!isset($this->qty_cap) && isset($rules['qty_cap']))
		{
			if(is_numeric($rules['qty_cap']))
			{
				$this->qty_cap = $rules['qty_cap'];
				$this->_rules_set['qty_cap'] = $rules['qty_cap'];
			}
		}
		
		if(!isset($this->dollar_cap) && isset($rules['dollar_cap']))
		{
			if(is_numeric($rules['dollar_cap']))
			{
				$this->dollar_cap = $rules['dollar_cap'];
				$this->_rules_set['dollar_cap'] = $rules['dollar_cap'];
			}
		}		
	}
	
	/**
	 *	Unsets all the rules.
	 */
	public function unset_rules()
	{
		unset($this->_rules_set);
	}
	
	/**
	 *Calls set_rules and adds the passed rules to the list, loops through all successfully set
	 * rules and calls their respective functions (_check_rulename) and stores the return arrays.
	 *Returns array['status'] TRUE or FALSE with the array['details'] of which rule succeeded/failed.
	 *@param 	Array  	$charity 	Charity details
	 *@param 	Array  	$rules 		The array we're using to set rules 
	 *@param 	String 	$file		The file we're going to scan for the validation
	 *@return 	Array	$status		Array of the rules that passed/failed and the status
	 */
	public function validate_charity($charity=NULL, $artez_pin_code_none, $artez_pin_code_expired)
	{
		//Make sure we have info
		if($charity === NULL && !isset($this->charity) && !isset($this->pin))
		{
			$return['status'] = FALSE;
			$return['error'][] = "No charity data set.";
			return $return;
		}
		unset($this->pin);
		
		//Check if we have to initialize our charity details
		if($charity !== NULL)
		{
			$this->charity = $charity['charity'];
			
			
			$this->charity_id = $charity['charity_id'];
			
			
			$this->pin = $charity['pin'];
			
		}
		
		$custom_artez = $this->getCustomCharities();	

// echo"<pre>";
// print_r($custom_artez);
// echo $this->pin;
// echo"</pre>";

		
		if(isset($this->_artez_file) || !empty($custom_artez) )
		{
			//Set our object variables into a data array
			$data = array();
			(isset($this->charity) ? $data['display'] = $this->charity : NULL);
			(isset($this->charity_id) ? $data['locationID'] = $this->charity_id : NULL);
			
			//Check if the charity is valid
			$hits = $this->read_data($this->_artez_file, $data, array('display', 'locationID', 'pin'));
			
			//Check if the pin is also valid
			if(count($hits) == 1)
			{
				if(isset($this->pin))
				{
					if($hits[0]['pin'] == $this->pin)
					{
						$return['status'] = TRUE;
					}
					else
					{
						$return['status'] = $artez_pin_code_none;
					}
				}
				else
				{
					$return['status'] = $artez_pin_code_none;
				}
			}
			else
			{
				if(isset($this->pin))
				{
					foreach($custom_artez as $id=>$data){
						if($data['display'] == $this->charity){
							if($data['pin'] == $this->pin)
							{
								$return['status'] = TRUE;
							}
							else
							{
								$return['status'] = $artez_pin_code_none;
							}
							break;
						}
					}
					
				}
				else
				{
					$return['status'] = $artez_pin_code_none;
				}
			}
		}
		else
		{
			$return['status'] = FALSE;
			$return['error'][] = "Artez data not found.";
		}
		
		return $return;		
	}
	
	/**
	 *Part of the validate_charity_rules function, this function is called
	 *when a qty_cap rule is set.
	 *@return 	Array	$return		Array of the results of our rule validation
	 */
	private function _check_qty_cap($artez_pin_code_none, $artez_pin_code_expired, $artez_past_discounts)
	{
		$return = array();
		
		if(isset($this->_file))
		{
			$data = array();
			(isset($this->charity) ? $data['charity'] = $this->charity : NULL);
			(isset($this->charity_id) ? $data['charity_id'] = $this->charity_id : NULL);
			(isset($this->pin) ? $data['pin'] = $this->pin : NULL);
			
			if(!isset($this->pin))
			{
				$return['status'] = FALSE;
				$return['error'][] = "Pin is not set";
				return $return;
			}
			
			$hits = $this->read_data($this->_file, $data, FALSE);
			
			$hits += $artez_past_discounts[$data['charity']]['uses'];
			
			// echo"<pre>";
			// print_r($hits);
			
			if($hits !== FALSE)
			{
				if(is_numeric($hits))
				{
					if($hits < $this->qty_cap)
					{
						$return['status'] = 'full';
					}
					else
					{
						$return['status'] = $artez_pin_code_expired;
						$return['error'][] = "Quantity reached";
					}
				}
			}
		}
		else
		{
			$return['status'] = FALSE;
			$return['error'][] = "File doesn't exist";
		}
		
		return $return;
	}
	
	private function _check_dollar_cap($artez_pin_code_none, $artez_pin_code_expired, $artez_past_discounts)
	{
		$return = array();
		
		if(isset($this->_file))
		{
			if(!isset($this->pin))
			{
				$return['status'] = FALSE;
				$return['error'][] = "Pin is not set";
				return $return;
			}
			
			$used = $this->read_data($this->_file, NULL, array("pin", "discount"));
			$totalUsed = 0;
			
			foreach($used as $id=>$info){
				if($info['pin'] == $this->pin){
					$totalUsed += $info['discount'];
				}
			}
			
			$totalUsed += $artez_past_discounts[$this->charity]['discount'];
			
			 // echo"<pre>";
			 // echo $this->charity;
			 // echo $totalUsed ."<br>";
			  // echo"</pre>";
			
			if($used !== FALSE)
			{
				if($totalUsed < $this->dollar_cap)
				{
					$return['status'] = 'full';
				}
				else
				{
					$return['status'] = $artez_pin_code_expired;
					$return['error'][] = "Quantity reached";
				}
			}
			
			
		}
		else
		{
			$return['status'] = FALSE;
			$return['error'][] = "File doesn't exist";
		}
		
		return $return;
	}
	
	/**
	 *Validates the charity, sets and validates against rules and returns the
	 *appropriate discount based on the extent of validation.
	 *
	 *There are 3 scenarios, a valid charity + a valid pin (and passing all the rules if any are set)
	 *will return a "full" discount. If the pin is invalid or we fail any rule, we return a "partial" discount,
	 *otherwise we return no discount.
	 *
	 *@param 	Array  	$charity 		Charity details
	 *@param 	Array  	$rules 			The array we're using to set rules and validate against
	 *@param 	Array 	$discounts		The discount amounts to return (full/partial)
	 *@param 	String	$file			The file to scan for validation of qty/dollar rules
	 *@return 	Array	$final_data		Final array containing the details of the validation process and discount amount.
	 */
	public function validate_charity_rules($charity=NULL, $rules=NULL, $discounts=NULL, $file=NULL, $artez_pin_code_none, $artez_pin_code_expired, $artez_past_discounts)
	{
		$return = array();
		
		if($discounts !== NULL)
		{
			(isset($discounts['partial']) ? $this->discount_partial = $discounts['partial'] : NULL);
			(isset($discounts['full']) ? $this->discount_full = $discounts['full'] : NULL);
			(isset($discounts['race']) ? $this->discount_race = $discounts['race'] : NULL);
		}
		
		$response = $this->validate_charity($charity, $artez_pin_code_none, $artez_pin_code_expired);
				
		//Make sure our charity exists
		if($response['status'] !== FALSE)
		{
			if(!isset($this->_file) && $file === NULL)
			{
				$return['status'] = FALSE;
				$return['error'][] = "Specify data file to search";
				return $return;
			}
			
			if($file !== NULL)
			{
				if(FILE_EXISTS($file))
				{
					$this->_file = $file;
				}
				else
				{
					$return['status'] = FALSE;
					$return['error'][] = "File specified doesn't exist";
					return $return;
				}
			}
			
			//Check if we have any information on rules and if we have none set
			if(!isset($this->qty_cap) && !isset($this->dollar_cap) && !isset($this->_rules_set))
			{
				if($rules !== NULL)
				{
					$this->set_rules($rules, $file);
					//$this->getArtezRules($charity);
				}
			}
			
			//Make sure we have rules before jumping into the magic		
			if(!empty($this->_rules_set))
			{
				//Call each rule's function
				foreach($this->_rules_set as $name => $qty)
				{
					$summary[$name] = $this->{"_check_".$name}($artez_pin_code_none, $artez_pin_code_expired, $artez_past_discounts);
				}
				
				$final_data['discount'] = 0;
				
				// echo"<pre>";
				// print_r($summary);
				// print_r($response);
				// echo"</pre>";

				foreach($summary as $details)
				{
					if($response['status'] === TRUE)
					{
						
						if($details['status'] == "partial")
						{
							$final_data['discount'] = $this->discount_partial;
							$final_data['set_race_fee'] = false;
							 //echo "partial reached" . "<br>";
							break;
						}
						else if($details['status'] == 'full')
						{
							$final_data['discount'] = $this->discount_full;
							$final_data['set_race_fee'] = false;
							// echo "full reached" . "<br>";
						}
						else if($details['status'] == 'race')
						{
							$final_data['discount'] = $this->discount_race;
							$final_data['set_race_fee'] = true;
							 //echo "race reached" . "<br>";
							break;
						}
						else
						{
							$final_data['discount'] = 0;
							$final_data['set_race_fee'] = false;
							 //echo "blank reached" . "<br>";
							break;
						}
					}
					else if($response['status'] == 'partial')
					{
						$final_data['discount'] = $this->discount_partial;
						$final_data['set_race_fee'] = false;
						// echo "partial reached" . "<br>";
						break;
					}
					else if($response['status'] == 'full')
					{
						$final_data['discount'] = $this->discount_full;
						$final_data['set_race_fee'] = false;
						// echo "full reached" . "<br>";
					}
					else if($response['status'] == 'race')
					{
						$final_data['discount'] = $this->discount_race;
						$final_data['set_race_fee'] = true;
						// echo "race reached" . "<br>";
					}
					else if($response['status'] == 'none')
					{
						$final_data['discount'] = 0;
						$final_data['set_race_fee'] = false;
						// echo "none reached" . "<br>";
					}
					else
					{
						$final_data['discount'] = 0;
						$final_data['set_race_fee'] = false;
						// echo "blank reached" . "<br>";
						// echo $response['status'] . "<br>";
						break;
					}
				}
				$final_data['rules_summary'] = $summary;
				
				return $final_data;
			}
			else
			{
				$return['status'] = FALSE;
				$return['error'][] = "No rules set";
				return $return;
			}
		}
		else
		{
			$return['data']['discount'] = 0;
			$return['status'] = FALSE;
			$return['error'][] = "Invalid charity";
			return $return;
		}
	}
	
	public function create_participant($input=NULL)
	{
		// $input['event_id'] = '96605';
		// $input['first_name'] = 'Eric';
		// $input['last_name'] = 'Roy';
		// $input['invoice'] = 'JME123';
		// $input['address1'] = '155 Colonnade';
		// $input['address2'] = '';
		// $input['address3'] = '';
		// $input['language_preference'] = 'en-CA';
		// $input['city'] = 'Ottawa';
		// $input['province'] = 'Ontario';
		// $input['country'] = 'Canada';
		// $input['postal_code'] = 'K1K 1K1';
		// $input['home_phone'] = '6135555555';
		// $input['email'] = 'eroy@eventsonline.ca';
		// $input['charity_id'] = json_encode(utf8_encode('ArthritisSociety'));
		// $input['registration_type'] = 'Individual Registration';
		// $input['artez_username'] = 'jimbo3005';
		// $input['artez_password'] = '1234';
		if(isset($input['event_id']) &&
		   isset($input['first_name']) &&
		   isset($input['last_name']) &&
		   isset($input['invoice']) &&
		   isset($input['address1']) &&
		   isset($input['address2']) &&
		   isset($input['address3']) &&
		   isset($input['language_preference']) &&
		   isset($input['city']) &&
		   isset($input['province']) &&
		   isset($input['country']) &&
		   isset($input['postal_code']) &&
		   isset($input['home_phone']) &&
		   isset($input['email']) &&
		   isset($input['charity_id']) &&
		   isset($input['registration_type']) &&
		   isset($input['artez_username']) &&
		   isset($input['artez_password']))
		{
			$c = curl_init();
			curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($c, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));
			curl_setopt($c, CURLOPT_URL, 'https://secure.e2rm.com/webSetService/RegisterParticipant.asmx/InsertJSONDebug');
			curl_setopt($c, CURLOPT_POSTFIELDS, '{"Registration":{
											"EventID":'.$input['event_id'] .',
											"PaymentAttempt":false,
											"PaymentReattempt":0,
											"Registrant":[{
												"FirstName":"'.$input['first_name'].'",
												"LastName":"'.$input['last_name'].'",
												"LanguagePref":"' . $input['language_preference'] . '",
												"BusinessTitle":"'.$input['invoice'].'",
												"Address":[{
													"AddressType":"Home Address",
													"AddressLine1":"'.$input['address1'].'",
													"AddressLine2":"'.$input['address2'].'",
													"AddressLine3":"'.$input['address3'].'",
													"City":"'.$input['city'].'",
													"Province":"'.$input['province'].'",
													"ProvinceCode":"",
													"Country":"'.$input['country'].'",
													"CountryCode":"",
													"PostalCode":"'.$input['postal_code'].'"
												}],
												"Phone":[{"PhoneType":"Home Phone","CountryCode":"","AreaCode":"","PhoneNumber":"'.$input['home_phone'].'","Extension":""}],
												"Email":[{"EmailType":"Home Email Address","EmailAddress":"'.$input['email'].'"}],
												"LocationExportID":'.$input['charity_id'].',
												"RegistrationFeeExportID":"'.$input['registration_type'].'",
												"FundraisingGoal":100,
												"SearchPermission":true,
												"ScoreboardPermission":true,
												"EmailPermission":true,
												"PostPermission":true,
												"SendNotifications":true,
												"WaiverAccepted":true,
												"LoginInfo":{"LoginName":"'.$input['artez_username'].'","Password":"'.$input['artez_password'].'","EncryptedPassword":""}
											}]}
										}');

		}
		//GET RETURN CONTENT
		$content = curl_exec($c);
		
		$retVal = json_decode($content, true);

		//CLOSE
		curl_close($c);
		
		if($retVal['d'][0]['type'] == "Success")
		{
			$return_data['status'] = TRUE;
			$return_data['description'] = $retVal['d'][0]['description'];
		}
		else
		{
			$return_data['status'] = FALSE;
			$return_data['description'] = $retVal['d'][0]['description'];
		}
		
		return $return_data;
	}
	
	public function login_participant_button($input=NULL)
	{
	
	}
	
	/**
	 *Reads data from given text file and returns rows found in a method similar to a database call
	 *
	 *@param string $_dir					A valid text file that contains data
	 *@param array  $filter   				Array that will be required, for example array('id' => "123") will find all ids 123.
	 *										You can also pass it a multi-dimensional array, for example array('id' => array('123, '1234', '12345')).
	 *@param array/boolean/string  $return	Returned data, will return the columns in the array('first_name', 'last_name') when an array is passed.
	 *										Will return everything when TRUE (default) and a count of rows on FALSE
	 *										When passed "COUNT_BY_FILTER", will return counts of instances per filter. Example array('race' => array('race1, 'race2')) will return array['race']['race1'] //5
	 *@return array/integer					Rows of data
	 */
	public function read_data($_dir=NULL, $filter=NULL, $return=TRUE)
	{
		if(FILE_EXISTS($_dir))
		{
			$_handle = new SplFileObject($_dir, 'r');
			$_handle->flock(LOCK_EX);
			$_headers = preg_replace('/^\s+|\n|\r|\s+$/m', '', $_handle->current());
			$headers = explode('%', $_headers);
			$_handle->seek(1);
			$data_found = array();
			
			if($return === FALSE)
			{
				$data_found['count'] = 0;
			}
			
			//If we have no requirements...
			if($filter === NULL)
			{
				while(!$_handle->eof())
				{
					$_ = $_handle->current();
					//If we request anything but false
					if($return !== FALSE)
					{
						if($_ != '')
						{
							$line = preg_replace('/^\s+|\n|\r|\s+$/m', '', $_);
							$line_array = explode('%', $line);
							
							//Safety when general modify is used (it adds trailing %)
							if(count($headers)+1 == (count($line_array)))
							{
								array_pop($line_array);
							}
							$data = array_combine($headers, $line_array);
							
							//Default, returns all
							if($return === TRUE)
							{
								$data_found[] = $data;
							}
							//Returns requested fields
							elseif(is_array($return))
							{
								$return_fields = array();
								foreach($return as $field)
								{
									$return_fields[$field] = $data[$field];
								}
								$data_found[] = $return_fields;
							}
						}
					}
					//If false, we return counts
					else
					{
						$data_found['count']++;
					}
					$_handle->next();
				}
				$_handle->flock(LOCK_UN);
				if($return === FALSE)
				{
					return $data_found['count'];
				}
				else
				{
					return $data_found;
				}
			}
			//Requiring specific fields/values
			elseif(is_array($filter))
			{
				//Requiring specific fields that equal specific values
				if($this->is_associative_array($filter))
				{
					$filter_count = count($filter);
					
					//Returning counts per array element
					if($return === 'COUNT_PER_FILTER')
					{
						$chrono_build = array();
						$validate = array();
						//Build the chronological order of array values
						//If passing an array, we're going deeper later on, store name
						//Otherwise, store as a validation field
						foreach($filter as $field => $required)
						{
							if(is_array($required))
							{
								$chrono_build[] = $field;
							}
							else
							{
								$validate[] = $required;
							}
						}
					}
					
					while(!$_handle->eof())
					{
						$_ = $_handle->current();
						
						if($_ != '')
						{
							$filter_check = 0;
							$line = preg_replace('/^\s+|\n|\r|\s+$/m', '', $_);
							$line_array = explode('%', $line);
							//Safety when general modify is used (it adds trailing %)
							if(count($headers)+1 == count($line_array))
							{
								array_pop($line_array);
							}
							
							//Make sure we're valid
							if(count($headers) === count($line_array))
							{
								$build_array = FALSE;
								$data = array_combine($headers, $line_array);
								//Here we go...
								//We flip through the required fields
								foreach($filter as $field => $required)
								{
									//If we have an array and we're not returning counts per filter
									//Check to see if our value is in the array of requirements (STRICT)
									if(is_array($required) && $return !== 'COUNT_PER_FILTER')
									{
										if(in_array($data[$field], $required, TRUE))
										{
											$filter_check++;
										}
									}
									//If we are returning counts per filter
									elseif($return === 'COUNT_PER_FILTER')
									{
										$chrono_count = count($chrono_build);
										
										//Verify if we're checking an array
										//Check if value is in the array of requirements (STRICT)
										if(is_array($required))
										{
											if(in_array($field, $chrono_build) && in_array($data[$field], $required, TRUE) && $build_array === FALSE)
											{
												//Green light, let's keep going...
												//Verify that this record is valid is ALL required fields
												$valid = TRUE;
												if(!empty($validate))
												{
													foreach($validate as $field)
													{
														if($data[$field] == '')
														{
															$valid = FALSE;
														}
													}
												}
												
												//We're clear... build the multidimensional array dynamically
												if($valid === TRUE)
												{
													$build_array = TRUE;
													$this->build_multidimensional_array($data_found, $chrono_build, $data);
												}
											}
										}
									}
									//Assume we want counts
									else
									{
										if($data[$field] == $required)
										{
											$filter_check++;
										}
									}
								}
								//If we passed all the tests...
								if($filter_check == $filter_count)
								{
									//Store everything
									if($return === TRUE)
									{
										$data_found[] = $data;										
									}
									//Store requested fields
									elseif(is_array($return))
									{
										$return_fields = array();
										foreach($return as $field)
										{
											$return_fields[$field] = $data[$field];
										}
										$data_found[] = $return_fields;
									}
									//Store count
									else
									{
										$data_found['count']++;
									}
								}
							}
						}
						$_handle->next();
					}
					
					$_handle->flock(LOCK_UN);
					
					//If FALSE, return count
					if($return === FALSE)
					{
						return $data_found['count'];
					}
					//Anything else
					else
					{
						return $data_found;
					}
				}
				//Filter is a numeric array
				else
				{
					$indices = array();
					//Find all the header positions
					foreach($filter as $needle)
					{
						$index = array_search($needle, $headers);
						if($index !== FALSE)
						{
							$indices[$needle] = $index;
						}
					}
					
					while(!$_handle->eof())
					{
						$_ = $_handle->current();
						
						if($_ != '')
						{
							$filter_check = 0;
							$line = preg_replace('/^\s+|\n|\r|\s+$/m', '', $_);
							$line_array = explode('%', $line);
							//Make sure we found something earlier
							if(!empty($indices))
							{
								foreach($indices as $header => $index)
								{
									if($line_array[$index] != '')
									{
										if($return === TRUE)
										{
											if(count($headers)+1 == count($line_array))
											{
												array_pop($line_array);
											}
											$data = array_combine($headers, $line_array);
											$return_fields[] = $data;
										}
										elseif(is_array($return))
										{
											if(count($headers)+1 == count($line_array))
											{
												array_pop($line_array);
											}
											
											$data = array_combine($headers, $line_array);
											$return_fields = array();
											foreach($return as $field)
											{
												$return_fields[$field] = $data[$field];
											}
											$data_found[] = $return_fields;
										}
										else
										{
											$data_found['count']++;
										}
									}
								}
							}
							else
							{
								//COULDN'T FIND REQUESTED HEADERS
								$_handle->flock(LOCK_UN);
								return FALSE;
							}
						}
						$_handle->next();
					}
					$_handle->flock(LOCK_UN);
					if($return === FALSE || $return === 'COUNT_PER_FILTER')
					{
						return $data_found['count'];
					}
					else
					{
						return $data_found;
					}
				}
			}
			else
			{
				//RETURN NOT VALID
				$_handle->flock(LOCK_UN);
				return FALSE;
			}
		}
		else
		{
			//FILE DOESN'T EXIST
			return FALSE;
		}
	}


	/**
	 *Builds a multidimensional array dynamically
	 *@param Array  $array 	The array we're using to continue building on
	 *@return Array 		The finished array
	 */
	public function build_multidimensional_array(&$array, $all_keys, $data)
	{
		$last = array_pop($all_keys);
		
		$last = $data[$last];
		foreach($all_keys as $key)
		{
			if((array_key_exists($key, $array) || !array_key_exists($key, $array)) && is_array($array[$key]))
			{
				$array[$data[$key]] = array();
			}
			$array = &$array[$data[$key]];
		}
		$count++;
		
		$array[$last]++;
		return $array;
	}
	
	/**
	 *Checks to see if and array is associative
	 *@param array 
	 *@return boolean
	 */
	public function is_associative_array($arr)
	{
		return array_keys($arr) !== range(0, count($arr) - 1);
	}
}
// $input = array();
// $artez = new EOL_Artez_Services('artez_charities.txt');

// $rules['qty_cap'] = 10;

// $discount['full'] = 150;
// $discount['partial'] = 70;

// $charity['pin'] = 'SFC2013';
// $charity['charity'] = 'Alzheimer Society of Nova Scotia';

// print_r($artez->validate_charity_rules($charity, $rules, $discount, 'artez_registrations.txt'));

// $artez->login_participant_button();
// $artez->create_participant($input);

//Code to create a "Visit my pledge profile"
//<!--<form method="post" action="https://secure.e2rm.com/registrant/LoginRegister.aspx?eventid=96605">    User ID: <input name="txtUserID" maxlength="255"><br />    Password: <input name="txtPassword" type="password" maxlength="50"><br />    <input type="submit" value="Log In"></form>-->
?>


