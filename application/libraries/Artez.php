<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
* Bridge with the Artez charity system
*
* @category   	EventsOnline
* @author				Eric Roy
* @package    	Artez
* @dependencies	Database Class
* @copyright 		Copyright (c) 2014-End of time Events Online (http://www.eventsonline.ca)
* @license   		Apache License 2.0 http://opensource.org/licenses/Apache-2.0
* @version   		Release: 1.0
* @since    		Class available since Release 1.0
* @deprecated 	
*/
class Artez
{
	private $_CI;
	private $db;  
  public $qty_cap;
  public $dollar_cap;
  public $charity_id;
  public $discount_full;
  public $discount_partial;
  public $charity;
  public $pin;
  private $_artez_file;
  private $_file;  
  private $_rules_set;

	public function __construct()
	{
			$this->_CI =& get_instance();
			$this->db = $this->_CI->db;
      unset($this->pin);
      unset($this->charity);
      unset($this->charity_id);
	}

  public function get_all_charities()
  {
    $artezCharities = $this->fetch_artez_charities();
    $liveCharities = $this->fetch_saved_artez_charities($_SESSION['admin']['adminEventID']);

    $custom = array();
    $live = array();
    $pending = array();

    foreach($artezCharities as $artezID => $artezName)
    {
      if(array_key_exists($artezID, $liveCharities))
      {
        $live[] = $liveCharities[$artezID];
      }
      else
      {
        $data = array(
          "artez_event_id" => $artezID,
          "artez_charity" => $artezName
          );
        $pending[] = $data;
      }
    }

    foreach($liveCharities as $artezID => $settings)
    {
      if(stripos($artezID, "CSTM") === 0)
      {
        $custom[] = $settings;
      }
    }

    $allCharities = array(
      "custom" => $custom,
      "live" => $live,
      "pending" => $pending
      );

    return $allCharities;
  }

  public function get_live_charities()
  {
    $sql = "SELECT entAC.intArtezCharityID AS artezCharityID, relACP.intEventID AS event_id, entAC.strArtezCharity AS artez_charity, entAC.strArtezOneTimeDonationID AS artez_donation_id, entACP.intArtezCharityID AS artez_charity_id, entAC.strArtezEventID AS artez_event_id, entACP.strPin AS pin, entACP.intQuantity AS qty, entACP.dblAmount AS amount FROM relArtezCharityPin AS relACP
    INNER JOIN entArtezCharityPin AS entACP
    ON relACP.intArtezCharityPinID = entACP.intArtezCharityID
    INNER JOIN entArtezCharity AS entAC
    ON entACP.intArtezCharityID = entAC.intArtezCharityID";

    $parsedArray = array();
    try
    {
      $statement = $this->db->prepare($sql);

      if($statement->execute())
      {
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        foreach($results as $row)
        {
          if($row['artez_event_id'] == NULL)
          {
            $parsedArray['CSTM'.uniqid()] = $row;
          }
          else
          {
            $parsedArray[$row['artez_event_id']] = $row; 
          }
        }
      }

    }
    catch(Exception $e)
    {
      //TODO: Proper catching
    }
    return $parsedArray;
  }

  public function get_valid_live_charities()
  {
    $live = $this->get_live_charities();
    $charityUses = $this->get_charity_pin_uses();
    $valid = array();
    foreach($live as $id => $charity)
    {
      $validUses = FALSE;
      $validAmount = FALSE;
      if(array_key_exists($charity['artez_charity_id'], $charityUses))
      {
        if($charityUses[$charity['artez_charity_id']]['uses'] < $charity['qty'])
        {
          $validUses = TRUE;
        }

        if($charityUses[$charity['artez_charity_id']]['discount'] < $charity['amount'])
        {
          $validAmount = TRUE;
        }

        if($validUses === TRUE && $validAmount === TRUE)
        {
          $valid[$charity['artezCharityID']] = $charity;
        }
      }
      else
      {
        $valid[$charity['artezCharityID']] = $charity;
      }
    }

    return $valid;
  }

  public function validate_username($username)
  {
    //Series OrgID
    $orgID = '291';
    $c = curl_init();
    curl_setopt($c, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($c, CURLOPT_HTTPGET, TRUE);
    curl_setopt($c, CURLOPT_URL, 'https://secure.e2rm.com/webgetservice/get.asmx/loginNameExists?orgID='.$orgID.'&logName='.$username);
    
    //GET RETURN CONTENT
    $content = curl_exec($c);
    // echo $content;exit;
    //CLOSE
    curl_close($c);
    
    $retVal = strstr($content, "false");

    if ($retVal == FALSE) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  public function get_charity_pin_uses()
  {
    $parsedArray = array();
    $sql = "SELECT COUNT(intArtezCharityID) AS uses, sum(case when strUsedPin = 'y' then 1 else 0 end) AS pin, intArtezCharityID AS artezID, SUM(dblDiscountedAmount) AS discount FROM relArtezCharityAccount
    GROUP BY artezID;";
    $stmt1 = $this->db->prepare($sql);
    if($stmt1->execute())
    {
      $results = $stmt1->fetchAll(PDO::FETCH_ASSOC);
      foreach($results as $row)
      {
        $parsedArray[$row['artezID']] = $row;
      }
    }
    else
    {
      //TODO: Alert - failed to get uses of artez discounts from relArtezCharityAccount
    }
    return $parsedArray;
  }

  public function fetch_saved_artez_charities()
  {
    $sql = "SELECT relACP.intEventID AS event_id, entAC.strArtezCharity AS artez_charity, entAC.strArtezOneTimeDonationID AS artez_donation_id, entACP.intArtezCharityID AS artez_charity_id, entAC.strArtezEventID AS artez_event_id, entACP.strPin AS pin, entACP.intQuantity AS qty, entACP.dblAmount AS amount FROM relArtezCharityPin AS relACP
    INNER JOIN entArtezCharityPin AS entACP
    ON relACP.intArtezCharityPinID = entACP.intArtezCharityID
    INNER JOIN entArtezCharity AS entAC
    ON entACP.intArtezCharityID = entAC.intArtezCharityID";

    $parsedArray = array();
        
    try
    {
      $stmt1 = $this->db->prepare($sql);

      if($stmt1->execute())
      {
        $results = $stmt1->fetchAll(PDO::FETCH_ASSOC);

        foreach($results as $row)
        {
          if($row['artez_event_id'] == NULL)
          {
            $parsedArray['CSTM'.uniqid()] = $row;
          }
          else
          {
            $parsedArray[$row['artez_event_id']] = $row; 
          }
        }
      }
    }
    catch(Exception $e)
    {
      //TODO: Proper catching
    }
    return $parsedArray;
  }

  public function fetch_artez_charities()
  {
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_URL => 'http://my.e2rm.com/webgetservice/get.asmx/getLocations?eventID=153464&languageCode=en-CA&eventLocationTypeID=&Source=eventsonline'
    ));
        
    //XML list of charities from Artez
    $response = curl_exec($curl);
    
    $charities = new SimpleXMLElement($response);
    
    foreach($charities->location_collection->location as $location)
    {
      $data = array(
        'display' => (string)$location->Name,
        'locationID' => (string)$location->locationID
      );
      $charityList[] = $data;
      $charityNames[(string)$location->locationID] = (string)$location->Name;
    }

    return $charityNames;
  }
	
	public function get_charities($eventID = NULL)
	{
		if($eventID === NULL)
		{
			return array();
		}
		$results = array();
    if(is_array($eventID))
    {
    	$parsedEventID = implode(",", $eventID);
    }
    else
    {
    	$parsedEventID = $eventID;
    }

		$sql = "SELECT relACP.intEventID AS event_id, entAC.strArtezCharity AS artez_charity, entAC.strArtezCharityDisplay AS artez_charity_display, entACP.intArtezCharityID AS artez_charity_id, entAC.strArtezEventID FROM relArtezCharityPin AS relACP
						INNER JOIN entArtezCharityPin AS entACP
						ON relACP.intArtezCharityPinID = entACP.intArtezCharityID
						INNER JOIN entArtezCharity AS entAC
						ON entACP.intArtezCharityID = entAC.intArtezCharityID
						WHERE relACP.intEventID IN (".$parsedEventID.")";

		try
		{

	    $statement = $this->db->query($sql);
      if($statement)
      {
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
      }
    }
    catch(Exception $e)
    {
    	//TODO: Proper catching
    }
    return $results;
	}
  
  // public function getCharities()
  // {
  //   $sql = "SELECT eACP.intArtezCharityID AS id, eACP.strPin AS pin, eAC.strArtezCharity AS locationID, eAC.strArtezOneTimeDonationID AS donationID, eACP.intQuantity AS qty_cap, eACP.dblAmount AS dollar_cap FROM entArtezCharityPin AS eACP
  //   INNER JOIN entArtezCharity AS eAC
  //   ON eAC.intArtezCharityID = eACP.intArtezCharityID";

  //   $statement = $this->db->prepare($sql);
  //   if($statement->execute())
  //   {
  //     $results = $statement->fetchAll(PDO::FETCH_ASSOC);

  //     return $results;
  //   }
  //   else
  //   {
  //     //TODO: Send alert - query failed
  //   }
  // }
    
  /**
   *Sets object variables used in validate_charity()
   *@param  Array   $rules    The array we're using to set rules and file
   *@return   Array $rules_set  Array of the rules that were set
   */
  public function set_rules($rules=NULL)
  {
    $this->_rules_set = array();
    
    if(!isset($this->qty_cap) && isset($rules['qty_cap']))
    {
      if(is_numeric($rules['qty_cap']))
      {
        $this->qty_cap = $rules['qty_cap'];
        $this->_rules_set['qty_cap'] = $rules['qty_cap'];
      }
    }
    
    if(!isset($this->dollar_cap) && isset($rules['dollar_cap']))
    {
      if(is_numeric($rules['dollar_cap']))
      {
        $this->dollar_cap = $rules['dollar_cap'];
        $this->_rules_set['dollar_cap'] = $rules['dollar_cap'];
      }
    }   
  }
  
  /**
   *  Unsets all the rules.
   */
  public function unset_rules()
  {
    unset($this->_rules_set);
  }
  
  /**
   *Calls set_rules and adds the passed rules to the list, loops through all successfully set
   * rules and calls their respective functions (_check_rulename) and stores the return arrays.
   *Returns array['status'] TRUE or FALSE with the array['details'] of which rule succeeded/failed.
   *@param  Array   $charity  Charity details
   *@param  Array   $rules    The array we're using to set rules 
   *@param  String  $file   The file we're going to scan for the validation
   *@return   Array $status   Array of the rules that passed/failed and the status
   */
  public function validate_charity($charity=NULL, $artez_pin_code_none, $artez_pin_code_expired)
  {
    //Make sure we have info
    if($charity === NULL && !isset($this->charity) && !isset($this->pin))
    {
      $return['status'] = FALSE;
      $return['error'][] = "No charity data set.";
      return $return;
    }
    unset($this->pin);
    
    //Check if we have to initialize our charity details
    if($charity !== NULL)
    {
      $this->charity = $charity['charity'];
      
      
      $this->charity_id = $charity['charity_id'];
      
      
      $this->pin = $charity['pin'];
      
    }

// echo"<pre>";
// print_r($custom_artez);
// echo $this->pin;
// echo"</pre>";

    //Set our object variables into a data array
    $data = array();
    (isset($this->charity) ? $data['display'] = $this->charity : NULL);
    (isset($this->charity_id) ? $data['locationID'] = $this->charity_id : NULL);
    
    //Check if the charity is valid
    $hits = $this->read_data($this->_artez_file, $data, array('display', 'locationID', 'pin'));
    
    //Check if the pin is also valid
    if(count($hits) == 1)
    {
      if(isset($this->pin))
      {
        if($hits[0]['pin'] == $this->pin)
        {
          $return['status'] = TRUE;
        }
        else
        {
          $return['status'] = $artez_pin_code_none;
        }
      }
      else
      {
        $return['status'] = $artez_pin_code_none;
      }
    }
    else
    {
      if(isset($this->pin))
      {
        foreach($custom_artez as $id=>$data){
          if($data['display'] == $this->charity){
            if($data['pin'] == $this->pin)
            {
              $return['status'] = TRUE;
            }
            else
            {
              $return['status'] = $artez_pin_code_none;
            }
            break;
          }
        }
        
      }
      else
      {
        $return['status'] = $artez_pin_code_none;
      }
    }
    
    return $return;   
  }
  
  /**
   *Part of the validate_charity_rules function, this function is called
   *when a qty_cap rule is set.
   *@return   Array $return   Array of the results of our rule validation
   */
  private function _check_qty_cap($artez_pin_code_none, $artez_pin_code_expired, $artez_past_discounts)
  {
    $return = array();
    
    if(isset($this->_file))
    {
      $data = array();
      (isset($this->charity) ? $data['charity'] = $this->charity : NULL);
      (isset($this->charity_id) ? $data['charity_id'] = $this->charity_id : NULL);
      (isset($this->pin) ? $data['pin'] = $this->pin : NULL);
      
      if(!isset($this->pin))
      {
        $return['status'] = FALSE;
        $return['error'][] = "Pin is not set";
        return $return;
      }
      
      $hits = $this->read_data($this->_file, $data, FALSE);
      
      $hits += $artez_past_discounts[$data['charity']]['uses'];
      
      // echo"<pre>";
      // print_r($hits);
      
      if($hits !== FALSE)
      {
        if(is_numeric($hits))
        {
          if($hits < $this->qty_cap)
          {
            $return['status'] = 'full';
          }
          else
          {
            $return['status'] = $artez_pin_code_expired;
            $return['error'][] = "Quantity reached";
          }
        }
      }
    }
    else
    {
      $return['status'] = FALSE;
      $return['error'][] = "File doesn't exist";
    }
    
    return $return;
  }
  
  private function _check_dollar_cap($artez_pin_code_none, $artez_pin_code_expired, $artez_past_discounts)
  {
    $return = array();
    
    if(isset($this->_file))
    {
      if(!isset($this->pin))
      {
        $return['status'] = FALSE;
        $return['error'][] = "Pin is not set";
        return $return;
      }
      
      $used = $this->read_data($this->_file, NULL, array("pin", "discount"));
      $totalUsed = 0;
      
      foreach($used as $id=>$info){
        if($info['pin'] == $this->pin){
          $totalUsed += $info['discount'];
        }
      }
      
      $totalUsed += $artez_past_discounts[$this->charity]['discount'];
      
       // echo"<pre>";
       // echo $this->charity;
       // echo $totalUsed ."<br>";
        // echo"</pre>";
      
      if($used !== FALSE)
      {
        if($totalUsed < $this->dollar_cap)
        {
          $return['status'] = 'full';
        }
        else
        {
          $return['status'] = $artez_pin_code_expired;
          $return['error'][] = "Quantity reached";
        }
      }
      
      
    }
    else
    {
      $return['status'] = FALSE;
      $return['error'][] = "File doesn't exist";
    }
    
    return $return;
  }
  
  /**
   *Validates the charity, sets and validates against rules and returns the
   *appropriate discount based on the extent of validation.
   *
   *There are 3 scenarios, a valid charity + a valid pin (and passing all the rules if any are set)
   *will return a "full" discount. If the pin is invalid or we fail any rule, we return a "partial" discount,
   *otherwise we return no discount.
   *
   *@param  Array   $charity    Charity details
   *@param  Array   $rules      The array we're using to set rules and validate against
   *@param  Array   $discounts    The discount amounts to return (full/partial)
   *@param  String  $file     The file to scan for validation of qty/dollar rules
   *@return   Array $final_data   Final array containing the details of the validation process and discount amount.
   */
  public function validate_charity_rules($charity=NULL, $rules=NULL, $discounts=NULL, $artez_pin_code_none, $artez_pin_code_expired, $artez_past_discounts)
  {
    $return = array();
    
    if($discounts !== NULL)
    {
      (isset($discounts['partial']) ? $this->discount_partial = $discounts['partial'] : NULL);
      (isset($discounts['full']) ? $this->discount_full = $discounts['full'] : NULL);
      (isset($discounts['race']) ? $this->discount_race = $discounts['race'] : NULL);
    }

    $response = $this->validate_charity($charity, $artez_pin_code_none, $artez_pin_code_expired);
        
    //Make sure our charity exists
    if($response['status'] !== FALSE)
    {
      //Check if we have any information on rules and if we have none set
      if(!isset($this->qty_cap) && !isset($this->dollar_cap) && !isset($this->_rules_set))
      {
        if($rules !== NULL)
        {
          $this->set_rules($rules);
        }
      }
      
      //Make sure we have rules before jumping into the magic   
      if(!empty($this->_rules_set))
      {
        //Call each rule's function
        foreach($this->_rules_set as $name => $qty)
        {
          $summary[$name] = $this->{"_check_".$name}($artez_pin_code_none, $artez_pin_code_expired, $artez_past_discounts);
        }
        
        $final_data['discount'] = 0;
        
        foreach($summary as $details)
        {
          if($response['status'] === TRUE)
          {
            
            if($details['status'] == "partial")
            {
              $final_data['discount'] = $this->discount_partial;
              $final_data['set_race_fee'] = false;
               //echo "partial reached" . "<br>";
              break;
            }
            else if($details['status'] == 'full')
            {
              $final_data['discount'] = $this->discount_full;
              $final_data['set_race_fee'] = false;
              // echo "full reached" . "<br>";
            }
            else if($details['status'] == 'race')
            {
              $final_data['discount'] = $this->discount_race;
              $final_data['set_race_fee'] = true;
               //echo "race reached" . "<br>";
              break;
            }
            else
            {
              $final_data['discount'] = 0;
              $final_data['set_race_fee'] = false;
               //echo "blank reached" . "<br>";
              break;
            }
          }
          else if($response['status'] == 'partial')
          {
            $final_data['discount'] = $this->discount_partial;
            $final_data['set_race_fee'] = false;
            // echo "partial reached" . "<br>";
            break;
          }
          else if($response['status'] == 'full')
          {
            $final_data['discount'] = $this->discount_full;
            $final_data['set_race_fee'] = false;
            // echo "full reached" . "<br>";
          }
          else if($response['status'] == 'race')
          {
            $final_data['discount'] = $this->discount_race;
            $final_data['set_race_fee'] = true;
            // echo "race reached" . "<br>";
          }
          else if($response['status'] == 'none')
          {
            $final_data['discount'] = 0;
            $final_data['set_race_fee'] = false;
            // echo "none reached" . "<br>";
          }
          else
          {
            $final_data['discount'] = 0;
            $final_data['set_race_fee'] = false;
            // echo "blank reached" . "<br>";
            // echo $response['status'] . "<br>";
            break;
          }
        }
        $final_data['rules_summary'] = $summary;
        
        return $final_data;
      }
      else
      {
        $return['status'] = FALSE;
        $return['error'][] = "No rules set";
        return $return;
      }
    }
    else
    {
      $return['data']['discount'] = 0;
      $return['status'] = FALSE;
      $return['error'][] = "Invalid charity";
      return $return;
    }
  }
  
  public function create_participant($input=NULL)
  {
    // $input['event_id'] = '96605';
    // $input['first_name'] = 'Eric';
    // $input['last_name'] = 'Roy';
    // $input['invoice'] = 'JME123';
    // $input['address1'] = '155 Colonnade';
    // $input['address2'] = '';
    // $input['address3'] = '';
    // $input['language_preference'] = 'en-CA';
    // $input['city'] = 'Ottawa';
    // $input['province'] = 'Ontario';
    // $input['country'] = 'Canada';
    // $input['postal_code'] = 'K1K 1K1';
    // $input['home_phone'] = '6135555555';
    // $input['email'] = 'eroy@eventsonline.ca';
    // $input['charity_id'] = json_encode(utf8_encode('ArthritisSociety'));
    // $input['registration_type'] = 'Individual Registration';
    // $input['artez_username'] = 'jimbo3005';
    // $input['artez_password'] = '1234';
    if(isset($input['event_id']) &&
       isset($input['first_name']) &&
       isset($input['last_name']) &&
       isset($input['invoice']) &&
       isset($input['address1']) &&
       isset($input['address2']) &&
       isset($input['address3']) &&
       isset($input['language_preference']) &&
       isset($input['city']) &&
       isset($input['province']) &&
       isset($input['country']) &&
       isset($input['postal_code']) &&
       isset($input['home_phone']) &&
       isset($input['email']) &&
       isset($input['charity_id']) &&
       isset($input['registration_type']) &&
       isset($input['artez_username']) &&
       isset($input['artez_password']))
    {
      $c = curl_init();
      curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($c, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));
      curl_setopt($c, CURLOPT_URL, 'https://secure.e2rm.com/webSetService/RegisterParticipant.asmx/InsertJSONDebug');
      curl_setopt($c, CURLOPT_POSTFIELDS, '{"Registration":{
                      "EventID":'.$input['event_id'] .',
                      "PaymentAttempt":false,
                      "PaymentReattempt":0,
                      "Registrant":[{
                        "FirstName":"'.$input['first_name'].'",
                        "LastName":"'.$input['last_name'].'",
                        "LanguagePref":"' . $input['language_preference'] . '",
                        "BusinessTitle":"'.$input['invoice'].'",
                        "Address":[{
                          "AddressType":"Home Address",
                          "AddressLine1":"'.$input['address1'].'",
                          "AddressLine2":"'.$input['address2'].'",
                          "AddressLine3":"'.$input['address3'].'",
                          "City":"'.$input['city'].'",
                          "Province":"'.$input['province'].'",
                          "ProvinceCode":"",
                          "Country":"'.$input['country'].'",
                          "CountryCode":"",
                          "PostalCode":"'.$input['postal_code'].'"
                        }],
                        "Phone":[{"PhoneType":"Home Phone","CountryCode":"","AreaCode":"","PhoneNumber":"'.$input['home_phone'].'","Extension":""}],
                        "Email":[{"EmailType":"Home Email Address","EmailAddress":"'.$input['email'].'"}],
                        "LocationExportID":'.$input['charity_id'].',
                        "RegistrationFeeExportID":"'.$input['registration_type'].'",
                        "FundraisingGoal":100,
                        "SearchPermission":true,
                        "ScoreboardPermission":true,
                        "EmailPermission":true,
                        "PostPermission":true,
                        "SendNotifications":true,
                        "WaiverAccepted":true,
                        "LoginInfo":{"LoginName":"'.$input['artez_username'].'","Password":"'.$input['artez_password'].'","EncryptedPassword":""}
                      }]}
                    }');

    }
    //GET RETURN CONTENT
    $content = curl_exec($c);
    
    $retVal = json_decode($content, true);

    //CLOSE
    curl_close($c);
    
    if($retVal['d'][0]['type'] == "Success")
    {
      $return_data['status'] = TRUE;
      $return_data['description'] = $retVal['d'][0]['description'];
    }
    else
    {
      $return_data['status'] = FALSE;
      $return_data['description'] = $retVal['d'][0]['description'];
    }
    
    return $return_data;
  }
}