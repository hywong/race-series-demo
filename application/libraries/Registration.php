<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
* EventsOnline registration library. Centralizes all generic functions (CRUD) used across multiple sections 
* (participant dashboard, admin dashboard, registration) with everything relating to the registration.
*
* @category   	EventsOnline
* @author				Eric Roy
* @package    	Registration
* @dependencies	Database Class
* @copyright 		Copyright (c) 2014-End of time Events Online (http://www.eventsonline.ca)
* @license   		Apache License 2.0 http://opensource.org/licenses/Apache-2.0
* @version   		Release: 1.0
* @since    		Class available since Release 1.0
* @deprecated 	
*/
class Registration
{
	private $_CI;
	private $db;
	
	public function __construct()
	{
			$this->_CI =& get_instance();
			$this->db = $this->_CI->db;
	}
	
	/**
	| -------------------------------------------------------------------
	|  Get valid activities
	| -------------------------------------------------------------------
	|
	| Retrieves all activities from the database and returns an associative array containiner
	|	all of the details per event per activity. 
	|
	| @return 	Array 	Multi-dimensional associative array with all activity details
	*/
	public function get_valid_activities()
	{		
		//Get all activities
		$sql1 = "SELECT * FROM (SELECT lkpE.strEvent AS event, lkpE.dttEventDate AS date, lkpE.intEventID AS event_id, lkpA.strActivity AS activity, relEAC.intActivityDisplayOrder AS aorder, lkpA.intActivityID AS activity_id, relPCD.dblPrice AS price, relPCD.dttDateTill AS date_until, relEAC.intCap AS cap FROM relPriceChangeDate AS relPCD
			INNER JOIN lkpEvent AS lkpE
			ON relPCD.intEventID = lkpE.intEventID
			INNER JOIN lkpActivity AS lkpA
			ON relPCD.intActivityID = lkpA.intActivityID
			INNER JOIN relEventActivityCap AS relEAC
			ON lkpE.intEventID = relEAC.intEventID AND lkpA.intActivityID = relEAC.intActivityID
			WHERE relPCD.dttDateTill >= NOW()) AS validPrices
			INNER JOIN lkpEvent 
			ON lkpEvent.intEventID = validPrices.event_id
			GROUP BY event_id,activity_id
			ORDER BY DATE(lkpEvent.dttEventDate), aorder;";

		$statement = $this->db->query($sql1);
		$rows = $statement->fetchAll(PDO::FETCH_ASSOC);
		$parsedArray = array();
    foreach($rows as $k => $row)
    {
  		$parsedArrayContent = array(
  			"event_id" => $row['event_id'],
  			"activity_id" => $row['activity_id'],
  			"cap" => $row['cap'],
  			"price"=> $row['price'],
  			"event_date" => $row['date'],
  			"date_until" => $row['date_until']
  			);
      $parsedArray[$row['event']][$row['activity']] = $parsedArrayContent;
    }

   	return $parsedArray;
	}

	public function get_all_priceflips()
	{
		$sql = "SELECT lkpe.strEvent AS event, lkpa.strActivity AS activity, DATE(relpcd.dttDateTill) AS date, relpcd.dblPrice AS price FROM relPriceChangeDate AS relpcd
		INNER JOIN lkpEvent AS lkpe
		ON relpcd.intEventID = lkpe.intEventID
		INNER JOIN lkpActivity AS lkpa
		ON relpcd.intActivityID = lkpa.intActivityID
		ORDER BY lkpe.strEvent, DATE(relpcd.dttDateTill) ASC;";

		$stmt = $this->db->prepare($sql);
		if($stmt->execute())
		{
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

			$cResults = count($results);

			$parsedArray = array();
			for($i = 0; $i < $cResults; $i++)
			{
				$parsedArray[$results[$i]['event']][$results[$i]['activity']][translate('Until'). ' ' .date("M d, Y", strtotime($results[$i]['date']))] = $results[$i]['price'];
			}
			return $parsedArray;
		}
		else
		{

		}
	}

	public function get_activity_caps()
	{		
    $sql2 = "SELECT intEventID AS event_id, intActivityID AS activity_id, COUNT(*) AS registrations FROM relAccountActivity AS relAcccountActivity
      GROUP BY intEventID, intActivityID;";

    $statement = $this->db->query($sql2);
    $rows = $statement->fetchAll(PDO::FETCH_ASSOC);

    $currentRegistrations = array();
    if($rows)
    {
	    foreach($rows as $row)
	    {
	      $currentRegistrations[$row['event_id']][$row['activity_id']] = $row['registrations'];
	    }
  	}

    return $currentRegistrations;
	}

	public function get_tax_rates()
	{
		$sql = "SELECT intEventID AS event_id, strType AS type, dblTax AS tax FROM lkpEventTax";
		$statement = $this->db->prepare($sql);
		$statement->execute();
		$results = $statement->fetchAll(PDO::FETCH_ASSOC);

		$parsedArray = array();
		foreach($results as $row)
		{
			$parsedArray[$row['event_id']][$row['type']] = $row['tax'];
		}

		return $parsedArray;
	}

	public function get_activity_prices()
	{
		$sql = "SELECT * FROM (SELECT lkpE.intEventID AS event_id, lkpA.intActivityID AS activity_id, relPCD.dblPrice AS price FROM relPriceChangeDate AS relPCD
						INNER JOIN lkpEvent AS lkpE
						ON relPCD.intEventID = lkpE.intEventID
						INNER JOIN lkpActivity AS lkpA
						ON relPCD.intActivityID = lkpA.intActivityID
						INNER JOIN relEventActivityCap AS relEAC
						ON lkpE.intEventID = relEAC.intEventID AND lkpA.intActivityID = relEAC.intActivityID
						WHERE relPCD.dttDateTill >= NOW() 
						ORDER BY DATE(relPCD.dttDateTill) ASC) AS validPrices
						GROUP BY event_id,activity_id;";
		$statement = $this->db->prepare($sql);
		$statement->execute();
		$results = $statement->fetchAll(PDO::FETCH_ASSOC);

		$parsedArray = array();
		$count = count($results);
		for($i = 0; $i < $count; $i++)
		{
			$parsedArray[$results[$i]["event_id"]][$results[$i]["activity_id"]] = $results[$i]["price"];
		}

		return $parsedArray;
	}

	public function get_tshirts()
	{
		$selectClauses = array();

		//Get the count of all participant shirts that have been taken so far based on event and activity
	  $sql1 = "SELECT COUNT(intShirtID) AS aCount, intShirtID, intEventID, intActivityID FROM relParticipantShirt GROUP BY intShirtID, intEventID, intActivityID";
		$sql2 = "SELECT ls.intShirtID, ls.strShirtDescription, rs.intShirtCap, intEventID, intActivityID FROM relShirt AS rs
    INNER JOIN lkpShirt AS ls ON ls.intShirtID = rs.intShirtID ORDER BY ls.intShirtID";

	  $parsedArray = array();
	  $stmt1 = $this->db->prepare($sql1);
	  if($stmt1->execute())
	  {
	  	$results1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
	  	foreach($results1 as $row)
	  	{
	  		$parsedArray[$row['intEventID']][$row['intActivityID']][$row['intShirtID']] = $row['aCount'];
	  	}
	  }

	  $parsedArray2 = array();
	  $stmt2 = $this->db->prepare($sql2);
	  if($stmt2->execute())
	  {
	  	$results2 = $stmt2->fetchAll(PDO::FETCH_ASSOC);
	  	foreach($results2 as $row)
	  	{
	  		$parsedArray2[$row['intEventID']][$row['intActivityID']][$row['intShirtID']]['cap'] = $row['intShirtCap'];
	  		$parsedArray2[$row['intEventID']][$row['intActivityID']][$row['intShirtID']]['description'] = $row['strShirtDescription'];
	  	}
	  }

	  //Get counts of shirts within session
	  $tempSess = $_SESSION['registration'];
	  unset($tempSess[$tempSess['active_user']]);
	  unset($tempSess['active_user']);
	  unset($tempSess['last_modified']);

	  $thisSessShirts = array();
	  if($_SESSION['type'] == 'group')
	  {
		  foreach($tempSess as $id => $participant)
		  {
		  	if(isset($participant['activities']))
		  	{
		  		$thisParticipantsEvents = array();
			  	foreach($participant['activities'] as $eA)
			  	{
			  		$expl1 = explode('_', $eA);
			  		$thisParticipantsEvents[$expl1[0]] = $expl1[1];
			  		$eID = $expl1[0];
			  		$aID = $expl1[1];
			  	}

			  	if(isset($participant['group_details']))
			  	{
			  		foreach($participant['group_details'] as $member)
			  		{
					  	if(isset($member['eventextras']))
					  	{
					  		if(isset($member['eventextras']['tshirt']))
					  		{
					  			if(!isset($thisSessShirts[$eID][$aID][$member['eventextras']['tshirt']]))
					  				$thisSessShirts[$eID][$aID][$member['eventextras']['tshirt']] = 0;

					  			$thisSessShirts[$eID][$aID][$member['eventextras']['tshirt']]++;
					  		}
					  	}			  			
			  		}
			  	}
		  	}
		  }
	  }
	  else
	  {
		  foreach($tempSess as $id => $participant)
		  {
		  	if(isset($participant['activities']))
		  	{
		  		$thisParticipantsEvents = array();
			  	foreach($participant['activities'] as $eA)
			  	{
			  		$expl1 = explode('_', $eA);
			  		$thisParticipantsEvents[$expl1[0]] = $expl1[1];
			  	}

			  	if(isset($participant['eventextras']))
			  	{
			  		foreach($participant['eventextras'] as $extraEID => $extras)
			  		{
				  		if(isset($extras['tshirt']))
				  		{
				  			$thisExtraAID = $thisParticipantsEvents[$extraEID];
				  			if(!isset($thisSessShirts[$extraEID][$thisExtraAID][$extras['tshirt']]))
				  				$thisSessShirts[$extraEID][$thisExtraAID][$extras['tshirt']] = 0;

				  			$thisSessShirts[$extraEID][$thisExtraAID][$extras['tshirt']]++;
				  		}
			  		}
			  	}
		  	}
		  }	  	
	  }

	  //Loop through and find valid shirts based on this participant's event/race selections
	  $currentParticipantsEvents = array();
	  $validShirts = array();
		foreach($_SESSION['registration'][$_SESSION['registration']['active_user']]['activities'] AS $eventActivity)
		{
			$expl = explode('_', $eventActivity);
			$thisAID = end($expl);
			$thisEID = reset($expl);
			$currentParticipantsEvents[$thisEID] = $thisAID;

			foreach($parsedArray2[$thisEID][$thisAID] as $shirtID => $arr)
			{
				if(isset($parsedArray[$thisEID][$thisAID][$shirtID]))
				{
					if(isset($thisSessShirts[$thisEID][$thisAID][$shirtID]))
					{
						if(($parsedArray[$thisEID][$thisAID][$shirtID] + $thisSessShirts[$thisEID][$thisAID][$shirtID]) < $arr['cap'])
						{
							$validShirts[$thisEID][$shirtID] = $arr['description'];
						}
					}
					else
					{
						if($parsedArray[$thisEID][$thisAID][$shirtID] < $arr['cap'])
						{
							$validShirts[$thisEID][$shirtID] = $arr['description'];
						}						
					}
				}
				else
				{
					if($arr['cap'] > 0)
					{
						$validShirts[$thisEID][$shirtID] = $arr['description'];
					}
				}
			}
		}

		return $validShirts;
	}

	public function get_event_ids()
	{
		$returnData = array();

		try
		{
			$sql = "SELECT lkpE.intEventID as eventID, lkpE.strEvent AS event FROM lkpEvent as lkpE;";
			$results = $this->db->query($sql);

			if($results)
			{
				$rows = $results->fetchAll(PDO::FETCH_ASSOC);
				foreach($rows as $row)
				{
					$returnData[$row['eventID']] = $row['event'];
				}
			}
			else
			{
				throw new Exception("No event data found");
			}

		}
		catch(Exception $e)
		{
			//TODO: Proper catching
		}

		return $returnData;
	}
}