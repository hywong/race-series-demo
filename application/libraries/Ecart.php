<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Ecart Class
 *
 * This class handles adding, removing and updating cart information throughout the registration process
 * 
 * @category     EventsOnline
 * @author       Eric Roy
 * @package      Ecart
 * @dependencies Session, Database Class
 * @copyright   Copyright (c) 2014-End of time Events Online (http://www.eventsonline.ca)
 * @license     Apache License 2.0 http://opensource.org/licenses/Apache-2.0
 * @version     Release: 1.0
 * @since       Class available since Release 1.0
 * @deprecated   
 */
class CI_Ecart {

  private $_CI;
  private $db;
  
  public function __construct()
  {
      $this->_CI =& get_instance();
      $this->db = $this->_CI->db;
  }

  public function save_cart($section = "registration")
  {
    if(isset($_SESSION['cartID']))
    {
      if($this->_cart_exists($_SESSION['cartID']))
      {
        $this->_update_cart($_SESSION['cartID'], $section);
      }
      else
      {
        $this->_create_cart();     
      }
    }
    else
    {
      $this->_create_cart();
    }
  }

  private function _update_cart($cartID, $section)
  {
    $sql = "UPDATE entActiveCart SET strActiveCartData = :json, dttDateModified = NOW()
    WHERE intActiveCartID = :cartID;";

    try
    {
      $statement = $this->db->prepare($sql);
      $statement->execute(array(
        "json" => json_encode($_SESSION[$section]),
        "cartID" => $cartID
        ));
    }
    catch(Exception $e)
    {
      //TODO: Proper catching
    }  
  }

  private function _create_cart()
  {    
    $sql = "INSERT INTO entActiveCart (intActiveCartID, strActiveCartData, dttDateCreated, dttDateModified)
    VALUES (NULL, :json, NOW(), NOW());"; 

    try
    {
      $statement = $this->db->prepare($sql);
      $statement->execute(array(
        "json" => json_encode($_SESSION['registration']),
        ));
      $lastID = $this->db->lastInsertId();
      $_SESSION['cartID'] = $lastID;
    }
    catch(Exception $e)
    {
      //TODO: Proper catching
    }
  }

  private function _cart_exists($cartID)
  {
    $sql = "SELECT EXISTS(SELECT * FROM entActiveCart WHERE intActiveCartID = :cartID LIMIT 1);";
    $statement = $this->db->prepare($sql);
    $statement->execute(array(
      "cartID" => $cartID
      ));
    $results = reset($statement->fetch(PDO::FETCH_NUM));

    if($results > 0)
    {
      return TRUE;
    }
    else
    {
      return FALSE;
    }
  }
}
// END Template Class

/* End of file Template.php */
/* Location: ./system/application/libraries/Template.php */