<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
* EventsOnline registration library. Centralizes all generic functions (CRUD) used across multiple sections 
* (participant dashboard, admin dashboard, registration) with everything relating to the registration.
*
* @category   	EventsOnline
* @author				Eric Roy
* @package    	Donation
* @dependencies	Database Class
* @copyright 		Copyright (c) 2014-End of time Events Online (http://www.eventsonline.ca)
* @license   		Apache License 2.0 http://opensource.org/licenses/Apache-2.0
* @version   		Release: 1.0
* @since    		Class available since Release 1.0
* @deprecated 	
*/
class Donation
{
	private $_CI;
	private $db;
	
	public function __construct()
	{
			$this->_CI =& get_instance();
			$this->db = $this->_CI->db;
	}

	public function get_charities($eventID = NULL)
	{
		if($eventID === NULL) return array();

		$results = array();
    if(is_array($eventID))
    {
    	$parsedEventID = implode(",", $eventID);
    }
    else
    {
    	$parsedEventID = $eventID;
    }

		$sql = "SELECT relEC.intEventID AS event_id, lkpC.strCharity AS charity, lkpC.intCharityID AS charity_id
		FROM relEventCharity AS relEC
		INNER JOIN lkpCharity AS lkpC
		ON relEC.intCharityID = lkpC.intCharityID
		WHERE relEC.intEventID IN (".$parsedEventID.") AND lkpC.strCharity != 'Charity 1'";

		try
		{
	    $statement = $this->db->query($sql);

	    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
      
    }
    catch(Exception $e)
    {
    	//TODO: Proper catching
    }
    return $results;
	}
}