<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
* EventsOnline email library. This library deals with sending out confirmation emails to participants
*
* @category   	EventsOnline
* @author				Ho-Yin Wong
* @package    	Email
* @dependencies	Database Class
* @copyright 		Copyright (c) 2014-End of time Events Online (http://www.eventsonline.ca)
* @license   		Apache License 2.0 http://opensource.org/licenses/Apache-2.0
* @version   		Release: 1.0
* @since    		Class available since Release 1.0
* @deprecated 	
*/
class ConfirmationEmail {
	private $_CI;
	private $db;
	
	public function __construct() {
			$this->_CI =& get_instance();
			$this->db = $this->_CI->db;
	}
	
	//ALL POSSIBLE TYPES: cartBillingMember, cartNonBillingMember, staffRegistration, extraPurchase, transferActivity, changeActivity, oneTimeDonation, oneTimePledge
	public function sendConfirmationEmail($type, $personalInformationID) {
		$groupNumberCount = "";
		$aTransactionType = $this->getLastTransactionType($personalInformationID);
		if ($aTransactionType == "groupRegistration") {
			$groupNumberCount = $this->getGroupNumberCount($personalInformationID);
		}
		
		if ($type == "unknown") {
			$type = $aTransactionType;
		}
		
		$registrationTransactionDetails = array();
		$addonTransactionDetails = array();
		$donationTransactionDetails = array();
		$dutTransactionDetails = array();
		$userEvents = array();
		$memberInfo = $this->getParticipantDetails($personalInformationID);
		$eventActivityInfo = $this->getEventActivityDetails($personalInformationID);
					
		if ($type == "cartBillingMember" || $type == "cartNonBillingMember" || $type == "individualRegistration" || $type == "groupRegistration" || $type == "staffRegistration" || $type == "transfer") {
			$registrationTransactionDetails = $this->getRegistrationTransactionDetails($personalInformationID);
			$addonTransactionDetails = $this->getAddonTransactionDetails($personalInformationID);
			$donationTransactionDetails = $this->getDonationTransactionDetails($personalInformationID);
			$dutTransactionDetails = $this->getDutTransactionDetails($personalInformationID);
			
			if ($memberInfo['strPersonalInformationType'] == "individualBillingMember" || $memberInfo['strPersonalInformationType'] == "groupBillingMember") {
				$type = "cartBillingMember";
			}
			else {
				$type = "cartNonBillingMember";
			}
		}
		else if ($type == "extraPurchase") {
			$addonTransactionDetails = $this->getAddonTransactionDetails($personalInformationID);
		}
		else if ($type == "oneTimeDonation" || $type == "oneTimePledge") {
			$donationTransactionDetails = $this->getDonationTransactionDetails($personalInformationID);
		}
		else if ($type == "transferActivity" || $type == "changeActivity" || $type == "downgrade" || $type == "upgrade" || $type == "transfer") {
			$dutTransactionDetails = $this->getDutTransactionDetails($personalInformationID);
			if ($dutTransactionDetails[0]['strDutTransactionType'] == "transfer") {
				$type = "transferActivity";
			}
			else {
				$type = "changeActivity";
			}
		}
		   		
		$subject = "";
		if ($type == 'cartBillingMember' || $type == 'cartNonBillingMember' || $type == 'staffRegistration') {
			$subject = SERIES_NAME . ' - ' . translate("K501");
		} 
		elseif ($type == 'extraPurchase') {
			$subject = SERIES_NAME . ' - ' . translate("Add-on(s) Purchase");
		} 
		elseif ($type == 'transferActivity') {
			$subject = SERIES_NAME . ' - ' . translate("Transfer Details");
		} 
		elseif ($type == 'changeActivity') {
			$subject = SERIES_NAME . ' - ' . translate("Race Change");
		} 
		elseif ($type == 'oneTimeDonation') {
			$subject = SERIES_NAME . ' - ' . translate("Donation");
		} 
		elseif ($type == 'oneTimePledge') {
			$subject = SERIES_NAME . ' - ' . translate("K437");
		}

	//Plain text body

		$noHtmlMessage = $memberInfo['strFirstName'] . " " . $memberInfo['strLastName'] . "\r\n";
		$noHtmlMessage .= SERIES_NAME . "\r\n\r\n";

		$noHtmlMessage .= translate("K240") . ": " . $memberInfo['intCartID'] . "\r\n\r\n";
		$noHtmlMessage .= translate("Participant ID") . ": " . $personalInformationID . "\r\n\r\n";

		$noHtmlMessage .= translate("K471") . " \r\n\r\n";
		$noHtmlMessage .= translate("K943") . ". \r\n";
		$noHtmlMessage .= base_url() . 'participant' . " \r\n\r\n";

		if ($type == "cartBillingMember" || $type == "staffRegistration" || $type == "extraPurchase" || $type == "transferActivity" || $type == "changeActivity") {
			$noHtmlMessage .= translate("K944") . ': ' . $memberInfo['strUserName'] . "\r\n\r\n";
		}

		//$noHtmlMessage .= translate("K474") . ucwords($memberInfo['race']) . "\r\n";
		$noHtmlMessage .= translate("K475") . $memberInfo['strFirstName'] . "\r\n";
		$noHtmlMessage .= translate("K476") . $memberInfo['strLastName'] . "\r\n";
		$noHtmlMessage .= translate("K479") . $memberInfo['strAddress'] . "\r\n";
		$noHtmlMessage .= translate("K480") . $memberInfo['strCity'] . "\r\n";
		$noHtmlMessage .= translate("K481") . $memberInfo['strProvince'] . "\r\n";
		$noHtmlMessage .= translate("K482") . $memberInfo['strCountry'] . "\r\n";
		$noHtmlMessage .= translate("K483") . $memberInfo['strPostalCode'] . "\r\n";
		$noHtmlMessage .= translate("K945") . $memberInfo['strHomePhone'] . "\r\n";
		$noHtmlMessage .= translate("K485") . $memberInfo['strMobilePhone'] . "\r\n";
		$noHtmlMessage .= translate("K486") . $memberInfo['strEmail'] . "\r\n";
		$noHtmlMessage .= translate("Date Of Birth") . $memberInfo['dteDateOfBirth'] . "\r\n";
		$noHtmlMessage .= translate("K490") . $memberInfo['intAgeOnRegistration'] . "\r\n";
		$noHtmlMessage .= translate("K491") . $memberInfo['strGender'] . "\r\n";

		$totalAddon = 0;

		if (!empty($donationTransactionDetails)) {
			$noHtmlMessage .= "\r\n";
			$countDonationTransactionDetails = count($donationTransactionDetails);
			for ($i = 0; $i < $countDonationTransactionDetails; $i++) {
				$noHtmlMessage .= translate("K493") . ' ' . number_format($donationTransactionDetails[$i]['dblTotal'], 2) . ' to ' . $donationTransactionDetails[$i]['strCharity'] . " \r\n\r\n";
			}
		}

		if ($type !== 'staffRegistration' && $type != 'cartNonBillingMember') {
			$noHtmlMessage .= translate("K495") . number_format($memberInfo['dblFinalTotal'], 2) . "\r\n\r\n";
		}


		$noHtmlMessage .= translate("K497") . "\r\n http://www.eventsonline.ca \r\n\r\n";



	//Html body
	//$message = "Content-type: text/html;charset=utf-8\r\n\r\n";
		$message = '

	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

	<html>
	<head>
		<meta content="text/html; charset=utf-8" http-equiv="content-type">
		<meta content="IE=8" http-equiv="X-UA-Compatible">

		<title></title>
	</head>

	<body style="font-family: verdana;font-size: 12px;color: #555555; line-height: 14pt">
		<div style="width: 500px;">
			<div style="background: url(\'https://secure.eventsonline.ca/images/email_top_eol.png\') no-repeat left top;width:100%; height: 95px; display:block">
				<div style="padding: 0px 50px 0px 50px;">
					<a href="">
						<img alt="Race Series Demo" src="' . base_url("assets/img/eol.jpg") . '" style="border:none;" width="146">
					</a>
				</div>
			</div>
			<div style="background: url(\'https://secure.eventsonline.ca/images/email_mid_eol.png\') repeat-y;width:100%; display:block">
				<div style="padding-left: 50px; padding-right: 50px; padding-bottom: 1px;">
					<div style="border-bottom: 1px solid #EDEDED; width:420px"></div>
					<br />
					<div style="font-size: 16px;">
						' . translate("Congratulations") . ', ' . $memberInfo['strFirstName'] . ' ' . $memberInfo['strLastName'] . '!
					</div>
					<div style="font-size: 16px;">';

		if ($type == "cartBillingMember" || $type == "cartNonBillingMember" || $type == "staffRegistration" || $type == "transferActivity") {
			$message .= translate("You have successfully registered to") . ":<br />";
			$countEventActivityInfo = count($eventActivityInfo);
			for ($i = 0; $i < $countEventActivityInfo; $i++) {
				if ($eventActivityInfo[$i]['intPersonalInformationID'] == $personalInformationID) {
					$message .= $eventActivityInfo[$i]['strEvent'] . " - " . $eventActivityInfo[$i]['strActivity'] . "<br />";
					$userEvents[$eventActivityInfo[$i]['intEventID']] = $eventActivityInfo[$i]['strEvent'];
				}
			}			
		} 
		else if ($type == "extraPurchase") {
			$message .= translate("You have purchased items for") . ': ' . SERIES_NAME;
		} 
		else if ($type == "changeActivity") {
			$message .= translate("You have modified information for") . ': ' . $dutTransactionDetails[0]['newActivity'];
		} 
		else if ($type == "oneTimeDonation") {
			$message .= translate("You have donated to") . ': ' . $donationTransactionDetails[0]['strCharity'];
		} 
		else if ($type == "oneTimePledge") {
			$message .= translate("You have pledged to") . ': ' . $donationTransactionDetails[0]['strCharity'];
		}
		$message .= '
					</div>';
		if ($type == "changeActivity") {
			$message .= '<br />
						<div style="font-weight:bold">' . translate("Changed Activity from") . ': ' . $dutTransactionDetails[0]['oldActivity'] . '</div><br />
						<div style="font-weight:bold">' . translate("Changed Activity to") . ': ' . $dutTransactionDetails[0]['newActivity'] . '</div>';
		}
				
		if ($type == "cartBillingMember" || $type == "cartNonBillingMember" || $type == "staffRegistration" || $type == "transferActivity" || $type == "changeActivity") {
			if ($aTransactionType != "groupRegistration") {
				$message .= '<br />
						<div style="font-weight:bold">' . translate("To check out your participant account please") . ' <a href="' . base_url() . 'participant/">' . translate("K802") . '</a></div>';
			}
		}
		if (($type == "cartBillingMember" || $type == "cartNonBillingMember" || $type == "staffRegistration" || $type == "transferActivity" || $type == "changeActivity") && strtolower($memberInfo['strIsCaptain']) == "yes") {
			$message .= '<br />
						<div style="font-weight:bold">' . translate("It is your responsibility to distribute team name and password to other participants") . '.</div>';
		}
		$message .= '<br />
					' . $memberInfo['strFirstName'] . ' ' . $memberInfo['strLastName'] . '<br />
					' . $memberInfo['strAddress'] . '<br />
					' . $memberInfo['strCity'] . ', ' . ucwords($memberInfo['strProvince']) . ', ' . $memberInfo['strCountry'] . '<br />
					' . $memberInfo['strPostalCode'] . '<br />
					' . $memberInfo['strEmail'] . '<br /><br />
					<div style="font-weight:bold">' . translate("K240") . ': ' . $memberInfo['intCartID'] . '</div><br />
					<div style="font-weight:bold">' . translate("Participant ID") . ': ' . $personalInformationID . '</div><br />';
		if ($type != "oneTimeDonation" && $type != "oneTimePledge") {
			if ($aTransactionType != "groupRegistration") {
				$message .= '<div style="font-weight:bold">' . translate("Username") . ': ' . $memberInfo['strUserName'] . '</div><br />';
			}
		}

		if ($type != "cartNonBillingMember" && $type != "staffRegistration") {
			$message .= $this->drawOrderSummary($groupNumberCount, $type, $memberInfo, $registrationTransactionDetails, $addonTransactionDetails, $donationTransactionDetails, $dutTransactionDetails, $eventActivityInfo);
		}

		/*
		global $currencyType;
		if ($currencyType == "US") {
			$message .= "<br><br><div style='width:500px;'><b>" . translate("ParticipantNation will appear as the merchant on your credit card statement for this transaction.") . "</b></div><br><br>";
		}  
				
		*/
		
		if ($type == "cartBillingMember" || $type == "cartNonBillingMember" || $type == "staffRegistration") {
			$message .= '<br /><br /><span style="font-weight:bold">DO NOT DELETE THIS EMAIL.  THIS IS YOUR PROOF OF PURCHASE AND CAN BE USED TO PICKUP YOUR BIB AT PACKET PICKUP</span>';
		
			//Check if the event is Banque Scotia 21K (intEventID = 2) and if so add the custom email message
			if (array_key_exists("2",$userEvents)) {
				$message .= '<br /><br /><hr><span style="font-weight:bold">Special Message From Banque Scotia 21K</font>
					<br /><br />
					Thank you for signing up for the 2015 Banque Scotia 21k & de Montreal on Sunday, April 26th!
					<br /><br />
					We are very excited to have you participating in the 12th annual Banque Scotia 21k event!
					<br /><br />
					Get ready for race day by reading up on Event Details, Hotel Details & Banque Scotia Charity Challenge fundraising information on our website at:
					<br />
					http://www.canadarunningseries.com/monthalf_en/montDET.htm
					<br /><br />
					Looking forward to seeing you bright and early on Sunday, April 26th at Parc Jean Drapeau!
				';
			}
			
			//Check if the event is Harry''s Spring Run Off (intEventID = 3) and if so add the custom email message
			if (array_key_exists("3",$userEvents)) {
				$message .= '<br /><br /><hr><span style="font-weight:bold">Special Message From Harry\'s Spring Run Off</font>
					<br /><br />
					Thank you for signing up for the 2015 Harry�s Spring Run Off on Saturday, April 4th!
					<br /><br />
					We are very excited to have you participating in the 37th annual Spring Run Off and the 10th anniversary of Harry Rosen being the title sponsor of the event!
					<br /><br />
					Get ready for race day by reading up on Event Details, Training Run dates & Fundraising for the Princess Margaret Cancer Foundation on our website at:
					<br />
					http://www.canadarunningseries.com/springrunoff/index.htm
					<br /><br />
					Looking forward to seeing you bright and early on Saturday, April 4th in High Park!
				';
			}
			
			//Check if the event is Toronto Yonge Street 10K (intEventID = 4) and if so add the custom email message
			if (array_key_exists("4",$userEvents)) {
				$message .= '<br /><br /><hr><span style="font-weight:bold">Special Message From Toronto Yonge Street 10K</font>
					<br /><br />
					Thank you for signing up for the 2015 Toronto Yonge Street 10k on Sunday, April 19th!
					<br /><br />					
					Get ready for race day by reading up on Event Details, Training Run dates, Hotel information and Fundraising details on our website at:
					<br />
					http://www.canadarunningseries.com/toronto10k/tys10kDET.htm
					<br /><br />
					Looking forward to seeing you bright and early on Sunday, April 19th at the Yonge Street 10k!
				';
			}

		}

		$message .= '
					' . translate("Find and Register for more Events") . ' <a href="http://www2.eventsonline.ca/site/events">' . translate("here") . '</a>.<br /><br />
					<a href="http://www2.eventsonline.ca/site/termsofuse">' . translate("Terms of Use") . '</a><br />
					<a href="http://www2.eventsonline.ca/site/privacypolicy">' . translate("Privacy Policy") . '</a><br />
					' . translate("Please do not reply to this message") . '.<br />
					&copy;' . date('Y') . ' JM EventsOnline.ca Inc. - ' . translate("All Rights Reserved") . '
				</div>
			</div>
			<div style="background: url(\'https://secure.eventsonline.ca/images/email_bottom_eol.png\') no-repeat;width:100%; height: 50px; display:block"></div>
		</div>
	</body>
	</html>';

		$CI =& get_instance();
		$CI->load->library('email');

		$config['mailtype'] = "html";
		$config['charset'] = "utf-8";
		$CI->email->initialize($config);
		$CI->email->from('Eventsonline.ca');
		$CI->email->to($memberInfo['strEmail']);

		$CI->email->subject($subject);
		$CI->email->message($message);
		$CI->email->set_alt_message($noHtmlMessage);

		$CI->email->send();
	}	
	
	public function drawOrderSummary($groupNumberCount, $type, $memberInfo, $registrationTransactionDetails, $addonTransactionDetails, $donationTransactionDetails, $dutTransactionDetails, $eventActivityInfo) {
		//ALL POSSIBLE TYPES: cartBillingMember, cartNonBillingMember, staffRegistration, extraPurchase, transferActivity, changeActivity, oneTimeDonation, oneTimePledge
		$subTotal = 0;
		$tax = 0;
		

		$retValue = '
			<table style="font-size:12px; width:420px">
				<tr>
					<td colspan="2"><hr /></td>
				</tr>
				<tr>
					<td colspan="2" style="font-weight:bold">' . translate("Order Summary") . '</td>
				</tr>
				<tr>
					<td>' . translate("Date") . '</td>
					<td>' . $memberInfo['dttDateCreated'] . '</td>
				</tr>
				<tr>
					<td colspan="2"><hr /></td>
				</tr>';
		//PROCESS REGISTRATION ACTIVITIES
		if ($type == "cartBillingMember" || $type == "staffRegistration") {
			$retValue .= '
					<tr>
						<td colspan="2" style="font-weight:bold">' . translate("K501") . '</td>
					</tr>';
				$countEventActivityInfo = count($eventActivityInfo);
				for ($i = 0; $i < $countEventActivityInfo; $i++) {
					$eventActivityDisplay = $eventActivityInfo[$i]['strEvent'] . " - " . $eventActivityInfo[$i]['strActivity'];
					$eventActivityAmount = number_format($eventActivityInfo[$i]['dblActivityPrice'], 2, '.', '');
					
					if ($groupNumberCount != "") {
						$eventActivityDisplay .= " (X " . $groupNumberCount . ")";
						$eventActivityAmount = number_format(($eventActivityAmount * $groupNumberCount),2);
					}
					$retValue .= '
						<tr>
							<td>' . $eventActivityDisplay . '</td>
							<td>$' . $eventActivityAmount . '</td>
						</tr>';
					//Only loop through once if a group registration
					if ($groupNumberCount != "") {
						break;
					}
				}	
				
			$retValue .= '
					<tr>
						<td colspan="2"><hr /></td>
					</tr>';
		}
		//PROCESS ADDONS
		if ($type == "cartBillingMember" || $type == "staffRegistration" || $type == "extraPurchase") {
			
			$countAddonTransactionDetails = count($addonTransactionDetails);
			
			if ($countAddonTransactionDetails > 0) {
				$retValue .= '
						<tr>
							<td colspan="2" style="font-weight:bold">' . translate("K547") . '</td>
						</tr>';
				for ($i = 0; $i < $countAddonTransactionDetails; $i++) {
					$retValue .= '
						<tr>
							<td>' . $addonTransactionDetails[$i]['strAddon'] . ' (x ' . $addonTransactionDetails[$i]['intQty'] . ')</td>
							<td>$' . number_format($addonTransactionDetails[$i]['dblAddonSubTotal'], 2, '.', '') . '</td>
						</tr>';
				}
				$retValue .= '
						<tr>
							<td colspan="2"><hr /></td>
						</tr>';
			}
		}
		//PROCESS DISCOUNTS
		if (($type == "cartBillingMember" || $type == "staffRegistration") && $registrationTransactionDetails[0]['dblDiscount'] != "") {
			$retValue .= '
					<tr>
						<td colspan="2" style="font-weight:bold">' . translate("K1024") . '</td>
					</tr>
					<tr>
						<td>' . translate("K1085") . '</td>
						<td>($' . number_format($memberInfo['dblDiscountTotal'], 2, '.', '') . ')</td>
					</tr>
					<tr>
						<td colspan="2"><hr /></td>
					</tr>';
			
		}
		if ($type != "transferActivity" && $type != "changeActivity" && $type != "oneTimeDonation" && $type != "oneTimePledge") {
			$subTotal = $memberInfo['dblActivityCostTotal'] + $memberInfo['dblAddonTotal'] + $memberInfo['dblDutTotal'] - $memberInfo['dblDiscountTotal'];
			
			$retValue .= '
				<tr>
					<td>' . translate("Sub-Total") . '</td>
					<td>$' . number_format($subTotal, 2, '.', '') . '</td>
				</tr>
				<tr>
					<td>' . translate("Tax") . '</td>
					<td>$' . number_format($memberInfo['dblTaxTotal'], 2, '.', '') . '</td>
				</tr>
				<tr>
					<td>' . translate("K1087") . '</td>
					<td>$' . number_format($memberInfo['dblEolFee'], 2, '.', '') . '</td>
				</tr>
				<tr>
					<td colspan="2"><hr /></td>
				</tr>';
		}
		
		$countDonationTransactionDetails = count($donationTransactionDetails);	
		if ($countDonationTransactionDetails > 0) {
			$retValue .= '
				<tr>
					<td colspan="2" style="font-weight:bold">' . translate("Donation") . '</td>
				</tr>';	
				for ($i = 0; $i < $countDonationTransactionDetails; $i++) {
			$retValue .= '
				<tr>
					<td>' . $donationTransactionDetails[$i]['strCharity'] . '</td>
					<td>$' . number_format($donationTransactionDetails[$i]['dblTotal'], 2, '.', '') . '</td>
				</tr>';
				}
			$retValue .= '	
				<tr>
					<td colspan="2"><hr /></td>
				</tr>';
		}
		$retValue .= '
				<tr>
					<td style="font-weight:bold">' . translate("K1088") . '</td>
					<td style="font-weight:bold">$' . number_format($memberInfo['dblFinalTotal'], 2, '.', '') . '</td>
				</tr>
				<tr>
					<td colspan="2"><hr /></td>
				</tr>
			</table>';

		return $retValue;
	}
	
	public function getParticipantDetails($personalInformationID) {	
		$retArray = array();
		
		try {
			$sql = "SELECT lpit.strPersonalInformationType, et.intCartID, et.intTransactionID, et.dttDateCreated, et.dblActivityCostTotal, et.dblAddonTotal, et.dblDutTotal, et.dblDiscountTotal, et.dblTaxTotal, et.dblEolFee, et.dblDonationTotal, et.dblFinalTotal, epi.strFirstName, epi.strLastName, epi.strAddress, epi.strCity, epi.strProvince, epi.strCountry, epi.strPostalCode, epi.strHomePhone, epi.strMobilePhone, epi.strEmail, epi.dteDateOfBirth, epi.intAgeOnRegistration, epi.strGender, ea.strUserName, rt.strIsCaptain FROM entPersonalInformation AS epi
				INNER JOIN relAccountPersonalInformation AS rapi ON rapi.intPersonalInformationID = epi.intPersonalInformationID
				INNER JOIN lkpPersonalInformationType AS lpit ON lpit.intPersonalInformationTypeID = rapi.intPersonalInformationTypeID
				INNER JOIN entAccount AS ea ON ea.intAccountID = rapi.intAccountID
				INNER JOIN relAccountTransaction AS rat ON rat.intAccountID = ea.intAccountID AND rat.strRefundStatus IS NULL
				INNER JOIN entTransaction AS et ON et.intTransactionID = rat.intTransactionID
				LEFT JOIN relTeam AS rt ON rt.intPersonalInformationID = epi.intPersonalInformationID
				WHERE epi.intPersonalInformationID = " . $personalInformationID . "
				ORDER BY et.dttDateCreated DESC
				LIMIT 1";

			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		//Check if any results were returned
		if (isset($result[0]) && count($result[0]) > 0) {
			$retArray = $result[0];
		}
		
		return $retArray;
	}
	
	public function getEventActivityDetails($personalInformationID) {
		$retArray = array();
		
		try {
			$sql = "SELECT rapi.intPersonalInformationID, raa.intAccountID, le.strEvent, le.intEventID, la.strActivity, la.intActivityID, raa.dblActivityPrice FROM relAccountActivity AS raa
				INNER JOIN relAccountPersonalInformation AS rapi ON rapi.intAccountID = raa.intAccountID
				INNER JOIN relAccountTransaction AS rat ON rat.intAccountID = raa.intAccountID AND rat.strRefundStatus IS NULL
				INNER JOIN lkpActivity AS la ON la.intActivityID = raa.intActivityID
				INNER JOIN lkpEvent AS le ON le.intEventID = raa.intEventID
				WHERE raa.strStatusChange IS NULL AND rat.intTransactionID =  
				(SELECT et.intTransactionID FROM entTransaction AS et
					INNER JOIN relAccountTransaction AS rat2 ON rat2.intTransactionID = et.intTransactionID AND rat2.strRefundStatus IS NULL	
					INNER JOIN relAccountPersonalInformation AS rapi2 ON rapi2.intAccountID = rat2.intAccountID
					WHERE rapi2.intPersonalInformationID = " . $personalInformationID . "
					ORDER BY et.dttDateCreated DESC LIMIT 1)";
				
			/*
			$sql = "SELECT rapi.intPersonalInformationID, raa.intAccountID, le.strEvent, le.intEventID, la.strActivity, la.intActivityID, raa.dblActivityPrice FROM relAccountActivity AS raa
				INNER JOIN relAccountPersonalInformation AS rapi ON rapi.intAccountID = raa.intAccountID
				INNER JOIN relAccountTransaction AS rat ON rat.intAccountID = raa.intAccountID AND rat.strRefundStatus IS NULL
				INNER JOIN lkpActivity AS la ON la.intActivityID = raa.intActivityID
				INNER JOIN lkpEvent AS le ON le.intEventID = raa.intEventID
				WHERE raa.strStatusChange IS NULL AND rat.intTransactionID =  
				(SELECT et.intTransactionID FROM entTransaction AS et
					INNER JOIN relAccountTransaction AS rat2 ON rat2.intTransactionID = et.intTransactionID AND rat2.strRefundStatus IS NULL	
					INNER JOIN relAccountPersonalInformation AS rapi2 ON rapi2.intAccountID = rat2.intAccountID
					WHERE rapi2.intPersonalInformationID = " . $personalInformationID . "
					ORDER BY et.dttDateCreated DESC LIMIT 1)
				GROUP BY la.strActivity, le.strEvent";
			*/
			/*
			$sql = "SELECT raa.intAccountID, le.strEvent, le.intEventID, la.strActivity, la.intActivityID, raa.dblActivityPrice FROM relAccountActivity AS raa
				INNER JOIN relAccountPersonalInformation AS rapi ON rapi.intAccountID = raa.intAccountID
				INNER JOIN lkpActivity AS la ON la.intActivityID = raa.intActivityID
				INNER JOIN lkpEvent AS le ON le.intEventID = raa.intEventID
				WHERE rapi.intPersonalInformationID = " . $personalInformationID;
			*/
		
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		//Check if any results were returned
		if (isset($result[0]) && count($result[0]) > 0) {
			$retArray = $result;
		}
		
		return $retArray;
	}
	
	public function getLastTransactionType($personalInformationID) {
		$retValue = "";
		
		try {
			$sql = "SELECT strTransactionType, et.dttDateCreated FROM relAccountPersonalInformation AS rapi 
				INNER JOIN relAccountTransaction AS rat ON rat.intAccountID = rapi.intAccountID AND rat.strRefundStatus IS NULL 
				INNER JOIN lkpTransactionType AS ltt ON ltt.intTransactionTypeID = rat.intTransactionTypeID 
				INNER JOIN entTransaction AS et ON et.intTransactionID = rat.intTransactionID 
				WHERE rapi.intPersonalInformationID = " . $personalInformationID . " ORDER BY et.dttDateCreated DESC";

			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		//Check if any results were returned
		if (isset($result[0]) && count($result[0]) > 0) {
			$retValue = $result[0]['strTransactionType'];
		}
		
		return $retValue;
	}
	
	public function getRegistrationTransactionDetails($personalInformationID) {
		$retArray = array();
		
		try {
			$sql = "SELECT rrt.intTransactionID, rrt.intAccountID, rrt.intDiscountCodeID, rrt.dblRegistrationSubTotal, rrt.dblRegistrationSubTotalTax, rrt.dblDiscount, rrt.dblTotal, lrtt.strRegistrationTransactionType FROM relRegistrationTransaction AS rrt 
				INNER JOIN lkpRegistrationTransactionType AS lrtt ON lrtt.intRegistrationTransactionTypeID = rrt.intRegistrationTransactionTypeID
				WHERE rrt.strRefundStatus IS NULL AND rrt.intTransactionID =
				(SELECT et.intTransactionID FROM entTransaction AS et
					INNER JOIN relAccountTransaction AS rat ON rat.intTransactionID = et.intTransactionID AND rat.strRefundStatus IS NULL	
					INNER JOIN relAccountPersonalInformation AS rapi ON rapi.intAccountID = rat.intAccountID
					WHERE rapi.intPersonalInformationID = " . $personalInformationID . "
					ORDER BY et.dttDateCreated DESC LIMIT 1)";
		
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		//Check if any results were returned
		if (isset($result[0]) && count($result[0]) > 0) {
			$retArray = $result;
		}
		
		return $retArray;
	}
	
	public function getAddonTransactionDetails($personalInformationID) {
		$retArray = array();
		
		try {
			$sql = "SELECT la.strAddon, rat.intTransactionID, rat.intAccountID, rat.intEventID, rat.intQty, rat.dblAddonSubTotal, rat.dblAddonSubTotalTax, rat.dblTotal, latt.strAddonTransactionType FROM relAddonTransaction AS rat 
				INNER JOIN lkpAddonTransactionType AS latt ON latt.intAddonTransactionTypeID = rat.intAddonTransactionTypeID
				INNER JOIN lkpAddon AS la ON la.intAddonID = rat.intAddonID
				WHERE rat.strRefundStatus IS NULL AND rat.intTransactionID =
				(SELECT et.intTransactionID FROM entTransaction AS et
					INNER JOIN relAccountTransaction AS rat ON rat.intTransactionID = et.intTransactionID AND rat.strRefundStatus IS NULL	
					INNER JOIN relAccountPersonalInformation AS rapi ON rapi.intAccountID = rat.intAccountID
					WHERE rapi.intPersonalInformationID = " . $personalInformationID . "
					ORDER BY et.dttDateCreated DESC LIMIT 1)";
		
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		//Check if any results were returned
		if (isset($result[0]) && count($result[0]) > 0) {
			$retArray = $result;
		}
		
		return $retArray;
	}
	
	public function getDonationTransactionDetails($personalInformationID) {
		$retArray = array();
		
		try {
			$sql = "SELECT rdt.intTransactionID, rdt.intAccountID, rdt.dblDonationSubTotal, rdt.dblEolFee, rdt.dblTotal, ldtt.strDonationTransactionType, lc.strCharity FROM relDonationTransaction AS rdt 
				INNER JOIN lkpDonationTransactionType AS ldtt ON ldtt.intDonationTransactionTypeID = rdt.intDonationTransactionTypeID
				INNER JOIN lkpCharity AS lc ON lc.intCharityID = rdt.intCharityID
				WHERE rdt.strRefundStatus IS NULL AND rdt.intTransactionID =
				(SELECT et.intTransactionID FROM entTransaction AS et
					INNER JOIN relAccountTransaction AS rat ON rat.intTransactionID = et.intTransactionID AND rat.strRefundStatus IS NULL	
					INNER JOIN relAccountPersonalInformation AS rapi ON rapi.intAccountID = rat.intAccountID
					WHERE rapi.intPersonalInformationID = " . $personalInformationID . "
					ORDER BY et.dttDateCreated DESC LIMIT 1)";
		
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		//Check if any results were returned
		if (isset($result[0]) && count($result[0]) > 0) {
			$retArray = $result;
		}
		
		return $retArray;
	}
	
	public function getDutTransactionDetails($personalInformationID) {
		$retArray = array();
		
		try {
			$sql = "SELECT rdt.intTransactionID, rdt.intAccountID, rdt.intActivityID, la1.strActivity AS 'newActivity', rdt.intOldActivityID, la2.strActivity AS 'oldActivity', rdt.dblDutFee, rdt.dblDutFeeTax, rdt.dblPriceDifference, rdt.dblPriceDifferenceTax, rdt.dblEolFee, rdt.dblEolFeeTax, rdt.dblTotal, ldtt.strDutTransactionType FROM relDutTransaction AS rdt 
				INNER JOIN lkpDutTransactionType AS ldtt ON ldtt.intDutTransactionTypeID = rdt.intDutTransactionTypeID
				INNER JOIN lkpActivity AS la1 ON la1.intActivityID = rdt.intActivityID
				INNER JOIN lkpActivity AS la2 ON la2.intActivityID = rdt.intOldActivityID
				WHERE rdt.strRefundStatus IS NULL AND rdt.intTransactionID =
				(SELECT et.intTransactionID FROM entTransaction AS et
					INNER JOIN relAccountTransaction AS rat ON rat.intTransactionID = et.intTransactionID AND rat.strRefundStatus IS NULL	
					INNER JOIN relAccountPersonalInformation AS rapi ON rapi.intAccountID = rat.intAccountID
					WHERE rapi.intPersonalInformationID = " . $personalInformationID . "
					ORDER BY et.dttDateCreated DESC LIMIT 1)";
		
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		//Check if any results were returned
		if (isset($result[0]) && count($result[0]) > 0) {
			$retArray = $result;
		}
		
		return $retArray;
	}
	
	public function getGroupNumberCount($personalInformationID) {
		$retValue = "";
		
		try {
			$sql = "SELECT COUNT(intPersonalInformationID) AS aCount FROM relAccountPersonalInformation AS rapi2
				INNER JOIN lkpPersonalInformationType AS lpit ON lpit.intPersonalInformationTypeID = rapi2.intPersonalInformationTypeID
				WHERE lpit.strPersonalInformationType = 'groupMember' AND rapi2.intAccountID = 
				(SELECT intAccountID FROM relAccountPersonalInformation AS rapi1 WHERE rapi1.intPersonalInformationID = " . $personalInformationID . " LIMIT 1)";
		
			$query = $this->db->query($sql);
			if (!$query) {
				throw new Exception();
			}
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $e) {	
			//Function located in general_helper.php
			processError($sql);
		}
		
		//Check if any results were returned
		if (isset($result[0]) && count($result[0]) > 0) {
			$retValue = $result[0]['aCount'];
		}
		
		return $retValue;
	}
}