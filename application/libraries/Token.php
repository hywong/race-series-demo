<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
* Creation of a secure and unique token
*
* @category   	EventsOnline
* @author		Eric Roy
* @package    	Token
* @dependencies	Session Class
* @copyright 	Copyright (c) 2014-End of time Events Online (http://www.eventsonline.ca)
* @license   	Apache License 2.0 http://opensource.org/licenses/Apache-2.0
* @version   	Release: 1.0
* @since    	Class available since Release 1.0
* @deprecated 	
*/
class Token
{
	private $_CI;
	
	public function __construct()
	{
			$this->_CI =& get_instance();
	}
	
	private function crypto_rand_secure($min, $max) 
	{
		$range = $max - $min;
		if ($range < 0) return $min; // not so random...
		$log = log($range, 2);
		$bytes = (int) ($log / 8) + 1; // length in bytes
		$bits = (int) $log + 1; // length in bits
		$filter = (int) (1 << $bits) - 1; // set all lower bits to 1
		do {
			$rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
			$rnd = $rnd & $filter; // discard irrelevant bits
		} while ($rnd >= $range);
		return $min + $rnd;
	}

	public function generate($length=12)
	{
		$token = "";
		$codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
		$codeAlphabet.= "0123456789";
		for($i=0;$i<$length;$i++){
			$token .= $codeAlphabet[$this->crypto_rand_secure(0,strlen($codeAlphabet))];
		}
		return $token;
	}
	
	public function validate($token=NULL)
	{
		if($token === NULL)
		{
			return FALSE;
		}
		
		if($this->_CI->session->userdata('form-token') === FALSE)
		{
			return FALSE;
		}
		
		if($this->_CI->session->userdata('form-token') === $token)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}		
	}
}