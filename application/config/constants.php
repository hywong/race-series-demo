<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

//define('DATA_XML_TRANSLATE', '/mnt/nfs/events/core/data/translate.xml');
define("DATA_FEEDBACK_FILE", "/mnt/nfs/events/race_series_demo/data/feedback.txt");
define('DATA_XML_TRANSLATE', 'application/helpers/translate.xml');
define('TRANSLATE_SUFFIX', FALSE);

if (strtolower($_SERVER['HTTP_HOST']) == "localhost" || strtolower($_SERVER['HTTP_HOST']) == "127.0.0.1") {
	define('LIVE_ENVIRONMENT', FALSE);
}
else if (stripos($_SERVER['HTTP_HOST'], 'eventsonline.ca')!==false ) {
	define('LIVE_ENVIRONMENT', FALSE);
}

define('EOL_FEE_TAX', '0.13');
/* End of file constants.php */
/* Location: ./application/config/constants.php */

define('ERROR_MSG', "Our technical staff have been automatically notified and will be looking into this with the utmost urgency.");
//define('EMAIL_ERROR_RECIPIENT', "urgent@eventsonline.ca");
define('EMAIL_ERROR_RECIPIENT', "urgent@eventsonline.ca");

define('SERIES_NAME', 'Race Series Demo');
define('SERIES_DIRECTORY', 'race_series_demo');

define('UPGRADE_FEE', 0);
define('DOWNGRADE_FEE', 10);
define('TRANSFER_FEE', 10);
