<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends Public_Controller {

  var $expired = false;

	public function index()
	{
    //Load the registration model
    if(isset($_SESSION['type']))
    {
      if($_SESSION['type'] != "individual")
      {
        unset($_SESSION['registration']);
      }
    }
    else
    {
      unset($_SESSION['registration']);
    }
	
	 $data = array(
		"pagesArr" => array(
					"Event Selection" => "active current",
					"Personal Information" => "",
					"Charities" => "",
					"Teams" => "",
					"Add-ons" => "",
					"Confirmation" => "",
					"Payment and Receipt" => ""
					)
      );
      $this->template->write_view("_steps", "steps", $data);

    $_SESSION['type'] = "individual";
    $this->load->model("registration_model", "registration");
    $wasExpired = $this->registration->initialize_session();
    $activities = $this->registration->get_form_activities();
	
    $data = array(
      "activities" => $activities,
      "expired" => $wasExpired,
      "formAction" => base_url("register/personal")
      );

		$this->template->write_view('_content', 'register/activities', $data);
		$this->template->render();
	}

  public function group()
  {    
    //Load the registration model
    if($_SESSION['type'] != "group")
    {
      unset($_SESSION['registration']);
      $_SESSION['registration']['was_expired'];
      $_SESSION['type'] = "group";
      redirect('register/group');
    }
	
		$data = array(
		"pagesArr" => array(
				"Billing Information" => "active current",
				"Group Information" => "",
				"Confirmation" => "",
				"Payment and Receipt" => ""
					)
      );
	 $this->template->write_view("_steps", "group_steps", $data);
	
    $this->load->model("registration_model", "registrationm");
    $wasExpired = $this->registrationm->initialize_session();

    $sections = $this->registrationm->get_group_setup();

    $data = array(
      "sections" => $sections,
      "expired" => $wasExpired
      );

    $this->template->write_view('_content', 'register/group', $data);
    $this->template->render();
  }
  
  public function members()
  {
    if($_SESSION['type'] != "group")
    {
      unset($_SESSION['registration']);

    }
    $uriString = uri_string();
    if (stripos($uriString,"resend") === FALSE) {
      if($_SESSION['type'] == "group")
      {
				$data = array(
				"pagesArr" => array(
						"Billing Information" => "active",
						"Group Information" => "active current",
						"Confirmation" => "",
						"Payment and Receipt" => ""
							)
			  );
			$this->template->write_view("_steps", "group_steps", $data);
      }
      else
      {
        $this->template->write_view("_steps", "steps");
      }
    }
    //We initialize again since we don't want it to expire going from page 1 to 2
    $this->load->model("registration_model", "registrationm");
    $this->registrationm->initialize_session();
    $fields = $this->registrationm->get_group_fields();

    $data = array(
      "fields" => $fields
      );

    $this->template->write_view("_content", "register/members", $data);
    $this->template->render();
  }

	public function personal()
	{
    if(!isset($_SESSION['registration']['active_user']))
    {
      redirect('register');
    }
    
    $uriString = uri_string();
    if (stripos($uriString,"resend") === FALSE) {
      if($_SESSION['type'] == "transfer")
      {
        $data = array(
        "pagesArr" => array(
              "Personal Information" => "active current",
              "Charities" => "",
              "Teams" => "",
              "Add-ons" => "",
              "Confirmation" => "",
              "Payment and Receipt" => ""
              )
          );
      }
      else
      {
        $data = array(
        "pagesArr" => array(
              "Event Selection" => "active",
              "Personal Information" => "active current",
              "Charities" => "",
              "Teams" => "",
              "Add-ons" => "",
              "Confirmation" => "",
              "Payment and Receipt" => ""
              )
          );
      }
      $this->template->write_view("_steps", "steps", $data);
    }
    //We initialize again since we don't want it to expire going from page 1 to 2
    $this->load->model("registration_model", "registration");
    $this->registration->initialize_session();

    $sections = $this->registration->get_form_sections();
    $data = array(
      "sections" => $sections,
      "formAction" => base_url("register/charities")
      );
		$this->template->write_view("_content", "register/personal", $data);
    $this->template->render();
	}

  public function teams()
  {
    if(!isset($_SESSION['registration']['active_user']))
    {
      redirect('register');
    }

    $uriString = uri_string();
    if (stripos($uriString,"resend") === FALSE) {
      if($_SESSION['type'] == "transfer")
      {
        $data = array(
        "pagesArr" => array(
          "Personal Information" => "active",
          "Charities" => "active",
          "Teams" => "active current",
          "Add-ons" => "",
          "Confirmation" => "",
          "Payment and Receipt" => ""
              )
          );
      }
      else
      {
        $data = array(
        "pagesArr" => array(
          "Event Selection" => "active",
          "Personal Information" => "active",
          "Charities" => "active",
          "Teams" => "active current",
          "Add-ons" => "",
          "Confirmation" => "",
          "Payment and Receipt" => ""
              )
          );
      }

      $this->template->write_view("_steps", "steps", $data);
      $this->template->write_view("_cart", "cart");      
    }
    //Get all teams by event
    $this->load->model('team_model', 'team');
    $teams = $this->team->get_existing_teams();
    
    $sections = $this->team->get_form_sections($teams);
    $data = array(
      "sections" => $sections,
      "formAction" => base_url("register/addons")
      );
    $this->template->write_view("_content", "register/teams", $data);
    $this->template->render();
  }

  public function charities()
  {
    if(!isset($_SESSION['registration']['active_user']))
    {
      redirect('register');
    }

    $uriString = uri_string();
    if (stripos($uriString,"resend") === FALSE) {
      if($_SESSION['type'] == "transfer")
      {
        $data = array(
        "pagesArr" => array(
            "Personal Information" => "active",
            "Charities" => "active current",
            "Teams" => "",
            "Add-ons" => "",
            "Confirmation" => "",
            "Payment and Receipt" => ""
              )
          );
      }
      else
      {
        $data = array(
        "pagesArr" => array(
          "Event Selection" => "active",
          "Personal Information" => "active",
          "Charities" => "active current",
          "Teams" => "",
          "Add-ons" => "",
          "Confirmation" => "",
          "Payment and Receipt" => ""
              )
          );
      }
      $this->template->write_view("_steps", "steps", $data);
      $this->template->write_view("_cart", "cart");
    }
    $this->load->model("donation_model", "donationm");
    //$charities = $this->donationm->get_charities();
	$charities = array();

    $sections = $this->donationm->get_form_sections($charities);

    $data = array(
      "sections" => $sections,
      "formAction" => base_url("register/teams")
      );

    $this->template->write_view("_content", "register/charities", $data);
    $this->template->render();
  }

  public function transfer()
  {
    $data = array();
    if($this->input->get('invalid') == 1)
    {
      $invalid = "The code you have entered is not valid.";
      $data['invalid'] = $invalid;
    }

    $this->template->write_view("_content", "register/transfer_code", $data);
    $this->template->render();
  }

  public function addons()
  {
    if(!isset($_SESSION['registration']['active_user']))
    {
      redirect('register');
    }

    $uriString = uri_string();
    if (stripos($uriString,"resend") === FALSE) {
      if($_SESSION['type'] == "transfer")
      {
        $data = array(
        "pagesArr" => array(
          "Personal Information" => "active",
          "Charities" => "active",
          "Teams" => "active",
          "Add-ons" => "active current",
          "Confirmation" => "",
          "Payment and Receipt" => ""
              )
          );
      }
      else
      {
        $data = array(
        "pagesArr" => array(
          "Event Selection" => "active",
          "Personal Information" => "active",
          "Charities" => "active",
          "Teams" => "active",
          "Add-ons" => "active current",
          "Confirmation" => "",
          "Payment and Receipt" => ""
              )
          );
      }

      $this->template->write_view("_steps", "steps", $data);
      $this->template->write_view("_cart", "cart");
    }
    $this->load->model("addons_model", "addonsm");
    $addons = $this->addonsm->get_addons();

    $parsedAddons = array();
    foreach($addons as $addon)
    {
      $parsedAddons[$addon['eventID']][] = $addon;
    }

    $data = array(
      "addons" => $parsedAddons,
      "formAction" => base_url("register/confirmation")
      );
    $this->template->write_view("_content", "register/addons", $data);
    $this->template->render();
  }

  public function confirmation()
  {
    if(!isset($_SESSION['registration']['active_user']))
    {
      redirect('register');
    }
    $this->load->library('ecart');
    $this->ecart->save_cart();

    $uriString = uri_string();
    if (stripos($uriString,"resend") === FALSE) {
      if($_SESSION['type'] == "transfer")
      {
        $data = array(
        "pagesArr" => array(
          "Personal Information" => "active",
          "Charities" => "active",
          "Teams" => "active",
          "Add-ons" => "active",
          "Confirmation" => "active current",
          "Payment and Receipt" => ""
              )
          );
        $this->template->write_view("_steps", "steps", $data);
      }
      else if($_SESSION['type'] == "group")
      {
				$data = array(
				"pagesArr" => array(
						"Billing Information" => "active",
						"Group Information" => "active",
						"Confirmation" => "active current",
						"Payment and Receipt" => ""
							)
			  );
			 $this->template->write_view("_steps", "group_steps", $data);
      }
      else
      {
    		$data = array(
    			"pagesArr" => array(
    						"Event Selection" => "active",
    						"Personal Information" => "active",
    						"Charities" => "active",
    						"Teams" => "active",
    						"Add-ons" => "active",
    						"Confirmation" => "active current",
    						"Payment and Receipt" => ""
    						)
    		);
        $this->template->write_view("_steps", "steps", $data);
      }
      $this->template->write_view("_cart", "cart");
    }
    $this->load->model("registration_model", "registration");
    //We manually refresh the session because addons doesn't post to here
    $this->registration->initialize_session();
    $summary = $this->registration->get_cart_summary();

    $data = array(
      "summary" => $summary,
      );
    $this->template->write_view("_content", "register/confirmation", $data);
    $this->template->render();
  }

  public function new_participant()
  {
    unset($_SESSION['registration']['active_user']);
    redirect("register");
  }

  public function new_group()
  {
    unset($_SESSION['registration']['active_user']);
    redirect("register/group");
  }
  
  public function logout()
  {
    $this->session->sess_destroy();
    redirect("register");
  }

  public function restart()
  {
    unset($_SESSION['registration']);
    unset($_SESSION['cartID']);
    unset($_SESSION['type']);

    if($_SESSION['type'] == "group")
    {
      redirect("register/group");
    }
    else
    {
      redirect("register");
    }
  }

  public function expired()
  {
    $this->load->model('registration_model', 'registration');
    $this->registration->reinitialize_expired_session();
    redirect("register");
  }
  
	public function fee()
  {
    $this->template->write_view("_content", "fees");
    $this->template->render();
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */