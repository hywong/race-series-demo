<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Confirmation extends Confirmation_Controller {

	public function participant_search() {
		$data = array();
		
		$aGet = $this->input->get();
		
		if (isset($aGet['msgDanger']) && $aGet['msgDanger'] == "noRecords") {
			$data['msgDanger'] = "No participants were found with that search criteria";
		}
		
		$this->template->write_view("_content", "confirmation/participant_search", $data);
		$this->template->render();
	}
	
	public function participant_list() {
		$this->load->model("registration_model");
		$participantList = $this->registration_model->searchParticipant();
		if (count($participantList) == 0) {
			redirect("confirmation/participant_search?msgDanger=noRecords");
			exit;
		}
		else {	
			$data['participantList'] = $participantList;
			$this->template->write_view("_content", "confirmation/participant_list", $data);
			$this->template->render();
		}
	}
	
	public function resend() {
		$aGet = $this->input->get();
		
		$data = array();
		
		if (isset($aGet['msgDanger']) && $aGet['msgDanger'] == "noRecordFound") {
			$data['msgDanger'] = "No participant was found with that search criteria";
		}
		$this->template->write_view("_content", "confirmation/resend", $data);
		$this->template->render();
	}
	
	public function resend_process() {		
		$aPost = $this->input->sanitizeForDbPost();
		
		if (!isset($aPost['email'])) {
			redirect("confirmation/resend");
		}
		else {
			$this->load->model("registration_model");
			
			$data['personalInformation'] = $this->registration_model->processResendPostData();
			if (empty($data['personalInformation'])) {
				redirect("confirmation/resend?msgDanger=noRecordFound");
			}
			else {
				$data['labelLinker'] = array(
					"First Name" => "strFirstName",
					"Last Name" => "strLastName",
					"Address" => "strAddress",
					"City" => "strCity",
					"Province/State" => "strProvince",
					"Country" => "strCountry",
					"Postal Code/Zip" => "strPostalCode",
					"Home Phone" => "strHomePhone",
					"Mobile Phone" => "strMobilePhone",
					"Email" => "strEmail",
					"Gender" => "strGender",
					"Date of Birth" => "dteDateOfBirth",					
					"Categories" => "strCategory",
					"I would like to receive newsletters" => "strSeriesNewsletter",
					"Nationality" => "strNationality",
					"Please specify how many months you have been in Canada" => "strNationalityDuration",
					"In what type of industry do you work?" => "strIndustryType",
					"Industry Other" => "strIndustryTypeOther",
					"What is your yearly salary?" => "strYearlySalary",
					"Emergency Contact Name" => "strEmergencyContactName",
					"Emergency Contact Number" => "strEmergencyContactNumber",
					"List any medical conditions" => "strMedicalCondition",
				);
				$this->template->write_view("_content", "confirmation/resend_process", $data);
				$this->template->render();
				
				$this->load->library("confirmationEmail");
				$this->confirmationemail->sendConfirmationEmail("unknown",$data['personalInformation']['intPersonalInformationID']);
			}
		}
	}
}