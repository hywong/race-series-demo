<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Participant extends Participant_Controller {

	public function login() {		
		$aPost = $this->input->sanitizeForDbPost();
		
		if (isset($aPost['username'])) {
			$this->load->model("participant_model");
			$verifyLoginFlag = $this->participant_model->verifyLogin($aPost['username'],$aPost['password']);

			if ($verifyLoginFlag == "valid") {
				redirect("participant/index");
			}
			else {
				$data['msgDanger'] = "Invalid Login";
				$this->template->write_view('_content', 'participant/login', $data);
				$this->template->render();
			}
			
		}
		else {
			$aGet = $this->input->get();
			$data = array();
			if (isset($aGet['msgDanger']) && $aGet['msgDanger'] == "invalidResetPasswordToken") {
				$data['msgDanger'] = "The reset password token is invalid.";
			}
			else if (isset($aGet['msgDanger']) && $aGet['msgDanger'] == "sessionExpired") {
				$data['msgDanger'] = "The session has expired.";
			}
			$this->template->write_view('_content', 'participant/login', $data);
			$this->template->render();
		}
	}
	
	public function logout() {
		$this->session->unset_userdata('participant');
		redirect("participant/login");
	}
	
	public function index() {
		$aGet = $this->input->get();
		
		if (isset($aGet['msgSuccess']) && $aGet['msgSuccess'] == "joinedTeam") {
			$data['msgSuccess'] = "You have successfully joined the team";
		}
		else if (isset($aGet['msgSuccess']) && $aGet['msgSuccess'] == "pledgeProfileCreated") {
			$data['msgSuccess'] = "The pledge profile has been created";
		}
		else if (isset($aGet['msgSuccess']) && $aGet['msgSuccess'] == "pledgeProfileUpdated") {
			$data['msgSuccess'] = "The pledge profile has been updated";
		}	
		else if (isset($aGet['msgSuccess']) && $aGet['msgSuccess'] == "detailsUpdated") {
			$data['msgSuccess'] = "Your personal information has been updated";
		}	
		else if (isset($aGet['msgSuccess']) && $aGet['msgSuccess'] == "firstLastNameSwapped") {
			$data['msgSuccess'] = "The first and last name have been swapped";
		}		
		else if (isset($aGet['msgSuccess']) && $aGet['msgSuccess'] == "downgradeUpgradeProcessed") {
			$data['msgSuccess'] = "The activity change has already been processed";
		}
		else if (isset($aGet['msgSuccess']) && $aGet['msgSuccess'] == "extraPurchaseProcessed") {
			$data['msgSuccess'] = "The addon purchase has already been processed";
		}

		$this->load->model("participant_model");
		$data['participantDetails'] = $this->participant_model->getParticipantDetailsForDashboard();
		$data['pledgeProfileAvailability'] = $this->participant_model->getEolCharityEvents();
		$data['transferToolAvailableFlag'] = $this->participant_model->checkTransferToolAvailability();
		$data['modifyToolAvailableFlag'] = $this->participant_model->checkModifyToolAvailability();
	
		$this->template->write_view('_content', 'participant/index', $data);
		$this->template->render();
	}
	
	public function retrieve() {
		$aPost = $this->input->sanitizeForDbPost();
		
		if (isset($aPost['username'])) {
			$this->load->model("participant_model");
			
			$arrDetails = $this->participant_model->checkForgotPasswordUsername();
			if (empty($arrDetails)) {
				$data['msgDanger'] = "Invalid User Name";
				$this->template->write_view('_content', 'participant/retrieve', $data);
				$this->template->render();
			}
			else {
				$this->load->library('token');
				$arrDetails['token'] = $this->token->generate();
				
				$this->participant_model->addForgotPasswordTokenToDb($arrDetails);
				$this->participant_model->buildSendResetPasswordEmail($arrDetails);
				$data['msgSuccess'] = "An email has been sent to reset your password";
				$this->template->write_view('_content', 'participant/retrieve', $data);
				$this->template->render();
			}			
		}
		else {
			$this->template->write_view('_content', 'participant/retrieve');
			$this->template->render();
		}
	}	

	public function reset_password() {		
		$aPost = $this->input->sanitizeForDbPost();
		
		$this->load->model("participant_model");
		
		if (isset($aPost['password'])) {
			
			$aStatus = $this->participant_model->resetAccountPassword();
			if ($aStatus == "notFound") {
				$data['msgDanger'] = "The Token ID is not valid";
			}
			else {
				$data['msgSuccess'] = "Your password has been reset";
			}
			
			$this->template->write_view('_content', 'participant/reset_password', $data);
			$this->template->render();
		}
		else {
			$checkValidToken = $this->participant_model->checkResetAccountPasswordToken();
			if ($checkValidToken == "valid") {
				$this->template->write_view('_content', 'participant/reset_password');
				$this->template->render();
			}
			else {
				redirect("participant/login?msgDanger=invalidResetPasswordToken");
			}
		}
	}
	
	public function join_team() {
		$this->load->model("participant_model");
		$aPost = $this->input->sanitizeForDbPost();
					
		if (isset($aPost['team_password'])) {
			$retValue = $this->participant_model->joinTeam($aPost['team'],$aPost['team_password']);
			
			if ($retValue == "valid") {
				redirect("participant/index?msgSuccess=joinedTeam");
			}
			else if ($retValue == "incorrectPassword") {
				$data['teams'] = $this->participant_model->getTeamsByEventID();
				$data['msgDanger'] = "The Team Password is not correct";
		
				$this->template->write_view('_content', 'participant/join_team', $data);
				$this->template->render();
			}
			else {
				echo "Error while processing."; exit;
			}	
		}
		else {			
			$data['teams'] = $this->participant_model->getTeamsByEventID();
		
			$this->template->write_view('_content', 'participant/join_team', $data);
			$this->template->render();
		}
	}
	
	public function view_team() {
		$this->load->model("participant_model");
		
		$data['teamMembers'] = $this->participant_model->getTeamMembersByAccountID();		
		
		$this->template->write_view('_content', 'participant/view_team', $data);
		$this->template->render();
	}
	
	public function edit_details() {
		$this->load->model("participant_model");
		
		$aSession = $this->session->userdata('participant');
		$aPost = $this->input->sanitizeForDbPost();
		$aGet = $this->input->get();
		
		if (isset($aGet['action']) && $aGet['action'] == "swap") {
			$retValue = $this->participant_model->swapFirstLastName();
			if ($retValue == "goodtogo") {
				redirect("participant/index?msgSuccess=firstLastNameSwapped");
			}
			else {
				echo "Error while processing."; exit;
			}	
		}
		else if (isset($aPost['address'])) {
			$retValue = $this->participant_model->updateParticipantDetails();
			if ($retValue == "goodtogo") {
				redirect("participant/index?msgSuccess=detailsUpdated");
			}
			else {
				echo "Error while processing."; exit;
			}	
		}
		else {
			$data = array();
			$data['pid'] = $aSession['participantPersonalInformationID'];
			if ($aSession['participantType'] == "individualBillingMember" || $aSession['participantType'] == "individual") {
				$data['participantInfo'] = $this->participant_model->buildIndividualParticipantArray();
				$data['participantShirtInfo'] = $this->participant_model->getIndividualShirtInfo();
				$data['drawIndividualShirtSection'] = $this->participant_model->drawIndividualShirtSection($data['participantShirtInfo']);				
			}
			/*
			else if (strtolower($aSession['participantType']) == "groupbillingmember") {
				$data['numOfGroupMembers'] = $this->participant_model->getNumGroupMembers();
				$billingMember = $this->participant_model->buildBillingMemberArray();				
				$groupMembers = $this->participant_model->buildGroupMembersArray();
				
				$data['groupInfo'] = array_merge($billingMember,$groupMembers);
			}
			*/
			$this->template->write_view('_content', 'participant/edit_details', $data);
			$this->template->render();
		}
	}
	
	public function create_pledge_profile() {
		$aPost = $this->input->sanitizeForDbPost();
	
		$this->load->model("participant_model");
		
		if (isset($aPost['charity'])) {
			$retValue = $this->participant_model->createPledgeProfile();
			if ($retValue == "goodtogo") {
				redirect("participant/index?msgSuccess=pledgeProfileCreated");
			}
			else {
				echo "Error while processing."; exit;
			}	
		}
		else {
			//Following code was taken from Core 1.6
			$characters = array("1", "2", "3", "4", "5", "6", "7", "8", "9");
			$passcodeValue = "";
			for ($i = 0; $i < 5; $i++) {
				$randomIndex = rand(0, 8);
				$passcodeValue .= $characters[$randomIndex];
				$randomIndex = "";
			}
			$profilePicFileName = $passcodeValue . "_" . microtime(true);

			$data['profilePicName'] = $profilePicFileName;
			$data['charities'] = $this->participant_model->getCharitiesForPledgeProfile();	
			$this->template->write_view('_content', 'participant/create_pledge_profile', $data);
			$this->template->render();
		}
	}
	
	public function view_pledge_profile() {
		$this->load->model("participant_model");
				
		$data['pledgeProfileDetails'] = $this->participant_model->getPledgeProfileByAccountID();
		$data['pledgesForProfile'] = $this->participant_model->getPledgesForPledgeProfile($data['pledgeProfileDetails']['intPledgeProfileID']);
		$this->template->write_view('_content', 'participant/view_pledge_profile', $data);
		$this->template->render();
	}
	
	public function pledge() {
		$aGet = $this->input->get();
		
		$this->load->model("participant_model");
		
		if (isset($aGet['id'])) {
			$data['pledgeProfileDetails'] = $this->participant_model->getPledgeProfileDetails($aGet['id']);
			$this->template->write_view('_content', 'participant/pledge', $data);
			$this->template->render();
		}
		else {
			echo "Invalid Pledge Profile"; exit;
		}
	}
	
	public function pledge_payment() {
		$this->template->write_view('_content', 'participant/pledge_payment');
		$this->template->render();
	}
	
	public function edit_pledge_profile() {
		$aPost = $this->input->sanitizeForDbPost();
		
		$this->load->model("participant_model");
		
		if (isset($aPost['profile_pledge_goal'])) {
			$retValue = $this->participant_model->editPledgeProfile();
			if ($retValue == "goodtogo") {
				redirect("participant/index?msgSuccess=pledgeProfileUpdated");
			}
			else {
				echo "Error while processing."; exit;
			}	
		}
		else {
			$data['pledgeProfileDetails'] = $this->participant_model->getPledgeProfileByAccountID();
			$this->template->write_view('_content', 'participant/edit_pledge_profile', $data);
			$this->template->render();
		}
	}
	
	public function change_activity() {
		$aGet = $this->input->get();

		$downgrade = array();
		$upgrade = array();
				
		$this->load->model("participant_model");
		
		$currentActivity = $this->participant_model->getCurrentActivity();
		$activityList = $this->participant_model->getActivityList($currentActivity['intActivityID']);		
		$registeredCount = $this->participant_model->getRegisteredCountForActivity();
		$getAge = $this->participant_model->getParticipantAgeOnRaceDay();
		//echo "AGE IS:" . $getAge; 
//echo "<pre>"; print_r($activityList); echo "</pre>"; exit;		
		$countActivityList = count($activityList);
		for ($i = 0; $i < $countActivityList; $i++) {
			$skipFlag = false;
			$anActivityID = $activityList[$i]['intActivityID'];
			$aCount = ((isset($registeredCount[$anActivityID]))?$registeredCount[$anActivityID]:0);
			
			//HARDCODED..  Check the age cap for Activity = Children's Run
			if ($anActivityID == 7) {
				if ($getAge < 5 || $getAge > 13) {
					unset($activityList[$i]);
					$skipFlag = true;					
				}
			}
			//HARDCODED..  Check for 1/2 marathon.  If it is a 1/2 marathon, there is an age check of 16 or greater
			else if (strtolower($activityList[$i]['strActivity']) == "1/2 marathon") {
				if ($getAge < 16) {
					unset($activityList[$i]);
					$skipFlag = true;	
				}
			}
			//HARDCODED..  Check for full marathon.  If it is a full marathon, there is an age check of 18 or greater
			else if (strtolower($activityList[$i]['strActivity']) == "marathon") {
				if ($getAge < 18) {
					unset($activityList[$i]);
					$skipFlag = true;	
				}
			}
			
			if (!$skipFlag) {
				//Check the cap
				if (($activityList[$i]['intCap'] - $aCount) > 0) {
					//DOWNGRADE
					if ($activityList[$i]['dblPrice'] <= $currentActivity['dblActivityPrice']) {
						$priceDifferenceFee = 0;
						$tempArray = array("activity" => $activityList[$i]['strActivity'], "priceDifferenceFee" => $priceDifferenceFee);
						$downgrade[$anActivityID] = $tempArray;
					}
					//UPGRADE
					else {
						$priceDifferenceFee = $activityList[$i]['dblPrice'] - $currentActivity['dblActivityPrice'];
						$tempArray = array("activity" => $activityList[$i]['strActivity'], "priceDifferenceFee" => $priceDifferenceFee);
						$upgrade[$anActivityID] = $tempArray;
					}
				}
			}
		}
		//echo "<pre>"; print_r($activityList); echo "</pre>"; exit;
		if (isset($aGet['action']) && $aGet['action'] == "process") {
			$aPost = $this->input->sanitizeForDbPost();
			$aSession = $this->session->userdata('participant');
						
			$newActivityID = $aPost['new_activity'];
			
			$aSession['cart']['changeActivity']['oldActivityID'] = $currentActivity['intActivityID'];
			$aSession['cart']['changeActivity']['newActivityID'] = $aPost['new_activity'];
			$aSession['cart']['changeActivity']['newShirtID'] = $aPost['new_shirt'];
			$aSession['cart']['changeActivity']['eventTax'] = $this->participant_model->getEventTax('registration');
			$aSession['cart']['changeActivity']['actualEventActivityPrice'] = $this->participant_model->getActualEventActivityPrice($aPost['new_activity']);
			
			if (array_key_exists($newActivityID,$downgrade)) {
				$aSession['cart']['changeActivity']['priceDifference'] = $downgrade[$newActivityID]['priceDifferenceFee'];
				$aSession['cart']['changeActivity']['dutType'] = "downgrade";
				
			}
			else if (array_key_exists($newActivityID,$upgrade)) {
				$aSession['cart']['changeActivity']['priceDifference'] = $upgrade[$newActivityID]['priceDifferenceFee'];
				$aSession['cart']['changeActivity']['dutType'] = "upgrade";
			}
			
			if (isset($aSession['cart']['changeActivity']['cartID'])) {
				$this->participant_model->updateSessionToCart($aSession['cart']['changeActivity']['cartID']);					
			}
			else {
				$cartID = $this->participant_model->insertCartSession();
				$aSession['cart']['changeActivity']['cartID'] = $cartID;
			}
			$this->session->set_userdata("participant",$aSession);
			
			redirect("participant/payment?action=changeActivity");
		}
		else {
			$data['currentActivity'] = $currentActivity['strActivity'];
			$data['activityList'] = $activityList;
			$data['downgradeList'] = $downgrade;
			$data['upgradeList'] = $upgrade;
		
			$this->template->write_view('_content', 'participant/change_activity', $data);
			$this->template->render();
		}
	}
	
	public function transfer_activity() {
		$this->load->model("participant_model");
		
		$aGet = $this->input->get();
		
		if (isset($aGet['action']) && $aGet['action'] == "process") {
			//$this->participant_model->setTransferActivitySession();
			redirect("participant/payment?action=transferActivity");
		}
		else {
			$data['transferCode'] = $this->participant_model->getTransferCode();
			
			$this->template->write_view('_content', 'participant/transfer_activity', $data);
			$this->template->render();
		}
	}
	
	public function payment() {
		$this->load->model("participant_model");
		
		$aSession = $this->session->userdata('participant');
		$aGet = $this->input->get();
		
		//echo "<pre>"; print_r($aSession); echo "</pre>"; exit;
		
		if (isset($aGet['action']) && $aGet['action'] == "changeActivity") {
			if (!isset($aSession['cart']['changeActivity'])) {
				redirect("participant/index?msgSuccess=downgradeUpgradeProcessed");
				exit;
			}

			$costTotalSection = $this->participant_model->drawChangeActivityPaymentCostTotals();
		}
		else if (isset($aGet['action']) && $aGet['action'] == "extraPurchase") {
			if (!isset($aSession['cart']['addons'])) {
				redirect("participant/index?msgSuccess=extraPurchaseProcessed");
				exit;
			}

			$costTotalSection = $this->participant_model->drawExtraPurchasePaymentCostTotals();
		}
		else {
			redirect("participant/index?msgDanger=invalidLink");
			exit;
		}
		$billingInfoSection = $this->participant_model->drawPaymentBillingInformation();
		$creditCardSection = $this->participant_model->drawPaymentCreditCard();
		
		$data['billingInfoSection'] = $billingInfoSection;
		$data['costTotalSection'] = $costTotalSection;
		$data['creditCardSection'] = $creditCardSection;
		
		$this->template->write_view('_content', 'participant/payment', $data);
		$this->template->render();	
	}
	
	public function transaction_process() {
		$this->load->model("participant_model");
		
		$aSession = $this->session->userdata('participant');
		$aPost = $this->input->sanitizeForDbPost();
		$aGet = $this->input->get();
						
		$data = array(); 
				
		if (isset($aGet['action']) && $aGet['action'] == "changeActivity") {
			if (!isset($aSession['cart']['changeActivity'])) {
				redirect("participant/index?msgSuccess=downgradeUpgradeProcessed");
				exit;
			}
			$aPost['total'] = $aSession['cart']['changeActivity']['total'];
			$aPost['invoice'] = $aSession['cart']['changeActivity']['cartID'];

			$this->load->library("payment");
			$this->payment->set_gateway("exact");
      $this->payment->set_currency('CA');    
			$response = $this->payment->process($aPost);
			
			if($response['status'] === TRUE) {
				$this->load->library("confirmationEmail");
				
				$this->participant_model->processChangeActivity();
				$this->confirmationemail->sendConfirmationEmail("changeActivity",$aSession['participantPersonalInformationID']);
				unset($_SESSION['participant']['cart']['changeActivity']);
			}
		}
		else if (isset($aGet['action']) && $aGet['action'] == "extraPurchase") {			
			if (!isset($aSession['cart']['addons'])) {
				redirect("participant/index?msgSuccess=extraPurchaseProcessed");
				exit;
			}
			$aPost['total'] = $aSession['cart']['addons'][$aSession['participantEventID']]['total'];
			$aPost['invoice'] = $aSession['cart']['addons'][$aSession['participantEventID']]['cartID'];

			$this->load->library("payment");
			$this->payment->set_gateway("exact");  
      $this->payment->set_currency('CA');     
			$response = $this->payment->process($aPost);
			
			if($response['status'] === TRUE) {
				$this->load->library("confirmationEmail");
				
				$this->participant_model->processAddonPurchase();
				$this->confirmationemail->sendConfirmationEmail("extraPurchase",$aSession['participantPersonalInformationID']);
				unset($_SESSION['participant']['cart']['addons']);
			}
		}
		else {
			redirect("participant/index?msgDanger=invalidLink");
			exit;
		}
		//echo "<pre>"; print_r($response); echo "</pre>"; exit;
		$data['response'] = $response;
		
		$this->template->write_view('_content', 'participant/transaction_process', $data);
		$this->template->render();	
	}
	
	public function purchase_addons() {
		$this->load->model("participant_model");
		
		$aSession = $this->session->userdata('participant');
		$aGet = $this->input->get();

		if (isset($aGet['action']) && $aGet['action'] == "process") {
			if (isset($aSession['cart']['addons'][$aSession['participantEventID']]['cartID'])) {
				$this->participant_model->updateSessionToCart($aSession['cart']['addons'][$aSession['participantEventID']]['cartID']);					
			}
			else {
				$cartID = $this->participant_model->insertCartSession();
				$aSession['cart']['addons'][$aSession['participantEventID']]['cartID'] = $cartID;
			}
			$this->session->set_userdata("participant",$aSession);
			
			redirect("participant/payment?action=extraPurchase");
		}
		else {
			$this->load->model("addons_model", "addonsm");
			
			$eventIDs[] = $aSession['participantEventID'];
			$addons = $this->addonsm->get_addons($eventIDs);
			//Get all the addons that this participant has purchased
			$participantAddonsPurchasedCount = $this->participant_model->getParticipantAddons();
			//Get all addons purchased by all participants
			$allParticipantAddonsPurchasedCount = $this->participant_model->getAllPurchasedAddons();
	//echo "<pre>"; print_r($allPurchasedCount); echo "</pre>"; exit;		
			$parsedAddons = array();
			foreach ($addons as $addon) {
				$purchasedCount = ((isset($participantAddonsPurchasedCount[$addon['addonID']]))?$participantAddonsPurchasedCount[$addon['addonID']]:0);
				$allPurchasedCount = ((isset($allParticipantAddonsPurchasedCount[$addon['addonID']]))?$allParticipantAddonsPurchasedCount[$addon['addonID']]:0);
				/*
				//Purchase limit was not set
				if (trim($addon['purchase_limit']) == "") {
					$parsedAddons[$addon['eventID']][] = $addon;
				}
				else if ($addon['purchase_limit'] > $purchasedCount) {			
					$parsedAddons[$addon['eventID']][] = $addon;
				}
				*/
				
				
				//Purchase limit and cap are both not set
				if (trim($addon['purchase_limit']) == "" && trim($addon['cap']) == "") {
					$parsedAddons[$addon['eventID']][] = $addon;
				}
				//Purchase limit is not set but cap is set
				else if (trim($addon['purchase_limit']) == "") {
					if ($addon['cap'] > $allPurchasedCount) {		
						$parsedAddons[$addon['eventID']][] = $addon;
					}
				}
				//Cap is not set but purchase limit is set
				else if (trim($addon['cap']) == "") {
					if ($addon['purchase_limit'] > $purchasedCount) {			
						$parsedAddons[$addon['eventID']][] = $addon;
					}
				}
				//Cap and purchase limit are both set
				else {
					if ($addon['cap'] > $allPurchasedCount[$addon['addonID']] && $addon['purchase_limit'] > $purchasedCount) {
						$parsedAddons[$addon['eventID']][] = $addon;
					}
				}
			}

			$data = array("addons" => $parsedAddons);
		  
			$this->template->write_view('_content', 'participant/purchase_addons', $data);
			$this->template->render();	
		}
	}
}