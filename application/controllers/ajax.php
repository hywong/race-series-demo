<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends MY_Controller {
	
	public function change_cart($id)
	{
		if(array_key_exists($id, $_SESSION['registration']))
		{
			$_SESSION['registration']['active_user'] = $id;
			echo TRUE;
			exit;
		}
		echo FALSE;
		exit;
	}
	
	public function send_feedback()
	{
		if(FILE_EXISTS(DATA_FEEDBACK_FILE))
		{
			$post = $this->input->post(NULL, TRUE);
			$line = $post['form']['fbemail'] . "%";
			$line .= $post['form']['fbname'] . "%";

			$feedback = str_replace('%', '',$post['form']['fbfeedback']);
			$feedback = preg_replace("/\r?\n/", "||", $feedback);

			$line .= $feedback . "%";

			$line .= $post['rating'] . "%";
			$line .= date('D M d H:i:s T Y') . "\r\n";

			$fh = new SplFileObject(DATA_FEEDBACK_FILE, 'a');
			if($fh->flock(LOCK_EX))
			{
				$fh->fwrite($line);
				$fh->flock(LOCK_UN);
			}
			unset($fh);
			echo TRUE;
		}
		else
		{
			echo FALSE;
		}
		exit;
	}
	
	public function sendTechsupport()
	{
		$to = 'techsupport@eventsonline.ca';	
		$subject = $_POST['event'] . ' (' . $_POST['type'].') - ' .$_POST['subject'];
		$from = $_POST['name']. '<'.$_POST['email'].'>';
		$body = '';
		if($_POST['invoice'] != ''){
			$body = "Invoice #: ".$_POST['invoice'] . "\r\n\r\n";
		}
		$body .= $_POST['msg'];
		
		$CI =& get_instance();
		$CI->load->library('email');

		$config['mailtype'] = "text";
		$config['charset'] = "utf-8";
		$CI->email->initialize($config);
		$CI->email->from($from);
		$CI->email->to($to);

		$CI->email->subject($subject);
		$CI->email->message($body);

		$result = $CI->email->send();
		
		if($result == true){
			echo "true";
			exit;
		}else{
			echo "false";
			exit;
		}
	}

	public function validate_username()
	{
		$username = $this->input->post('username');
		$tempSession = $_SESSION['registration'];
		if(isset($_SESSION['registration']['active_user']))
		{
			unset($tempSession[$_SESSION['registration']['active_user']]);
		}
		if(array_exists_recursive('username', $username, $tempSession, TRUE))
		{
			echo FALSE;
			exit;
		}

		//Validate against ARTEZ
		$this->load->library('artez');
		$validWithArtez = $this->artez->validate_username($username);

		if($validWithArtez === FALSE)
		{
			echo FALSE;
			exit;
		}

		$sqlUsername = "SELECT count(strUserName) FROM entAccount WHERE strUserName = :username;";

		$stmt = $this->db->prepare($sqlUsername);
		
		$dataToExecute = array(
			"username" => $username
			);

		if($stmt->execute($dataToExecute))
		{
			$count = $stmt->fetchColumn();

			if($count == 0)
			{
				echo TRUE;
				exit;
			}
			else
			{
				echo FALSE;
				exit;
			}
		}
		else
		{
			//TODO: Alert - failed to validate username
			echo FALSE;
			exit;
		}
	}

	public function validate_new_team()
	{
		$post = $this->input->post();

		$c = 0;
		$sqlTeams = array();
		foreach($post as $key => $value)
		{
  		$expl = explode("_", $key);
  		$eventID = end($expl);

  		$field = str_replace(array("_".$eventID, "eventteam_"), "", $key);

			if($value != "")
			{
	  		if($field == "team_action" && $value == "create")
	  		{
	  			if($post["eventteam_team_name_create_".$eventID] == "")
	  			{
	  				echo FALSE;
	  				exit;
	  			}
	  			$sqlTeams[] = "(relt.intEventID = :eventID".$c." AND entt.strTeamName = :team".$c.")";
	  			$sqlTeamData['eventID'.$c] = $eventID;
	  			$sqlTeamData['team'.$c] = $post["eventteam_team_name_create_".$eventID];
	  			$c++;
	  		}
			}
		}
  	//Check session first
    $tempSession = $_SESSION['registration'];
    unset($tempSession['last_modified']);
    unset($tempSession[$tempSession['active_user']]);
    unset($tempSession['active_user']);
		$invalid = array();
		$sessionValid = TRUE;

    foreach($tempSession as $id => $details)
    {
    	if(isset($details['teams']))
    	{
	    	foreach($details['teams'] as $eID => $teamDetails)
	    	{
	    		if(isset($teamDetails['team_action']))
	    		{
	    			if($teamDetails['team_action'] == "create")
	    			{
	    				if($post['eventteam_team_name_create_'.$eID] == $teamDetails['team_name_create'])
	    				{
	    					$invalid[$eID] = $post['eventteam_team_name_create_'.$eID];
	    					$sessionValid = FALSE;
	    				}
	    			}
	    		}
	    	}
    	}
    }

		$sql = "SELECT entt.strTeamName as team, relt.intEventID as eventID FROM relTeam as relt
		INNER JOIN entTeam as entt
		ON entt.intTeamID = relt.intTeamID
		WHERE ";


		$sqlAppended = $sql . implode(" OR ", $sqlTeams) . " GROUP BY entt.strTeamName";

		$stmt = $this->db->prepare($sqlAppended);

		if($stmt->execute($sqlTeamData))
		{
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

			if(!$results && $sessionValid === TRUE)
			{
				echo TRUE;
				exit;
			}
			else
			{
				foreach($results as $row)
				{
					$invalid[$row['eventID']][] = $row['team'];
				}
			}
		}
		else
		{
			echo FALSE;
		}

		if($invalid)
		{
			echo json_encode($invalid);
			exit;
		}
	}
	
	public function validate_existing_team()
  {
  	$post = $this->input->post();
  	
  	$teamInformation = array();
  	$teamField = array();
  	$c = 0;
		$teamsValidated = 0;

  	//Check session first
    $tempSession = $_SESSION['registration'];
    unset($tempSession['last_modified']);
    unset($tempSession[$tempSession['active_user']]);
    unset($tempSession['active_user']);

  	foreach($tempSession as $id => $details)
  	{
  		if(isset($details['teams']))
  		{
	  		foreach($details['teams'] as $eID => $teamDetails)
	  		{
	  			if($post['eventteam_team_action_'.$eID] == 'join' && $post['eventteam_team_name_'.$eID] != '')
	  			{
	  				if(strpos($post['eventteam_team_name_'.$eID], 'TEMPTEAM_') === 0)
	  				{
	  					$captainSessionID = stristr($post['eventteam_team_name_'.$eID], '_');
	  					$captainSessionID = str_replace("_", '', $captainSessionID);
	  					if($post['eventteam_team_password_'.$eID] == $tempSession[$captainSessionID]['teams'][$eID]['team_password_create'])
	  					{
				  			$teamInformation[$eID]['id'] = $post["eventteam_team_name_".$eID];
				  			$teamInformation[$eID]['password'] = $post["eventteam_team_password_".$eID];
	  						unset($post['eventteam_team_action_'.$eID]);
	  						unset($post['eventteam_team_name_'.$eID]);
	  						unset($post['eventteam_team_password_'.$eID]);
	  						$teamsValidated++;
	  						break 2;
	  					}
	  					else
	  					{
				  			$teamInformation[$eID]['id'] = $post["eventteam_team_name_".$eID];
				  			$teamInformation[$eID]['password'] = $post["eventteam_team_password_".$eID];
								$invalidTeams[$eID] = $post["eventteam_team_name_".$eID];
	  						unset($post['eventteam_team_action_'.$eID]);
	  						unset($post['eventteam_team_name_'.$eID]);
	  						unset($post['eventteam_team_password_'.$eID]);
	  						break 2;
	  					}
	  				}
	  			}
	  		}  			
  		}
  	}

  	$teamWhereClauses = array();
  	foreach($post as $key => $value)
  	{
  		if($value != "")
  		{
	  		$expl = explode("_", $key);
	  		$eventID = end($expl);

	  		$field = str_replace(array("_".$eventID, "eventteam_"), "", $key);

	  		if($field == "team_action" && $value == "join")
	  		{
	  			if($post["eventteam_team_password_".$eventID] == "")
	  			{
	  				echo FALSE;
	  				exit;
	  			}
	  			$teamInformation[$eventID]['id'] = $post["eventteam_team_name_".$eventID];
	  			$teamInformation[$eventID]['password'] = $post["eventteam_team_password_".$eventID];
	  			$teamWhereClauses[] = "(relt.intEventID = :eventID".$c." AND entt.intTeamID = :teamID".$c.")";
	  			$teamWhereClauseData["eventID".$c] = $eventID;
	  			$teamWhereClauseData["teamID".$c] = $post["eventteam_team_name_".$eventID];
	  			$teamField[$eventID] = $post['eventteam_team_name_'.$eventID];
	  		}
  		}
  	}
		
		$teamsToValidate = count($teamInformation);

  	if($teamInformation && $teamWhereClauses && $teamField)
  	{
	  	$sqlTeamValidation = "SELECT relt.intTeamID AS teamID, relt.intEventID AS eventID, entt.strTeamPassword AS teamPassword, entt.strTeamName AS team FROM relTeam AS relt
			INNER JOIN entTeam AS entt
			ON entt.intTeamID = relt.intTeamID
			WHERE ";

			$sqlAppended = $sqlTeamValidation . implode(",", $teamWhereClauses);
			$stmt = $this->db->prepare($sqlAppended);

			if($stmt->execute($teamWhereClauseData))
			{
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

				$parsedResults = array();
				$teamNames = array();
				foreach($results as $row)
				{
					$parsedResults[$row['eventID']][$row['teamID']] = $row['teamPassword'];
					$teamNames[$row['teamID']] = $row['team'];
				}

				foreach($teamInformation as $eID => $team)
				{
					if(crypt($team['password'], $parsedResults[$eID][$team['id']]) == $parsedResults[$eID][$team['id']])
					{
						$teamsValidated++;
					}
					else
					{
						$invalidTeams[$eID] = $team['id'];
					}
				}

			}
			else
			{
				echo FALSE;
			}  		
  	}

		if($teamsToValidate === $teamsValidated)
		{
			echo TRUE;
			exit;
		}
		else
		{
			echo json_encode($invalidTeams);
			exit;
		}
  }

  public function validate_new_team_group()
  {
		$post = $this->input->post();

		$c = 0;
		$sqlTeams = array();
		$expl = explode("_", $post['eventID']);
		$eventID = reset($expl);

		$sqlTeams[] = "(relt.intEventID = :eventID".$c." AND entt.strTeamName = :team".$c.")";
		$sqlTeamData['eventID'.$c] = $eventID;
		$sqlTeamData['team'.$c] = $post["team_name_create"];

  	//Check session first
    $tempSession = $_SESSION['registration'];
    unset($tempSession['last_modified']);
    unset($tempSession['active_user']);
		$invalid = array();
		$sessionValid = TRUE;

    foreach($tempSession as $id => $details)
    {
    	if(isset($details['teams']))
    	{
	    	foreach($details['teams'] as $eID => $teamDetails)
	    	{
	    		if(isset($teamDetails['team_name_create']))
	    		{
    				if($post['team_name_create'] == $teamDetails['team_name_create'])
    				{
    					$invalid[$eID] = $post['team_name_create'];
    					$sessionValid = FALSE;
    				}
	    		}
	    	}
    	}
    }

		$sql = "SELECT entt.strTeamName as team, relt.intEventID as eventID FROM relTeam as relt
		INNER JOIN entTeam as entt
		ON entt.intTeamID = relt.intTeamID
		WHERE ";


		$sqlAppended = $sql . implode(" OR ", $sqlTeams). "GROUP BY (entt.strTeamName)";

		$stmt = $this->db->prepare($sqlAppended);

		if($stmt->execute($sqlTeamData))
		{
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

			if(!$results && $sessionValid === TRUE)
			{
				echo TRUE;
				exit;
			}
			else
			{
				foreach($results as $row)
				{
					$invalid[$row['eventID']][] = $row['team'];
				}
			}
		}
		else
		{
			echo FALSE;
		}

		if($invalid)
		{
			echo json_encode($invalid);
			exit;
		}
  }

  public function validate_group_shirts()
  {
  	$post = $this->input->post();
  	$this->load->model('ajax_model', 'ajax');
  	$this->ajax->validate_count_group_shirts();
  }

  public function validate_discount()
  {
  	$post = $this->input->post();
  	
  	$this->load->model('ajax_model', 'ajax');
  	$response = $this->ajax->validate_discount_code($post['code']);
  }

  public function remove_discount()
  {
  	if(isset($_SESSION['registration'][$_SESSION['registration']['active_user']]['discount_code']))
  	{
  		unset($_SESSION['registration'][$_SESSION['registration']['active_user']]['discount_code']);
  		echo TRUE;
  		exit;
  	}
  	else
  	{
  		echo FALSE;
  		exit;
  	}

  }

	public function set_cart_state()
	{
		(!$_SESSION['cart-state'] ? $_SESSION['cart-state'] = TRUE : $_SESSION['cart-state'] = FALSE);
	}

	public function adminChangePass() {
		$aPost = $this->input->sanitizeForDbPost();
				
		$this->load->model("ajax_model");
		$changePassFlag = $this->ajax_model->changeAdminPassword($aPost['target'],$aPost['new_password']);
		
		echo $changePassFlag;
	}
	
	public function adminChangeAccountDetails() {
				
		$this->load->model("ajax_model");
		$changeDetailsFlag = $this->ajax_model->changeAdminAccountDetails();
		
		echo $changeDetailsFlag;
	}
	
	public function changeAdminEvent() {
		$aPost = $this->input->sanitizeForDbPost();
		$aSession = $this->session->userdata('admin');
		
		$aSession['adminEventID'] = $aPost['anEventID'];
		$this->session->set_userdata("admin",$aSession);
		
		echo $aPost['anEventID'];
	}

	public function cart_add_addon()
	{
		$post = $this->input->post(NULL, TRUE);
		$this->load->model('addons_model', 'addonsm');
		$addons = $this->addonsm->get_addons();
		$addonShirt = $this->addonsm->get_addon_shirt_cap();
		$addonCounts = $this->addonsm->get_addon_counts();

		//Get current addon name and parse some values
		$cAddons = count($addons);
		for($i = 0; $i < $cAddons; $i++)
		{
			if($addons[$i]['addonID'] == $post['addonID'])
			{
				$thisName = $addons[$i]['addon'];
				$thisPrice = $addons[$i]['price'];
				$addonLimits[$addons[$i]['eventID']][$addons[$i]['addonID']]['limit'] = $addons[$i]['purchase_limit'];
				$addonLimits[$addons[$i]['eventID']][$addons[$i]['addonID']]['cap'] = $addons[$i]['cap'];
				break;
			}
		}

		if(isset($addonShirt[$post['eventID']][$post['addonID']]))
		{
			if(isset($post['details']['tshirt']))
			{
				$shirtID = $post['details']['tshirt'];
				$shirtDetails = $addonShirt[$post['eventID']][$post['addonID']][$shirtID];
				if($shirtDetails)
				{
					if(($shirtDetails['count'] + 1) >= $shirtDetails['cap'])
					{
						echo FALSE;
						exit;
					}					
				}
			}
		}

		//Loop and calculate how many items we have in our active cart
		$allAddonIDs = array();
		if(isset($_SESSION['registration'][$_SESSION['registration']['active_user']]['addons']))
		{
			$sessAddons = $_SESSION['registration'][$_SESSION['registration']['active_user']]['addons'];

			foreach($sessAddons as $eID => $aIDList)
			{
				foreach($aIDList as $aID => $itemList)
				{
					foreach($itemList as $dets)
					{
						if(isset($dets['qty']))
						{
							if(!isset($counts[$eID][$aID]))
								$counts[$eID][$aID] = 0;
							
							$counts[$eID][$aID] += $dets['qty'];
						}
					}
					$allAddonIDs[] = $aID;
				}
			}
		}

		//HARDCODED: Shuttle BEFORE and AFTER cross check
		if($post['addonID'] == 3 && in_array(4, $allAddonIDs))
		{
			echo FALSE;
			exit;
		}

		if($post['addonID'] == 4 && in_array(3, $allAddonIDs))
		{
			echo FALSE;
			exit;
		}

		if(isset($sessAddons[$post['eventID']][$post['addonID']]))
		{
			if(!isset($post['details']['tshirt']))
			{
				echo FALSE;
				exit;
			}
		}

		if(isset($addonLimits[$post['eventID']][$post['addonID']]))
		{
			//Check if we exceed our limit per customer
			if(isset($counts[$post['eventID']][$post['addonID']]))
			{
				if($addonLimits[$post['eventID']][$post['addonID']]['limit'] > 0)
				{
					if($counts[$post['eventID']][$post['addonID']] >= $addonLimits[$post['eventID']][$post['addonID']]['limit'])
					{
						echo FALSE;
						exit;
					}
				}				
			}

			//Check if we exceed our addon caps
			if($addonLimits[$post['eventID']][$post['addonID']]['cap'] > 0)
			{
				if(isset($addonCounts[$post['eventID']][$post['addonID']]))
				{
					//Check if we have previously selected this item
					if(isset($counts[$post['eventID']][$post['addonID']]))
					{
						(isset($post['details']['qty']) ? $qtyToAdd = $post['details']['qty'] : $qtyToAdd = 1);
						if(($addonCounts[$post['eventID']][$post['addonID']] + $counts[$post['eventID']][$post['addonID']] + $qtyToAdd) > $addonLimits[$post['eventID']][$post['addonID']]['cap'])
						{
							echo FALSE;
							exit;
						}
					}
					else
					{
						(isset($post['details']['qty']) ? $qtyToAdd = $post['details']['qty'] : $qtyToAdd = 1);
						if(($addonCounts[$post['eventID']][$post['addonID']] + $qtyToAdd) > $addonLimits[$post['eventID']][$post['addonID']]['cap'])
						{
							echo FALSE;
							exit;
						}
					}
				}
			}
		}

		$data = array();

		if(isset($post['details']))
		{
			$data = $post['details'];
			if(!isset($post['details']['qty']))
			{
				$data['qty'] = 1;
			}
		}
		else
		{
			$data['qty'] = 1;
		}

		$_SESSION['registration'][$_SESSION['registration']['active_user']]['addons'][$post['eventID']][$post['addonID']][] = $data;
		end($_SESSION['registration'][$_SESSION['registration']['active_user']]['addons'][$post['eventID']][$post['addonID']]);
		$key = key($_SESSION['registration'][$_SESSION['registration']['active_user']]['addons'][$post['eventID']][$post['addonID']]);
		reset($_SESSION['registration'][$_SESSION['registration']['active_user']]['addons'][$post['eventID']][$post['addonID']]);
		$string = "<tr id=\"".uniqid()."\"><td>".$_SESSION['general'][$post['eventID']]['display']."</td><td>".$thisName."</td><td>$".number_format($thisPrice * $data['qty'], 2, '.', '')."</td><td>".$data['qty']."</td><td><button class=\"btn btn-xs btn-danger remove-addon\" data-cart-addon-eid=\"".$post['eventID']."\" data-cart-addon-aid=\"".$post['addonID']."\" data-cart-addon-id=\"".$key."\">".translate("Remove")."</button></td></tr>";
		echo $string;
		exit;
	}
	
	public function cart_remove_addon()
	{
		$post = $this->input->post(NULL, TRUE);
		$sessAddons = $_SESSION['registration'][$_SESSION['registration']['active_user']]['addons'];
		$unset = FALSE;
		if(isset($sessAddons[$post['cartAddonEvent']][$post['cartAddonID']][$post['cartAddonPos']]))
		{
			unset($sessAddons[$post['cartAddonEvent']][$post['cartAddonID']][$post['cartAddonPos']]);

			$unset = TRUE;
			if(empty($sessAddons[$post['cartAddonEvent']][$post['cartAddonID']]))
					unset($sessAddons[$post['cartAddonEvent']][$post['cartAddonID']]);
			
			if(empty($sessAddons[$post['cartAddonEvent']]))
				unset($sessAddons[$post['cartAddonEvent']]);			
		}

		$_SESSION['registration'][$_SESSION['registration']['active_user']]['addons'] = $sessAddons;

		if($unset === TRUE)
		{
			echo $post['rowID'];
		}
		else
		{
			echo FALSE;
		}
	}
	
	public function cart_add_addon_dashboard()
	{
		$aSession = $this->session->userdata('participant');
		$post = $this->input->post(NULL, TRUE);
		
		$this->load->model('addons_model', 'addonsm');
		$eventIDs[] = $aSession['participantEventID'];
		$addons = $this->addonsm->get_addons($eventIDs);
	
		$this->load->model('ajax_model');
		$alreadyPurchasedCount = $this->ajax_model->getAddonPurchasedCount($post['addonID']);
		$alreadyPurchasedByAllCount = $this->ajax_model->getAllAddonPurchasedCount($post['addonID']);

		$data = array();
		$quantitySelected = 1;
		$addonIdSelected = $post['addonID'];
		
		//Get current addon name and parse some values
		$cAddons = count($addons);
		for($i = 0; $i < $cAddons; $i++)
		{
			if($addons[$i]['addonID'] == $post['addonID'])
			{
				$thisName = $addons[$i]['addon'];
				$thisPrice = $addons[$i]['price'];
				$addonLimits = $addons[$i]['purchase_limit'];
				$addonCap = $addons[$i]['cap'];
			}
		}
		//echo "THISNAME:" . $thisName; exit;
		//Loop and check what we have
		
		$sessAddons = ((isset($_SESSION['participant']['cart']['addons'][$_SESSION['participant']['participantEventID']]))?$_SESSION['participant']['cart']['addons'][$_SESSION['participant']['participantEventID']]:array());
		//echo "<pre>"; print_r($sessAddons); echo "</pre>"; exit;
		/*
		
		foreach($sessAddons as $eID => $aIDList)
		{
			foreach($aIDList as $aID => $itemList)
			{
				$counts[$eID][$aID] = count($itemList);
				if(isset($addonLimits))
				{
					if($counts[$eID][$aID] < $addonLimits[$eID][$aID])
					{
						echo FALSE;
						exit;
					}
				}
			}
		}
		*/
		
		//Check if the user has already selected this addon
		if (array_key_exists($addonIdSelected,$sessAddons)) {
			echo "addonAlreadySelected";
			exit;
		}

		//Check for limits and set variables.  It checks that the addonName isn't tshirt since aValue is actually the shirt size, not the quantity chosen
		if (isset($post['details'])) {
			foreach ($post['details'] as $aValue) {
				if ($thisName == "TYS In-Training Long Sleeve Shirt") {
					//Check the cap for this addon.  This check may not even be needed since only 1 shirt can be bought at a time...
					if ((1 + $alreadyPurchasedByAllCount) > $addonCap) {
						echo "capReached";
						exit;
					}
					else {
						$data['tshirtID'] = $aValue;
					}
				}
				else {
					$quantitySelected = $aValue;
					
					//Check the addon limit per participant
					if (($quantitySelected + $alreadyPurchasedCount) > $addonLimits) {
						echo "limitReached";
						exit;
					}
					//Check the cap for this addon
					else if (($quantitySelected + $alreadyPurchasedByAllCount) > $addonCap) {
						echo "capReached";
						exit;
					}
				}
			}
		}


		//Calculate the price x quantity
		$thisPrice = $thisPrice * $quantitySelected;

		//TODO: Addon validation in cart (limits per customer, caps)
		
		//echo "<pre>"; print_r($post['details']); echo "</pre>"; exit;

		//$data = $post['details'];
		$data['name'] = $thisName;
		$data['qty'] = $quantitySelected;
		$data['price'] = $thisPrice;

		
		
		
		$_SESSION['participant']['cart']['addons'][$post['eventID']][$post['addonID']][] = $data;
		end($_SESSION['participant']['cart']['addons'][$post['eventID']][$post['addonID']]);
		$key = key($_SESSION['participant']['cart']['addons'][$post['eventID']][$post['addonID']]);
		reset($_SESSION['participant']['cart']['addons'][$post['eventID']][$post['addonID']]);
		
		$string = "<tr id=\"".uniqid()."\"><td>".$_SESSION['general'][$post['eventID']]['display']."</td><td>".$thisName."</td><td>".$thisPrice."</td><td>".$quantitySelected."</td><td><button class=\"btn btn-xs btn-danger remove-addon\" data-cart-addon-eid=\"".$post['eventID']."\" data-cart-addon-aid=\"".$post['addonID']."\" data-cart-addon-id=\"".$key."\">".translate("Remove")."</button></td></tr>";
		echo $string;
		exit;
	}
	
	public function cart_remove_addon_dashboard()
	{
		$post = $this->input->post(NULL, TRUE);
		$sessAddons = $_SESSION['participant']['cart']['addons'];
		$unset = FALSE;
		if(isset($sessAddons[$post['cartAddonEvent']][$post['cartAddonID']][$post['cartAddonPos']]))
		{
			unset($sessAddons[$post['cartAddonEvent']][$post['cartAddonID']][$post['cartAddonPos']]);

			$unset = TRUE;
			if(empty($sessAddons[$post['cartAddonEvent']][$post['cartAddonID']]))
					unset($sessAddons[$post['cartAddonEvent']][$post['cartAddonID']]);
			
			if(empty($sessAddons[$post['cartAddonEvent']]))
				unset($sessAddons[$post['cartAddonEvent']]);			
		}

		$_SESSION['participant']['cart']['addons'] = $sessAddons;

		if($unset === TRUE)
		{
			echo $post['rowID'];
		}
		else
		{
			echo FALSE;
		}
	}
	
	public function buildActivityDropdownByPID() {	
		$this->load->model("ajax_model");
		$activities = $this->ajax_model->getActivityByPID();
		
		$retString = "<select id=\"activity\" name=\"activity\" class=\"form-control required\" style=\"width:200px\">
			<option value=\"\">" . translate("Select") . "</option>";
		$countActivities = count($activities);
		for ($i = 0; $i < $countActivities; $i++) {
			$retString .= "<option value=\"" . $activities[$i]['intActivityID'] . "\">" . $activities[$i]['strActivity'] . "</option>";
		}
		$retString .= "</select>";
		
		echo $retString;
	}
	
	public function buildActivityDropdownByEID() {	
		$this->load->model("ajax_model");
		
		$aPost = $this->input->sanitizeForDbPost();
		
		$activities = $this->ajax_model->getActivityByEID($aPost['anEventID']);
		
		$retString = "
			<label for=\"activity\">" . translate("Activity") . "</label>
			<select id=\"activity\" name=\"activity\" class=\"form-control required\">			
			<option value=\"\">" . translate("Select") . "</option>
			<option value=\"1\">" . translate("All") . "</option>";
		$countActivities = count($activities);
		for ($i = 0; $i < $countActivities; $i++) {
			$retString .= "<option value=\"" . $activities[$i]['intActivityID'] . "\">" . $activities[$i]['strActivity'] . "</option>";
		}
		$retString .= "</select>";
		
		echo $retString;
	}
	
	public function changeParticipantEvent() {
		$aPost = $this->input->sanitizeForDbPost();
		$aSession = $this->session->userdata('participant');
		
		$aSession['participantEventID'] = $aPost['anEventID'];
		$this->session->set_userdata("participant",$aSession);
		
		//If the eventID is 1 it means that "All" was chosen
		echo $aPost['anEventID'];
	}
	
	public function generateTransferCode() {
		$aCode = uniqid();
		
		$this->load->model("ajax_model");
		
		$this->ajax_model->setTransferCode($aCode);
		
		echo $aCode;
	}
	
	public function generateShirtDropdown() {
		$this->load->model("ajax_model");
		
		$aString = $this->ajax_model->generateShirtDropdown();
		
		echo $aString;
	}
	
	public function buildShirtSelectOptions() {
		$this->load->model("ajax_model");
						
		$aString = $this->ajax_model->generateShirtOptions();
		
		echo $aString;
	}
}