<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller {

	public function login() {		
		$aPost = $this->input->sanitizeForDbPost();
		
		if (isset($aPost['email'])) {
			//$this->load->model("admin_model", "admin");
			$this->load->model("admin_model");
			$verifyLoginFlag = $this->admin_model->verifyLogin($aPost['email'],$aPost['passcode']);

			if ($verifyLoginFlag == "valid") {
				redirect("admin/index");
			}
			else {
				//$data = array("errorMsg" => "invalidLogin");
				$data['msgDanger'] = "Invalid Login";
				$this->template->write_view('_content', 'admin/login', $data);
				$this->template->render();
			}
			
		}
		else {
			$aGet = $this->input->get();
			$data = array();
			if (isset($aGet['msgDanger']) && $aGet['msgDanger'] == "sessionExpired") {
				$data['msgDanger'] = "The session has expired.";
			}
			else if (isset($aGet['msgSuccess']) && $aGet['msgSuccess'] == "loggedOut") {
				$data['msgSuccess'] = "You have successfully logged out.";
			}
			$this->template->write_view('_content', 'admin/login', $data);
			$this->template->render();
		}
	}
	
	public function logout() {
		$this->session->unset_userdata('admin');
		redirect("admin/login?msgSuccess=loggedOut");
	}
	
	public function index() {	
		$this->load->model("admin_model");
		
		$aSession = $this->session->userdata('admin');
		
		//Get the data for the line graph
		$data['indivRegistrationCountByDay'] = $this->admin_model->getIndivRegistrationCountsByDate();
		$data['groupRegistrationCountByDay'] = $this->admin_model->getGroupRegistrationCountsByDate();
		
		//Get the data for the Number of Registrations bar graph
		if ($aSession['adminEventID'] == 1) {
			//$data['registrationCountByEventOrActivity'] = $this->admin_model->getIndivRegistrationCountsByEvent();
			
			$indivRegCounts = $this->admin_model->getIndivRegistrationCountsByEvent();
			$groupRegCounts = $this->admin_model->getGroupRegistrationCountsByEvent();
						
		}
		else {
			//$data['registrationCountByEventOrActivity'] = $this->admin_model->getRegistrationCountsByActivity();
			$indivRegCounts = $this->admin_model->getIndivRegistrationCountsByActivity();
			$groupRegCounts = $this->admin_model->getGroupRegistrationCountsByActivity();
		}
		//Set up the array that outputs the bar graph 
		$arrAllRegCounts = array();
		$countIndivRegCounts = count($indivRegCounts);
		for ($i = 0; $i < $countIndivRegCounts; $i++) {
			$arrAllRegCounts[$indivRegCounts[$i]['aLabel']] = $indivRegCounts[$i]['aCount'];
		}
		
		$countGroupRegCounts = count($groupRegCounts);
		for ($i = 0; $i < $countGroupRegCounts; $i++) {
			if (isset($arrAllRegCounts[$groupRegCounts[$i]['aLabel']])) {
				$arrAllRegCounts[$groupRegCounts[$i]['aLabel']] += $groupRegCounts[$i]['aCount'];
			}
			else {
				$arrAllRegCounts[$groupRegCounts[$i]['aLabel']] = $groupRegCounts[$i]['aCount'];
			}
		}
		$data['registrationCountByEventOrActivity'] = $arrAllRegCounts;
		
		//Get the charity amounts for the donut graph
		$data['charityTotals'] = $this->admin_model->getTotalsByCharity();	
		
		//Get the recent transactions
		$data['recentTransactions'] = $this->admin_model->getRecentTransactions();
		
		$this->template->write_view('_content', 'admin/index', $data);
		$this->template->render();
	}
	
	public function admin_manager() {
		$aGet = $this->input->get();
		$this->load->model("admin_model");
		
		if (isset($aGet['action']) && $aGet['action'] == "add") {
			$retValue = $this->admin_model->addAdminAccount();
			if ($retValue == "goodtogo") {
				redirect("admin/admin_manager?msg=added");
			}
			else {
				echo "Error while processing."; exit;
			}			
		}
		else if (isset($aGet['action']) && $aGet['action'] == "remove") {
			$retValue = $this->admin_model->removeAdminAccount($aGet['id']);
			if ($retValue == "goodtogo") {
				redirect("admin/admin_manager?msg=removed");
			}
			else {
				echo "Error while processing."; exit;
			}				
		}
		else {
			$data['adminAccounts'] = $this->admin_model->getAllAdminAccount();
			$data['adminAccountTypes'] = $this->admin_model->getAllAdminAccountType();
			$this->template->write_view('_content', 'admin/admin_manager', $data);
			$this->template->render();
		}
	}

  public function charity_challenge_manager()
  {
    $this->load->library("artez");
    $charities = $this->artez->get_all_charities();

    $data = array(
      "live" => $charities['live'],
      "pending" => $charities['pending'],
      "custom" => $charities['custom']
      );
    $this->template->write_view('_content', 'admin/charity_challenge_manager', $data);
    $this->template->render();
  }
	
	public function discount_code_manager() {
		$this->load->model("admin_model");
		
		$aGet = $this->input->get();
		
		if (isset($aGet['msgSuccess']) && $aGet['msgSuccess'] == "added") {
			$data['msgSuccess'] = "The discount code has been added";
		}
		else if (isset($aGet['msgSuccess']) && $aGet['msgSuccess'] == "updated") {
			$data['msgSuccess'] = "The discount code has been updated";
		}
		
		$data['discountCodes'] = $this->admin_model->getAllDiscountCodes();
		$data['discountCodesUsed'] = $this->admin_model->getDiscountCodesUsed();
		$this->template->write_view('_content', 'admin/discount_code_manager', $data);
		$this->template->render();
	}
	
	public function discount_code_add() {
		$this->load->model("admin_model");
		
		$aPost = $this->input->sanitizeForDbPost();
		
		if (isset($aPost['code'])) {
			$retValue = $this->admin_model->addDiscountCode();
			if ($retValue == "goodtogo") {
				redirect("admin/discount_code_manager?msgSuccess=added");
			}
			else {
				echo "Error while processing."; exit;
			}
		}
		else {
			$data['formType'] = "add";
			$data['discountCodeTypes'] = $this->admin_model->getAllDiscountCodeType();
			$data['events'] = $this->admin_model->getAllEvent();
			//$data['activities'] = $this->admin_model->getAllActivity();
			$data['activities'] = array();
			$data['statuses'] = $this->admin_model->getAllActiveStatus();
			$this->template->write_view('_content', 'admin/discount_code_add_edit', $data);
			$this->template->render();
		}
	}
	
	public function discount_code_edit() {
		$this->load->model("admin_model");
		
		$aPost = $this->input->sanitizeForDbPost();
		$aGet = $this->input->get();
		
		if (isset($aPost['code'])) {
			$retValue = $this->admin_model->editDiscountCode();
			if ($retValue == "goodtogo") {
				redirect("admin/discount_code_manager?msgSuccess=updated");
			}
			else {
				echo "Error while processing."; exit;
			}
		}
		else {
			$data['formType'] = "edit";
			$data['discountCodeDetails'] = $this->admin_model->getDiscountCodeByID($aGet['cid']);
			$data['discountCodeTypes'] = $this->admin_model->getAllDiscountCodeType();
			$data['events'] = $this->admin_model->getAllEvent();
			$data['activities'] = $this->admin_model->getAllActivity($data['discountCodeDetails']['intEventID']);
			$data['statuses'] = $this->admin_model->getAllActiveStatus();
			
			$this->template->write_view('_content', 'admin/discount_code_add_edit', $data);
			$this->template->render();
		}
	}
	
	public function activity_cap_manager() {
		$this->load->model("admin_model");
		
		$aGet = $this->input->get();
		
		if (isset($aGet['msgSuccess']) && $aGet['msgSuccess'] == "added") {
			$data['msgSuccess'] = "The activity cap has been added";
		}
		else if (isset($aGet['msgSuccess']) && $aGet['msgSuccess'] == "updated") {
			$data['msgSuccess'] = "The activity cap has been updated";
		}
		
		$data['activities'] = $this->admin_model->getActivityByEventID();
		$data['participantsRegistered'] = $this->admin_model->getRegisteredCountForActivity();
		$this->template->write_view('_content', 'admin/activity_cap_manager', $data);
		$this->template->render();
	}
	
	public function activity_cap_add() {
		$this->load->model("admin_model");
		
		$aPost = $this->input->sanitizeForDbPost();
		
		if (isset($aPost['activity'])) {
			$retValue = $this->admin_model->updateActivityCap();
			if ($retValue == "goodtogo") {
				redirect("admin/activity_cap_manager?msgSuccess=added");
			}
			else {
				echo "Error while processing."; exit;
			}
		}
		else {
			$data['formType'] = "add";
			$data['activities'] = $this->admin_model->getActivityByEventID();			
			$this->template->write_view('_content', 'admin/activity_cap_add_edit', $data);
			$this->template->render();
		}
	}
	
	public function activity_cap_edit() {
		$this->load->model("admin_model");
		
		$aPost = $this->input->sanitizeForDbPost();
		$aGet = $this->input->get();
		
		if (isset($aPost['activity'])) {
			$retValue = $this->admin_model->updateActivityCap();
			if ($retValue == "goodtogo") {
				redirect("admin/activity_cap_manager?msgSuccess=updated");
			}
			else {
				echo "Error while processing."; exit;
			}
		}
		else {
			$data['formType'] = "edit";
			$data['activities'] = $this->admin_model->getActivityCapByActivityID($aGet['aid']);			
			$this->template->write_view('_content', 'admin/activity_cap_add_edit', $data);
			$this->template->render();
		}
	}
	
	public function transfer_date_manager() {
		$this->load->model("admin_model");
		
		$aGet = $this->input->get();
		
		if (isset($aGet['msgSuccess']) && $aGet['msgSuccess'] == "added") {
			$data['msgSuccess'] = "The transfer tool availability dates have been added";
		}
		else if (isset($aGet['msgSuccess']) && $aGet['msgSuccess'] == "updated") {
			$data['msgSuccess'] = "The transfer tool availability dates have been updated";
		}
		
		$data['transferAvailabilityDates'] = $this->admin_model->getTransferAvailabilityDates();
		$this->template->write_view('_content', 'admin/transfer_date_manager', $data);
		$this->template->render();
	}
	
	public function transfer_date_add() {
		$this->load->model("admin_model");
		
		$aPost = $this->input->sanitizeForDbPost();
		
		if (isset($aPost['txtTransferOpenDate'])) {
			$retValue = $this->admin_model->addTransferAvailabilityDate();
			if ($retValue == "goodtogo") {
				redirect("admin/transfer_date_manager?msgSuccess=added");
			}
			else {
				echo "Error while processing."; exit;
			}
		}
		else {
			$data['formType'] = "add";			
			$this->template->write_view('_content', 'admin/transfer_date_add_edit',$data);
			$this->template->render();
		}
	}
	
	public function transfer_date_edit() {
		$this->load->model("admin_model");
		
		$aPost = $this->input->sanitizeForDbPost();
		
		if (isset($aPost['txtTransferOpenDate'])) {
			$retValue = $this->admin_model->editTransferAvailabilityDate();
			if ($retValue == "goodtogo") {
				redirect("admin/transfer_date_manager?msgSuccess=updated");
			}
			else {
				echo "Error while processing."; exit;
			}
		}
		else {
			$data['formType'] = "edit";	
			$data['transferAvailabilityDates'] = $this->admin_model->getTransferAvailabilityDates();
			$this->template->write_view('_content', 'admin/transfer_date_add_edit',$data);
			$this->template->render();
		}
	}
	
	public function participant_modify_date_manager() {
		$this->load->model("admin_model");
		
		$aGet = $this->input->get();
		
		if (isset($aGet['msgSuccess']) && $aGet['msgSuccess'] == "added") {
			$data['msgSuccess'] = "The participant modify tool availability dates have been added";
		}
		else if (isset($aGet['msgSuccess']) && $aGet['msgSuccess'] == "updated") {
			$data['msgSuccess'] = "The participant modify tool availability dates have been updated";
		}
		
		$data['participantModifyAvailabilityDates'] = $this->admin_model->getParticipantModifyAvailabilityDates();
		$this->template->write_view('_content', 'admin/participant_modify_date_manager', $data);
		$this->template->render();
	}
	
	public function participant_modify_date_add() {
		$this->load->model("admin_model");
		
		$aPost = $this->input->sanitizeForDbPost();
		
		if (isset($aPost['txtParticipantModifyOpenDate'])) {
			$retValue = $this->admin_model->addParticipantModifyAvailabilityDate();
			if ($retValue == "goodtogo") {
				redirect("admin/participant_modify_date_manager?msgSuccess=added");
			}
			else {
				echo "Error while processing."; exit;
			}
		}
		else {
			$data['formType'] = "add";			
			$this->template->write_view('_content', 'admin/participant_modify_date_add_edit',$data);
			$this->template->render();
		}
	}
	
	public function participant_modify_date_edit() {
		$this->load->model("admin_model");
		
		$aPost = $this->input->sanitizeForDbPost();
		
		if (isset($aPost['txtParticipantModifyOpenDate'])) {
			$retValue = $this->admin_model->editParticipantModifyAvailabilityDate();
			if ($retValue == "goodtogo") {
				redirect("admin/participant_modify_date_manager?msgSuccess=updated");
			}
			else {
				echo "Error while processing."; exit;
			}
		}
		else {
			$data['formType'] = "edit";	
			$data['participantModifyAvailabilityDates'] = $this->admin_model->getParticipantModifyAvailabilityDates();
			$this->template->write_view('_content', 'admin/participant_modify_date_add_edit',$data);
			$this->template->render();
		}
	}

	public function finalize()
	{    
    $this->load->model("registration_model", "registrationm");
    $this->load->model("payment_model", "paymentm");
    $this->load->library("registration");

    $taxRates = $this->registration->get_tax_rates();
    $prices = $this->registration->get_activity_prices();    
    $discountDetails = $this->paymentm->calculate_cart_discounts($prices);

    $addonPrices = $this->paymentm->get_addon_prices();
    $registrationData = $this->paymentm->get_activity_total_costs($prices, $discountDetails, $addonPrices);
    $totalTransactionData = $registrationData['summary'];
    $activityDataByParticipant = $registrationData['participants'];

    //Get post, XSS filter.
    $post = $this->input->post(NULL, TRUE);

    if(isset($post['total']))
    {
    	if(is_numeric($post['total']))
    	{
    		$totalTransactionData['finalTotal'] = $post['total'];
    	}
    	else
    	{
    		$totalTransactionData['finalTotal'] = 0;
    	}
    }
    else
    {
    	$totalTransactionData['finalTotal'] = 0;
    }
    
    $totalTransactionData['eolFee'] = 0;
    $totalTransactionData['taxTotal'] = 0;
    $totalTransactionData['discountTotal'] = 0;
    $totalTransactionData['remit'] = 0;
    $totalTransactionData['eolFeeTax'] = 0;
   
    $this->db->beginTransaction();
    try
    {
        $this->registrationm->set_transaction_status();
        $this->registrationm->set_registration_transaction_type($_SESSION['type']);
        $this->registrationm->set_account_personal_rel_type();
        $this->registrationm->set_addon_transaction_type($_SESSION['type']);
        $this->registrationm->set_donation_transaction_type($_SESSION['type']);
        $this->registrationm->set_account_transaction_type($_SESSION['type']);

        //If we're dealing with a transfer, deactivate the previous user's event/activity
        if($_SESSION['type'] == "transfer")
        {
          $this->registrationm->deactivate_transfer($_SESSION['transfer_code']);
        }
        
        if($_SESSION['type'] == 'transfer')
        {
          $this->registrationm->insert_transaction_transfer($totalTransactionData, TRUE);
        }
        else
        {
          $this->registrationm->insert_transaction($totalTransactionData, TRUE);
        }

        foreach($_SESSION['registration'] as $id => $participant)
        {
          if($id != "active_user" && $id != "last_modified")
          {
            if($_SESSION['type'] == "group")
            {
              $this->registrationm->insert_account_group($id, TRUE);
            }
            else
            {
              $this->registrationm->insert_account($id, TRUE);
            }

            if($_SESSION['type'] == "group")
            {
              $this->registrationm->insert_personal_group($id, TRUE);
              $this->registrationm->insert_account_personal_relation_group($id);
            }
            else
            {
              $this->registrationm->insert_personal($id, TRUE);
              $this->registrationm->insert_account_personal_relation($id);
            }

            $this->registrationm->insert_account_transaction();

            $transactionRelationData = array(
              "registrationSubTotal" => number_format($activityDataByParticipant[$id]['activityCostTotal'], 2, '.', ''),
              "registrationSubTotalTax" => number_format($activityDataByParticipant[$id]['taxTotal'], 2, '.', '')
              );

            if(isset($discountDetails['data'][$id]))
            {
              $transactionRelationData["discount"] = number_format($activityDataByParticipant[$id]['discount'], 2, '.', '');
            }
            else
            {
              if($_SESSION['type'] == "group")
              {
                if(isset($discountDetails['type']))
                {
                  if($discountDetails['type'] == 'group')
                  {
                    $transactionRelationData['discount'] = number_format($discountDetails['data']['discount'] * $participant['personal']['members'], 2, '.', '');
                  }
                }
                else
                {
                  $transactionRelationData["discount"] = 0;
                }
              }
              else
              {
                $transactionRelationData["discount"] = 0;
              }
            }

            if(isset($discountDetails['type']))
            {
              $transactionRelationData["discountType"] = $discountDetails['type'];
              $transactionRelationData['artezDiscount'] = 0;
              //Loop to see if we need to insert artez information
              $insertArtez = FALSE;
              $artezDiscountTotal = 0;

              if(isset($discountDetails['data'][$id]['artez']))
              {
                foreach($discountDetails['data'][$id]['artez'] as $eID => $artez)
                {
                  if(isset($artez['artez_discount']))
                    $insertArtez = TRUE;

                  $artezDiscountTotal += $artez['artez_details']['discount'];
                }                
              }

              if($insertArtez === TRUE)
              {
                $transactionRelationData['artezDiscount'] = $artezDiscountTotal;

                $this->load->library('artez');
                // $artezProfileDetails = array(
                //   'event_id' => $participant['personal']['event_id'],
                //   'first_name' => $participant['personal']['first_name'],
                //   'last_name' => $participant['personal']['last_name'],
                //   'invoice' => $participant['personal']['invoice'],
                //   'address1' => $participant['personal']['address1'],
                //   'address2' => '',
                //   'address3' => '',
                //   'language_preference' => $participant['personal']['language_preference'],
                //   'city' => $participant['personal']['city'],
                //   'province' => $participant['personal']['province'],
                //   'country' => $participant['personal']['country'],
                //   'postal_code' => $participant['personal']['postal_code'],
                //   'home_phone' => $participant['personal']['home_phone'],
                //   'email' => $participant['personal']['email'],
                //   'charity_id' => json_encode(utf8_encode()),
                //   'registration_type' => 'Individual Registration',
                //   'artez_username' => $participant['personal']['username'],
                //   'artez_password' => $participant['personal']['password']
                //   );

                $this->registrationm->insert_artez_account_relation($id, $discountDetails['data'][$id]);
              }

              if($discountDetails['type'] == "code")
              {
                $transactionRelationData["discountCodeID"] = $discountDetails['data'][$id]['discountID'];
              }    
              else
              {
                $transactionRelationData['discountCodeID'] = NULL;
              }
            }
            else
            {
              $transactionRelationData['artezDiscount'] = 0;
              $transactionRelationData['discountCodeID'] = NULL;
              $transactionRelationData["discountType"] = NULL;
            }

            if($_SESSION['type'] == "transfer")
            {
              $transferEolFee = 5;
              $transferEolFeeTax = $transferEolFee * EOL_FEE_TAX;
              $transferEolFee -= $transferEolFeeTax;
              $transferDutFee = $totalTransactionData['activityCostTotal'];
              $thisEventID = reset(explode("_", $participant['activities'][0]));
              $transferDutFeeTax = $totalTransactionData['activityCostTotal'] * $taxRates[$thisEventID]['registration'];
              $transferDutFee -= $transferDutFeeTax;
              $dutTransactionRelationData = array(
                "dutFee" => number_format($transferDutFee, 2, '.', ''),
                "dutFeeTax" => number_format($transferDutFeeTax, 2, '.', ''),
                "eolFee" => number_format($transferEolFee, 2, '.', ''),
                "eolFeeTax" => number_format($transferEolFeeTax, 2, '.', ''),
                "total" => number_format($totalTransactionData['activityCostTotal'] + $transferEolFee + $transferEolFeeTax, 2, '.', '')
                );
              $this->registrationm->insert_dut_transaction_relation($id, $dutTransactionRelationData);
            }
            else
            {
              $this->registrationm->insert_registration_transaction_relation($id, $transactionRelationData);
            }

            if(isset($participant['activities']))
            {
              $this->registrationm->insert_activities($id, $prices);

              if($_SESSION['type'] == "group")
              {
                $this->registrationm->insert_finishtime_group($id);
                $this->registrationm->insert_participant_shirt_group($id); 
              }
              else
              {
                $this->registrationm->insert_finishtime($id);
                $this->registrationm->insert_participant_shirt($id);                
              }
            }
            
            if($_SESSION['type'] == "group")
            {
              $groupAddons = FALSE;
              foreach($participant['group_details'] as $memberDetails)
              {
                if(isset($memberDetails['addon']))
                {
                  $groupAddons = TRUE;
                  break;
                }
              }
              
              if($groupAddons)
              {
                $this->registrationm->insert_addons_group($id, $addonPrices);
              }
            }
            else
            {
              if(isset($participant['addons']))
              {
                if(!empty($participant['addons']))
                  $this->registrationm->insert_addons($id, $addonPrices);
              }
            }

            if(isset($participant['aka']))
            {
              $this->load->library('aka');

              foreach($participant['aka'] as $eventID => $akaDetails)
              {
                $fieldsForAka = array(
                  'first_name' => $participant['personal']['first_name'],
                  'last_name' => $participant['personal']['last_name'],
                  'email' => $participant['personal']['email'],
                  'home_phone' => $participant['personal']['home_phone'],
                  'address' => $participant['personal']['address'],
                  'country' => $participant['personal']['country'],
                  'city' => $participant['personal']['city'],
                  'postal_code' => $participant['personal']['postal_code'],
                  'gender' => $participant['personal']['gender'],
                  'aka_team_name' => $akaDetails['aka_team_name'],
                  'aka_fundraising_option' => $akaDetails['aka_fundraising_option']
                  );

                $this->aka->create_profile($fieldsForAka);
              }
              $this->registrationm->insert_aka($id);
            }

            if(isset($participant['seriesfoundation']))
            {
              $this->registrationm->insert_seriesdonation($id);
            }

            if(isset($participant['charities']))
            {
              $donationMade = FALSE;
              $profileCreated = FALSE;
              //Loop to find if we need to do anything
              foreach($participant['charities'] as $eventID => $fields)
              {
                if(isset($fields['donation']))
                  $donationMade = TRUE;
                if(isset($fields['fundraising_profile']))
                {
                  if($fields['fundraising_profile'] == "Yes")
                    $profileCreated = TRUE;
                }
              }
              //This will parse and add all donations (for each event)
              if($donationMade === TRUE)
                $this->registrationm->insert_donations($id);

              //This will parse and add all pledge profiles (for each event)
              if($profileCreated === TRUE)
                $this->registrationm->insert_fundraising_profiles($id, TRUE);
            }

            if($_SESSION['type'] == "group")
            {
              $this->registrationm->insert_teams_group($id);
            }
            else
            {
              if(isset($participant['teams']))
              {
                $this->registrationm->insert_teams($id);
              }              
            }

            if(isset($participant['eventextras']))
            {
              //This includes 2 inserts, the extras insert and the account-extra relation
              $this->registrationm->insert_extras($id, TRUE);
            }
          }
        }

      $this->db->commit();
      // $this->db->rollback();
      // exit;

      $this->load->library("confirmationEmail");

      foreach($this->registrationm->insertIDs['all-personal-id'] as $personalID)
      {
         $this->confirmationemail->sendConfirmationEmail("staffRegistration", $personalID);
      }
      
      unset($_SESSION['registration']);
      unset($_SESSION['cartID']);
      unset($_SESSION['cart-state']);
      unset($_SESSION['type']);

      $data = array(
      	"created" => TRUE
      	);
    }
    catch(Exception $e)
    {
      $this->db->rollback();

      $data = array(
      	"created" => FALSE
      	);
    }

    $this->template->write_view('_content', 'admin/staff_entry_completed', $data);
    $this->template->render();
	}
	
	public function confirmation_list() {
		$this->load->model("admin_model");
		
		$data['confirmationList'] = $this->admin_model->getAllParticipants();
		$this->template->write_view('_content', 'admin/confirmation', $data);
		$this->template->render();
	}

	public function staff_entry()
	{
    $_SESSION['type'] = "staffEntry";

    if(!isset($_SESSION['registration']))
    {
      $uID = uniqid();
      $_SESSION['registration']['active_user'] = $uID;
      $_SESSION['registration']['last_modified'] = strtotime('now');
      $_SESSION['registration'][$uID] = array();
    }

    if(!isset($_SESSION['registration']['active_user']))
    {
      $_SESSION['registration']['active_user'] = uniqid();
    }
	
		 $data = array(
		"pagesArr" => array(
					"Event Selection" => "active current",
					"Personal Information" => "",
					"Charities" => "",
					"Teams" => "",
					"Add-ons" => "",
					"Confirmation" => "",
					)
      );
      $this->template->write_view("_steps", "admin/steps", $data);

    $this->load->model("registration_model", "registration");
    $activities = $this->registration->get_form_activities();

    $wasExpired = FALSE;
    $data = array(
      "activities" => $activities,
      "expired" => $wasExpired,
      "formAction" => base_url("admin/personal")
      );

		$this->template->write_view('_content', 'register/activities', $data);
		$this->template->render();
	}

	public function reset_staff_entry()
	{
		unset($_SESSION['registration']);
		redirect('admin/staff_entry');
	}

	public function personal()
	{
    //We initialize again since we don't want it to expire going from page 1 to 2
    $this->load->model("registration_model", "registration");
    $this->registration->initialize_session();
	
		 $data = array(
		"pagesArr" => array(
					"Event Selection" => "active",
					"Personal Information" => "active current",
					"Charities" => "",
					"Teams" => "",
					"Add-ons" => "",
					"Confirmation" => "",
					)
      );
      $this->template->write_view("_steps", "admin/steps", $data);

    $sections = $this->registration->get_form_sections();
    $data = array(
      "sections" => $sections,
      "formAction" => base_url("admin/charities")
      );
		$this->template->write_view("_content", "register/personal", $data);
    $this->template->render();
	}

  public function teams()
  {
    //Get all teams by event
    $this->load->model('team_model', 'team');
    $teams = $this->team->get_existing_teams();
    $sections = $this->team->get_form_sections($teams);
	
		 $data = array(
		"pagesArr" => array(
					"Event Selection" => "active",
					"Personal Information" => "active",
					"Charities" => "active",
					"Teams" => "active current",
					"Add-ons" => "",
					"Confirmation" => "",
					)
      );
      $this->template->write_view("_steps", "admin/steps", $data);
	
    $data = array(
      "formAction" => base_url("admin/addons"),
      "sections" => $sections
      );
    $this->template->write_view("_content", "register/teams", $data);
    $this->template->render();
  }

  public function charities()
  {
    $this->load->model("donation_model", "donationm");
    $charities = $this->donationm->get_charities();
    $sections = $this->donationm->get_form_sections($charities);
	
			 $data = array(
		"pagesArr" => array(
					"Event Selection" => "active",
					"Personal Information" => "active",
					"Charities" => "active current",
					"Teams" => "",
					"Add-ons" => "",
					"Confirmation" => "",
					)
      );
      $this->template->write_view("_steps", "admin/steps", $data);

    $data = array(
      "sections" => $sections,
      "formAction" => base_url("admin/teams")
      );

    $this->template->write_view("_content", "register/charities", $data);
    $this->template->render();
  }

  public function addons()
  {
    $this->load->model("addons_model", "addonsm");
    $addons = $this->addonsm->get_addons();
    $parsedAddons = array();
    foreach($addons as $addon)
    {
      $parsedAddons[$addon['eventID']][] = $addon;
    }
	
			 $data = array(
		"pagesArr" => array(
					"Event Selection" => "active",
					"Personal Information" => "active",
					"Charities" => "active",
					"Teams" => "active",
					"Add-ons" => "active current",
					"Confirmation" => "",
					)
      );
      $this->template->write_view("_steps", "admin/steps", $data);

    $data = array(
      "addons" => $parsedAddons
      );
    $this->template->write_view("_content", "register/addons", $data);
    $this->template->render();
  }

  public function confirmation()
  {
    $this->load->model("registration_model", "registration");
    //We manually refresh the session because addons doesn't post to here
    $this->registration->initialize_session();
    $summary = $this->registration->get_cart_summary();
	
				 $data = array(
		"pagesArr" => array(
					"Event Selection" => "active",
					"Personal Information" => "active",
					"Charities" => "active",
					"Teams" => "active",
					"Add-ons" => "active",
					"Confirmation" => "active current",
					)
      );
      $this->template->write_view("_steps", "admin/steps", $data);
	
    $data = array(
      "summary" => $summary
      );
    $this->template->write_view("_content", "admin/admin_confirmation", $data);
    $this->template->render();
  }

	public function staff_entry_manager() {
		$aGet = $this->input->get();
		$this->load->model("admin_model");
		
		if (isset($aGet['action']) && $aGet['action'] == "update") {
			$retValue = $this->admin_model->updateStaffEntryPaidStatuses();
			if ($retValue == "goodtogo") {
				redirect("admin/staff_entry_manager?msgSuccess=updated");
			}
			else {
				echo "Error while processing."; exit;
			}	
		}
		else {		
			if (isset($aGet['msgSuccess']) && $aGet['msgSuccess'] == "updated") {
				$data['msgSuccess'] = "The payment status has been updated";
			}
			$data['staffEntries'] = $this->admin_model->getAllStaffEntries();
			$data['paidStatuses'] = $this->admin_model->getAllPaidStatuses();
			$this->template->write_view('_content', 'admin/staff_entry_manager', $data);
			$this->template->render();
		}
	}
	
	public function download_race_data() {
		$this->load->model("admin_model");
		
		$aSession = $this->session->userdata('admin');
		
		$data = $this->admin_model->getRaceData();

		//GENERATE THE CSV FILE
		header('Content-Encoding: UTF-8');
		header('Content-type: text/csv; charset=UTF-8');
		header('Content-Disposition: attachment; filename=race_data.csv');		
		header("Pragma: no-cache");
		header("Expires: 0");
		
		if ($aSession['adminEventID'] == 1) {
			$arrHeaders = array("Participant ID", "Event", "Activity", "User Name", "First Name", "Last Name", "Address", "City", "Province", "Country", "Postal Code", "Home Phone", "Mobile Phone", "Email", "Gender", "Date Of Birth", "Age On Registration", "Age On Dec 31", "Age On Race Day", "Series Newsletter", "Nationality", "Nationality Duration", "Industry Type", "Industry Type Other", "Yearly Salary", "Emergency Contact Name", "Emergency Contact Number", "Medical Condition", "Shirt Size", "Finish Time", "Learn About Event", "Learn About Event Other", "First Time Participating", "How Many Times Participated", "Why Participating", "Is Captain", "Team Name", "Group Name", "Date Created");
		}
		else {
			$arrHeaders = array("Participant ID", "Activity", "User Name", "First Name", "Last Name", "Address", "City", "Province", "Country", "Postal Code", "Home Phone", "Mobile Phone", "Email", "Gender", "Date Of Birth", "Age On Registration", "Age On Dec 31", "Age On Race Day", "Series Newsletter", "Nationality", "Nationality Duration", "Industry Type", "Industry Type Other", "Yearly Salary", "Emergency Contact Name", "Emergency Contact Number", "Medical Condition", "Shirt Size", "Finish Time", "Learn About Event", "Learn About Event Other", "First Time Participating", "How Many Times Participated", "Why Participating", "Is Captain", "Team Name", "Group Name", "Date Created");
		}
		echo implode(",", $arrHeaders) . "\r\n";
		
		$countData = count($data);
		for ($i = 0; $i < $countData; $i++) {
			$removeCommaData = str_ireplace(",", "", $data[$i]);
			$removeCommaData = array_map("utf8_decode",$removeCommaData);
			echo implode(",", $removeCommaData) . "\r\n";
		}
	}
	
	public function download_account_data() {
		$this->load->model("admin_model");
		
		$data = $this->admin_model->getAccountData();

		//GENERATE THE CSV FILE
		header('Content-Encoding: UTF-8');
		header('Content-type: text/csv; charset=UTF-8');
		header('Content-Disposition: attachment; filename=race_data.csv');		
		header("Pragma: no-cache");
		header("Expires: 0");
		
		$arrHeaders = array("TransactionID", "Invoice ID", "Transaction Type", "AccountID", "First Name", "Last Name", "Activity Cost Total", "Addon Total", "Dut Total", "Discount Total", "Tax Total", "Eol Fee", "Donation Total", "Final Total", "Remit", "Date Created");
		echo implode(",", $arrHeaders) . "\r\n";
		
		$countData = count($data);
		for ($i = 0; $i < $countData; $i++) {
			$dataMod = array_map("utf8_decode",$data[$i]);
			echo implode(",", $dataMod) . "\r\n";
		}
	}
	
	public function download_addons() {
		$this->load->model("admin_model");
		
		$aSession = $this->session->userdata('admin');
		
		$data = $this->admin_model->getAddonData();
		
		//GENERATE THE CSV FILE
		header('Content-Encoding: UTF-8');
		header('Content-type: text/csv; charset=UTF-8');
		header('Content-Disposition: attachment; filename=race_data.csv');		
		header("Pragma: no-cache");
		header("Expires: 0");
		
		if ($aSession['adminEventID'] == 1) {
			$arrHeaders = array("Participant ID", "Event", "First Name", "Last Name", "Addon", "Qty", "Addon Transaction Type", "Shirt Size", "Date");
		}
		else {
			$arrHeaders = array("Participant ID", "First Name", "Last Name", "Addon", "Qty", "Addon Transaction Type", "Shirt Size", "Date");
		}
		echo implode(",", $arrHeaders) . "\r\n";
		
		$countData = count($data);
		for ($i = 0; $i < $countData; $i++) {
			$dataMod = array_map("utf8_decode",$data[$i]);
			echo implode(",", $dataMod) . "\r\n";
		}
	}
	
	public function download_pledge_donation() {
		$this->load->model("admin_model");
		
		$data = $this->admin_model->getPledgeDonationData();
		
		//GENERATE THE CSV FILE
		header('Content-Encoding: UTF-8');
		header('Content-type: text/csv; charset=UTF-8');
		header('Content-Disposition: attachment; filename=race_data.csv');		
		header("Pragma: no-cache");
		header("Expires: 0");
		
		$arrHeaders = array("Participant ID", "First Name", "Last Name", "Charity", "Donation Total", "Donation Transaction Type", "Date");
		echo implode(",", $arrHeaders) . "\r\n";
		
		$countData = count($data);
		for ($i = 0; $i < $countData; $i++) {
			$dataMod = array_map("utf8_decode",$data[$i]);
			echo implode(",", $dataMod) . "\r\n";
		}
	}
	
	public function tshirt_cap_manager() {
		$this->load->model("admin_model");
		
		$aGet = $this->input->get();
		$activityID = 0;
		if (isset($aGet['aid']) && trim($aGet['aid']) != "") {
			$activityID = $aGet['aid'];
		}
		
		if (isset($aGet['msgSuccess']) && $aGet['msgSuccess'] == "updated") {
			$data['msgSuccess'] = "The t-shirt cap has been updated";
		}

		$data['selectedActivity'] = $activityID;
		$data['activities'] = $this->admin_model->getActivityByEventID();
		$data['tshirtCaps'] = $this->admin_model->getTshirtCaps($activityID);
		$data['tshirtAssigned'] = $this->admin_model->getTshirtAssigned($activityID);
		$this->template->write_view('_content', 'admin/tshirt_cap_manager', $data);
		$this->template->render();
	}
	
	public function tshirt_cap_edit() {
		$this->load->model("admin_model");
		
		$aPost = $this->input->sanitizeForDbPost();
		$aGet = $this->input->get();
		
		if (isset($aPost['cap'])) {
			$retValue = $this->admin_model->editTshirtCap();
			if ($retValue == "goodtogo") {
				redirect("admin/tshirt_cap_manager?aid=" . $aPost['activity'] . "&msgSuccess=updated");
			}
			else {
				echo "Error while processing."; exit;
			}
		}
		else {
			$data['tshirtCap'] = $this->admin_model->getTshirtCapByTshirtID($aGet['aid'],$aGet['tid']);
			$this->template->write_view('_content', 'admin/tshirt_cap_edit',$data);
			$this->template->render();
		}
	}
	
	public function participant_search() {
		$this->load->model("admin_model");
		
		$aGet = $this->input->get();
		
		if (isset($aGet['msgSuccess'])) {
			$data['msgSuccess'] = "The participant has been updated";
		}
		
		$data['participants'] = $this->admin_model->getAllParticipantTokens();
		$this->template->write_view('_content', 'admin/participant_search', $data);
		$this->template->render();
	}
	
	public function participant_search_modify() {
		$this->load->model("admin_model");
		
		$aPost = $this->input->sanitizeForDbPost();
		$aGet = $this->input->get();
		
		if (isset($aGet['action']) && $aGet['action'] == "update") {
			$retValue = $this->admin_model->updateParticipantDetails();
			if ($retValue == "goodtogo") {
				redirect("admin/participant_search?msgSuccess=updated");
			}
			else {
				echo "Error while processing."; exit;
			}	
		}
		else {
			$data['pid'] = $aPost['invoice'];
			$data['pType'] = $aPost['piType'];
			$data['pActivity'] = $aPost['activity'];
			
			$data['participantInfo'] = $this->admin_model->getParticipantInfoByPID();
			$this->template->write_view('_content', 'admin/participant_search_modify', $data);
			$this->template->render();
		}
	}
	
	public function team_manager() {
		$this->load->model("admin_model");
		
		$aPost = $this->input->sanitizeForDbPost();
		
		if (isset($aPost['teamPassword'])) {
			$this->admin_model->processChangeTeamPassword();
			$data['msgSuccess'] = "The team password has been changed.";
		}
		
		$data['teams'] = $this->admin_model->getTeamByEventID();
		$this->template->write_view('_content', 'admin/team_manager', $data);
		$this->template->render();
	}
	
	public function generate_txt() {
		$this->generate_data_txt();
		$this->generate_pledge_data_txt();
		$this->generate_addon_data_txt();
	}
	
	private function generate_data_txt() {
		$this->load->model("admin_model");
		
		$file = "/mnt/nfs/events/" . SERIES_DIRECTORY . "/data/data.txt";
		$writeData = "";
		$delimiter = "%";
				
		$data = $this->admin_model->getAllTransactionData();
		//Get all activity event costs from relAccountActivity
		$activityEventCost = $this->admin_model->getAllActivityEventCost();
		//Get discount codes and their associated event
		$discountCode = $this->admin_model->getAllDiscountCodeEvents();
		//Get all possible events
		$allEvent = $this->admin_model->getAllEvent();
		
//echo "<pre>"; print_r($activityEventCost); echo "</pre>"; exit;
		
		$events = array();
		$eventCosts = array();
		$eventDiscounts = array();
		//Build event cost and event discount headers
		foreach ($allEvent as $anEvent) {
			if (strtolower($anEvent['strEvent']) != "all") {
				$events[] = $anEvent['strEvent'];
				$eventCosts[] = $anEvent['strEvent'] . " Cost";
				$eventDiscounts[] = $anEvent['strEvent'] . " Discount";
			}
		}
		
		$arrHeader = array("invoice", "transaction_type", "first_name", "last_name", "city", "province", "home_phone", "email", "donation");
		$arrHeader2 = array("activity_cost", "addon", "dut");
		$arrHeader3 = array("discount", "tax", "total", "online_fee", "remit", "date");
		
		//Write the header
		$writeData .= implode($delimiter, $arrHeader) . $delimiter . implode($delimiter, $eventCosts) . $delimiter . implode($delimiter, $arrHeader2) . $delimiter . implode($delimiter, $eventDiscounts) . $delimiter . implode($delimiter, $arrHeader3) . "\r\n";	
		
		foreach ($data as $someData) {
			$numOfEvents = 0;
			if (isset($activityEventCost[$someData['intAccountID']])) {
				$numOfEvents = count($activityEventCost[$someData['intAccountID']]);
			}
			
			$strTransactionType = $someData['strTransactionType'];
			$dblDonationTotal = number_format($someData['dblDonationTotal'],2);
			$dblDiscountTotal = number_format($someData['dblDiscountTotal'],2);
			$dblTaxTotal = number_format($someData['dblTaxTotal'],2);
			$dblFinalTotal = number_format($someData['dblFinalTotal'],2);
			$dblEolFee = number_format($someData['dblEolFee'],2);
			$dblRemit = number_format($someData['dblRemit'],2);
			$dblActivityCostTotal = number_format($someData['dblActivityCostTotal'],2);
			$dblAddonTotal = number_format($someData['dblAddonTotal'],2);
			$dblDutTotal = number_format($someData['dblDutTotal'],2);
			
			//Check for cart member
			if ($someData['strPersonalInformationType'] == "individual") {
				$strTransactionType = "cartMember";
				$dblDonationTotal = "0.00";
				$dblDiscountTotal = "0.00";
				$dblTaxTotal = "0.00";
				$dblFinalTotal = "0.00";
				$dblEolFee = "0.00";
				$dblRemit = "0.00";
				$dblActivityCostTotal = "0.00";
				$dblAddonTotal = "0.00";
				$dblDutTotal = "0.00";
			}

			//Write the first set of data
			$writeData .= $someData['intCartID'] . $delimiter . $strTransactionType . $delimiter . $someData['strFirstName'] . $delimiter . $someData['strLastName'] . 	$delimiter . $someData['strCity'] . $delimiter . $someData['strProvince'] . $delimiter . $someData['strHomePhone'] . $delimiter . $someData['strEmail'] . $delimiter . $dblDonationTotal . $delimiter;
			
			//Write the event costs
			foreach ($events as $eventName) {
				if ($someData['strTransactionType'] == "individualRegistration" || $someData['strTransactionType'] == "transfer") {
					$writeData .= ((isset($activityEventCost[$someData['intAccountID']][$eventName]))?number_format($activityEventCost[$someData['intAccountID']][$eventName],2):"0.00") . $delimiter;
				}
				else if ($someData['strTransactionType'] == "groupRegistration") {
					$writeData .= ((isset($activityEventCost[$someData['intAccountID']][$eventName]))?$dblActivityCostTotal:"0.00") . $delimiter;
				}
				else {
					$writeData .= "0.00" . $delimiter;
				}
			}
			
			//Write the second set of data
			$writeData .= $dblActivityCostTotal . $delimiter . $dblAddonTotal . $delimiter . $dblDutTotal . $delimiter;
			
			//Write the event discounts
			foreach ($events as $eventName2) {
				if ($someData['strTransactionType'] == "individualRegistration" || $someData['strTransactionType'] == "groupRegistration") {
					//Check for discount type = "event"
					if ($someData['strDiscountType'] == "event") {
						if (array_key_exists($eventName2,$activityEventCost[$someData['intAccountID']])) {
							$writeData .= number_format(($someData['dblDiscount'] / $numOfEvents),2) . $delimiter;
						}
						else {
							$writeData .= "0.00" . $delimiter;
						}
					}
					//Check for discount type = "group"
					else if ($someData['strDiscountType'] == "group") {
						if (array_key_exists($eventName2,$activityEventCost[$someData['intAccountID']])) {
							$writeData .= number_format($someData['dblDiscount'],2) . $delimiter;
						}
						else {
							$writeData .= "0.00" . $delimiter;
						}
					}
					//Check for discount type = "code"
					else if ($someData['strDiscountType'] == "code") {
						if ($eventName2 == $discountCode[$someData['intDiscountCodeID']]) {
							$writeData .= number_format($someData['dblDiscount'],2) . $delimiter;
						}
						else {
							$writeData .= "0.00" . $delimiter;
						}						
					}
					//Check for discount type = "artez"
					else if ($someData['strDiscountType'] == "artez") {
					
					}
					else {
						$writeData .= "0.00" . $delimiter;
					}
				}
				else {
					$writeData .= "0.00" . $delimiter;
				}
			}
									
			//Write the third set of data
			$writeData .= $dblDiscountTotal . $delimiter . $dblTaxTotal . $delimiter . $dblFinalTotal . $delimiter . $dblEolFee . $delimiter . $dblRemit . $delimiter . $someData['dttDateCreated'] . "\r\n";
		}
											
		$fh = fopen($file, 'r+');
		flock($fh, LOCK_EX);	
		ftruncate($fh, 0);
		fwrite($fh, $writeData);
		flock($fh, LOCK_UN);
		fclose($fh);	

		//echo $writeData;
	}
	
	private function generate_pledge_data_txt() {
		$this->load->model("admin_model");
		
		$file = "/mnt/nfs/events/" . SERIES_DIRECTORY . "_pledge/data/data.txt";
		$writeData = "";
		$delimiter = "%";
		
		$donationData = $this->admin_model->getAllDonationData();
		
		$arrHeader = array("invoice", "event", "donation_type", "charity", "donation", "first_name", "last_name", "city", "province", "home_phone", "email", "online_fee_tax", "total", "online_fee", "remit", "date");
		
		$writeData .= implode($delimiter, $arrHeader) . "\r\n";
		
		foreach ($donationData as $someData) {
			$writeData .= implode($delimiter, $someData) . "\r\n";
		}

		$fh = fopen($file, 'r+');
		flock($fh, LOCK_EX);	
		ftruncate($fh, 0);
		fwrite($fh, $writeData);
		flock($fh, LOCK_UN);
		fclose($fh);	
	}
	
	private function generate_addon_data_txt() {
		$this->load->model("admin_model");
		
		$file = "/mnt/nfs/events/" . SERIES_DIRECTORY . "/data/addon_data.txt";
		$writeData = "";
		$delimiter = "%";
		
		$donationData = $this->admin_model->getAllAddonData();
		
		$arrHeader = array("invoice", "event", "addon_type", "first_name", "last_name", "city", "province", "home_phone", "email", "addon", "quantity", "shirt_size", "total", "date");
		
		$writeData .= implode($delimiter, $arrHeader) . "\r\n";
		
		foreach ($donationData as $someData) {
			$writeData .= implode($delimiter, $someData) . "\r\n";
		}

		$fh = fopen($file, 'r+');
		flock($fh, LOCK_EX);	
		ftruncate($fh, 0);
		fwrite($fh, $writeData);
		flock($fh, LOCK_UN);
		fclose($fh);	
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */