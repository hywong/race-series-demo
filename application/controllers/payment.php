<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment extends Public_Controller {

  public function __construct()
  {
    parent::__construct();

    if(!isset($_SESSION['registration']))
    {
      redirect('register');
    }
    else
    {
      $tempSession = $_SESSION['registration'];
      unset($tempSession['active_user']);
      unset($tempSession['last_modified']);
      if(empty($tempSession))
      {
        redirect('register');
      }
      
      foreach($_SESSION['registration'] as $id => $p)
      {
        if($id != "last_modified" && $id != "active_user")
        {
          if(!isset($p['activities']) || count($p['activities']) === 0)
          {
            redirect('register');
          }
          if(!isset($p['personal']))
          {
            unset($_SESSION['registration'][$id]);
          }
        }
      }
    }
  }

	public function index()
	{
    $this->load->model("payment_model", "paymentm");
    $this->load->library("registration");
    $this->load->model("registration_model", "registrationm");
    $prices = $this->registration->get_activity_prices();
    $paymentForm = $this->paymentm->get_form_sections();
    // $discountSummary = $this->paymentm->calculate_discounts($prices);
    $registrationSummary = $this->paymentm->get_registration_summary($prices);

    $paymentSummary = $registrationSummary['summary'];

    if(!empty($discountSummary['summaryArray']))
    {
      $paymentSummary = array_merge($paymentSummary, $discountSummary['summaryArray']);
    }

    if(isset($paymentSummary['Summary']))
    {
      $grandTotalToBottom = $paymentSummary['Summary'];
      unset($paymentSummary['Summary']);
      $paymentSummary['Summary'] = $grandTotalToBottom;
    }
    else
    {
      $paymentSummary['Summary']['Grand Total'][' '][0] = 0;
      $grandTotalToBottom = $paymentSummary['Summary'];
      unset($paymentSummary['Summary']);
      $paymentSummary['Summary'] = $grandTotalToBottom;
    }

  if($_SESSION['type'] == "transfer")
  {
      $data = array(
        "pagesArr" => array(
          "Event Selection" => "active",
          "Personal Information" => "active",
          "Charities" => "active",
          "Teams" => "active",
          "Add-ons" => "active",
          "Confirmation" => "active",
          "Payment and Receipt" => "active current"
              )
        );
  }
	else if($_SESSION['type'] == "group")
    {
			$data = array(
				"pagesArr" => array(
						"Billing Information" => "active",
						"Group Information" => "active",
						"Confirmation" => "active",
						"Payment and Receipt" => "active current"
							)
			  );
			$this->template->write_view("_steps", "group_steps", $data);
	}else{
	 $data = array(
		"pagesArr" => array(
					"Event Selection" => "active",
					"Personal Information" => "active",
					"Charities" => "active",
					"Teams" => "active",
					"Add-ons" => "active",
					"Confirmation" => "active",
					"Payment and Receipt" => "active current"
					)
	  );
	  $this->template->write_view("_steps", "steps", $data);
	}

    $data = array(
      "paymentForm" => $paymentForm,
      "summary" => $paymentSummary,
      "values" => $registrationSummary['values']
      );
		$this->template->write_view('_content', 'payment', $data);
		$this->template->render();
	}

  public function process()
  {
    //If successful
    $this->load->model("registration_model", "registrationm");
    $this->load->model("payment_model", "paymentm");
    $this->load->library("payment");

    $this->load->library("registration");
    $taxRates = $this->registration->get_tax_rates();
    $prices = $this->registration->get_activity_prices();

    //Get addon prices
    $addonPrices = $this->paymentm->get_addon_prices();
    $discountDetails = $this->paymentm->calculate_cart_discounts($prices);

    $registrationData = $this->paymentm->get_activity_total_costs($prices, $discountDetails, $addonPrices);

    $totalTransactionData = $registrationData['summary'];
    $activityDataByParticipant = $registrationData['participants'];

    //Get post, XSS filter.
    $post = $this->input->post(NULL, TRUE);
    $exactInformation = $post;
    $exactInformation['invoice'] = $_SESSION['cartID'];
    $exactInformation['total'] = $totalTransactionData['finalTotal'];

    if($exactInformation['total'] > 0)
    {
      $this->payment->set_gateway("exact"); 
      $this->payment->set_currency('CA');   
      $response = $this->payment->process($exactInformation);      
    }
    else
    {
      $response['status'] = TRUE;
    }

    if($response['status'] === TRUE)
    {
      $this->db->beginTransaction();
      try
      {
        $this->registrationm->set_transaction_status();
        $this->registrationm->set_registration_transaction_type($_SESSION['type']);
        $this->registrationm->set_account_personal_rel_type();
        $this->registrationm->set_addon_transaction_type();
        $this->registrationm->set_donation_transaction_type();
        $this->registrationm->set_account_transaction_type($_SESSION['type']);

        //If we're dealing with a transfer, deactivate the previous user's event/activity
        if($_SESSION['type'] == "transfer")
        {
          $this->registrationm->deactivate_transfer($_SESSION['transfer_code']);
        }
        
        if($_SESSION['type'] == 'transfer')
        {
          $this->registrationm->insert_transaction_transfer($totalTransactionData, TRUE);
        }
        else
        {
          $this->registrationm->insert_transaction($totalTransactionData, TRUE);
        }

        foreach($_SESSION['registration'] as $id => $participant)
        {
          if($id != "active_user" && $id != "last_modified")
          {
            if($_SESSION['type'] == "group")
            {
              $this->registrationm->insert_account_group($id, TRUE);
            }
            else
            {
              $this->registrationm->insert_account($id, TRUE);
            }

            if($_SESSION['type'] == "group")
            {
              $this->registrationm->insert_personal_group($id, TRUE);
              $this->registrationm->insert_account_personal_relation_group($id);
            }
            else
            {
              $this->registrationm->insert_personal($id, TRUE);
              $this->registrationm->insert_account_personal_relation($id);
            }

            $this->registrationm->insert_account_transaction();

            $transactionRelationData = array(
              "registrationSubTotal" => number_format($activityDataByParticipant[$id]['activityCostTotal'], 2, '.', ''),
              "registrationSubTotalTax" => number_format($activityDataByParticipant[$id]['taxTotal'], 2, '.', '')
              );

            if(isset($discountDetails['data'][$id]))
            {
              $transactionRelationData["discount"] = number_format($activityDataByParticipant[$id]['discount'], 2, '.', '');
            }
            else
            {
              if($_SESSION['type'] == "group")
              {
                if(isset($discountDetails['type']))
                {
                  if($discountDetails['type'] == 'group')
                  {
                    $transactionRelationData['discount'] = number_format($discountDetails['data']['discount'] * $participant['personal']['members'], 2, '.', '');
                  }
                }
                else
                {
                  $transactionRelationData["discount"] = 0;
                }
              }
              else
              {
                $transactionRelationData["discount"] = 0;
              }
            }


            if(isset($discountDetails['type']))
            {
              $transactionRelationData["discountType"] = $discountDetails['type'];
              $transactionRelationData['artezDiscount'] = 0;
              //Loop to see if we need to insert artez information
              $insertArtez = FALSE;
              $artezDiscountTotal = 0;

              if(isset($discountDetails['data'][$id]['artez']))
              {
                foreach($discountDetails['data'][$id]['artez'] as $eID => $artez)
                {
                  if(isset($artez['artez_discount']))
                    $insertArtez = TRUE;

                  $artezDiscountTotal += $artez['artez_details']['discount'];
                }                
              }

              if($insertArtez === TRUE)
              {
                $transactionRelationData['artezDiscount'] = $artezDiscountTotal;

                $this->load->library('artez');
                // $artezProfileDetails = array(
                //   'event_id' => $participant['personal']['event_id'],
                //   'first_name' => $participant['personal']['first_name'],
                //   'last_name' => $participant['personal']['last_name'],
                //   'invoice' => $participant['personal']['invoice'],
                //   'address1' => $participant['personal']['address1'],
                //   'address2' => '',
                //   'address3' => '',
                //   'language_preference' => $participant['personal']['language_preference'],
                //   'city' => $participant['personal']['city'],
                //   'province' => $participant['personal']['province'],
                //   'country' => $participant['personal']['country'],
                //   'postal_code' => $participant['personal']['postal_code'],
                //   'home_phone' => $participant['personal']['home_phone'],
                //   'email' => $participant['personal']['email'],
                //   'charity_id' => json_encode(utf8_encode()),
                //   'registration_type' => 'Individual Registration',
                //   'artez_username' => $participant['personal']['username'],
                //   'artez_password' => $participant['personal']['password']
                //   );

                $this->registrationm->insert_artez_account_relation($id, $discountDetails['data'][$id]);
              }

              if($discountDetails['type'] == "code")
              {
                $transactionRelationData["discountCodeID"] = $discountDetails['data'][$id]['discountID'];
              }    
              else
              {
                $transactionRelationData['discountCodeID'] = NULL;
              }
            }
            else
            {
              $transactionRelationData['artezDiscount'] = 0;
              $transactionRelationData['discountCodeID'] = NULL;
              $transactionRelationData["discountType"] = NULL;
            }

            if($_SESSION['type'] == "transfer")
            {
              $transferEolFee = 5;
              $transferEolFeeTax = $transferEolFee * EOL_FEE_TAX;
              $transferEolFee -= $transferEolFeeTax;
              $transferDutFee = $totalTransactionData['activityCostTotal'];
              $thisEventID = reset(explode("_", $participant['activities'][0]));
              $transferDutFeeTax = $totalTransactionData['activityCostTotal'] * $taxRates[$thisEventID]['registration'];
              $transferDutFee -= $transferDutFeeTax;
              $dutTransactionRelationData = array(
                "dutFee" => number_format($transferDutFee, 2, '.', ''),
                "dutFeeTax" => number_format($transferDutFeeTax, 2, '.', ''),
                "eolFee" => number_format($transferEolFee, 2, '.', ''),
                "eolFeeTax" => number_format($transferEolFeeTax, 2, '.', ''),
                "total" => number_format($totalTransactionData['activityCostTotal'] + $transferEolFee + $transferEolFeeTax, 2, '.', '')
                );
              $this->registrationm->insert_dut_transaction_relation($id, $dutTransactionRelationData);
            }
            else
            {
              $this->registrationm->insert_registration_transaction_relation($id, $transactionRelationData);
            }

            if(isset($participant['activities']))
            {
              $this->registrationm->insert_activities($id, $prices);

              if($_SESSION['type'] == "group")
              {
                $this->registrationm->insert_finishtime_group($id);
                $this->registrationm->insert_participant_shirt_group($id); 
              }
              else
              {
                $this->registrationm->insert_finishtime($id);
                $this->registrationm->insert_participant_shirt($id);                
              }
            }
            
            if($_SESSION['type'] == "group")
            {
              $groupAddons = FALSE;
              foreach($participant['group_details'] as $memberDetails)
              {
                if(isset($memberDetails['addon']))
                {
                  $groupAddons = TRUE;
                  break;
                }
              }
              
              if($groupAddons)
              {
                $this->registrationm->insert_addons_group($id, $addonPrices);
              }
            }
            else
            {
              if(isset($participant['addons']))
              {
                if(!empty($participant['addons']))
                  $this->registrationm->insert_addons($id, $addonPrices);
              }
            }

            if(isset($participant['aka']))
            {
              $this->load->library('aka');

              foreach($participant['aka'] as $eventID => $akaDetails)
              {
                $fieldsForAka = array(
                  'first_name' => $participant['personal']['first_name'],
                  'last_name' => $participant['personal']['last_name'],
                  'email' => $participant['personal']['email'],
                  'home_phone' => $participant['personal']['home_phone'],
                  'address' => $participant['personal']['address'],
                  'country' => $participant['personal']['country'],
                  'city' => $participant['personal']['city'],
                  'postal_code' => $participant['personal']['postal_code'],
                  'gender' => $participant['personal']['gender'],
                  'aka_team_name' => $akaDetails['aka_team_name'],
                  'aka_fundraising_option' => $akaDetails['aka_fundraising_option']
                  );

                $this->aka->create_profile($fieldsForAka);
              }
              $this->registrationm->insert_aka($id);
            }

            if(isset($participant['seriesfoundation']))
            {
              $this->registrationm->insert_seriesdonation($id);
            }

            if(isset($participant['charities']))
            {
              $donationMade = FALSE;
              $profileCreated = FALSE;
              //Loop to find if we need to do anything
              foreach($participant['charities'] as $eventID => $fields)
              {
                if(isset($fields['donation']))
                  $donationMade = TRUE;
                if(isset($fields['fundraising_profile']))
                {
                  if($fields['fundraising_profile'] == "Yes")
                    $profileCreated = TRUE;
                }
              }
              //This will parse and add all donations (for each event)
              if($donationMade === TRUE)
                $this->registrationm->insert_donations($id);

              //This will parse and add all pledge profiles (for each event)
              if($profileCreated === TRUE)
                $this->registrationm->insert_fundraising_profiles($id, TRUE);
            }

            if($_SESSION['type'] == "group")
            {
              $this->registrationm->insert_teams_group($id);
            }
            else
            {
              if(isset($participant['teams']))
              {
                $this->registrationm->insert_teams($id);
              }              
            }

            if(isset($participant['eventextras']))
            {
              //This includes 2 inserts, the extras insert and the account-extra relation
              $this->registrationm->insert_extras($id, TRUE);
            }
          }
        }

        $this->db->commit();
        // $this->db->rollback();
        // exit;
        
        $this->load->library("confirmationEmail");

        if($_SESSION['type'] == "transfer")
        {
          foreach($this->registrationm->insertIDs['all-personal-id'] as $personalID)
          {
            $this->confirmationemail->sendConfirmationEmail("transferActivity", $personalID);
          } 
        }
        else if ($_SESSION['type'] == "group")
        {
          for($i = 0; $i < $this->registrationm->personalInsertsCount; $i++)
          {
            if($i == 0)
            {
              $this->confirmationemail->sendConfirmationEmail("cartBillingMember", $this->registrationm->insertIDs['personal-group'] + $i);
            }
            else
            {
              $this->confirmationemail->sendConfirmationEmail("cartNonBillingMember", $this->registrationm->insertIDs['personal-group'] + $i);
            }
          }
        }
        else
        {
          foreach($this->registrationm->insertIDs['all-personal-id'] as $personalID)
          {
            if($this->registrationm->billingMemberPersonalID === $personalID)
             $this->confirmationemail->sendConfirmationEmail("cartBillingMember", $personalID);
            else            
             $this->confirmationemail->sendConfirmationEmail("cartNonBillingMember", $personalID);
          } 
        }
        
        unset($_SESSION['registration']);
        unset($_SESSION['cartID']);
        unset($_SESSION['cart-state']);
        unset($_SESSION['type']);
      }
      catch(Exception $e)
      {
        $this->db->rollback();
      }
    }
    else
    {
      //TODO: Process failed response
    }

    $data = array(
      "response" => $response
      );
    $this->template->write_view("_content", "receipt", $data);
    $this->template->render();
  }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */