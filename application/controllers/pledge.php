<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pledge extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();

    //Destroy any possible conflicts with registration
    unset($_SESSION['registration']);

    if(!isset($_SESSION['pledge']))
    {
      redirect('register');
    }

		//Add the custom includes (css/js)
		$this->template->write_view("_includes", "includes");

		//Load banner into view
		$this->template->write_view("_banner", "banner");
		//Load navigation into view
		$this->template->write_view("_navigation", "navigation");
		
		//Load footer into view
		$this->template->write_view("_footer", "footer");
	}

  public function profile($id)
  {
    $this->load->model("pledge_model", 'pledge');

    $pledgeProfileDetails = $this->pledge->get_profile_details($id);
    $pledgesForProfile = $this->pledge->get_profile_pledges($id);
    
    $data = array(
      "pledgeProfileDetails" => $pledgeProfileDetails,
      "pledgesForProfile" => $pledgesForProfile
      );

    $this->template->write_view('_content', 'pledge/profile', $data);
    $this->template->render();
  }

	public function athlete($id)
	{
		if(isset($_SESSION['registration']))
		{
			unset($_SESSION['registration']);
		}

		$this->load->model('pledge_model', 'pledge');
		$details = $this->pledge->get_profile_details($id);
		$form = $this->pledge->get_form_sections();

		$_SESSION['pledge']['athleteID'] = $details['profileID'];
		$_SESSION['pledge']['athleteName'] = $details['firstName']. ' '.$details['lastName'];
		$_SESSION['pledge']['athleteEventID'] = $details['eventID'];
		$_SESSION['pledge']['athleteCharityID'] = $details['charityID'];

		$data = array(
			"profile" => $details,
			'sections' => $form
			);
		$this->template->write_view("_content", "pledge/index", $data);
		$this->template->render();
	}

	public function confirm()
	{

		$post = $this->input->post(NULL, TRUE);

		if($post)
		{
			foreach($post as $field => $value)
			{
				if(stripos($field, "pledge_") === 0)
				{
					$newField = str_replace("pledge_", "", $field);
					$_SESSION['pledge'][$newField] = $value;
				}
			}
			$this->load->library("ecart");
			$this->ecart->save_cart("pledge");
		}

		$this->template->write_view("_content", "pledge/confirmation");
		$this->template->render();
	}

	public function payment()
	{
    $this->load->model("payment_model", "paymentm");
    $paymentForm = $this->paymentm->get_form_sections();

    $paymentForm['action'] = base_url('pledge/process');

    $paymentSummary = array(
    	"Donation Summary" => array(
    		"Athlete Pledges" => array(
    			"Pledge to ".$_SESSION['pledge']['athleteName'] => array($_SESSION['pledge']['donation'])
    			)
    		),
    	"Summary" => array(
    		"Total" => array(
    				'' => array($_SESSION['pledge']['donation'])
    		)
    	)
    	);

    $data = array(
      "paymentForm" => $paymentForm,
      "summary" => $paymentSummary
    	);


		$this->template->write_view('_content', 'pledge/payment', $data);
		$this->template->render();
	}

	public function process()
	{
    $this->load->library("payment");		
    $this->load->model("pledge_model", "pledge");
    $this->payment->set_gateway("exact");    
    $this->payment->set_currency('CA');

    $exactInformation = $this->input->post();
    $exactInformation['total'] = number_format($_SESSION['pledge']['donation'], 2, '.', '');
    $exactInformation['invoice'] = $_SESSION['cartID'];

    $response = $this->payment->process($exactInformation);

    $this->db->beginTransaction();

    try
    {
	    $this->pledge->insert_transaction();
	    $this->pledge->insert_account();
      $this->pledge->insert_personal_informatino();
      $this->pledge->insert_account_personal_relation();
	    $this->pledge->insert_donation_transaction();
	    $this->pledge->insert_account_transaction();

	    $this->db->commit();

      $this->load->library("confirmationEmail");
      $this->confirmationemail->sendConfirmationEmail("oneTimePledge", $this->pledge->personalInformationID);

      unset($_SESSION['pledge']);
    }
    catch(Exception $e)
    {
	    $this->db->rollback();
    }

    $data = array(
      "response" => $response
      );
    $this->template->write_view("_content", "receipt", $data);
    $this->template->render();
	}
}