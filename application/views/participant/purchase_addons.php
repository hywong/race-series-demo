<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <div class="row">
        <table class="table table-striped" id="addon-cart-summary">
          <thead>
            <tr>
              <th colspan="4" class="text-center"><h3><?php echo translate("My Cart"); ?></h3></th>
            </tr>
            <tr>
              <th width="20%"><?php echo translate("K1018"); ?></th><th width="20%"><?php echo translate("Item"); ?></th><th width="20%"><?php echo translate("Price"); ?></th><th width="20%"><?php echo translate("Quantity"); ?></th><th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php
			
			$addonParsed = array();
			
            foreach($addons as $eventID => $addonList)
            {
              foreach($addonList as $addon)
              {
                $addonParsed[$addon['addonID']]['name'] = $addon['addon'];
                $addonParsed[$addon['addonID']]['price'] = $addon['price'];
              }
            }
            if(isset($_SESSION['participant']['cart']['addons']))
            {
              foreach($_SESSION['participant']['cart']['addons'] as $eID => $addonByActivity)
              {
				if ($eID == $_SESSION['participant']['participantEventID']) {
					foreach($addonByActivity as $addonID => $addonList)
					{
						if ($addonID != "cartID" && $addonID != "total") {
							$c = count($addonList);
							foreach($addonList as $k => $addonDetails) {
								if (!empty($addonParsed)) {
									echo "<tr id=\"".uniqid()."\"><td>".$_SESSION['general'][$eID]['display']."</td><td>".$addonDetails['name']."</td><td>".$addonDetails['price']."</td><td>".$addonDetails['qty']."</td><td><button data-cart-addon-eid=\"".$eID."\" data-cart-addon-aid=\"".$addonID."\" data-cart-addon-id=\"".$k."\" class=\"remove-addon btn btn-xs btn-danger\">Remove</button></td></tr>";
								}
							}
						}
					}
				}
              }
            }
            ?>
          </tbody>
        </table>
      </div>
      <div class="row">
        <?php
		
		if (empty($addons)) {
			echo '<div class="row">
				<div class="col-sm-offset-3 col-sm-6">';
			echo translate("There are no available addons for this event");
			echo '</div></div>';
		}
		else {
			/*
			foreach($addons as $eventID => $addonList)
			{
			  echo "<h3>".$_SESSION['general'][$eventID]['display']." Addons</h3><hr />";
			  foreach($addonList as $addon)
			  {
				echo "
				<div class=\"col-sm-4\">
				  <div class=\"well\">
					<h4>".$addon['title']."</h4>
					Item: ".$addon['addon']." (".$addon['price']."$) <br /><p>".
					$addon['description']."</p>
					<p>
					<form class=\"addon-details\">
					";
					if(isset($addon['fields']))
					{
					  foreach($addon['fields'] as $field)
					  {
						formit_build_field($field, NULL, NULL, NULL);
					  }
					}
				echo "
					 </form>
					<button data-event-id=\"".$addon['eventID']."\" data-addon-id=\"".$addon['addonID']."\" type=\"button\" class=\"add-addon btn btn-success btn-sm\">Add to cart</button>
					</div>
				</div>";
			  }
			  echo "<div class=\"clearfix\"></div><br />";
			}
			*/
			foreach($addons as $eventID => $addonList)
			{
			  echo "<h3>".$_SESSION['general'][$eventID]['display']." Addons</h3><hr />";
			  $c = 0;
			  foreach($addonList as $addon)
			  {
				if($c == 0)
				{
				  echo "<div class=\"row\">";
				}
				echo "
				<div class=\"col-sm-4\">
				  <div class=\"well\">
					<h4>".$addon['title']."</h4>
					Item: ".$addon['addon']." (".$addon['price']."$) <br /><p>";
					$addonImg = base_url('assets/img/addons/'.$eventID.'_'.$addon['addonID'].'.png');
					if(@getimagesize($addonImg))
					{
					  echo "<img src=\"".$addonImg."\" alt=\"".$addon['addon']."\" title=\"".$addon['addon']."\" class=\"img-responsive\" />";
					}
				echo $addon['description']."</p>
					<p>
					<form class=\"addon-details\">
					";
					if(isset($addon['fields']))
					{
					  foreach($addon['fields'] as $field)
					  {
						formit_build_field($field, NULL, NULL, NULL);
					  }
					}
				echo "
					 </form>
					<button data-event-id=\"".$addon['eventID']."\" data-addon-id=\"".$addon['addonID']."\" type=\"button\" class=\"add-addon btn btn-success btn-sm\">" . translate("Add to cart") . "</button>
					</div>
				</div>";
				$c++;
				if($c == 3)
				{
				  $c = 0;
				  echo "</div>";
				}
			  }
			  if($c > 0 && $c < 3)
			  {
				echo "</div>";
			  }
			  echo "<div class=\"clearfix\"></div><br />";
			}
		}
        ?>
      </div>
		<?php
			if (!empty($addons)) {
		?>
      <div class="row">
        <div class="col-sm-offset-3 col-sm-6">
          <a href="purchase_addons?action=process" class="btn btn-block btn-success">Submit</a>
        </div>
      </div>
		<?php
		}
		?>
    </div>
  </div>
</div>
<script>
$(function(){
  $("body").on('click', '.add-addon', function(){	
    $(this).siblings('.addon-details').find(':input').each(function(){
      if($(this).val() == "" || $(this).val() == 0)
      {
        alert("You must select options on your addon.");
        return false;
      }
    });
	//alert($(this).data("addon-id"));
    $.post("<?php echo base_url("ajax/cart_add_addon_dashboard"); ?>", {addonID: $(this).data("addon-id"), eventID: $(this).data("event-id"), details: $(this).siblings('.addon-details').serializeObject()}, function(response){
	//alert(response);
		if (response == "addonAlreadySelected") {
			alert("<?php echo translate("This item has already been added to the cart.  To change the quantity, please remove the item and select a different quantity amount (if applicable)."); ?>");
		}
		else if (response == "limitReached") {
			alert("<?php echo translate("You've reached the limit of this item per participant."); ?>");
		}
		else if (response == "capReached") {
			alert("<?php echo translate("The cap for this item has been reached."); ?>");
		}
		else {
			$('table > tbody').append(response);
		}
		/*
      if(response != false)
      {
        $('table > tbody').append(response);
      }
      else
      {
        alert("");
      }
	  */
    });
    $(this).siblings('.addon-details').trigger("reset");
  });

  $("#addon-cart-summary").on('click', '.remove-addon', function(){
    $.post('<?php echo base_url("ajax/cart_remove_addon_dashboard"); ?>', {rowID: $(this).parent().parent().attr("id"), cartAddonPos: $(this).data("cart-addon-id"), cartAddonID: $(this).data("cart-addon-aid"), cartAddonEvent: $(this).data("cart-addon-eid")}, function(response){
		//alert(response);
      if(response != false)
      {
        $('#'+response).remove();
      }
    });
  });
});
</script>