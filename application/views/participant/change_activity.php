<script type='text/javascript'>
	$(document).ready(function () {
		$("#frmChangeRace").validate();
		
		
		$("#new_activity").change(function() {
			var aUrl = "<?php echo base_url("ajax/generateShirtDropdown"); ?>";
			var activityID = $("#new_activity").val();
			
			$.ajax({
				type: "POST",
				url: aUrl,
				data: { activityID: activityID },
				async: false,
				success: function (result) {
					//alert(result);	
					$("#divShirt").html(result);
					$("#new_shirt").rules('add', {required: true});
				}
			});
		});
	});
</script>
<br /><br />
<div id="wrapper-participantdash">	
	<div id="mainContainer" class="col-sm-10 col-sm-offset-1 col-xs-12">
		<div class="col-sm-8 col-sm-offset-2">
		<div class="alert alert-info">
		<?php echo translate("When changing races, a flat fee is charged, as well as any prices differences. No refunds will be granted, and all prices are subject to applicable taxes and processing fees."); ?>
		</div>
			<div class="table-responsive">
				<table class="table table-striped common-table">
					<thead>
					<tr>
						<th colspan='2'>&nbsp;</th>
					</tr>
					<tr>
						<th style="width:50%"><?php echo translate("K705"); ?></th>
						<th style="width:50%"><?php echo translate("K706"); ?></th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td><?php echo translate("K707"); ?></td>
						<td>$<?php echo number_format(UPGRADE_FEE, 2, '.', '') . " + " . translate("Price-Difference"); ?></td>
					</tr>
					<tr>
						<td><?php echo translate("K708"); ?></td>
						<td>$<?php echo number_format(DOWNGRADE_FEE, 2, '.', ''); ?></td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
		<?php 
		
		if (!empty($upgradeList)) { 
		
		?>
			<!-- Display Upgrades -->
			<div class="col-sm-8 col-sm-offset-2">
				<div class="table-responsive">
					<table class="table table-striped common-table">
						<thead>
						<tr>
							<th colspan='2'><?php echo translate("K709"); ?></th>
						</tr>
						<tr>
							<th style='width:50%'><?php echo translate("K710"); ?></th>
							<th style="width:50%"><?php echo translate("K711"); ?></th>
						</tr>
						</thead>
						<tbody>
						<?php 
							foreach ($upgradeList as $upgradeID => $upgradeDetails) {
		echo "
								<tr>
									<td>" . $upgradeDetails['activity'] . "</td>
									<td>$" . number_format($upgradeDetails['priceDifferenceFee'], 2, '.', '') . " +
										$" . number_format(UPGRADE_FEE, 2, '.', '') . " Fee
									</td>
								</tr>";
							
							}
						?>				
						</tbody>
					</table>
				</div>
			</div>
		<?php } ?>

		<?php 
		
		if (!empty($downgradeList)) {
			
		?>
			<div class="col-sm-8 col-sm-offset-2">
				<div class="table-responsive">
					<table class="table table-striped common-table">
					<thead>
					<tr>
						<th colspan='2'><?php echo translate("K712"); ?></th>
					</tr>
					<tr>
						<th style="width:50%"><?php echo translate("K713"); ?></th>
						<th style="width:50%"><?php echo translate("K714"); ?></th>
					</tr>
					</thead>
					<tbody>
					<?php
						foreach ($downgradeList as $downgradeID => $downgradeDetails) {
		echo "
							<tr>
								<td>" . $downgradeDetails['activity'] . "</td>
								<td>$" . number_format(DOWNGRADE_FEE, 2, '.', '') . " Fee
								</td>
							</tr>";
						
						}
					?>
					
					</tbody>
				</table>
				</div>
			</div>
		<?php
		} 
		?>

		<div class="col-sm-8 col-sm-offset-2">		
		<hr />
		<form method='post' id="frmChangeRace" role="form" name="frmChangeRace" action='<?php echo base_url("participant/change_activity?action=process"); ?>'>
			<div class="form-group">
				<i><b><?php echo translate("My current race is") . ": " . $currentActivity; ?></b></i>
			</div>
		
		
			<div class="form-group">
				<label for="new_activity"><?php echo translate("K715"); ?></label>
			<select id='new_activity' name='new_activity' class="form-control required">
				<option value=""><?php echo translate("Select"); ?></option>
				<?php
				foreach ($activityList as $aNumber => $activityDetails) {
					echo "<option value='" . $activityDetails['intActivityID'] . "'>" . $activityDetails['strActivity'] . "</option>";
				}
				?>
			</select>
			</div>
			<div id="divShirt" class="form-group"></div>
			<button type="submit" id="btnSubmit" class="btn btn-success btn-block"><span class="glyphicon glyphicon-ok"></span> <?php echo translate("K716"); ?></button>
		</form>
		</div>
		<div class="clearfix"></div>
		<br />	
		<a href="<?php echo base_url("participant/"); ?>" class="btn btn-default"><span class="glyphicon glyphicon-circle-arrow-left"></span> <?php echo translate("K85"); ?></a>
	</div>
</div>