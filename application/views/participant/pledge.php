<script language="javascript">
	function imposeMaxLength(Event, Object, MaxLen) {
		return (Object.value.length <= MaxLen) || (Event.keyCode == 8 || Event.keyCode == 46 || (Event.keyCode >= 35 && Event.keyCode <= 40))
	}
	
	$(document).ready(function () {
		$("#myform").validate();
		
		$('#pledge_message').bind('keypress', function (event) {
			//var regex = new RegExp("^[a-zA-Z0-9~!@#^*()-_=+\b\t]+$");
			var regex = new RegExp("^[%\"]+$");
			var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
			if (regex.test(key)) {
			   event.preventDefault();
			   return false;
			}
		});
	});
</script>
<br /><br />
<div id="wrapper-participantdash">	
	<div id="mainContainer" class="col-sm-10 col-sm-offset-1 col-xs-12">
		<script>
			var thisPage = "donation";
		</script>
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<div class="alert alert-info text-center">
					<h4><?php echo translate("K891"); ?> <?php echo $pledgeProfileDetails['strFirstName'] . " " . $pledgeProfileDetails['strLastName']; ?></h4>
				</div>			
			</div>
		</div>
		<form action="pledge_payment" class="form-horizontal" method="post" id="myform" name="myform">
			<div class="form-group">
				<label class="control-label col-sm-3" for="first_name">
					<span class="req">*</span><?php echo translate("K22"); ?>
				</label>
				<div class="col-sm-8">
					<input type="text" name="first_name" id="first_name" class="form-control required" />
				</div>
			</div>
			<div class="form-group">
				<label for="last_name" class="control-label col-sm-3">
					<span class="req">*</span><?php echo translate("K23"); ?>
				</label>
				<div class="col-sm-8">
					<input type="text" name="last_name" id="last_name" class="form-control required"/><span><?php $errormsg ?></span>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3">
					<span class="req">*</span><?php echo translate("K24"); ?>
				</label>
				<div class="col-sm-8">
					<input type="text" name="address" id="address" class="form-control required"/>
				</div>
			</div>
			<div class="form-group">
				<label for="city" class="control-label col-sm-3">
					<span class="req">*</span><?php echo translate("K25"); ?>
				</label>
				<div class="col-sm-8">
					<input type="text" name="city" id="city" class="form-control required"/>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3" for="province">
					<span class="req">*</span><?php echo translate("K26"); ?>
				</label>
				<div class="col-sm-8">
					<?php
					echo '
					<select name="province" id="province" class="form-control required">
						<option></option>
						<optgroup id="canada" label="Canada">
							<option value="asdf">asdf</option>';
				/*
				foreach ($provinces as $k => $province) {
				if ($saved_details['province'] == $province) {
				echo "<option selected=\"selected\" value=\"" . $province . "\">" . ucwords($province) . "</option>";
				} else {
				echo "<option value=\"" . $province . "\">" . ucwords($province) . "</option>";
				}
				}
				echo '
						</optgroup>
						<optgroup id="international" label="International">
							<option value="international">International</option>
						</optgroup>
						<optgroup id="us" label="United States">';
				foreach ($states as $k => $state) {
				if ($saved_details['province'] == $state) {
				echo "<option selected=\"selected\" value=\"" . $state . "\">" . ucwords($state) . "</option>";
				} else {
				echo "<option value=\"" . $state . "\">" . ucwords($state) . "</option>";
				}
				}
				*/
				echo '
						</optgroup>
					</select>';	
					
					?>
				</div>
			</div>
			<div class="form-group">
				<label for="country" class="control-label col-sm-3">
					<span class="req">*</span><?php echo translate("K27"); ?>
				</label>
				<div class="col-sm-8">
					<?php //displayCountryDropdown(); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="postal_code" class="control-label col-sm-3">
					<span class="req">*</span><?php echo translate("K28"); ?>
				</label>
				<div class="col-sm-8">
					<input type="text" name="postal_code" id="postal_code" class="form-control required"/>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3" for="day_phone">
					<span class="req">*</span><?php echo translate("K29"); ?>
				</label>
				<div class="col-sm-8">
					<input type="text" name="day_phone" id="day_phone" size="30" maxlength="30" class="form-control required"/>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3" for="email">
					<span class="req">*</span><?php echo translate("K111"); ?>			
				</label>
				<div class="col-sm-8">
					<input type="text" name="email" id="email" class="form-control required"/>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3" for="donation">
					<span class="req">*</span><?php echo translate("K819"); ?>
				</label>
				<div class="col-sm-8">
					<div class="input-group">
						<span class="input-group-addon">$</span>
						<select name="donation" id="donation" class="form-control">
							<?php
							//10-95
							for ($i = 10; $i <= 95; $i += 5) {
								echo "<option>" . $i . "</option>";
							}
							//100-500
							for ($i = 100; $i <= 500; $i += 50) {
								echo "<option>" . $i . "</option>";
							}
							//500-1000
							for ($i = 600; $i <= 1000; $i += 100) {
								echo "<option>" . $i . "</option>";
							}
							?>
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3" for="pledge_message">
					<?php echo translate("K1037"); ?>:<br><?php echo translate("K868"); ?>
				</label>
				<div class="col-sm-8">
					<textarea id="pledge_message" name="pledge_message" class="form-control required" rows="5" cols="35"
							  onKeyPress="return imposeMaxLength(event, this, 50);"></textarea>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6 col-sm-offset-3">
					<button class='btn btn-default btn-block'><?php echo translate("K265"); ?></button>
				</div>
			</div>
		</form>
	</div>
</div>