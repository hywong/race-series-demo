<br /><br />
    <div id="mainContainer" class="col-xs-12">
		<div class="col-sm-6 col-sm-offset-3">			
			<div class="well">
				<h4><?php echo translate("Reset Password"); ?></h4>
				<form method="post" action="<?php echo base_url("participant/retrieve"); ?>">
					<?php 

					if (isset($msgDanger)) {
						echo "<div class=\"alert alert-danger\">" . translate($msgDanger) . ".</div>";
					}
					else if (isset($msgSuccess)) {
						echo "<div class=\"alert alert-success\">" . translate($msgSuccess) . ".</div>";
					}
					
					?>
					<div class="form-group">
						<label for="username"><?php echo translate("Username"); ?>: </label>
						<input type="text" class="form-control" id="username" name="username" size="30" maxlength="50"/>
					</div>
					<button class='btn btn-default btn-block' type="submit" value="Login"><?php echo translate("Reset Password"); ?></button>
				</form>
				<br />
				<div class="row">
					<div class="col-xs-12">
						<a href="<?php echo base_url("participant/login"); ?>" class="btn btn-default"><span class="glyphicon glyphicon-circle-arrow-left"></span> <?php echo translate("Back to Login"); ?></a>
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-xs-12">
						<a href="<?php echo base_url("/"); ?>" class="btn btn-default"><span class="glyphicon glyphicon-circle-arrow-left"></span> <?php echo translate("K223"); ?></a>
					</div>
				</div>
			</div>
		</div>
    </div>
