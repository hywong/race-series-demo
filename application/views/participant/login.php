<?php

	displayJsNoScript();
	
?>
<script>
	$(document).ready(function () {
		$("#mainContainer").show();
	});
</script>
<br /><br />
<div id="mainContainer" class="col-xs-12" style="display:none">
	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<div class="well">
				<h3><?php echo translate("K1039"); ?></h3>
				<form method="post" role="form" action="<?php echo base_url("participant/login"); ?>">
					<?php 
	
					if (isset($msgDanger)) {
						echo "<div class=\"alert alert-danger\">" . translate($msgDanger) . "</div>";
					}
					else if (isset($msgSuccess)) {
						echo "<div class=\"alert alert-success\">" . translate($msgSuccess) . "</div>";
					}
					
					?>
					<div class="form-group">
						<label for="usernameTxt"><?php echo translate("K912"); ?></label>
						<input type="text" class="form-control" name="username" id="usernameTxt" size="30" maxlength="50"/>
					</div>
					<div class="form-group">
						<label><?php echo translate("K308"); ?></label>
						<input type="password" class="form-control" name="password" size="30" maxlength="50"/>
					</div>
					<div class="form-group">
						<button class='btn btn-success btn-block' type="submit" value="Login"><span class="glyphicon glyphicon-log-in"></span> <?php echo translate("K882"); ?></button>
					</div>
				</form>		
				<a href='<?php echo base_url("participant/retrieve"); ?>' class="btn btn-info btn-block">
					<span class="glyphicon glyphicon-question-sign"></span> 
					<?php echo translate("Forgot my password"); ?>
				</a>
				
				<br />
				<div class="row">
				<a href='<?php echo base_url("/"); ?>' class='btn btn-default'>
					<span class="glyphicon glyphicon-circle-arrow-left"></span> 
					<?php echo translate("K748"); ?>
				</a>
				</div>
			</div>
		</div>
	</div>
</div>
