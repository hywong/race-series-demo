<script>
	$(document).ready(function () {
		$("#myform").validate();
	});
</script>
<br /><br />
<div id="wrapper-participantdash">	
	<div id="mainContainer" class="col-sm-8 col-sm-offset-2 col-xs-12">
		<div class="row">
			<div class="col-xs-12">
				<div class="well well-sm text-center">
					<h4><?php echo translate("K238"); ?></h4>
				</div>
			</div>
		</div>
		<br />
		<form id="myform" name="myform" method="post" action="<?php echo base_url("participant/edit_details"); ?>">
			<table class="table table-striped table-hover common-table">
			<?php
			$swapButton = translate("K754");
			
			if ($_SESSION['participant']['participantType'] == "individualBillingMember" || $_SESSION['participant']['participantType'] == "individual") {			
		echo '			
				<tr>
					<td><strong>' . translate("Participant ID") . '</td>
					<td><strong>' . $pid . '</td>
					<td><strong>' . translate("K241") . '</td>
				</tr>';
			
				//From general_helper.php
				drawEditForm($participantInfo);
				
				echo $drawIndividualShirtSection;
			
			}
			/*
			else if (strtolower($_SESSION['participant']['participantType']) == "groupbillingmember") {		
				$swapButton = translate("Swap First/Last Name of Billing Member");
			
				//From general_helper.php
				drawEditForm($groupInfo);
				
		echo '
				<tr>
					<td colspan="3">
						<input type="hidden" name="memberCount" id="memberCount" value="' . $numOfGroupMembers . '">
					</td>
				</tr>';		
				
			}
			*/
		echo '
				<tr>
                    <td colspan="3" align="center">
						<div class="col-sm-6 col-sm-offset-3">
							<a href="' . base_url("participant/edit_details?action=swap") . '" class="btn btn-success btn-block"><span class="glyphicon glyphicon-retweet"></span> ' . $swapButton . '</a>
						</div>
					</td>
                </tr>
				<tr>
					<td colspan="3" align="center">
						<div class="col-sm-6 col-sm-offset-3">
							<button class="btn btn-block btn-success">' . translate("K242") . '</button>
						</div>
					</td>
				</tr>
			</table>';
			
			?>
		</form>
		<a href="<?php echo base_url("participant/"); ?>" class="btn btn-default"><span class="glyphicon glyphicon-circle-arrow-left"></span> <?php echo translate("K85"); ?></a>
	</div>
</div>
