<!--
<div id='payment-processing'>
    
			<h4>Processing Payment...</h4>
			<p>
				Payment is being processed<br />    This process can't be interrupted.<br/>Stand by while awaiting a response<br/><br/><img
        src='../core_support_files_v2/images/loading_large.gif' title='Processing'
        alt='Processing Loader'/><br/><br/>This may take a few moments due to high traffic</p>
</div>

<div id='cart-edit-form-shadow'></div>
<div id='payment-processing-shadow'></div>
-->
<?php
	//echo "<pre>"; print_r($_SESSION['participant']); echo "</pre>";
?>
<script>
	$(document).ready(function() {
		$("#fCheckout").validate();
	});
</script>
<br /><br />
<div id="mainContainer" class="col-sm-10 col-sm-offset-1 col-xs-12">	
	<center><img src="../assets/img/eol_logo_sm.png"/></center>
	<center>
		<form method="POST" action="<?php echo base_url("participant/transaction_process?action=" . $_GET['action']); ?>" name="fCheckout" id="fCheckout">
			<div class='individual_reg'>
				<br />
				<?php
				
				echo $billingInfoSection;
				
				echo $costTotalSection;
				
				?>
				
			</div> 
			<div class="row">
				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 text-center">
					<hr />
					<br />
					<?php
					
					echo $creditCardSection;
					
					?>
				</div>
			</div>
		</form>
	</center>
</div>