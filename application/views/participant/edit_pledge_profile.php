<script language="javascript">
    $(document).ready(function () {

        $('#pledgeForm').validate({
            rules: {
                profile_pledge_goal: {
                    number: true
                }
            }
        });

        $('#remLen2').html(300 - document.forms['myform'].profile_pledge_message.value.length);

    });

    function textCounter(field, cntfield, maxlimit) {
        if (field.value.length > maxlimit) // if too long...trim it!
            field.value = field.value.substring(0, maxlimit);
        // otherwise, update 'characters left' counter
        else
            $('#remLen2').html(maxlimit - field.value.length);
    }
</script>
<br /><br />
<div id="wrapper-participantdash">	
	<div id="mainContainer" class="col-sm-10 col-sm-offset-1 col-xs-12">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<div class="well">
					<form  id="pledgeForm" method="post" action="<?php echo base_url("participant/edit_pledge_profile"); ?>" name="myform">
						<input type="hidden" name="pid" id="pid" value="<?php echo $pledgeProfileDetails['intPledgeProfileID']; ?>">
						<div class="well-title">
							<h4><?php echo translate("K871"); ?></h4>
						</div>
						
							<h4><?php echo translate('K877') . ' ' . $pledgeProfileDetails['strFirstName'] . " " . $pledgeProfileDetails['strLastName']; ?></h4>
						<?php if (isset($msg)) {
							?>
							<BR>
							<center>
								<table border='0' width='95%'>
									<tr>
										<td>
											<div align="center">
												<?php echo $msg; ?>
											</div>
										</td>
									</tr>
								</TABLE>
							</center>
						<?php } ?>
						<div class="form-group">
							<label><?php echo translate("K794"); ?></label>
							
						</div>
						<div class="row">
							<div class="col-sm-6 text-center">
								<img class="border"
									 src="https://secure.eventsonline.ca/profile_uploads/pledge_profile/default/_load.php?id=<?php echo $pledgeProfileDetails['strPledgeProfilePicture'] . ".jpg"; ?>&blank=blank.jpg&<?php echo strtotime("now");?>"
									 width="200"/>
							</div>
							<div class="col-sm-6 text-center">
								<iframe
									src="https://secure.eventsonline.ca/profile_uploads/pledge_profile/default/_upload.php?filename=<?php echo $pledgeProfileDetails['strPledgeProfilePicture']; ?>&btnTitle=Profile"
									width="100%" height="260" frameBorder="0"></iframe>						
							</div>
						</div>
							
						<div class="form-group">
							<label for="profile_pledge_goal"><?php echo translate("K875"); ?></label>
							<div class="input-group">
								<span class="input-group-addon">$</span>
								<input type="text" class="form-control required" id= "profile_pledge_goal" name="profile_pledge_goal" value="<? echo $pledgeProfileDetails['dblPledgeGoal']; ?>" >
							</div>
						</div>
						<?php
						$pledgeProfileDetails['strPledgeMessage'] = str_replace('|', "\n", $pledgeProfileDetails['strPledgeMessage']);
						?>
						<div class="form-group">
							<label>
								<?php echo translate("K631"); ?> 
								&nbsp;<label class="badge" id="remLen2">300</label><br />
								<?php echo translate("K632"); ?>(Max:300)
							</label>
							<textarea id="pledgemsg" cols='50' rows='8' class="form-control" name="profile_pledge_message"
							  onKeyDown='textCounter(document.forms["myform"].profile_pledge_message,document.forms["myform"].remLen2,300)'
							  onKeyUp='textCounter(document.forms["myform"].profile_pledge_message,document.forms["myform"].remLen2,300)'><?php echo $pledgeProfileDetails['strPledgeMessage']; ?></textarea>
						</div>
							<div class="row">
								<div class="col-xs-12">
									<button class='btn btn-success btn-block' id="update"><?php echo translate("K876"); ?></button>
								</div>
							</div>
							<br />
							<div class="row">
								<div class="col-xs-12">
									<a href='<?php echo base_url("participant/view_pledge_profile"); ?>'
									   class='btn btn-default'>
									   <span class="glyphicon glyphicon-arrow-left"></span>
									   <?php echo translate("K796"); ?>
									</a>
								</div>
							</div>
							<br />
							<div class="row">
								<div class="col-xs-12">
									<a class="btn btn-default" href='<?php echo base_url("participant/"); ?>' class='eolbutton'>
									   <span class="glyphicon glyphicon-arrow-left"></span> 
									   <?php echo translate("K85"); ?></a>
								</div>
							</div>
							
							
						</center>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>