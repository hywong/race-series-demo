<script>
	$(document).ready(function () {
		$.tablesorter.defaults.widgets = ['zebra'];

		$("#teamtbl").tablesorter({
			stripingRowClass: ['even', 'odd'],
			stripeRowsOnStartUp: true,
		});
	});
</script>
<style>
	#teamtbl {
		width:700px;
	}
</style>
</head>
<body>
<div id="wrapper-participantdash">	
	<div id="eolcontent">
		<div id='eolmain'>
		<center>
			<form method="post" action="<?php echo base_url("participant/view_team?action=update"); ?>">
				<br/><br/>
				<table class='tablesorter' id='teamtbl'>
					<thead>
					<tr>
						<th><?php echo translate("K115"); ?></th>
						<th><?php echo translate("K116"); ?></th>
						<th><?php echo translate("K282"); ?></th>
					</tr>
					</thead>
					<tbody>
					<?php 
					$countTeamMembers = count($teamMembers);
					for ($i = 0; $i < $countTeamMembers; $i++) {
						echo "<tr><td>" . $teamMembers[$i]['strFirstName'] . " " . $teamMembers[$i]['strLastName'] . "</td><td>" . $teamMembers[$i]['strEmail'] . "</td><td>" . $teamMembers[$i]['strActivity'] . "</td></tr>";
					}
					?>
					</tbody>
				</table>
				<br/><br/>
			</form>

			<br/><br/>

			<div align="middle">
				<a href="<?php echo base_url("participant/"); ?>" class="btn btn-default"><span class="glyphicon glyphicon-circle-arrow-left"></span> <?php echo translate("K85"); ?></a>
			</div>
			<br/><br/>
		</center>
	</div>
</div>