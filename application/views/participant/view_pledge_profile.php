<script type="text/javascript">
    $(function () {
        $('#news-container').vTicker({
            speed: 600,
            pause: 2500,
            showItems: 2
        });
    });
</script>
<script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
<br /><br />
<div id="wrapper-participantdash">	
	<div id="mainContainer" class="col-sm-10 col-sm-offset-1 col-xs-12">
		<div class="col-sm-8">
			<div class="well">
				<div class="row">
					<div class="col-sm-5 text-center">
						<div class="row text-center">
						<!--
							<div class="col-xs-12">
								<a href="<?php echo base_url("participant/pledge?id=" . $pledgeProfileDetails['intPledgeProfileID']); ?>" class="btn btn-block btn-success">
									<span class="glyphicon glyphicon-heart"></span> <?php echo translate("Make a Pledge");?>
								</a>
							</div>
						-->
						</div>
						<!--<input type="hidden" name="rid" id="rid" value="<?php //echo $pledgeRandom; ?>">-->
						<h4><?php echo $pledgeProfileDetails['strFirstName'] . " " . $pledgeProfileDetails['strLastName']; ?></h4>
							<span class='st_sharethis_large' displayText='ShareThis'></span>
							<span class='st_facebook_large' displayText='Facebook'></span>
							<span class='st_twitter_large' displayText='Tweet'></span>
							<span class='st_linkedin_large' displayText='LinkedIn'></span>
							<span class='st_email_large' displayText='Email'></span>
						
							<div class="row">
							<img class="img-responsive" style="margin:auto;" src="https://secure.eventsonline.ca/profile_uploads/pledge_profile/default/_load.php?id=<?php echo $pledgeProfileDetails['strPledgeProfilePicture']; ?>.jpg&blank=blank.jpg&<?php echo strtotime("now");?>" />
							</div>
								
						<?php
						
						if (!isset($_GET['id'])) {
							?>
							<br />
						<div class="row">
							<div class="col-xs-12">
								<a href='<?php echo base_url("participant/edit_pledge_profile"); ?>' class='btn btn-default btn-block'><span class="glyphicon glyphicon-pencil"></span> <?php echo translate("K836"); ?></a>
							</div>
						</div>
						<?php } ?>
					</div>
					<div class="col-sm-7 text-left">
						<h4><?php echo translate("Charity"). ': ' . $pledgeProfileDetails['strCharity']; ?></h4><br />
						<h4><?php echo translate("Pledge Goal"); ?>: $<?php echo $pledgeProfileDetails['dblPledgeGoal']; ?></h4>	<br />		
						<h4><?php echo translate("K870"); ?>:</h4>
						<br/>
						<?php
						/*
						if ($profileStandard) {
							echo $genericMessage;
						} else {
							if($custommessage == '&nbsp;')
							{
								echo translate("This participant has not set a message.");
							}
							else
							{
								echo $custommessage;
							}
						} 
						*/
						echo $pledgeProfileDetails['strPledgeMessage'];
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4 text-left">
			<div class="well well-sm text-center">
				<h4><?php echo translate("Recent Pledges"); ?></h4>
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div id="news-container">
							<ul style="list-style-type:none;">
								<?php
								
								$totalRaised = 0;
								$percentRaisedSoFar = 0;
								
								$countPledges = count($pledgesForProfile);
								if ($countPledges == 0) {
									echo "<li>" . translate("K837") . "</li>";
								}
								else {
									for ($i = 0; $i < $countPledges; $i++) {
										echo "<li style='word-wrap: break-word;'><strong>" . $pledgesForProfile[$i]['strFirstName'] . " " . $pledgesForProfile[$i]['strLastName'] . " $" . $pledgesForProfile[$i]['dblDonationSubTotal'] . "</strong>";
										if (trim($pledgesForProfile[$i]['strPledgingMessage']) != "") {
											echo "<br />" . $pledgesForProfile[$i]['strPledgingMessage'];
										}
										echo "</li>";
										$totalRaised += $pledgesForProfile[$i]['dblDonationSubTotal'];
									}
								}
								$percentRaisedSoFar = round((($totalRaised / $pledgeProfileDetails['dblPledgeGoal']) * 100),0);
								
								?>
							</ul>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 text-center">
						<?php 						
						echo "<span>" . $percentRaisedSoFar . "% " . translate("K887") . "</span><br />";
						echo translate("K839"); echo "$".number_format($totalRaised, 2, '.', '');
						?>
						<img src="../assets/img/0.png"/>
					</div>
				</div>
			</div>
		</div>

		<?php 
		if (!isset($_GET['id'])) {
		?>
		<br />	
		<a href="<?php echo base_url("participant/"); ?>" class="btn btn-default"><span class="glyphicon glyphicon-circle-arrow-left"></span> <?php echo translate("K32"); ?></a>
		<br />
		<?php 
		} ?>
		<br />	
		<a href="<?php echo base_url("/"); ?>" class="btn btn-default"><span class="glyphicon glyphicon-circle-arrow-left"></span> <?php echo translate("K223"); ?></a>
	</div>
</div>