<?php
	$uriString = uri_string(); 
?>
<script>
	$(document).ready(function () {
		$("#nav_event").change(function() {
			var anEventID = $("#nav_event").val();
			var aUrl = "<?php echo base_url("ajax/changeParticipantEvent"); ?>";

			$.ajax({
				type: "POST",
				url: aUrl,
				data: { anEventID: anEventID },
				async: false,
				success: function (result) {
					//alert(result);	
					if (result == 1) {
						window.location.replace('<?php echo base_url("participant/"); ?>');
					}
					else {
						location.reload(true);
					}
				}
			});
		});
	});
</script>
<nav class="navbar navbar-fixed-top" role="navigation" id="navigationBar">
	<div class="container-fluid">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#template-navbar-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" target="_blank" href="https://secure.eventsonline.ca"><img src="<?php echo base_url('assets/img/eol_logo_sm.png');?>" alt="EventsOnline" title="Powered by EventsOnline" /></a>
    </div>
	<div class="collapse navbar-collapse" id="template-navbar-collapse">
		<ul class="nav navbar-nav">
			<li><a href="<?php echo base_url("/"); ?>"><span class="glyphicon glyphicon-home"></span> <?php echo translate("Registration"); ?></a></li>
			<li class="active"><a href="<?php echo base_url("participant/"); ?>"><span class="glyphicon glyphicon-user"></span> <?php echo translate("My Dashboard"); ?></a></li>
			<li><a href="<?php echo base_url("confirmation/resend"); ?>"><span class="glyphicon glyphicon-envelope"></span> <?php echo translate("Resend Confirmation"); ?></a></li>
			<li><a href="<?php echo base_url("confirmation/participant_search"); ?>"><span class="glyphicon glyphicon-search"></span> <?php echo translate("Search"); ?></a></li>
			<!--<li><a href="<?php //echo base_url("participant/"); ?>"><span class="glyphicon glyphicon-gift"></span> <?php //echo translate("Donation"); ?></a></li>-->
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<?php
			if (isset($_SESSION['participant']) && stripos($uriString,"payment") === FALSE && stripos($uriString,"login") === FALSE) {
			?>
			<li>
			<p class="navbar-text navbar-right"><b><?php echo translate("Event"); ?>:</b></p>
			</li>
			<?php
			
			}
						
			if (isset($_SESSION['participant']) && stripos($uriString,"payment") === FALSE && stripos($uriString,"login") === FALSE) {
				echo '<li>
				<form class="navbar-form">
				<div class="form-group">
				<select name="nav_event" id="nav_event" class="form-control">
					<option value="1">' . translate("My Dashboard") . '</option>';
					$countNavEvents = count($navEvents);
					for ($i = 0; $i < $countNavEvents; $i++) {
						echo "<option value=\"" . $navEvents[$i]['intEventID'] . "\" " . (($navEvents[$i]['intEventID'] == $_SESSION['participant']['participantEventID'])?'selected':'') . ">" . $navEvents[$i]['strEvent'] . "</option>";
					}
				echo '					
				</select>
				</div>
				</form>
				</li>
				<li>&nbsp;&nbsp;&nbsp;</li>';
			}
			?>
			
			<li>
				<form action="?lang=fr" method="post" id="translate-form">
					<ul class="nav navbar-nav">
						<li>
							<?php
							if (isset($_SESSION['language']) && $_SESSION['language'] == "fr") {
								echo '<a href="' . current_url() . '?lang=en" id="href-post">English</a>';
							}
							else {
								echo '<a href="' . current_url() . '?lang=fr" id="href-post">Fran&ccedil;ais</a>';
							}
							?>
						</li>
					</ul>
				</form>		
			</li>
			<li>&nbsp;&nbsp;&nbsp;</li>
			<?php
			if (isset($_SESSION['participant']) && stripos($uriString,"login") === FALSE) {
			?>
				<li>
					<a href="<?php echo base_url("participant/logout"); ?>"><i class=""></i> <?php echo translate("Log Out"); ?><b></b></a>
				</li>
			<?php
			}
			?>
		</ul>
	</div>
</nav>