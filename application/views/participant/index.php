<?php
	//echo "<pre>"; print_r($_SESSION['participant']); echo "</pre>";
?>
<div id="wrapper-participantdash">	
	<div id="mainContainer" class="col-sm-10 col-sm-offset-1 col-xs-12">
		<h3 class="text-center"><span class="glyphicon glyphicon-user"></span><?php echo translate("My Participant Dashboard"); ?></h3>
		<div class="col-sm-4">
			<b><?php echo translate("K110"); ?></b> <?php echo $participantDetails['strFirstName'] . " " . $participantDetails['strLastName']; ?><br/>
			<b><?php echo translate("Address"); ?>:</b> <?php echo $participantDetails['strAddress']; ?><br/>
			<b><?php echo translate("Email"); ?>:</b> <?php echo $participantDetails['strEmail']; ?><br/>
			<b><?php echo translate("Date of Birth"); ?>:</b> <?php echo $participantDetails['dteDateOfBirth']; ?><br/>
			<br/>
			<!--
			<a href='<?php //echo base_url("participant/logout"); ?>'>
				<button class='btn btn-default btn-sm' type='button'><span class="glyphicon glyphicon-log-out"></span> <?php //echo translate("K222"); ?></button>
			</a>
			-->
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<br />
				<div class="alert alert-info text-center">
				<i class="glyphicon glyphicon-exclamation-sign"></i>
				<b><?php echo translate("To view and access tools and details about a specific event that you've registered for, please select the event you wish to see at the top right hand corner. Here you'll be able to join or view your team, change your race, transfer your race, and purchase extra items."); ?></b>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<br />
		<?php
			
		if (isset($msgDanger)) {
			echo "<div class=\"alert alert-danger\">" . $msgDanger . "</div>";
		}
		else if (isset($msgSuccess)) {
			echo "<div class=\"alert alert-success\">" . $msgSuccess . "</div>";
		}
		
		?>
		<div class="clearfix"></div>
		
			<?php
			if (($_SESSION['participant']['participantType'] == "individualBillingMember" || $_SESSION['participant']['participantType'] == "individual") && in_array($_SESSION['participant']['participantEventID'],$pledgeProfileAvailability)) {
				if ($_SESSION['participant']['participantEventID'] != 1 && trim($participantDetails['intPledgeProfileID']) == "") {
				?>
				
				<div class='col-md-4 col-sm-6 item-participant'>
					<a href="<?php echo base_url("participant/create_pledge_profile"); ?>" class="btn btn-primary btn-block">
						<?php echo translate("Fundraising Profile"); ?>
						<br />
						<?php echo translate("Create a fundraising profile"); ?>
					</a>
				</div>
				
				<?php
				}
				else if ($_SESSION['participant']['participantEventID'] != 1) {
				?>
				
				<div class='col-md-4 col-sm-6 item-participant'>
					<a href="<?php echo base_url("participant/view_pledge_profile"); ?>" class="btn btn-primary btn-block">
						<?php echo translate("K736"); ?>
						<br />
						<?php echo translate("K737"); ?>
					</a>
				</div>
				
				<?php
				}
			}
			?>
			<?php
			if ($_SESSION['participant']['participantType'] == "individualBillingMember" || $_SESSION['participant']['participantType'] == "individual") {
				if ($_SESSION['participant']['participantEventID'] != 1 && trim($participantDetails['strTeamName']) == "") {
				?>
				
				<div class='col-md-4 col-sm-6 item-participant'>
					<a href="<?php echo base_url("participant/join_team"); ?>" class="btn btn-primary btn-block">                  
						<?php echo translate("K672"); ?>
						<br />
						<?php echo translate("Click here to join a team"); ?>
					</a>
				</div>
				
				<?php
				}
				else if ($_SESSION['participant']['participantEventID'] != 1) {
				?>
				
				<div class='col-md-4 col-sm-6 item-participant'>
					<a href="<?php echo base_url("participant/view_team"); ?>" class="btn btn-primary btn-block">                 
						<?php echo translate("K734"); ?>
						<br />
						<?php echo translate("K735"); ?>
					</a>
				</div>	 
				
				<?php
				}
			}
			?>
			<?php
			if (($_SESSION['participant']['participantType'] == "individualBillingMember" || $_SESSION['participant']['participantType'] == "individual") && $_SESSION['participant']['participantEventID'] != 1) {
			?>
			
			<div class='col-md-4 col-sm-6 item-participant'>
				<a href='<?php echo base_url("participant/change_activity"); ?>' class="btn btn-primary btn-block">                     
					<?php echo translate("K702"); ?>
					<br />
					<?php echo translate("K738"); ?>
				 </a>
			</div>
			
			<?php
			}
			?>
			<?php
			if (($_SESSION['participant']['participantType'] == "individualBillingMember" || $_SESSION['participant']['participantType'] == "individual") && $_SESSION['participant']['participantEventID'] != 1 && $transferToolAvailableFlag == "available") {
			?>
			
			<div class='col-md-4 col-sm-6 item-participant'>
				<a href="<?php echo base_url("participant/transfer_activity"); ?>" class="btn btn-primary btn-block">
					<?php echo translate("K739"); ?>
					<br />
					<?php echo translate("K740"); ?>
				</a>
			</div>
			
			<?php
			}
			?>
			<?php
			if (($_SESSION['participant']['participantType'] == "individualBillingMember" || $_SESSION['participant']['participantType'] == "individual") && $_SESSION['participant']['participantEventID'] != 1) {
			?>
			
			<div class='col-md-4 col-sm-6 item-participant'>
				<a href="<?php echo base_url("participant/purchase_addons"); ?>" class="btn btn-primary btn-block">             
					<?php echo translate("K741"); ?>
					<br />
					<?php echo translate("Shop for merchandise here."); ?>
				</a>
			</div>
			
			<?php
			}
			?>
			<?php
			if ($modifyToolAvailableFlag == "available") {
			?>
			<div class='col-md-4 col-sm-6 item-participant'>
				<a href="<?php echo base_url("participant/edit_details"); ?>" class="btn btn-primary btn-block">             
					<?php echo translate("K743"); ?>
					<br />
					<?php echo translate("K744"); ?>
				</a>
			</div>
			
			<?php
			}
			?>
			<!--
			<div class='col-md-4 col-sm-6 item-participant'>
				<a href="<?php echo base_url("confirmation/participant_search"); ?>" class="btn btn-primary btn-block">               
					<?php echo translate("K1067"); ?>
					<br />
					<?php echo translate("K1068"); ?>
				</a>
			</div>
			-->
					
		<div class="clearfix"></div>
		<br />
		<a class="btn btn-default" href='<?php echo base_url("/"); ?>'><span class="glyphicon glyphicon-arrow-left"></span> <?php echo translate("K748"); ?> </a>
		</div>
	</div>
</div>