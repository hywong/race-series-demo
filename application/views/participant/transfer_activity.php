<script>
	$(document).ready(function() {
		$("#genCode").click(function() {
			var aUrl = "<?php echo base_url("ajax/generateTransferCode"); ?>";
			
			$.ajax({
				type: "POST",
				url: aUrl,
				data: { },
				async: false,
				success: function (result) {
					//alert(result);
					var output = "<div class=\"col-sm-6 col-sm-offset-2 text-center\"><h2><?php echo translate("K776"); ?><br /><b>" + result + "</b></h2></div>";
					$("#divCode").html(output);
				}
			});
		});
	});
</script>
<br /><br />
<div id="wrapper-participantdash">	
	<div id="mainContainer" class="col-sm-10 col-sm-offset-1 col-xs-12">
		<div class="alert alert-danger text-black text-center col-sm-8 col-sm-offset-2">
			<h3><?php echo translate("K1046"); ?></h3>	
			<p><?php echo translate("K1089"); ?></p>
			<p><?php echo translate("K1090"); ?></p>
			<p><?php echo translate("K1097"); ?></p>
			<br />
			<h4 class="text-underline"><?php echo translate("K1091"); ?><h4>
		</div>
		<div class="col-sm-8 col-sm-offset-2">
			<h3><?php echo translate("K1092"); ?>:</h3>
			<ol>
				<li><?php echo translate("K1093"); ?></li>
				<li><?php echo translate("K1094"); ?>:
					<ul>
						<li><?php echo translate("K1095"); ?></li>
						<li><?php echo translate("K774"); ?>  <a target="_blank" href="<?php echo base_url("register/transfer"); ?>">TRANSFERLINK</a></li>
					</ul>
				</li>
				<li><?php echo translate("K1096"); ?></li>
			</ol>
			<br />
		</div>
		<div class="clearfix"></div>		
		<div id="divCode">
		<?php

			if ($transferCode != "") {
				echo "
				<div class=\"col-sm-8 col-sm-offset-2 text-center\">
					<h2> " . translate("K776") . "<br /><b>" . $transferCode . "</b></h2>
				</div>";
			}
			else {
				echo "
					<div class=\"col-sm-8 col-sm-offset-2\">
					<div id=\"genCode\" class='btn btn-success btn-block'><span class=\"glyphicon glyphicon-plus-sign\"></span> " . translate("K1047") . "</div>
				</div>";
			}
		?>
		</div>		
		<div class="clearfix"></div>
		<br />	
		<a href="<?php echo base_url("participant/"); ?>" class="btn btn-default"><span class="glyphicon glyphicon-circle-arrow-left"></span> <?php echo translate("K85"); ?></a>
	</div>
</div>