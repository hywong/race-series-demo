<br /><br />
<div id="wrapper-participantdash">	
	<div id="mainContainer" class="col-sm-10 col-sm-offset-1 col-xs-12">
		<form method="post" role="form" class="form-horizontal"action="<?php echo base_url("participant/join_team"); ?>">
			<?php 

			if (isset($msgDanger)) {
				echo "<div class=\"alert alert-danger\">" . $msgDanger . "</div>";
			}
			else if (isset($msgSuccess)) {
				echo "<div class=\"alert alert-success\">" . $msgSuccess . "</div>";
			}
			
			?>
			<div class="form-group">
				<label for="team" class="control-label col-sm-4"><?php echo translate("K878"); ?></label>
				<div class="col-sm-4">
					<?php if (!empty($teams)) { ?>
						<select name='team' id="team" class="form-control">
							<?php 
							$countTeams = count($teams);
							for ($i = 0; $i < $countTeams; $i++) {
								echo "<option value=\"" . $teams[$i]['intTeamID'] . "\">" . $teams[$i]['strTeamName'] . "</option>";
							}
							?>
						</select>
					<?php
					} else {
						echo translate("K761");
					}?>
				</div>
			</div>
			<div class="form-group">
				<label for="team_password" class="control-label col-sm-4"><?php echo translate("K677"); ?></label>
				<div class="col-sm-4">
					<input type='text' class="form-control" id="team_password" name='team_password'/>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-4 col-sm-offset-4">
					<?php if (!empty($teams)) { ?>
						<button type='submit' class='btn btn-success btn-block'><span class="glyphicon glyphicon-ok"></span> <?php echo translate("K830");?></button>
					<?php } ?>
				</div>
			</div>
		</form>
		<div class="clearfix"></div>
		<br />	
		<a href="<?php echo base_url("participant/"); ?>" class="btn btn-default"><span class="glyphicon glyphicon-circle-arrow-left"></span> <?php echo translate("K85"); ?></a>
	</div>
</div>