<br /><br />
<div id="mainContainer" class="col-sm-10 col-sm-offset-1 col-xs-12">
	<?php if (!LIVE_ENVIRONMENT) { ?>
		<div class="clearfix"></div>
			<div class='alert alert-danger text-center'>
				<img style='margin:auto;' src='../assets/img/small_warning.png' />
				<br/> <?php echo translate("K391"); ?>
				<span style='text-decoration:underline;'><b><?php echo translate("K392"); ?></b></span> 
				<?php echo translate("K393"); ?>
				<br/>
				<?php echo translate("K394"); ?>
				<br/> 
				<?php echo translate("K395"); ?>
			</div>
	<?php } ?>
	<div class="row text-center">
		<h4><?php echo translate("K963"); ?></h4>
	</div>
	
	<?php

		if($response['status'] === TRUE) {
			echo "<div class=\"alert alert-success text-center\"><h4><b>";
		}
		else {
			echo "<div class=\"alert alert-danger text-center\"><h4><b>";
		}
		echo "Bank: (".$response['bank_error_code1']."-".$response['bank_error_code2'].") ".$response['bank_error_message']."<br />";
		echo "System: (".$response['gateway_error_code'].") ".$response['gateway_error_message']."<br /><br />";
		if(isset($response['receipt'])) {
			echo $response['receipt'];
		}
		if($response['status'] === FALSE) {
			echo "<br /> <br /><a href=\"".base_url("participant/payment?action=changeActivity")."\" class=\"btn btn-success\">".translate("Try Again")."</a>";
		}
		echo "</b></h4></div>";
		
	?>

	<br /><br />
	<div class="col-sm-4 col-sm-offset-4 text-center">
		<a href="<?php echo base_url("participant/"); ?>" class="btn btn-default btn-block stacked-btn">
			<h5><?php echo translate("Return to your Participant Dashboard"); ?></h5>
			<i><?php echo translate("K383"); ?></i>
		</a>
	</div>		
	<br /><br /><br /><br />
	<div class="col-sm-4 col-sm-offset-4 text-center">
		<a href='http://eventsonline.ca' target='_blank'>
			<img src="../assets/img/eol_logo_sm.png"/>
		</a>
	</div>
	<div class='clearfix'></div>
	<br/>		
</div>    