<script language="javascript">		
	$(document).ready(function() {
		$("#fCheckout").validate();
	});
</script>
<br /><br />
<div id="wrapper-participantdash">	
	<div id="mainContainer" class="col-sm-10 col-sm-offset-1 col-xs-12">
		<script>
			var thisPage = "confirm";
		</script>
		<div class="col-sm-8 col-sm-offset-2">
			<div class="row">
				<div class="alert alert-info text-center">
					<?php echo translate("K823");?>
				</div>
			</div>
				
			<div class="row">
				<div class="table-responsive">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Field</th>
								<th>Details</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							foreach ($_POST as $key => $value) {
								echo "<tr>
									<td>" . translate($key) . ":</td>
									<td>" . $value . "</td>
								</tr>";
							}
							//echo "<pre>"; print_r($_POST); echo "</pre>";
							?>
							<tr class="well">
								<td><h4><?php echo translate("K824"); ?></h4></td>
								<td><h4>$<?php echo number_format($_POST['donation'], 2, '.', ''); ?></h4></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<hr />
		</div>
			<form method="POST" action="checkout.php" name="fCheckout" id="fCheckout">
				<?php //echo $hiddenFields ?>	
				<?php
					echo "
					<hr style='width:60%;'/><br/>    
					<div class=\"row\">
						<div class=\"col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 text-center\">
							<div class=\"well well-sm\">
								<div class=\"card-details\">
									<h3>Accepted Credit Cards</h3>
									<div class=\"row\">
										<img src=\"../assets/img/visa_48.png\" title=\"Visa\" alt=\"Visa\" />
										<img src=\"../assets/img/mastercard_48.png\" title=\"Master Card\" alt=\"Master Card\" />
										<img src=\"../assets/img/amex_48.png\" title=\"American Express\" alt=\"American Express\" />
									</div>
									<br />
									  <div class=\"form-group\">
										<label for=\"ccName\">" . translate("K542") . "</label>
										<input type=\"text\" name=\"ccName\" class=\"form-control\" id=\"ccName\" placeholder=\"" . translate("K542") . "\">
									  </div>
									  <div class=\"form-group\">
										<label for=\"ccNum\">" . translate("K543") . "</label>
										<input type=\"text\" name=\"ccNum\" class=\"form-control\" id=\"ccNum\" placeholder=\"XXXXXXXXXXXX1234\">
									  </div>
									  <div class=\"form-group\">
										<label>" . translate("K544") . "</label>
										<br />
										<div class=\"row\">
										<div class=\"col-sm-6\">
											<small>Month</small><br />
											<select class=\"form-control\" name=\"expMonth\">
											";
					for ($i = 1; $i <= 12; $i++) {
						$padded = str_pad($i, 2, '0', STR_PAD_LEFT);
						echo "<option value=\"" . $padded . "\">" . $padded . "</option>";
					}
					echo "
											</select>
										</div>
										<div class=\"col-sm-6\">
											<small>Year</small><br />
											<select name=\"expYear\" class=\"form-control\" required>";
											$thisYear = date('Y');
					for ($i = $thisYear; $i <= ($thisYear + 25); $i++) {
						echo "<option value=\"" . substr($i, -2) . "\">" . $i . "</option>";
					}
					echo "
											</select>
										</div>
										</div>
									  </div>
									  <div class=\"form-group\">
										<button class=\"btn btn-block btn-success\"><h5><span class=\"glyphicon glyphicon-credit-card\"></span> " . translate("Purchase") . "</h5></button>
									  </div>
								  </div>
							</div>
						</div>
					</div>
					";
				?>
				
			</form>
	</div>
</div>