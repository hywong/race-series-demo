<script>
	function textCounter(field, cntfield, maxlimit) {
		if (field.value.length > maxlimit) // if too long...trim it!
			field.value = field.value.substring(0, maxlimit);
		// otherwise, update 'characters left' counter
		else
			$('#remLen2').html(maxlimit - field.value.length);
	}

	$(document).ready(function () {
		$.tablesorter.defaults.widgets = ['zebra'];

		$(".tablesorter").tablesorter({
			stripingRowClass: ['even', 'odd'],
			stripeRowsOnStartUp: true,
		});

		//Charity Icon Toggle
		var showcharity = $('#showcharity');
		showcharity.hide();
		var newIMGpath = '../charities/';
		
		
		$('#pledgemsg').bind('keypress', function (event) {
			//var regex = new RegExp("^[a-zA-Z0-9~!@#^*()-_=+\b\t]+$");
			var regex = new RegExp("^[%\"]+$");
			var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
			if (regex.test(key)) {
			   event.preventDefault();
			   return false;
			}
		});
		
		$("#myform").validate({
			rules: {
				profile_pledge_goal: {
					digits: true
				}
			}
		});
	});
</script>
<div id="wrapper-participantdash">	
	<div id="mainContainer" class="col-sm-10 col-sm-offset-1 col-xs-12">
		<form id="myform" name="myform" class="form-horizontal" method='post' action='<?php echo base_url("participant/create_pledge_profile"); ?>'>
			<h3><?php echo translate("K626"); ?></h3>

			<div class="form-group">
				<label for="charity" class="control-label col-sm-3"><?php echo translate("K719"); ?></label>
				<div class="col-sm-8">
					<select name="charity" class="form-control" id='charity'>
						<?php 
						
						$countCharities = count($charities);
						for ($i = 0; $i < $countCharities; $i++) {
							echo "<option value=\"" . $charities[$i]['intCharityID'] . "\">" . $charities[$i]['strCharity'] . "</option>";
						}
																				
						?>
					</select>
				</div>
			</div>
		

			<div id='showcharity' style='margin:auto; height:auto; padding:20px;'>
				<br/>		
					<img src=''/>				
			</div>
			<div class='profileInfo'>
				<input type='hidden' value='<?php echo $profilePicName; ?>' name='profile_pic'/>
				<iframe
					src="https://secure.eventsonline.ca/profile_uploads/pledge_profile/default/_upload.php?filename=<?php echo $profilePicName; ?>&btnTitle=Profile"
					width="100%" height="230" frameBorder="0"></iframe>
			</div>
		
			<div class='form-group profileInfo'>
				<label class="control-label col-sm-3"><?php echo translate("K630"); ?>$</label>
				<div class="col-sm-8">
					<input type="text" name="profile_pledge_goal" size="15" value="500" class="form-control required" />
				</div>
			</div>
			<div class='form-group profileInfo'>
				<label class="control-label col-sm-3">
				<?php echo translate("K631"); ?><br />
				<label class="badge" id='remLen2'>300</label> <?php echo translate("K632"); ?>
				</label>
				<div class="col-sm-8">
				<textarea id="pledgemsg" name="profile_pledge_message" class="form-control"
						  onKeyDown='textCounter(document.forms["myform"].profile_pledge_message,document.forms["myform"].remLen2,300)'
						  onKeyUp='textCounter(document.forms["myform"].profile_pledge_message,document.forms["myform"].remLen2,300)'></textarea>
				</div>
			</div>
			<div class="col-sm-6 col-sm-offset-3">
				<button type='submit' class='btn btn-success btn-block'><span class="glyphicon glyphicon-ok"></span> <?php echo translate("K830"); ?></button>
			</div>
		</form>
		<div class="clearfix"></div>
		<br />	
		<a href="<?php echo base_url("participant/"); ?>" class="btn btn-default"><span class="glyphicon glyphicon-circle-arrow-left"></span> <?php echo translate("K85"); ?></a>
	</div>
</div>