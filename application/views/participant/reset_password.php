<script>
	$(document).ready(function () {
		$("#frmNewPassword").validate({
			rules: {
				 password_verify: {
					equalTo: "#password"
				}
			}
		});
	});
</script>
<br /><br />
<div class="col-sm-6 col-sm-offset-3">
	<div class="well">
		<h4><?php echo translate("Set New Password"); ?></h4>
		<form id="frmNewPassword" method="post" action="<?php echo base_url("participant/reset_password") . "?id=" . $_GET['id']; ?>">
			<?php
				if (isset($msgDanger)) {
					echo "<div class=\"alert alert-danger\">" . $msgDanger . "</div>"; exit;
				}
				else if (isset($msgSuccess)) {
					echo "<div class=\"alert alert-success\">" . $msgSuccess . "</div>";
				}
				
				if (isset($msgSuccess) || isset($msgDanger)) {
				echo "					
					<div class=\"row\">
						<div class=\"col-xs-12\">
							<a href=\"" . base_url("participant/login") . "\" class=\"btn btn-default\"><span class=\"glyphicon glyphicon-circle-arrow-left\"></span> " . translate("Back to Login") . "</a>
						</div>
					</div>";
				}
				else {
					echo "					
					<div class=\"form-group\">
						<label for=\"password\">" . translate("New Password") . ": </label>
						<input type=\"password\" id=\"password\" name=\"password\" size=\"30\" maxlength=\"50\" class=\"form-control required\" />
					</div>
					<div class=\"form-group\">
						<label for=\"password_verifiy\">" . translate("Verify Password") . ": </label>
						<input type=\"password\" id=\"password_verify\" name=\"password_verify\" size=\"30\" maxlength=\"50\" class=\"form-control required\" />
					</div>
					<div class=\"form-group\">
						<button class=\"btn btn-success btn-block\"><span class=\"glyphicon glyphicon-ok\"></span> " . translate("K701") . "</button>
					</div>";
				}
			?>
		</form>
	</div>
</div>