<?php
  
  $formitSettings = array(
    "id" => "paymentForm"
    );
  // echo "<PRE>";
  // print_r($_SESSION);
  // echo "</PRE>";
  // exit;
?>
<div class="container">
  <div class="row">
    <div class="col-sm-offset-2 col-sm-8">
      <?php
      foreach($summary as $event => $breakdown)
      {
        echo "<div class=\"well\"><fieldset><legend>".translate($event)."</legend>";
        foreach($breakdown as $section => $itemDetails)
        {
          echo "<div class=\"list-group\"><div class=\"col-xs-12 list-group-item active\">".translate($section)."</div>";

          foreach($itemDetails as $item => $qty)
          {
            $itemCount = count($qty);
            for($i = 0; $i < $itemCount; $i++)
            {
              if($item == "Sub-total")
              {
                echo "<div class=\"col-xs-12 list-group-item\"><div class=\"col-xs-5 col-xs-offset-1\"><b>".$item."</b></div>";
                echo "<div class=\"col-xs-6 text-right\"><b>$".number_format($qty[$i], 2, '.', '')."</b></div></div><div class=\"clearfix\"></div>";
              }
              else
              {
                echo "<div class=\"col-xs-12 list-group-item\"><div class=\"col-xs-7 col-xs-offset-1\">".$item."</div>";
                echo "<div class=\"col-xs-4 text-right\">$".number_format($qty[$i], 2, '.', '')."</div><div class=\"clearfix\"></div></div>";
              }
            }
          }
          echo"<br /></div>";
        }
		echo "</fieldset></div>";
      }
      ?>
    </div>
  </div>
  <br />
  <div class="row">
    <div class="col-sm-offset-2 col-sm-8">
    <?php
      if($_SESSION['type'] == 'group')
      {
        echo "<a href=\"". base_url("register/new_group") ."\" class=\"btn btn-info btn-block\">".translate("Add new group")."</a>";
      }

      if($_SESSION['type'] == 'individual')
      {
        echo "<a href=\"". base_url("register/new_participant") ."\" class=\"btn btn-info btn-block\">".translate("Add new participant")."</a>";
      }
    ?>
    </div>
  </div>
  <br />
  <?php if($values['total'] > 0) { ?>
  <div class="row">
    <div class="col-sm-offset-2 col-sm-8">
      
        <div class="well">
          <div id="cardInformation">
            <fieldset>
              <legend class="text-center"><?php echo translate("Supported Credit Cards");?></legend>
            </fieldset>
            <div class="row">
              <div class="col-sm-offset-3 col-sm-6 text-center">
                <img src="<?php echo base_url("assets/img/visa_48.png");?>" title="Visa" alt="Visa" />
                <img src="<?php echo base_url("assets/img/mastercard_48.png");?>" title="Master Card" alt="Master Card" />
                <img src="<?php echo base_url("assets/img/amex_48.png");?>" title="American Express" alt="American Express" />
              </div>
            </div>
            <?php
              formit($paymentForm, $formitSettings);
            ?>
          </div>
          <div id="processingCard">
            <fieldset>
              <legend class="text-center"><?php echo translate("Processing Payment");?></legend>
            </fieldset>
            <div class="row">
              <div class="col-sm-offset-3 col-sm-6 text-center">
                <img src="<?php echo base_url("assets/img/loading_large.gif"); ?>" title="Loading" alt="Loading"/>
              </div>
            </div>
          </div>
        </div>

    </div>
  </div>
  <?php } else { ?> 
  <div class="row">
    <div class="col-xs-12">
      <div class="col-sm-offset-2 col-sm-8">
        <div class="well text-center">
          <h3><?php echo translate("Registration is free, click below to complete the process");?></h3>
          <br />
          <div class="row">
          <div class="col-sm-offset-3 col-sm-6">
            <a href="<?php echo base_url("payment/process"); ?>" class="btn btn-success btn-block"> <?php echo translate("Submit"); ?></a>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>
</div>
<script>
$(function(){
  $("#paymentForm").on("submit", function(){
    $("#cardInformation").slideUp();
    $("#processingCard").slideDown();
  });
});
</script>