<?php
  // echo "<PRE>";
  // print_r($sections);
  // echo "</PRE>";

  $formitForm = array(
    "method" => "POST",
    "action" => $formAction,
    "sections" => $sections
    );

  $formitSettings = array(
      "label_columns" => "col-sm-3",
      "field_columns" => "col-sm-9",
      "class" => "customClass",
      "id" => "myForm",
      "type" => "horizontal",
	  "skip" => TRUE
    );
?>
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <?php echo formit($formitForm, $formitSettings); ?>
    </div>
  </div>
</div>
<script>
  $(function(){
    princessMargaretID = $('.aka-donation-charity').val();

    $('.aka-selection').on('change', function(){
      if($(this).val() != "" && $(this).is(':checked'))
      {
        $('.aka-teams').parent().parent().slideDown();
      }
      else
      {
        $('.aka-teams').parent().parent().slideUp();
        $('.aka-teams').val('');
      }
    });
    $('.aka-selection').change();

    $('.aka-donation').on('change', function(){
      if($(this).val() > 0)
      {
        $('.aka-donation-charity').val(princessMargaretID);
      }
      else
      {
        $('.aka-donation-charity').val(''); 
      }

    });
    $('.aka-donation').change();

    $('.eol-fundraising-account').on('change', function(){
      var eventID = $(this).attr('id').substr($(this).attr('id').length - 1);
      if($(this).val() == "Yes")
      {
        $("#inpFundraisingGoal"+eventID).parent().parent().show();
        $("#txtFundraisingMessage"+eventID).parent().parent().show();
        $("#iframeProfileImg_"+eventID).parent().show();
      }
      else
      {
        $("#inpFundraisingGoal"+eventID).parent().parent().hide();
        $("#inpFundraisingGoal"+eventID).val('');
        $("#txtFundraisingMessage"+eventID).parent().parent().hide();
        $("#txtFundraisingMessage"+eventID).val('');
        $("#iframeProfileImg_"+eventID).parent().hide();
      }
    });
    $('.eol-fundraising-account').change();

    $('#myForm').on('submit', function(){
		var valSuccess = true;
	  $(this).find('.donation-charity-amount').each(function(){
      if(this.value != "")
      {
        var eventID = this.id.substr($(this).attr('id').length - 1);
        if($("#inpCharity"+eventID).val() == '')
        {
          alert("<?php echo translate("Please select a charty to make your donation.");?>");
          valSuccess=false;
        }
      }
    });
	  
	  <?php
    foreach($_SESSION['registration'][$_SESSION['registration']['active_user']]['activities'] as $eventActivity)
    {
		$eventIDs = explode("_", $eventActivity);
	?>
		var eventIDNum = "<?php echo $eventIDs[0]; ?>";
	  
		if($('input[name=charitydetails_fundraising_goal_'+eventIDNum+']').val() != "" && typeof $('input[name=charitydetails_fundraising_goal_'+eventIDNum+']').val() != 'undefined' && $('input[name=charitydetails_fundraising_goal_'+eventIDNum+']').val() != parseInt($('input[name=charitydetails_fundraising_goal_'+eventIDNum+']').val()) )
		{
            alert("<?php echo translate("Please enter a numeric value for your pledge goal.");?>");
			$('input[name=charitydetails_fundraising_goal_'+eventIDNum+']').focus();
            valSuccess=false;
        }
	<?php
	}
    ?>
	  
	  return valSuccess;
      
      
    });
  });
</script>