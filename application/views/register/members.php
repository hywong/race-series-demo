<?php
// echo "<PRE>";
// print_r($_SESSION);
// echo "</pRE>";

  $form = array(
    "method" => "POST",
    "action" => base_url("register/confirmation"),
    );
?>
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <div class="well">
		<div class="alert alert-info">
			<i class="glyphicon glyphicon-exclamation-sign"></i> <b><?php echo translate("REGISTERING MULTIPLE PARTICIPANTS:");?></b> <?php echo translate("please DO NOT use the same e-mail address for each registrant to ensure you receive your separate confirmation e-mail/last minute information."); ?>
		</div>
        <?php
          formit_dynamic($form, $fields, $_SESSION['registration'][$_SESSION['registration']['active_user']]['personal']['members']);
        ?>
      </div>
    </div>
  </div>
</div>
<script>
$(function(){
  emailValid = true;
  $('.verify-email, .verify-email-confirm').on('blur', function(){
    var thisMember = $(this).data('member');
    if($('input[name=g_email_'+thisMember).val() == $('input[name=g_email_verify_'+thisMember).val())
    {
      if($('input[name=g_email_'+thisMember).parent().parent().hasClass('has-error'))
      {
         $('input[name=g_email_'+thisMember).parent().parent().removeClass('has-error');
      }
      if($('input[name=g_email_verify_'+thisMember).parent().parent().hasClass('has-error'))
      {
         $('input[name=g_email_verify_'+thisMember).parent().parent().removeClass('has-error');
      }
    }
    else
    {
      $('input[name=g_email_'+thisMember).parent().parent().addClass('has-error');
      $('input[name=g_email_verify_'+thisMember).parent().parent().addClass('has-error');
    }
  });

  $('form').on('submit', function(){
    shirtsValid = false;
    shirtsToCheck = {};
    $(this).find('.validate-shirt').each(function(){
      if(shirtsToCheck.hasOwnProperty($(this).val()))
      {
        shirtsToCheck[$(this).val()] += 1;
      }
      else
      {
        shirtsToCheck[$(this).val()] = 1;
      }
    });

    $.ajax({
      url: "<?php echo base_url("ajax/validate_group_shirts");?>",
      method: "post",
      async: false,
      data: {shirts: shirtsToCheck}
    })
    .done(function(response){
      
      if(response == true)
      {
        shirtsValid = true;
      }
      else
      {
        shirtsValid = false;
        var jsonResponse = $.parseJSON(response);
        var errorString = 'Shirts for the following sizes are over our stock limit, please change accordingly:'+ "\r\n";
        $.each(jsonResponse, function(k, v){
          errorString += k+" - over our stock limit by: "+v+"\r\n";
        });
        alert(errorString);
      }
    });

    if(!shirtsValid)
    {
      return false;
    }

    emailValid = true;
    $(this).find('.verify-email').each(function(){
      var thisMember = $(this).data('member');
      if($('input[name=g_email_'+thisMember).val() != $('input[name=g_email_verify_'+thisMember).val())
      {
        $('input[name=g_email_'+thisMember).parent().parent().addClass('has-error');
        $('input[name=g_email_'+thisMember).focus();
        $('input[name=g_email_verify_'+thisMember).parent().parent().addClass('has-error');

        emailValid = false;
      }
    });

    if(!emailValid)
    {
      alert("Please ensure the emails provided match");
      return false;
    }

    validDOB = true;
    <?php
    if($_SESSION['registration'][$_SESSION['registration']['active_user']]['activities'][0] == "3_7")
    {
    ?>
      $('body').find('.date-picker').each(function(){
        var thisDOB = $(this).val();
          var age = getAge(thisDOB);
          if(age < 5 || age > 13)
          {
            alert("Required age for the Children's Run is 5 - 13 years old");
            $(this).focus();
            validDOB = false;
          }
      });    
    <?php
    }
    ?>

    if(!validDOB)
    {
      return false;
    }

  });
});
</script>