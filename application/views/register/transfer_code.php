<div class="container">
  <div class="row">
    <div class="col-sm-offset-2 col-sm-8">
      <div class="well">
        <?php
          if(isset($invalid))
          {
            echo "<div class=\"alert alert-danger\">".translate($invalid)."</div>";
          }
        ?>
        <h3>Please enter your transfer code</h3>
        <form method="post" action="<?php echo base_url("register/personal");?>">
          <div class="form-group">
            <label for="inpTransferCode"><?php echo translate("Transfer Code");?></label>
            <input type="text" class="form-control" name="transfer_code" required />
          </div>
          <div class="form-group">
            <button class="btn btn-success btn-block">Validate</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>