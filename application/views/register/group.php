<?php
  // echo "<PRE>";
  // print_r($_SESSION);
  // echo "</PRE>";
  $formitForm = array(
    "method" => "POST",
    "action" => base_url("register/members"),
    "sections" => $sections
    );

  $formitSettings = array(
      "label_columns" => "col-sm-3",
      "field_columns" => "col-sm-9",
      "class" => "customClass",
      "id" => "myForm",
      "type" => "horizontal"
    );
?>
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <div class="well">
	  <?php if(!isset($_SESSION['cartID']))
        { ?>
        <div class="row">
          <div class="col-xs-12 text-right">
            <a class="btn btn-danger btn-sm" href="<?php echo base_url("register");?>"><?php echo translate("Register an individual");?></a>
          </div>
        </div>
		<br />
		<?php 
        }
        ?>        
        <div class="row">
          <div class="col-xs-12">
            <div class="alert alert-info text-center"><i class="glyphicon glyphicon-exclamation-sign"></i> <b><?php echo translate("The billing member will NOT be registered as a participant. It is strictly for record keeping.");?></b></div>
          </div>
        </div>
        <?php
          formit($formitForm, $formitSettings);
        ?>
      </div> 
    </div>
  </div>
</div>
<script>
$(function(){
  $('body').on('click', '#validate-discount', function(){
    if($('input[name=discount_code]').val() != "")
    {
      $.post('<?php echo base_url("ajax/validate_discount");?>', {code: $('input[name=discount_code]').val()}, function(response){
        if(response == true)
        {
          $('.discount-alert-row').remove();
          $('.discount-row').before("<div class=\"row discount-applied\"><div class=\"col-xs-12\"><div class=\"alert alert-success text-center\"><i class=\"glyphicon glyphicon-ok\"></i> <?php echo translate("Discount code has been applied to your registration"); ?></div></div></div><div class=\"row discount-removal\"><div class=\"col-xs-12 text-center\"><button class=\"btn btn-danger btn-sm remove-discount\" type=\"button\"><?php echo translate("Remove Discount Code"); ?></button></div></div>");
          $('input[name=discount_code]').val('');
          $('.discount-row').remove();
        }
        else
        {
          $('.discount-alert-row').remove();
          $('.discount-row').before("<div class=\"row discount-alert-row\"><div class=\"col-xs-12\"><div class=\"alert alert-danger text-center\"><i class=\"glyphicon glyphicon-remove\"></i> <?php echo translate("Discount code is invalid"); ?></div></div></div>");
          $('input[name=discount_code]').val('');
        }
      });
    }
  });

  $('body').on('click', '.remove-discount', function(){
    $.post('<?php echo base_url("ajax/remove_discount"); ?>', {code: $('input[name=discount_code]').val()}, function(response){
      if(response == true)
      {
        $('.discount-applied').before('<div class="form-group discount-row"><label class="control-label col-sm-3" for="inpDiscountCode"><?php echo translate("Discount Code");?></label><div class="col-sm-9"><div class="input-group"><input type="text" name="discount_code" class="form-control" id="inpDiscountCode"><span class="input-group-btn"><button id="validate-discount" type="button" class="btn btn-success"><?php echo translate("Validate");?></button></span></div></div></div>');
        $('.discount-applied').remove();
        $('.discount-removal').remove();

      }
    });
  });

  teamValid = false;

  $('form').on('submit',function(){
    if($('input[name=group_event]:checked').val() == "")
    {
      alert("You must select a race");
      return false;
    }
    var formData = $(this).serialize();
    $.ajax({
      url: "<?php echo base_url("ajax/validate_new_team_group"); ?>",
      method: "post",
      async: false,
      data: {team_name_create: $('input[name=team_name_create]').val(), eventID: $('input[name=group_event]:checked').val()}
    })
    .done(function(response)
    {
      if(response == true)
      {
        teamValid = true;
      }
      else if(response == false)
      {
        return false
      }
      else
      {
        var json = $.parseJSON(response);
        $.each(json, function(k, v){
          $(':input').filter(function(){
            if($(this).val() == v && $(this).hasClass('team-create-field'))
            {
              if($(this).parent().parent().hasClass("has-error"))
                $("."+$(this).attr("name")+"-error").remove();

              $('input[name=team_name_create]').parent().parent().append("<div class=\"form-group "+$(this).attr("name")+"-error\"><div class=\"col-sm-offset-3 col-sm-8 text-danger\">Team name already exists</div></div>");
              $('input[name=team_name_create]').parent().parent().addClass('has-error');
              $('input[name=team_name_create]').focus();
            }
           });
        });        
      }

    });

    if($('#inpEmail').val() != $("#inpEmailVerify").val())
    {
      $("#inpEmail").focus();
      alert("Your emails must match");
      return false;
    }

	  if($("input[name='group_event']:checked").val() == "" || $("input[name='group_event']:checked").val() == null)
    {
	    $("input[name='group_event']").focus();
      alert("<?php echo translate('Please choose an event to register for.'); ?>");
      return false;
    }



    if(!teamValid)
    {
      return false;
    }
  });

  
});
</script>