<?php
  $formitForm = array(
    "method" => "POST",
    "action" => $formAction,
    "sections" => $sections
    );

  $formitSettings = array(
      "label_columns" => "col-sm-3",
      "field_columns" => "col-sm-9",
      "class" => "customClass",
      "id" => "myForm",
      "type" => "horizontal"
    );
	
	// echo"<pre>";
	// print_r($_SESSION);
	// echo"</pre>";
	// exit();
?>

<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <?php echo formit($formitForm, $formitSettings); ?>
    </div>
  </div>
</div>
<script>
usernameValid = false;
$(function(){

  $('body').on('click', '#validate-discount', function(){
    if($('input[name=discount_code]').val() != "")
    {
      $.post('<?php echo base_url("ajax/validate_discount");?>', {code: $('input[name=discount_code]').val()}, function(response){
        if(response == true)
        {
          $('.discount-alert-row').remove();
          $('.discount-row').before("<div class=\"row discount-applied\"><div class=\"col-xs-12\"><div class=\"alert alert-success text-center\"><i class=\"glyphicon glyphicon-ok\"></i> <?php echo translate("Discount code has been applied to your registration"); ?></div></div></div><div class=\"row discount-removal\"><div class=\"col-xs-12 text-center\"><button class=\"btn btn-danger btn-sm remove-discount\" type=\"button\"><?php echo translate("Remove Discount Code"); ?></button></div></div>");
          $('input[name=discount_code]').val('');
          $('.discount-row').remove();
        }
        else
        {
          $('.discount-alert-row').remove();
          $('.discount-row').before("<div class=\"row discount-alert-row\"><div class=\"col-xs-12\"><div class=\"alert alert-danger text-center\"><i class=\"glyphicon glyphicon-remove\"></i> <?php echo translate("Discount code is invalid"); ?></div></div></div>");
          $('input[name=discount_code]').val('');
        }
      });
    }
  });

  $('body').on('click', '.remove-discount', function(){
    $.post('<?php echo base_url("ajax/remove_discount"); ?>', {code: $('input[name=discount_code]').val()}, function(response){
      if(response == true)
      {
        $('.discount-applied').before('<div class="form-group discount-row"><label class="control-label col-sm-3" for="inpDiscountCode"><?php echo translate("Discount Code");?></label><div class="col-sm-9"><div class="input-group"><input type="text" name="discount_code" class="form-control" id="inpDiscountCode"><span class="input-group-btn"><button id="validate-discount" type="button" class="btn btn-success"><?php echo translate("Validate");?></button></span></div></div></div>');
        $('.discount-applied').remove();
        $('.discount-removal').remove();

      }
    });
  });

  $('.validate-username').on('blur', function(){
    if($('.validate-username').val() != "")
    {
      $('.username-custom-error').remove();
      $('.validate-username-status').remove();
      $(this).parent().parent().removeClass('has-success has-error');
      $(this).parent().parent().addClass('has-warning');
      $('.validate-username').after("<span class=\"validate-username-status form-control-feedback\" title=\"Validating username...\"><img src=\"<?php echo base_url("assets/img/loading_sm.gif");?>\" title=\"Validating username...\" alt=\"Validating username...\" /></span>");
      var myStr = $(this).val();
      myStr=myStr.toLowerCase();
      myStr=myStr.replace(/ /g,"");
      var illegal_characters = false;
      if(myStr.match(/[^a-zA-Z0-9\.]+/g))
      {
        var illegal_characters = true;
      }

      if(illegal_characters)
      {

        usernameValid = false;
        $('.validate-username-status').remove();
        $('.validate-username').parent().parent().removeClass('has-success has-warning');
        $('.validate-username').parent().parent().addClass('has-error');
        $('.validate-username').after("<span class=\"validate-username-status glyphicon glyphicon-remove form-control-feedback\" title=\"Username is invalid\"></span>");
        $(this).parent().parent().after('<div class="form-group has-error username-custom-error"><div class="col-sm-offset-3 col-sm-9"><label class=\"control-label\" for=\"inpUsername\"><?php echo translate("Your username contains invalid characters"); ?></label></div></div>');
      }
      else
      {
        $.post('<?php echo base_url("ajax/validate_username");?>', {username: $(this).val()}, function(response){
          if(response == true)
          {
            $('.validate-username-status').remove();
            $('.validate-username').parent().parent().removeClass('has-error has-warning');
            $('.validate-username').parent().parent().addClass('has-success');
            $('.validate-username').after("<span class=\"validate-username-status glyphicon glyphicon-ok form-control-feedback\" title=\"Username is valid\"></span>");
            usernameValid = true;
          }
          else
          {
            usernameValid = false;
            $('.validate-username-status').remove();
            $('.validate-username').parent().parent().removeClass('has-success has-warning');
            $('.validate-username').parent().parent().addClass('has-error');
            $('.validate-username').after("<span class=\"validate-username-status glyphicon glyphicon-remove form-control-feedback\" title=\"Username is invalid\"></span>");
          }
        });
      }
    }
  });
  if($('.validate-username').val() != "")
  {
    $('.validate-username').blur();
  }

  $('.first-time-participating').on('change', function(){
    var eventID = $(this).attr('id').substr($(this).attr('id').length - 1);
    if($(this).val() == "No")
    {
      $("#selTimesParticipated"+eventID).parent().parent().show();
    }
    else
    {
      $("#selTimesParticipated"+eventID).parent().parent().hide();
    }
  });
  $('.first-time-participating').change();

  $('.learn-about-event').on('change', function(){
    var eventID = $(this).attr('id').substr($(this).attr('id').length - 1);
    if($(this).val() != "Other")
    {
      $("#inpLearnAboutOther"+eventID).parent().parent().hide();
      $("#inpLearnAboutOther").val('');
    }
    else
    {
      $("#inpLearnAboutOther"+eventID).parent().parent().show();
    }
  });
  $('.learn-about-event').change();

  $('#selTypeOfIndustry').on('change', function(){    
    if($(this).val() != "Other")
    {
      $("#inpTypeOfIndustry").parent().parent().hide();
      $("#inpTypeOfIndustry").val('');
    }
    else
    {
      $("#inpTypeOfIndustry").parent().parent().show();
    }
  });
  $('#selTypeOfIndustry').change();

  $(':input').on('blur', function(){
    if($('input[name=email]').val() == $('input[name=email_verify]').val())
    {
      if($('input[name=email]').parent().hasClass('has-error'))
      {
        $('input[name=email]').parent().removeClass('has-error');
        $('input[name=email_verify]').parent().removeClass('has-error');
      }
    }
  });

  $('select[name=nationality]').on('change', function(){
    if($(this).val() != "")
    {
      if($(this).val() != "Canadian")
      {
        $('select[name=nationality_duration]').parent().parent().show();
      }
      else
      {
        $('select[name=nationality_duration]').parent().parent().hide();
        $('select[name=nationality_duration]').val('');
      }
    }
    else
    {
      $('select[name=nationality_duration]').parent().parent().hide();
      $('select[name=nationality_duration]').val('');
    }
  });
  $('select[name=nationality]').change();

  $('form').on('submit',function(){
    if(usernameValid == false)
    {
      $('.validate-username').focus();
      return false;
    }
    <?php
    foreach($_SESSION['registration'][$_SESSION['registration']['active_user']]['activities'] as $eventActivity)
    {
	  $activityNum = explode("_", $eventActivity);
	  $activityCheck = $_SESSION['general']['activities'][$activityNum[1]];

	  if($activityCheck == "1/2 Marathon"){
		?>
		var age = getAge($('#datDateOfBirth').val());
        if(age < 16)
        {
          alert("<?php echo translate("AgeError Half"); ?>");
          $("#datDateOfBirth").focus();
          return false;
        }
		<?php
	  }
	  
	  if($activityCheck == "Marathon"){
		?>
		var age = getAge($('#datDateOfBirth').val());
        if(age < 18)
        {
          alert("<?php echo translate("AgeError Full"); ?>");
          $("#datDateOfBirth").focus();
          return false;
        }
		<?php
	  }
	  
	  if($eventActivity == "3_7")
      {
        ?>
        var age = getAge($('#datDateOfBirth').val());
        if(age < 5 || age > 13)
        {
          alert("Required age for the Children's Run is 5 - 13 years old");
          $("#datDateOfBirth").focus();
          return false;
        }
        <?php
      }
    }
    ?>

    if($('input[name=email]').val() != $('input[name=email_verify]').val())
    {
      alert("Please ensure the emails provided match");
      $('input[name=email]').parent().addClass('has-error');
      $('input[name=email_verify]').parent().addClass('has-error');
      $('input[name=email]').focus();
      return false;
    }

    if($('input[name=password]').val() != $('input[name=password_verify]').val())
    {
      alert("Please ensure the passwords provided match");
      $('input[name=password]').parent().addClass('has-error');
      $('input[name=password_verify]').parent().addClass('has-error');
      $('input[name=password]').focus();
      return false;
    }
	
	<?php
    foreach($_SESSION['registration'][$_SESSION['registration']['active_user']]['activities'] as $eventActivity)
    {
	  $eventIDs = explode("_", $eventActivity);
	  ?>
	  var eventIDNum = "<?php echo $eventIDs[0]; ?>";

	  if($('select[name=extrainfo_first_time_'+eventIDNum+'] option:selected').val() == "No" && $('select[name=extrainfo_times_participated_'+eventIDNum+'] option:selected').val() == "")
		{
		  alert("<?php echo translate("Please specify how many times you have participated before.");?>");
		  $('select[name=extrainfo_times_participated_'+eventIDNum+']').parent().addClass('has-error');
		  $('select[name=extrainfo_times_participated_'+eventIDNum+'_verify]').parent().addClass('has-error');
		  $('select[name=extrainfo_times_participated_'+eventIDNum+']').focus();
		  return false;
		}else{
		  $('select[name=extrainfo_times_participated_'+eventIDNum+']').parent().removeClass('has-error');
		  $('select[name=extrainfo_times_participated_'+eventIDNum+'_verify]').parent().removeClass('has-error');
		}
	  <?php
    }
    ?>
  });
})
</script>