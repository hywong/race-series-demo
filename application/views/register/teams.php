<?php
  $formitForm = array(
    "method" => "POST",
    "action" => $formAction,
    "sections" => $sections
    );

  $formitSettings = array(
      "label_columns" => "col-sm-3",
      "field_columns" => "col-sm-9",
      "class" => "customClass",
      "id" => "myForm",
      "type" => "horizontal",
	  "skip" => TRUE
    );
  // echo "<PRE>";
  // print_r($_SESSION);
  // echo "</PRE>";
?>

<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <?php echo formit($formitForm, $formitSettings); ?>
    </div>
  </div>
</div>
<script>
$(function(){

  var countTeamsExistToValidate = 0;
  var countTeamsCreatingToValidate = 0;
  allTeamsCreatingValid = true;
  allTeamsJoiningValid = true;

  $('.team-action').on('change', function(){
    countTeamsCreatingToValidate = 0;
    countTeamsExistToValidate = 0;
    $('.team-action').each(function(){
      if($(this).val() == "create")
      {
        countTeamsCreatingToValidate++;
      }

      if($(this).val() == "join")
      {
        countTeamsExistToValidate++;
      }
    });

    if($(this).val() == "join")
    {
      $(this).closest('fieldset').find('.team-create-field').val('');
      $(this).closest('fieldset').find('.team-name-create, .team-password-create').slideUp('fast', function(){
        $(this).closest('fieldset').find('.team-name-exist, .team-password-exist').slideDown();
      });
    }

    if($(this).val() == "create")
    {
      $(this).closest('fieldset').find('.team-exist-field').val('');
      $(this).closest('fieldset').find('.team-name-exist, .team-password-exist').slideUp('fast', function(){
        $(this).closest('fieldset').find('.team-name-create, .team-password-create').slideDown();
      });
    }

    if($(this).val() == "")
    {
      $(this).closest('fieldset').find('.team-create-field').val('');
      $(this).closest('fieldset').find('.team-exist-field').val('');
      $(this).closest('fieldset').find('.team-name-exist, .team-password-exist').slideUp();
      $(this).closest('fieldset').find('.team-name-create, .team-password-create').slideUp();
    }
  });

  $('.team-action').change();

  $('.team-create-field').on('blur', function(){
    if($(this).parent().parent().hasClass('has-error'))
    {
      $(this).parent().parent().removeClass("has-error");
      $("."+$(this).attr("name")+"-error").remove();
    }
  })

  $("form").on('submit', function(){
    //Validate existing team information
    if(countTeamsExistToValidate > 0)
    {
      allTeamsJoiningValid = false;
      var formData = $(this).serialize();

      missingJoinInfo = false;
      alertOnce = false;
      $('body').find('.team-action').each(function(){
        if($(this).val() == "join")
        {
          var eventID = $(this).attr('name').substr($(this).attr('name').length - 1);
          if($('select[name=eventteam_team_name_'+eventID+']').val() == "" || $('input[name=eventteam_team_password_'+eventID+']').val() == '' && !alertOnce)
          {
            alert("You selected to join a team, please fill out all the fields");
            missingJoinInfo = true;
            alertOnce = true;
          }
        }
      });

      if(missingJoinInfo)
      {
        return false;
      }

      $.ajax({
        url: "<?php echo base_url("ajax/validate_existing_team");?>",
        method: "post",
        async: false,
        data: formData
      })
      .done(function(response){
        $('.team-exist-field').each(function(){
          $(this).parent().parent().removeClass('has-error');
          $("."+$(this).attr('name')+"-error").remove();
        });
        if(response == true)
        {
          allTeamsJoiningValid = true;
        }
        
        if(response == false)
        {
          $('.team-validate-teamname').each(function(){
            if($(this).val() == '')
            {
              $(this).parent().parent().addClass('has-error');
            }
          });
          return false;
        }

        var json = $.parseJSON(response);
        $.each(json, function(k, v){
          $(':input').filter(function(){ 
            if($(this).val() == v && $(this).hasClass('team-exist-field'))
            {
              if($('input[name=eventteam_team_password_'+k+']').parent().parent().hasClass("has-error"))
              {
                $(".eventteam_team_password_"+k+"-error").remove();
              }

              $('input[name=eventteam_team_password_'+k+']').parent().parent().append("<div class=\"form-group eventteam_team_password_"+k+"-error\"><div class=\"col-sm-offset-3 col-sm-8 text-danger\">Invalid Team Password</div></div>");
              $('input[name=eventteam_team_password_'+k+']').parent().parent().addClass('has-error');
            }
           });
        });
      });

    }
    else
    {
      allTeamsJoiningValid = true;
    }

    //Validate against duplication creating new team name 
    if(countTeamsCreatingToValidate > 0)
    {
      allTeamsCreatingValid = false;

      missingCreateInfo = false;
      alertOnce = false;
      $('body').find('.team-action').each(function(){
        if($(this).val() == "create")
        {
          var eventID = $(this).attr('name').substr($(this).attr('name').length - 1);
          if($('input[name=eventteam_team_name_create_'+eventID+']').val() == "" || $('input[name=eventteam_team_password_create_'+eventID+']').val() == '' && !alertOnce)
          {
            alert("You selected to create a team, please fill out all the fields");
            missingCreateInfo = true;
            alertOnce = true;
          }
        }
      });

      if(missingCreateInfo)
      {
        return false;
      }

      var formData = $(this).serialize();
      $.ajax({
        url: "<?php echo base_url("ajax/validate_new_team"); ?>",
        method: "post",
        async: false,
        data: formData
      })
      .done(function(response)
      {
        if(response == true)
        {
          allTeamsCreatingValid = true;
        }

        if(response == false)
        {
          return false;
        }

        var json = $.parseJSON(response);
        $.each(json, function(k, v){
          $(':input').filter(function(){ 
            if($(this).val() == v && $(this).hasClass('team-create-field'))
            {
              $('input[name=eventteam_team_name_create_'+k+']').parent().parent().append("<div class=\"form-group "+$(this).attr("name")+"-error field-error\"><div class=\"col-sm-offset-3 col-sm-8 text-danger\">Team name already exists</div></div>");
              $('input[name=eventteam_team_name_create_'+k+']').parent().parent().addClass('has-error');
            }
           });
        });
      });
    }
    else
    {
      allTeamsCreatingValid = true;
    }
    
    if(allTeamsCreatingValid == true && allTeamsJoiningValid == true)
      return true;
    else
      return false;

  });
})
</script>