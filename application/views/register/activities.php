<?php

  $formitForm = array(
    "method" => "POST",
    "action" => $formAction,
    "sections" => $activities
    );
  $formitSettings = array(
      "label_columns" => "col-sm-3",
      "field_columns" => "col-sm-9",
      "class" => "customClass",
      "id" => "myForm",
      "type" => "horizontal"
    );
  
?>
<noscript>
  <center>
  <div style="border: 2px solid red; width: 600px; background-color:#FF8181; padding: 5px">
    <b>
    <h4 style="text-decoration: underline;"><?php echo translate("K359") . '</h4>' . translate("K360"); ?><span style="text-decoration: underline;"><?php echo translate("K361"); ?></span><br /><?php echo  translate("K362");?>
    please <?php echo translate("K363"); ?>
    <br />
    <?php echo translate("K364"); ?><br />
    <ul style="list-style-type: none;">
    <li style="display:inline;"> &nbsp &nbsp <a href="https://www.google.com/intl/en/chrome/browser/" target="_blank">Google Chrome</a> &nbsp &nbsp </li>
    <li style="display:inline;"> &nbsp &nbsp <a href="http://www.mozilla.org/en-US/firefox/new/" target="_blank">Mozilla FireFox</a> &nbsp &nbsp </li>
    <li style="display:inline;"> &nbsp &nbsp <a href="http://www.microsoft.com/en-us/download/details.aspx?id=13950" target="_blank">Internet Explorer 9</a> &nbsp &nbsp</li>
    </ul></b>
  </div>
  </center>
</noscript>
<div class="container" style="display:none;">
  <?php 
  if($expired === TRUE)
  {?>
  <div class="row">
    <div class="col-xs-12">
      <div class="alert alert-danger">
      <b><?php echo translate("Your session has expired. Please keep in mind your session will only remain active for 15 minutes."); ?></b>
      </div>
    </div>
  </div>
  <?php
  }
  ?>
  
  <div class="row">
  <div class="col-xs-12">
  <div class="panel panel-default">
	<div class="panel-heading"><h4><strong><?php echo translate("Important Notes");?></strong></h4></div>
	<div class="panel-body">
	<ul>
	  <li><b><?php echo translate("ALL ENTRY FEES ARE NON-REFUNDABLE, NON-TRANSFERABLE TO ANOTHER EVENT, NON-DEFERRABLE UNDER ANY CIRCUMSTANCES");?></b></li>
	  <li><?php 
		if (isset($_SESSION['language']) && $_SESSION['language'] == "fr") {
			echo "Les frais indiqu&#233;s n'incluent pas les <a href=\"".base_url("register/fee")."\" target=\"_blank\">frais de traitement en ligne</a> ou le TVH";
		}
		else {
			echo "The entry fees listed do not include the <a href=\"".base_url("register/fee")."\" target=\"_blank\">online processing fee</a> or HST";
		}
		?>
		</li>
	  <li><?php echo translate("REGISTERING MULTIPLE PARTICIPANTS: please DO NOT use the same e-mail address for each registrant to ensure you receive your separate confirmation e-mail/last minute information");?></li>
	  <li><?php echo translate("AgeError"); ?></li>
	</ul>
	</div>
  </div>
  </div>
</div>
  <div class="row">
    <div class="col-xs-12">
      <div class="well">
        <?php if($_SESSION['type'] == "individual" && count($_SESSION['registration']) <= 3)
        { ?>
        <div class="row">
          <div class="col-xs-12 text-right">
            <a class="btn btn-danger btn-sm" href="<?php echo base_url("register/group");?>"><?php echo translate("Register a group");?> (5+)</a>
          </div>
        </div>
        <br />
        <?php 
        }
        ?>
        <?php echo formit($formitForm, $formitSettings); ?>
      </div>
    </div>
  </div>
</div>
<script>
$(function(){
  $('.container').show();
  $('.view-pricing').on('click', function(){
    if($(this).data('state') == "")
    {
      $(this).data("state", "hide");
      $(this).html("<i class=\"glyphicon glyphicon-minus\"></i> <?php echo translate('Hide Pricing and Info');?>");
    }
    else
    {
      $(this).data("state", "");
      $(this).html("<i class=\"glyphicon glyphicon-plus\"></i> <?php echo translate('View Pricing and Info');?>");
    }


    $(this).parent().siblings('.pricing-table').toggle();
  });
  
  $('#myForm').on('submit', function(){
	var valSuccess = false;
	var elements = document.querySelectorAll("input[name*='race_event_']:checked");
	
	for(var i = 0 ; i < elements.length; i++)
	{
		if($(elements[i]).val() != ""){
			valSuccess = true;
		}
	}
	
	if(valSuccess == false){
		alert("<?php echo translate('Please choose an event to register for.'); ?>");
	}
	
	return valSuccess;
	
  });
});
</script>