<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <div class="row">
        <table class="table table-striped" id="addon-cart-summary">
          <thead class="panel panel-default no-border">
            <tr>
              <th colspan="5" class="text-center"><h4><strong><?php echo translate("My Addons"); ?></strong></h4></th>
            </tr>
            <tr class="panel-heading border-sides">
              <th width="20%"><?php echo translate("Location"); ?></th><th width="20%"><?php echo translate("Item"); ?></th><th width="20%"><?php echo translate("Price"); ?></th><th width="20%"><?php echo translate("Qty"); ?></th><th width="10%"><?php echo translate("Action");?></th>
            </tr>
          </thead>
          <tbody class="panel panel-default">
            <?php
            foreach($addons as $eventID => $addonList)
            {
              foreach($addonList as $addon)
              {
                $addonParsed[$addon['addonID']]['name'] = $addon['addon'];
                $addonParsed[$addon['addonID']]['price'] = $addon['price'];
              }
            }

            if(isset($_SESSION['registration'][$_SESSION['registration']['active_user']]['addons']))
            {
              foreach($_SESSION['registration'][$_SESSION['registration']['active_user']]['addons'] as $eID => $addonByActivity)
              {
                foreach($addonByActivity as $addonID => $addonList)
                {
                  $c = count($addonList);
                  foreach($addonList as $k => $addonItem)
                  {
                    echo "<tr id=\"".uniqid()."\"><td>".$_SESSION['general'][$eID]['display']."</td><td>".$addonParsed[$addonID]['name']."</td><td>$".number_format($addonParsed[$addonID]['price'] * $addonItem['qty'], 2, '.' ,'')."</td><td>".$addonItem['qty']."</td><td><button data-cart-addon-eid=\"".$eID."\" data-cart-addon-aid=\"".$addonID."\" data-cart-addon-id=\"".$k."\" class=\"remove-addon btn btn-xs btn-danger\">Remove</button></td></tr>";
                  }
                }
              }
            }
            ?>
          </tbody>
        </table>
      </div>
      <div class="row">
        <?php
        foreach($addons as $eventID => $addonList)
        {
          echo "<fieldset><legend>".$_SESSION['general'][$eventID]['display']." Addons</legend>";
          $c = 0;
          foreach($addonList as $addon)
          {
            if($c == 0)
            {
              echo "<div class=\"row\">";
            }
			$num_decimals = (intval($addon['price']) == $addon['price']) ? 0 :2;
            echo "
            <div class=\"col-sm-4\">
              <div class=\"well\">
			  
                <h4>".$addon['title']."</h4>
                Item: ".$addon['addon']." ($".number_format($addon['price'],$num_decimals).") <br /><br /><p>";
                $addonImg = base_url('assets/img/addons/'.$eventID.'_'.$addon['addonID'].'.png');
                if(@getimagesize($addonImg))
                {
                  echo "<img src=\"".$addonImg."\" alt=\"".$addon['addon']."\" title=\"".$addon['addon']."\" class=\"img-responsive\" />";
                }
            echo '<br />'.$addon['description']."</p>
                <p>
                <form class=\"addon-details\">
                ";
                if(isset($addon['fields']))
                {
                  foreach($addon['fields'] as $field)
                  {
                    formit_build_field($field, NULL, NULL, NULL);
                  }
                }
            echo "
                 </form>
                <button data-event-id=\"".$addon['eventID']."\" data-addon-id=\"".$addon['addonID']."\" type=\"button\" class=\"add-addon btn btn-success btn-sm\">" . translate("Add to cart") . "</button>
                </div>
            </div>";
            $c++;
            if($c == 3)
            {
              $c = 0;
              echo "</div>";
            }
          }
          if($c > 0 && $c < 3)
          {
            echo "</fieldset></div>";
          }
          echo "<div class=\"clearfix\"></div><br />";
        }
        ?>
      </div>
      <div class="row">
        <div class="col-sm-offset-3 col-sm-6">
          <a href="confirmation" class="btn btn-block btn-success"><?php echo translate("Submit"); ?></a>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(function(){
  $("body").on('click', '.add-addon', function(){
    $(this).siblings('.addon-details').find(':input').each(function(){
      if($(this).val() == "" || $(this).val() == 0)
      {
        alert("You must select options on your addon.");
        return false;
      }
    });
    $.post("<?php echo base_url("ajax/cart_add_addon"); ?>", {addonID: $(this).data("addon-id"), eventID: $(this).data("event-id"), details: $(this).siblings('.addon-details').serializeObject()}, function(response){

      if(response != false)
      {
        $('table > tbody').append(response);
      }
      else
      {
        alert("You've reached the limit of this item per participant or the item is now sold out.");
      }
    });
    $(this).siblings('.addon-details').trigger("reset");
  });

  $("#addon-cart-summary").on('click', '.remove-addon', function(){
    $.post('<?php echo base_url("ajax/cart_remove_addon"); ?>', {rowID: $(this).parent().parent().attr("id"), cartAddonPos: $(this).data("cart-addon-id"), cartAddonID: $(this).data("cart-addon-aid"), cartAddonEvent: $(this).data("cart-addon-eid")}, function(response){
      if(response != false)
      {
        $('#'+response).remove();
      }
    });
  });
});
</script>