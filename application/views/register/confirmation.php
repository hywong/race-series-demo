<?php
  // echo "<PRE>";
  // print_r($summary);
  // echo "</PRE>";
  // exit;
?>

<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <div class="row">
        <div class="col-xs-12">
          <div class="alert alert-info text-center">
            <i class="glyphicon glyphicon-exclamation-sign"></i> <strong><?php echo translate("To edit your information, please navigate using the steps above. Don't forget to submit your changes after editing a step."); ?></strong>
          </div>
        </div>
      </div>
	  <div class="well">
          <fieldset>
           <legend><?php echo translate("Registrations"); ?></legend>
          
          <?php
          if(isset($summary['Activities']))
          {
            foreach($summary['Activities'] as $event => $activityIDs)
            {
              echo "<strong>".translate($event)."</strong>";
              echo "<ul class='activity-list-display'>";
              foreach($activityIDs as $id)
              {
                echo "<li>".$_SESSION['general']['activities'][$id]."</li>";
              }
              echo "</ul>";
            }
          }
          ?>

		  </fieldset>
      </div>
      <div class="well">
          <fieldset>
           <legend><?php echo translate("Personal Information"); ?></legend>
          <table class="table table-condensed table-hover summary-table">
          <thead>
            <tr><th><?php echo translate("Field"); ?></th><th><?php echo translate("Input"); ?></th></tr>
          </thead>
          <tbody>
            <?php
            foreach($summary['Personal Info'] as $field => $value)
            {
              if(is_array($value))
              {
                $value = implode(', ', $value);
              }
              echo "<tr><td class=\"half-width\">".translate(ucwords(str_replace("_", " ", $field)))."</td><td class=\"half-width\">".$value."</td></tr>";
            }
            ?>
          </tbody>
          </table>
		  </fieldset>
        </div>
        <?php
        foreach($summary as $event => $subsection)
        {
          if($event != "Activities" && $event != "Personal Info")
          {
        ?>
          <div class="well">
			<fieldset>
            <legend><?php echo translate($event);?> - <?php echo translate("Details"); ?></legend>
            <?php
            foreach($subsection as $section => $details)
            {
            ?>
            <h4 class="text-left"><strong><?php echo $section;?></strong></h4>
            <table class="table table-condensed table-hover summary-table">
              <thead>
                <tr><th><?php echo translate("Field"); ?></th><th><?php echo translate("Input"); ?></th></tr>
              </thead>
              <tbody>
                <?php
                foreach($details as $field => $value)
                {
                  if($value != '')
                  echo "<tr><td class=\"half-width\">".translate(ucwords(str_replace("_", " ", $field)))."</td><td class=\"half-width\">".$value."</td></tr>";
                }
                ?>
              </tbody>
            </table>
            <?php
            }
            ?>
			</fieldset>
          </div>
          <?php
          }
        }
        ?>
        <div class="row">
          <div class="col-sm-offset-3 col-sm-5">
            <a href="<?php echo base_url("payment");?>" class="btn btn-block btn-success"><i class="glyphicon glyphicon-ok"></i> <?php echo translate("Confirm"); ?></a>
          </div>
        </div>
    </div>
  </div>
</div>