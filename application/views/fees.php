<div class="container">
  <div class="row">
    <div class="col-sm-offset-3 col-sm-6">
      <div class="well">
        <table class="table table-hover">
          <thead>
            <tr>
              <th><?php echo translate("Online Purchase"); ?></th><th><?php echo translate("Online Processing Fee"); ?></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>$0.01 <?php echo translate("to"); ?> $20.00</td><td>$2.50 + <?php echo translate("Applicable taxes"); ?></td>
            </tr>
            <tr>
              <td>$20.01 <?php echo translate("to"); ?> $30.00</td><td>$3.00 + <?php echo translate("Applicable taxes"); ?></td>
            </tr>
            <tr>
              <td>$30.01 <?php echo translate("to"); ?> $40.00</td><td>$3.50 + <?php echo translate("Applicable taxes"); ?></td>
            </tr>
            <tr>
              <td>$40.01 <?php echo translate("to"); ?> $50.00</td><td>$4.00 + <?php echo translate("Applicable taxes"); ?></td>
            </tr>
            <tr>
              <td>$50.01 <?php echo translate("to"); ?> $60.00</td><td>$4.50 + <?php echo translate("Applicable taxes"); ?></td>
            </tr>
            <tr>
              <td>$60.01 <?php echo translate("to"); ?> $70.00</td><td>$5.00 + <?php echo translate("Applicable taxes"); ?></td>
            </tr>
            <tr>
              <td>$70.01 <?php echo translate("to"); ?> $80.00</td><td>$5.50 + <?php echo translate("Applicable taxes"); ?></td>
            </tr>
            <tr>
              <td>$80.01 <?php echo translate("to"); ?> $90.00</td><td>$6.00 + <?php echo translate("Applicable taxes"); ?></td>
            </tr>
            <tr>
              <td>$90.01 <?php echo translate("to"); ?> $100.00</td><td>$6.50 + <?php echo translate("Applicable taxes"); ?></td>
            </tr>
            <tr>
              <td>$100.01 <?php echo translate("and over"); ?></td><td>6.5% + <?php echo translate("Applicable taxes"); ?></td>
            </tr>
          </tbody>
        </table>
        <br />
        <a href="<?php echo base_url("/"); ?>" class="btn btn-default"><span class="glyphicon glyphicon-circle-arrow-left"></span> <?php echo translate("K223"); ?></a>
      </div>
    </div>
  </div>
</div>