<nav class="navbar" role="navigation" id="navigationBar">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#template-navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" target="_blank" href="https://secure.eventsonline.ca"><img src="<?php echo base_url('assets/img/eol_logo_sm.png');?>" alt="EventsOnline" title="Powered by EventsOnline" /></a>
    </div>                   
    <div class="collapse navbar-collapse" id="template-navbar-collapse">
    <ul class="nav navbar-nav">
      <li><a href="<?php echo base_url("participant"); ?>"><i class="glyphicon glyphicon-user"></i> <?php echo translate("Participant Dashboard"); ?></a></li>
      <li><a href="<?php echo base_url("confirmation/resend"); ?>"><span class="glyphicon glyphicon-envelope"></span> <?php echo translate("Resend Confirmation"); ?></a></li>
	  <li><a href="<?php echo base_url("confirmation/participant_search");?>"><i class="glyphicon glyphicon-search"></i> <?php echo translate("Search Participant"); ?></a>
    </ul>
	  <ul class="nav navbar-nav navbar-right">
		
	  <div class="clearfix hidden-lg hidden-xl"></div>
	  <li>
		</li>
		<li>
    <?php
    if(isset($_SESSION['language']))
    {
      if($_SESSION['language'] == "en")
      {
        echo "<a href=\"?lang=fr\">Français</a>";
      }
      else if ($_SESSION['language'] == "fr")
      {
        echo "<a href=\"?lang=en\">English</a>";
      }
      else
      {
        echo "<a href=\"?lang=fr\">Français</a>";
      }
    }
    else
    {
      echo "<a href=\"?lang=fr\">Français</a>";
    }
    ?>
		</li>
   <li>
       <a href="#" id="techsupport-btn" data-toggle="modal" data-target="#techsupport-form"><i class="glyphicon glyphicon-comment"></i> <?php echo translate("Contact Techsupport"); ?></a>
     </li>
		<li>&nbsp;&nbsp;&nbsp;</li>
	  </ul>
    </div>
  </div><!-- /.container-fluid -->
</nav>
<div class="clearfix"></div>