<script>	
	$(document).ready(function () {
		$("#frmResend").validate();
	});
</script>
<style>
	.error {
		color: red;
		font-style: italic;
	}
	
	.alert {
		text-align: center;
		font-weight: bold;
	}
</style>
<script src="<?php echo base_url("assets/js/jquery.validation/jquery.validate.min.js");?>"></script>
<div class="col-xs-12">
	
	<div class="col-xs-8 col-xs-offset-2 text-center">
		<h4><strong><?php echo translate("K11"); ?></strong></h4>
	</div>
	<br /><br /><br />
	<?php 
	
	if (isset($msgDanger)) {
		echo "<div class=\"alert alert-danger\">" . $msgDanger . "</div>";
	}
	else if (isset($msgSuccess)) {
		echo "<div class=\"alert alert-success\">" . $msgSuccess . "</div>";
	}
		

	?>
		
		<form id="frmResend" action='resend_process' class="form-horizontal" method='post'>			
			<div class='form-group'>
				<label class="control-label col-sm-3" for="inpEmail">
						<?php echo translate("K251"); ?>
				</label>
				<div class="col-sm-8">
					<input type='text' class='form-control required' name='email' id='inpEmail'/>
				</div>
			</div>
			
			<?php echo "      
				<div class=\"form-group\">
					<label class=\"col-sm-3 control-label\" for=\"year_of_birth\"><span class=\"req\">*</span>". translate("K37") ."</label>					
					<div class=\"col-sm-3\">
						<div class=\"input-group\">
					<span class=\"input-group-addon\">". translate("K38") ."</span>
					<select name=\"year_of_birth\" id=\"year_of_birth\" class=\"form-control required\">
						<option></option>";
						//fast way to show the birth dates options
						$last_birth_date = date("Y");
						for ($i = 0; $i <= 93; $i++) {        
							echo '<option value="' . $last_birth_date . '">' . $last_birth_date . '</option>';
							$last_birth_date--;
						}
    echo "			</select>
						</div>
					</div>
					<div class=\"col-sm-3\">
						<div class=\"input-group\">
							<span class=\"input-group-addon\">" . translate("K39") . "</span>
							<select id=\"month_of_birth\" name=\"month_of_birth\" class=\"form-control required\">
								<option></option>
								<option value=\"01\">" . translate("K40") . "</option>
								<option value=\"02\">" . translate("K41") . "</option>
								<option value=\"03\">" . translate("K42") . "</option>
								<option value=\"04\">" . translate("K43") . "</option>
								<option value=\"05\">" . translate("K44") . "</option>
								<option value=\"06\">" . translate("K45") . "</option>
								<option value=\"07\">" . translate("K46") . "</option>
								<option value=\"08\">" . translate("K47") . "</option>
								<option value=\"09\">" . translate("K48") . "</option>
								<option value=\"10\">" . translate("K49") . "</option>
								<option value=\"11\">" . translate("K50") . "</option>
								<option value=\"12\">" . translate("K51") . "</option>
							</select>
						</div>
					</div>
					<div class=\"col-sm-2\">
						<div class=\"input-group\">
						<span class=\"input-group-addon\">".translate("K313")."</span>
						<select id=\"date_of_birth\" name=\"date_of_birth\" class=\"form-control required\">
							<option></option>
							<option value=\"01\">01</option>
							<option value=\"02\">02</option>
							<option value=\"03\">03</option>
							<option value=\"04\">04</option>
							<option value=\"05\">05</option>
							<option value=\"06\">06</option>
							<option value=\"07\">07</option>
							<option value=\"08\">08</option>
							<option value=\"09\">09</option>
							<option value=\"10\">10</option>
							<option value=\"11\">11</option>
							<option value=\"12\">12</option>
							<option value=\"13\">13</option>
							<option value=\"14\">14</option>
							<option value=\"15\">15</option>
							<option value=\"16\">16</option>
							<option value=\"17\">17</option>
							<option value=\"18\">18</option>
							<option value=\"19\">19</option>
							<option value=\"20\">20</option>
							<option value=\"21\">21</option>
							<option value=\"22\">22</option>
							<option value=\"23\">23</option>
							<option value=\"24\">24</option>
							<option value=\"25\">25</option>
							<option value=\"26\">26</option>
							<option value=\"27\">27</option>
							<option value=\"28\">28</option>
							<option value=\"29\">29</option>
							<option value=\"30\">30</option>
							<option value=\"31\">31</option>
						</select>
						</div>
					</div>
                </div>
			";?>
			<div class="clearfix"></div>
			<div class='col-sm-4 col-sm-offset-4'>
				<button type='submit' class='btn btn-block btn-success'><span class="glyphicon glyphicon-search"></span> <?php echo translate("K566"); ?></button>
			</div>
		</form>

		
		
	<?php 

	if (isset($_SESSION['participant'])) {
	
	?>
		<div class="clearfix"></div>
		<br />	
		<a href="<?php echo base_url("participant/"); ?>" class="btn btn-default"><span class="glyphicon glyphicon-circle-arrow-left"></span> <?php echo translate("K85"); ?></a>
	<?php 
	
	} 
	
	?>
	
	<div class="clearfix"></div>
	<br />	
	<a href="<?php echo base_url(""); ?>" class="btn btn-default"><span class="glyphicon glyphicon-circle-arrow-left"></span> <?php echo translate("K1004"); ?></a>
</div>