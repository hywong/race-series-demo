<div class="col-xs-12">
	<h4><?php echo translate("K12"); ?></h4>
	<?php
	
		echo translate("K568") . ' <a href="participant_search">' . translate("K569") . '</a><br /><br />';					
		
		echo "
		<div class=\"table-responsive\">
			<table class='table table-striped table-hover'>
				<thead>
					<tr>
						<th>" . translate("K571") . "</th>
						<th>" . translate("K68") . "</th>
						<th>" . translate("K572") . "</th>
					</tr>
				</thead>
				<tbody>";
				
			$countParticipantList = count($participantList);
			for ($i = 0; $i < $countParticipantList; $i++) {
		echo "
				<tr>
					<td>
						" . $participantList[$i]['strFirstName'] . " " . $participantList[$i]['strLastName'] . "
					</td>
					<td>" . $participantList[$i]['strActivity']  . "</td>
					<td>";
						if ($participantList[$i]['intPledgeProfileID'] == "") {
							echo translate("K574");
						}
						else {
							echo "<a class=\"btn btn-default btn-sm\" href='".base_url("pledge/profile")."/". $participantList[$i]['intPledgeProfileID'] . "'>" . translate("View Profile") . "</a>";
						}
		echo "
					</td>
				</tr>";
			}
			
		echo "
				</tbody>
			</table>
		</div>";
		
	?>
	<div class="clearfix"></div>
	<br />	
	<a href="../" class="btn btn-default"><span class="glyphicon glyphicon-circle-arrow-left"></span> <?php echo translate("K223"); ?></a>
</div>
<div class="clearfix"></div>