<?php
/*
require_once 'config/main.config.php';

session_name($siteSessionName);
session_start();

require_once 'includes/helper_functions.php';
require_once 'translate/translate.php';

$pageSearch = "class=\"active\"";
include 'header.php';

if ($showBanner) {
    include 'banner.php';
}
*/
?>
<script>
	$(document).ready(function () {
		$("#myform").validate();
	});
</script>
<div class='col-xs-12'>
	<div class="col-sm-6 col-sm-offset-3">
		<div class="well">
			<h3 class='main_ind_title'><?php echo translate("K427"); ?></h3>
			<?php
				if (isset($msgDanger)) {
					echo "<div class=\"alert alert-danger\">" . $msgDanger . "</div>";
				}
				else if (isset($msgSuccess)) {
					echo "<div class=\"alert alert-success\">" . $msgSuccess . "</div>";
				}
			?>
			
			<p><?php echo translate("Enter the first or last name of the registration you would like to pledge/search"); ?></p>
			<form id="myform" name="myform" method="post" action="participant_list">
				<div class="form-group">
					<label for="searchtype"><?php echo translate("K429"); ?></label>
					<select name="searchtype" id="searchtype" class="form-control">
						<option value="last_name"><?php echo translate("K431"); ?></option>
						<option value="first_name"><?php echo translate("K432"); ?></option>
					</select>
				</div>
				<div class="form-group">
					<label for="search"><?php echo translate("K430"); ?></label>
					<input type="text" name="search" class="form-control required" id="search">
				</div>
				<div class="form-group">
					<button class="btn btn-success btn-block"><span class="glyphicon glyphicon-search"></span> <?php echo translate("K434"); ?></button>
				</div>
			</form>
		</div>
	</div>
	<div class="clearfix"></div>
	<br />	
	<a href="<?php echo base_url(""); ?>" class="btn btn-default"><span class="glyphicon glyphicon-circle-arrow-left"></span> <?php echo translate("K223"); ?></a>
</div>