<div class="col-xs-12">
	<div class="col-xs-8 col-xs-offset-2 text-center text-bold">
		<h4><strong><?php echo translate("A confirmation email has been resent."); ?></strong></h4>
	</div>

	<div class="col-sm-8 col-sm-offset-2">
		<table class="table table-striped">
			<thead>
				<tr>
					<th><?php translate("Field"); ?></th>
					<th><?php translate("Details"); ?></th>
				</tr>
			</thead>
			<tbody>
	<?php
			
			foreach ($labelLinker as $label => $linker) {
				if (trim($personalInformation[$linker]) != "") {
					echo '
						<tr>
							<td>' . translate($label) . ':</td>
							<td>' . $personalInformation[$linker] . '</td>
						</tr>';
				}
			}
		
	?>
			</tbody>
		</table>
	</div>
	<?php 

		if (isset($_SESSION['participant'])) {
		
	?>
			<div class="clearfix"></div>
			<br />	
			<a href="<?php echo base_url("participant/"); ?>" class="btn btn-default"><span class="glyphicon glyphicon-circle-arrow-left"></span> <?php echo translate("K85"); ?></a>
	<?php 

		} 
		
	?>
		<div class="clearfix"></div>
		<br />	
		<a href="<?php echo base_url(""); ?>" class="btn btn-default"><span class="glyphicon glyphicon-circle-arrow-left"></span> <?php echo translate("K1004"); ?></a>
</div>
<div class="clearfix"></div>