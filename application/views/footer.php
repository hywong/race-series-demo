</div><!-- End #wrap -->
<div class="eol-footer-container">
  <div class="well well-sm eol-footer text-center">
    <a class="link-muted" href="http://www.eventsonline.ca/" target="_blank">Powered by www.EventsOnline.ca</a>
  </div>
</div><script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script src="<?php echo base_url("assets/js/jquery.validation/jquery.validate.min.js");?>"></script>
<script src="<?php echo base_url("assets/js/jquery.validation/additional-methods.min.js");?>"></script>
<script src="<?php echo base_url("assets/js/jquery.validation/custom.validation.js");?>"></script>
<script src="<?php echo base_url("assets/js/bootstrap-datepicker.js");?>"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-6190158-1', 'canadarunningseries.com');
  ga('send', 'pageview');
</script>
<script type='text/javascript'>
	$(function(){
		
		$('#box_form').on('blur', 'input[name=z_subject], input[name=z_requester], input[name=z_name], textarea[name=z_description], select[name=z_type]' ,function(){
			if($(this).val() == ''){
				$(this).parent('.control-group').addClass('error');
			}else{
				$(this).parent('.control-group').removeClass('error');
			}
		});
		
		

		$('#sendform').on('click', validateForm);
		
		function validateForm(){
			
			var subject = $('input[name=z_subject]').val();
			var email = $('input[name=z_requester]').val();
			var name = $('input[name=z_name]').val();
			var msg = $('textarea[name=z_description]').val();
			var loading = $('#loading');
			var event = $('input[name=event]').val();
			var type = $('select[name=z_type]').val();
			var invoice = $('input[name=z_invoice]').val();
			var valid = false;
			var validEmail = isValidEmailAddress(email);
						
			if(subject == ''){
				$('input[name=z_subject]').focus();
				$('input[name=z_subject]').siblings('.alert').show();
			}
			if(email == '' || validEmail == false){
				alert('Please enter a valid email.');
				$('input[name=z_requester]').focus();
				$('input[name=z_requester]').siblings('.alert').show();
			}
			if(name == ''){
				$('input[name=z_name]').focus();
				$('input[name=z_name]').siblings('.alert').show();
			}
			if(type == ''){
				$('select[name=z_type]').focus();
				$('select[name=z_type]').siblings('.alert').show();
			}
			if(msg == ''){
				$('textarea[name=z_description]').focus();
				$('textarea[name=z_description]').siblings('.alert').show();
			}
			
			if(subject != '' && validEmail && name != '' && msg != '' && type != ''){
				valid = true;
			}
			
			if(valid) {
				// $('#box_form').modal('hide');
				$('#loading').addClass('well').html("<img src='<?php echo base_url("assets/img/loader.gif");?>' alt='loading' style='text-align:center' />&nbsp;&nbsp;&nbsp;&nbsp;Sending, please wait...");
				$('#loading').show();
							
				$.post('<?php echo base_url("ajax/sendTechsupport"); ?>', {subject: subject, email: email, name: name, msg: msg, event: event, type: type, invoice: invoice}, function(data){
					if(data == "true"){
						$('#loading').removeClass().addClass('well alert-success').html('Message sent!');

					} else {
						$('#loading').removeClass().addClass('well alert-error').html('Unable to send message.');
						
					}
				});
			}
			
		}
		
		function isValidEmailAddress(emailAddress) {
			var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
			return pattern.test(emailAddress);
		}
		
		
	});    
	
</script>
<script>
$(function(){
  $('.date-picker').datepicker({
    format: 'yyyy/mm/dd',
    startView: 2,
    autoclose: true
  });
});

function resetForm(myform) {
	myform.find('input:text, input:password, input:file, select, textarea').val('');
	$('#loading').html('');
	myform.find('input:radio, input:checkbox')
		 .removeAttr('checked').removeAttr('selected');
}
</script>
<div class="modal fade" id="techsupport-form" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="box_form">
            <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel"><?php echo translate("Contact Tech Support"); ?></h4>
            </div>
            <div class="modal-body">
				<div id="loading"></div>
                <form id='zForm' role="form" method='post'>
				<input type='hidden' value='Race Series Demo 2015' name='event' />
				<br />
				<div class='form-group'>
					<label for='inputEmail'>
						<span class='req'>*</span> 
						<?php echo translate("Email (for us to reply to)"); ?>
					</label>
					<input type='text' name='z_requester' class="form-control" id='inputEmail' placeholder='Email'>
				</div>
				<div class='form-group'>						
					<label for='inputFname'>
						<span class='req'>*</span>
						<?php echo translate("Full Name"); ?>
					</label>
					<?php echo "<input type='text' class='form-control' name='z_name' id='inputFname' placeholder='".translate("Name")."'>"; ?>
				</div>
				<div class='form-group'>
					<label for='selectType'>
						<span class='req'>*</span>
						<?php echo translate("Type of Issue"); ?>
					</label>
					<select name='z_type' class="form-control" id='selectType'>
						<option></option>
						<option>
						<?php echo translate("General Inquiry"); ?>
						</option>
						<option>
						<?php echo translate("Registration"); ?>
						</option>
						<option>
						<?php echo translate("Pledge"); ?>
						</option>
						<option>
						<?php echo translate("Confirmation"); ?>
						</option>
						<option>
						<?php echo translate("Bug/Error Report"); ?>
						</option>
						<option>
						<?php echo translate("Other"); ?>
						</option>
					</select>
				</div>
				<div class='form-group'>
					<label for='inputJME'>						
						<?php echo translate("JME/Invoice # (if you have it)"); ?>
					</label>						
					<input type='text' class="form-control" name='z_invoice' id='inputJME' placeholder='JME#######'>
				</div>
				<div class='form-group'>						
					<label for='inputSubject'>
						<span class='req'>*</span>
						<?php echo translate("Subject"); ?>:
					</label>
					<?php echo "<input name='z_subject' class=\"form-control\" id='inputSubject' type='text' placeholder='".translate("Subject")."'/>"; ?>
				</div>
				<div class='form-group'>
					<label for='textMsg'>
						<span class='req'>*</span>
						<?php echo translate("Message"); ?>:
					</label>
						<?php echo "<textarea style='height:4em' class=\"form-control\" id='textMsg' name='z_description' placeholder=\"".translate("I'm having issues with.../I would like to know...")."\"></textarea>"; ?>
						
				</div>
			</form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" onClick="resetForm($('#zForm'));"><?php echo translate("Close"); ?></button>
                <button type="button" class="btn btn-primary" id='sendform'><?php echo translate("Send"); ?></button>
        </div>
    </div>
  </div>
</div>

</body>

</html>
