<?php
  if(count($_SESSION['registration']) > 3)
  {
?>
<nav class="navbar navbar-fixed-bottom active-cart" role="navigation">
  <div class="container-footer-cart">
    <ul class="nav navbar-nav">
      <li class="text-center<?php if($_SESSION['cart-state']) echo ' cart-visible';?>" id="cart-toggle">
        <button type="button" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-shopping-cart"></i> <span class="cart-btn-text"><?php ($_SESSION['cart-state'] == TRUE ? $state = "Hide" : $state = "View"); echo $state." my cart";?></span></button>
      </li>
    <?php
      foreach($_SESSION['registration'] as $id => $participant)
      {
        if($id != "last_modified" && $id != "active_user")
        {
          if(isset($participant['personal']['first_name']) && isset($participant['personal']['last_name']))
          {
            echo "<li class=\"text-center\"><a href=\"#\" class=\"cart-registration\" data-cart-id=\"".$id."\"><i class=\"glyphicon glyphicon-user\"></i> <br />".$participant['personal']['first_name']." ".$participant['personal']['last_name']."</a></li>";
          }
        }
      }
    ?>
    </ul>
  </div> 
</nav>
<?php
  }
?>
<script>
  $(function(){
    $(".active-cart .cart-registration").on('click', function(e){
      e.preventDefault();
      $.post("<?php echo base_url("ajax/change_cart"); ?>"+"/"+$(this).data('cart-id'), function(response ){
        if(response == true)
        {
          location.reload();
        }
        else
        {
          alert("An error occurred, that person is no longer in your cart.");
        }
      });
    });

    if($("#cart-toggle").hasClass("cart-visible"))
    {
      $('.active-cart').css("bottom", "0px");
    }

    $(".active-cart").on('click', '#cart-toggle', function(e){
      var selected = $(this).hasClass('cart-visible');

      $.post("<?php echo base_url("ajax/set_cart_state"); ?>", function(response){});

      $(e.delegateTarget).stop().animate({
        bottom: selected ? "-100px" : "0px"
      });

      $("#cart-toggle .cart-btn-text").html(selected ? "View my cart" : "Hide my cart");
      $(this).toggleClass("cart-visible", !selected);
    });
  });
</script>