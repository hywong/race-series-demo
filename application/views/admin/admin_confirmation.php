<?php
  // echo "<PRE>";
  // print_r($summary);
  // echo "</PRE>";
  // exit;
?>

<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <div class="row">
        <div class="col-xs-12">
          <div class="alert alert-info text-center">
            <i class="glyphicon glyphicon-exclamation-sign"></i> <b>To edit your information, please navigate using the steps above. Don't forget to submit your changes after editing a step.</b>
          </div>
        </div>
      </div>
      <div class="well">
        <div class="well">
          <h3>Registrations</h3>
          <ul>
          <?php
          if(isset($summary['activities']))
          {
            foreach($summary['Activities'] as $event => $activityIDs)
            {
              echo "<li>".$event."</li>";
              echo "<ul>";
              foreach($activityIDs as $id)
              {
                //TODO: Get activity name
                echo "<li>".$id."</li>";
              }
              echo "</ul>";
            }
          }
          ?>
          </ul>
        </div>
        <div class="well">
          <h3>Personal Information </h3>
          <table class="table table-condensed table-hover">
          <thead>
            <tr><th>Field</th><th>Input</th></tr>
          </thead>
          <tbody>
            <?php
            foreach($summary['Personal Info'] as $field => $value)
            {
              if(is_array($value))
              {
                $value = implode(', ', $value);
              }
              echo "<tr><td class=\"half-width\">".ucwords(str_replace("_", " ", $field))."</td><td class=\"half-width\">".$value."</td></tr>";
            }
            ?>
          </tbody>
          </table>
        </div>
        <?php
        foreach($summary as $event => $subsection)
        {
          if($event != "Activities" && $event != "Personal Info")
          {
        ?>
          <div class="well">
            <h3><?php echo $event;?> - Details</h3>
            <?php
            foreach($subsection as $section => $details)
            {
            ?>
            <div class="well">
            <h4><?php echo $section;?></h4>
            <table class="table table-condensed table-hover">
              <thead>
                <tr><th>Field</th><th>Input</th></tr>
              </thead>
              <tbody>
                <?php
                foreach($details as $field => $value)
                {
                  if($value != '')
                  echo "<tr><td class=\"half-width\">".ucwords(str_replace("_", " ", $field))."</td><td class=\"half-width\">".$value."</td></tr>";
                }
                ?>
              </tbody>
            </table>
            </div>
            <?php
            }
            ?>
          </div>
          <?php
          }
        }
        ?>
        <form class="form-horizontal" method="post" action="finalize">
          <div class="form-group">
            <label for="total" class="col-sm-3 control-label">
            <?php echo translate('Payment Amount');?>
            </label>
            <div class="col-sm-6">
              <input type="text" name="total" id="total" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
              <button class="btn btn-block btn-success"><i class="glyphicon glyphicon-ok"></i> Confirm</button>
            </div>          
          </div>
        </form>
      </div>
    </div>
  </div>
</div>