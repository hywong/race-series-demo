<div id="wrapper">
	<div id="mainContainer" class="col-xs-12">
		<div class="row">
			<div class="col-xs-12">
				<div class="well well-sm text-center">
					<h4><?php echo translate("K966"); ?></h4>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<table id="confirmtbl" class="tablesorter table table-striped table-hover">
					<thead>
					<tr>
						<th><?php echo translate("K436"); ?></th>
						<th><?php echo translate("K281"); ?></th>
						<th><?php echo translate("Activity"); ?></th>						
					</tr>
					</thead>
					<tbody>
					<?php
					$countConfirmationList = count($confirmationList);
					for ($i = 0; $i < $countConfirmationList; $i++) {
						echo "<tr>
							<td>" . $confirmationList[$i]['strFirstName'] . "</td>
							<td>" . $confirmationList[$i]['strLastName'] . "</td>
							<td>" . $confirmationList[$i]['strActivity'] . "</td>
						</tr>";
					}
					
					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>