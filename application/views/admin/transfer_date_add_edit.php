<?php

	$transferOpenDate = "";
	$transferCloseDate = "";
	$submitButton = translate("K830");
	if (isset($formType) && $formType == "edit") {
		$transferOpenDate = $transferAvailabilityDates['dttTransferStartDate'];
		$transferCloseDate = $transferAvailabilityDates['dttTransferEndDate'];
		$submitButton = translate("K876");
	}
	
?>
<script>
	$(document).ready(function() {	
	
		$('#txtTransferOpenDate').datetimepicker({ language: 'en', pick12HourFormat: false });
		$('#txtTransferCloseDate').datetimepicker({ language: 'en', pick12HourFormat: false });
		
		jQuery.validator.addMethod("validateDates", function (value, element) {
			var openDate = $("#txtTransferOpenDate").val();
			var closeDate = $("#txtTransferCloseDate").val();
			var openTime = $("#txtTransferOpenTime").val();
			var closeTime = $("#txtTransferCloseTime").val();
			var newOpenDate = new Date(openDate + " " + openTime);
			var newCloseDate = new Date(closeDate + " " + closeTime);		
			
			if (newOpenDate > newCloseDate) {
				return false;
			}
			else {
				return true;
			}
		}, $("#hidInvalidDates").val());						
		
		$("#myform").validate({
			rules: {
				txtTransferCloseDate: {
					validateDates: true
				}				
			}
		});
	});
</script>
<style>

	.form-control[readonly] {
		background-color: white;
		cursor: auto;
	}

</style>
<div id="wrapper">
	<div id="mainContainer" class="col-xs-12">
		<div class="row">
			<div class="col-xs-12">
				<div class="well well-sm text-center">
					<h4><?php echo translate("Add Transfer Tool Availability Dates"); ?></h4>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<div class="well">
					<form method="POST" action="<?php echo base_url("admin/transfer_date_" . $formType); ?>" id="myform" name="myform">
						<input type="hidden" id="hidInvalidDates" name="hidInvalidDates" value="<?php echo translate("The Transfer Close Date cannot be before the Transfer Open Date"); ?>">
						<div class="form-group">
							<label><?php echo translate("Transfer Open Date"); ?> (EST)</label>
							<input type="text" id="txtTransferOpenDate" name="txtTransferOpenDate" class="form-control required" value="<?php echo $transferOpenDate; ?>" readonly />
						</div>
						<div class="form-group">
							<label for=""><?php echo translate("Transfer Close Date"); ?> (EST)</label>
							<input type="text" id="txtTransferCloseDate" name="txtTransferCloseDate" class="form-control required" value="<?php echo $transferCloseDate; ?>" readonly />
						</div>
						<div class="form-group">
							<button class='btn btn-success btn-block'><span class="glyphicon glyphicon-ok"></span> <?php echo $submitButton; ?></button>
						</div>
						<div class="form-group">
							<a href="<?php echo base_url("admin/transfer_date_manager"); ?>" class='btn btn-danger btn-block'><span class="glyphicon glyphicon-remove"></span> <?php echo translate("Cancel"); ?></a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>