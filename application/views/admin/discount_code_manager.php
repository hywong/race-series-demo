<script>
	jQuery.fn.center = function () {
		this.css("position", "fixed");
		this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) +
			$(window).scrollTop()) + "px");
		this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +
			$(window).scrollLeft()) + "px");
		return this;
	}

	$(document).ready(function () {
		$.tablesorter.defaults.widgets = ['zebra'];
		$("#discounttbl").tablesorter();
		/*
		var viewn = $('.view-notes');
		var notebox = $('#box-note');
		var shadow = $('#shadowbox');
		var addnote = $('#add-note');
		var close = $('#close');
		var addnotebox = $('.add-note-box');
		var createnote = $('#create-note');

		notebox.center();
		
		viewn.on('click', function () {
			$('.note-content').html('');
			shadow.fadeIn();
			notebox.fadeIn();
			$('#code-viewed').val($(this).attr('id'));
			//$.post('../includes/get_discount_note.php', {code: $(this).attr('id')}, function(data)
			$.post('../includes/ajaxHandler.php', {aFunction: "get_discount_note", code: $(this).attr('id')}, function (data) {
				//alert(data);
				var notes = JSON.parse(data);
				var count = 0;

				for (property in notes)          // should return 2
				{
					if (notes.hasOwnProperty(property)) {
						count++;
					}
				}

				for (i = 0; i < count; i++) {
					$('.note-content').append('<div class="note"><h4 class="note-num">' + (i + 1) + '</h4><div class="note-text">' + ' ' + notes[i].note + '</div><div class="note-date">' + notes[i].date + '</div><br /></div>');//<div class="note-edit eolbutton_small" id="'+notes[i].id+'">Edit</div>
				}
			});
		});

		addnote.on('click', function () {
			addnotebox.slideDown();
		});

		createnote.on('click', function () {
			//$.post('../includes/create_discount_note.php', {code: $('#code-viewed').val(), text: $('textarea[name=note]').val()}, function(data)
			$.post('../includes/ajaxHandler.php', {aFunction: "create_discount_note", code: $('#code-viewed').val(), text: $('textarea[name=note]').val()}, function (data) {
				$('textarea[name=note]').val('');
				addnotebox.slideUp();
				$('.note-content').html('');
				//$.post('../includes/get_discount_note.php', {code: $('#code-viewed').val()}, function(data)
				$.post('../includes/ajaxHandler.php', {aFunction: "get_discount_note", code: $('#code-viewed').val()}, function (data) {
					var notes = JSON.parse(data);
					var count = 0;

					for (property in notes)          // should return 2
					{
						if (notes.hasOwnProperty(property)) {
							count++;
						}
					}

					for (i = 0; i < count; i++) {
						$('.note-content').append('<div class="note"><h4 class="note-num">' + (i + 1) + '</h4><div class="note-text">' + ' ' + notes[i].note + '</div><br /></div>');//<div class="note-edit eolbutton_small" id="'+notes[i].id+'">Edit</div>
					}
					$('#' + $('#code-viewed').val()).children().html(parseInt($('#' + $('#code-viewed').val()).children().html(), 10) + 1)
				});
			});
		});

		shadow.add(close).on('click', function () {
			shadow.fadeOut();
			notebox.fadeOut();
		});
		*/
	});
</script>
<div id="wrapper">
    <div id='shadowbox'></div>
    <div id='box-note' style="display:none">
        <h3><?php echo translate("K994"); ?></h3>
        <div class="row">
            <div class='btn btn-success' id='add-note'><?php echo translate("K995"); ?></div>
        </div>
        <br />
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <div class='well add-note-box'>
                    <div class="row">
                        <div class="col-xs-12">
                            <br/>
                            <textarea name='note' class="form-control" placeholder='<?php echo translate("K996"); ?>...'></textarea>
                            <br/>
                            <div class="row">
                                <div class='btn btn-success btn-sm' id='create-note'><?php echo translate("K997"); ?></div>
                            </div>
                            <br />
                            <input type='hidden' name='current_code' id='code-viewed' value=''/>
                        </div>
                    </div>
                </div>
                <br />
            </div>
        </div>
        <div class='btn btn-default' id='close'><?php echo translate("K998"); ?></div>
        <div class='note-content'>
        </div>
    </div>
	<div class="col-xs-12" id="mainContainer">
		<div class="well well-sm text-center">
			<h4><?php echo translate("K169"); ?></h4>
		</div>
		<?php 
	
		if (isset($msgDanger)) {
			echo "<div class=\"alert alert-danger\">" . translate($msgDanger) . "</div>";
		}
		else if (isset($msgSuccess)) {
			echo "<div class=\"alert alert-success\">" . translate($msgSuccess) . "</div>";
		}
		
		?>
		<div class="row">
			<div class="col-xs-12">
				<a href="<?php echo base_url("admin/discount_code_add"); ?>" class='btn btn-default'>
					<span class="glyphicon glyphicon-plus"></span> <?php echo translate("K199"); ?>
				</a>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<div class="table-responsive">
					<table id='discounttbl' class="table table-striped table-hover">
						<thead>
						<tr>
							<th><?php echo translate("K984"); ?></th>
							<th><?php echo translate("K987"); ?></th>
							<th><?php echo translate("K94"); ?></th>
							<th><?php echo translate("K96"); ?></th>
							<th><?php echo translate("Activity"); ?></th>
							<th><?php echo translate("K999"); ?></th>
							<th><?php echo translate("K1000"); ?></th>
							<th><?php echo translate("K1001"); ?></th>
							<th><?php echo translate("K210"); ?></th>
							
							<!--<th><?php //echo translate("K915"); ?></th>-->
							<th><?php echo translate("K1002"); ?></th>

						</tr>
						</thead>
						<tbody>
						<?php

						$countDiscountCodes = count($discountCodes);
						for ($i = 0; $i < $countDiscountCodes; $i++) {
							//Check for the number of uses
							$intDiscountCodeID = $discountCodes[$i]['intDiscountCodeID'];
							$numOfUsed = 0;
							if (isset($discountCodesUsed[$intDiscountCodeID])) {
								$numOfUsed = $discountCodesUsed[$intDiscountCodeID];
							}
							echo "
								<tr>
									<td>" . $discountCodes[$i]['strDiscountCode'] . "</td>
									<td>" . $discountCodes[$i]['strDiscountCodeType'] . "</td>
									<td>" . $discountCodes[$i]['dblDiscountAmount'] . "</td>
									<td>" . $discountCodes[$i]['strEvent'] . "</td>
									<td>" . $discountCodes[$i]['strActivity'] . "</td>
									<td>" . $discountCodes[$i]['dttExpiration'] . "</td>
									<td>" . $discountCodes[$i]['intQuantity'] . "</td>
									<td>" . ($discountCodes[$i]['intQuantity'] - $numOfUsed) . "</td>
									<td>" . $discountCodes[$i]['strActiveStatus'] . "</td>";
									
									//<td class=\"view-notes\" id=\"" . $code . "\"><div class=\"btn btn-sm btn-info\">" . $info['notes'] . "</div></td>
							echo "
									<td><a class=\"btn btn-info btn-sm\" href='" . base_url("admin/discount_code_edit?cid=" . $intDiscountCodeID) . "'><span class=\"glyphicon glyphicon-pencil\"></span> " . translate("K991") . "</a></td>
								</tr>";
						}
						//echo "<pre>"; print_r($discountCodes); echo "</pre>";

						/*
						foreach ($discountCodes as $code => $info) {							
							echo "<tr><td>$code</td><td>" . $info['type'] . "</td><td>" . $info['amount'] . "</td><td>" . $info['event'] . "</td><td>" . date('D M d Y', strtotime($info['expiredate'])) . "</td><td>" . $info['qty'] . "</td><td>" . ((trim($qtyLeft[$code]) == "") ? $info['qty'] : ($info['qty'] - $qtyLeft[$code])) . "</td><td>" . (($info['status'] == 1) ? "Live" : "Not Live") . "</td><td>" . $info['validated'] . "</td><td>" . date('D M d Y', strtotime($info['date'])) . "</td><td class=\"view-notes\" id=\"$code\"><div class=\"btn btn-sm btn-info\">" . $info['notes'] . "</div></td><td><a class=\"btn btn-info btn-sm\" href='edit_discountcode.php?code=" . $code . "'><span class=\"glyphicon glyphicon-pencil\"></span> " . translate("K991") . "</a></td></tr>";
						}
						*/
						
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>