<script>
	$(document).ready(function () {
		$("#myform").validate();
		
		$('#date_of_birth').datepicker({
			format: 'yyyy-mm-dd',
			startView: 2,
			autoclose: true
		});
		
		$("#selActivity").change(function() {
			var activityID = $("#selActivity").val();
			var aUrl = "<?php echo base_url("ajax/buildShirtSelectOptions"); ?>";
			$.ajax({
				type: "POST",
				url: aUrl,
				data: { activityID: activityID },
				async: false,
				success: function (result) {
					//alert(result);	
					$("#tshirt").empty();
					$("#tshirt").append(result);
					//$("#tshirt").append('<option value="">Select</option><option value="1">asdf</option>');
				}
			});
			
		});
	});
</script>
<style>

	.form-control[readonly] {
		background-color: white;
		cursor: auto;
	}

</style>
<div id="wrapper">
	<div id="mainContainer" class="col-xs-12">
		<div class="row">
			<div class="col-xs-12">
				<div class="well well-sm text-center">
					<h4><?php echo translate("K238"); ?></h4>
				</div>
			</div>
		</div>
		<?php if(isset($_GET['action']))
		{
			if($_GET['action'] == "update")
			{
				?>
				<div class="row">
					<div class="alert alert-success text-center">
						<?php echo translate("K216"); ?>
					</div>
				</div>
			<?php
			}
		}
		?>
		<div class="row">
			<div class="col-xs-12">
				<a href="<?php echo base_url("admin/participant_search"); ?>" class='btn btn-default'><span class="glyphicon glyphicon-arrow-left"></span> <?php echo translate("New Search"); ?></a>
			</div>
		</div>
		<br />
		<form id="myform" name="myform" method="post" action="<?php echo base_url("admin/participant_search_modify?action=update"); ?>">
			<input type="hidden" id="pid" name="pid" value="<?php echo $pid; ?>">
			<input type="hidden" id="pType" name="pType" value="<?php echo $pType; ?>">
			<input type="hidden" id="pActivity" name="pActivity" value="<?php echo $pActivity; ?>">
			<table class="table table-striped table-hover">
				<tr>
					<td><strong><?php echo translate("Participant ID"); ?></td>
					<td><strong><?php echo $pid; ?></td>
					<td><strong><?php echo translate("K241"); ?></td>
				</tr>
				<?php
				
					drawEditForm($participantInfo);
				
				?>
				<tr>
					<td colspan='3' align='center'>
						<button class='btn btn-block btn-success'><?php echo translate("K242"); ?></button>
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>
