<script>
    $(document).ready(function () {
        $.tablesorter.defaults.widgets = ['zebra'];
        $("#staffentrytbl").tablesorter({
            stripingRowClass: ['even', 'odd'],
            stripeRowsOnStartUp: true,
        });
        $('.check').bind('click', function () {
            $(this).fadeOut();
        });
    });
</script>
<div id="wrapper">
	<div id="mainContainer" class="col-xs-12">
		<div class="row">
			<div class="col-xs-12">
				<div class="well well-sm text-center">
					<h4><?php echo translate("K234"); ?></h4>
				</div>
			</div>
		</div>
		<?php 
	
		if (isset($msgDanger)) {
			echo "<div class=\"alert alert-danger\">" . translate($msgDanger) . "</div>"; 
		}
		else if (isset($msgSuccess)) {
			echo "<div class=\"alert alert-success\">" . translate($msgSuccess) . "</div>";
		}
		
		?>
		<form method='POST' action='<?php echo base_url("admin/staff_entry_manager?action=update"); ?>'>
			<table id="staffentrytbl" class="table">
				<thead>
				<tr>
					<th><?php echo translate("K280"); ?></th>
					<th><?php echo translate("K281"); ?></th>
					<th><?php echo translate("K282"); ?></th>
					<th><?php echo translate("K283"); ?></th>
					<th><?php echo translate("K284"); ?></th>
					<th><?php echo translate("K286"); ?></th>
				</tr>
				</thead>
				<tbody>
				<?php
				
				if (empty($staffEntries)) {
					echo "<tr><td colspan='8'><h2>" . translate("K1032") . ".</h2></td></tr>";
				}
				else {
					$countStaffEntries = count($staffEntries);
					for ($i = 0; $i < $countStaffEntries; $i++) {
						echo "<tr>
							<td>" . $staffEntries[$i]['strFirstName'] . "</td>
							<td>" . $staffEntries[$i]['strLastName'] . "</td>
							<td>" . $staffEntries[$i]['strActivity'] . "</td>
							<td>" . $staffEntries[$i]['dttDateCreated'] . "</td>
							<td>" . $staffEntries[$i]['dblFinalTotal'] . "</td>
							<td>
								<select id=\"paidStatus_" . $staffEntries[$i]['intTransactionID'] . "\" name=\"paidStatus_" . $staffEntries[$i]['intTransactionID'] . "\">";
									$countPaidStatuses = count($paidStatuses);
									for ($j = 0; $j < $countPaidStatuses; $j++) {
										echo "<option value=\"" . $paidStatuses[$j]['intTransactionPaidStatusID'] . "\" " . (($paidStatuses[$j]['intTransactionPaidStatusID'] == $staffEntries[$i]['intTransactionPaidStatusID'])?'selected':'') . ">" . $paidStatuses[$j]['strTransactionPaidStatus'] . "</option>";
									}
						echo "
								</select>
							</td>
						</tr>";
					}
				}
				/*
				$counter = 1;
				if (!empty($staffEntries)) {
					foreach ($staffEntries as $k => $userArray) {
						if ($userArray['invoice'] != "") {
							echo '<tr>';
							echo '<input type="hidden" name="invoice_' . $counter . '" value="' . $userArray['invoice'] . '" />';
							$explodeArray = explode('-', $userArray['cartID']);
							echo '<td>' . ucwords(end($explodeArray)) . '</td>';
							echo '<td>' . ucwords($userArray['first_name']) . '</td>';
							echo '<td>' . ucwords($userArray['last_name']) . '</td>';
							echo '<td>' . ucwords($userArray['race']) . '</td>';
							echo '<td>' . ucwords($userArray['date']) . '</td>';
							echo '<td>' . ucwords($userArray['total']) . '</td>';
							echo '<td>' . ucwords($userArray['event_price']) . '</td>';
							echo '<td style="min-width:140px;"><select class="form-control" name="paid_' . $counter . '">';
							if ($userArray['paid'] == 'n') {
								echo '<option selected="selected" value="n">' . translate("K287") . '</option>
										<option value="y">' . translate("K288") . '</option>';
							} else {
								echo '<option value="y">' . translate("K288") . '</option>
										<option value="n">' . translate("K287") . '</option>';
							}
							echo '</select></td>';
							echo '</tr>';
						}
						$counter++;
					}
				} else {
					echo "<tr><td colspan='8'><h2>" . translate("K1032") . ".</h2></td></tr>";
				}
				*/
				
				?>
				</tbody>
			</table>
			<br/>
			<?php if (!empty($staffEntries)) { ?>
			<div class="row">
				<div class="col-sm-6 col-sm-offset-3">
					<button class='btn btn-success btn-block'><span class="glyphicon glyphicon-ok"></span> <?php echo translate("K290"); ?></button>
				</div>
			<?php } ?>
		</form>
	</div>
</div>