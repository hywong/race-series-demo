<script>
	$(document).ready(function () {
		$('#fsform').hide();
		var save = $('#save');
		save.hide();
		var admintbl = $('#admintbl');
		admintbl.on('click', 'tr .editfield', toggleToInput);
		var editingID = '';
		var master = false;

		function toggleToInput() {
			var ID = $(this).parent('tr').attr('id');
			var rowName = $('#' + ID + '_name').text();
			var rowEmail = $('#' + ID + '_email').text();
			var rowPassword = $('#' + ID + '_password').text();
			var rowAccess = $('#' + ID + '_access').text();

			editingID = ID;

			//Disable Click Event
			admintbl.off('click');

			//Convert HTML to Inputs
			$('#' + ID + '_name').html('<input type="text" id="' + ID + '_name_input" name="' + ID + '_name" value="' + rowName + '" />');
			$('#' + ID + '_email').html('<input type="text" id="' + ID + '_email_input" name="' + ID + '_name" value="' + rowEmail + '" />');
			$('#' + ID + '_password').html('<input type="text" id="' + ID + '_password_input" name="' + ID + '_name" value="' + rowPassword + '" />');

			//Add Pre-selected Variable
			if (rowAccess != 'Master') {
				$('#' + ID + '_access').html('<select id="' + ID + '_access_select" class="form-control"><option value="master">Master</option><option value="1">1 (All but manage admins)</option><option value="2">2 (View/Download only)</option></select>');
			} else {
				master = true;
				$('#' + ID + '_access').html('Master');
			}
			// $('#'+this.id+'_access').html('<input type="text" id="'+this.id+'_access_input" name="'+this.id+'_name" value="'+rowAccess+'" />');
			save.show();
		}

		save.click(function () {

			//Revert Inputs to Text on Success
			var inputText = $("#" + editingID + "_name_input").val();
			$('#' + editingID + '_name').html(inputText);

			var inputEmail = $("#" + editingID + "_email_input").val();
			$('#' + editingID + '_email').html(inputEmail);

			var inputPassword = $("#" + editingID + "_password_input").val();
			$('#' + editingID + '_password').html(inputPassword);

			if (!master) {
				var inputAccess = $("#" + editingID + "_access_select :selected").val();
				$('#' + editingID + '_access').html(inputAccess);
			} else {
				var inputAccess = 'master';
			}
			//Enable Click Event
			admintbl.on('click', 'tr .editfield', toggleToInput);

			//Hide Save button
			save.hide();

			//Change Text File
			$.post('<?php echo base_url("ajax/adminChangeAccountDetails"); ?>', {name: inputText, email: inputEmail, access: inputAccess, id: editingID}, function (data) {
				//alert(data);
				if (data) {
					$('#status-msg').hide();
					$('#status-msg').html("<div class='alert alert-success'><?php echo translate("K101"); ?></div>");
					$('#status-msg').show();
				} else {
					$('#status-msg').hide();
					$('#status-msg').text('<div class="alert alert-danger"><?php echo translate("K103"); ?></div>');
					$('#status-msg').show();					
				}
				//location.reload(true);
			});
		});

		$('#createadmin').on('click', function () {
			$('#fsform').slideDown();
		});

		$('.delete').on('click', function (event) {
			event.preventDefault();
			var r = confirm("<?php echo translate("K104"); ?>");
			if (r == true) {
				window.location = $(this).attr('href');
			}
		});
		$('.btn-change-pw').on('click', function() {
			var id = $(this).data('id');
			$('#changeSubmit').data('id', id);
		});
		
		$('#changeSubmit').on('click', function() {
			$.post('<?php echo base_url("ajax/adminChangePass"); ?>', { target: $(this).data('id'), new_password: $('input[name=new_password]').val()}, function(data){
				//alert(data);
				$('#changePassModal').modal('hide');
				if(data == true)
				{
					$('#status-msg').hide();
					$('#status-msg').removeClass("alert alert-danger");
					$('#status-msg').addClass("alert alert-success");					
					$('#status-msg').html("<?php echo translate("Successfully changed password");?>");
					$('#status-msg').show();
				}
				else
				{
					$('#status-msg').hide();
					$('#status-msg').removeClass("alert alert-success");
					$('#status-msg').addClass("alert alert-danger");					
					$('#status-msg').html("<?php echo translate("Failed to change password");?>");
					$('#status-msg').show();
				}
			});
		});
	});	
</script>
<div id="wrapper">
	<div class="modal fade" id="changePassModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3 class="modal-title" id="myModalLabel"><?php echo translate("Change Password");?></h3>
		  </div>
		  <div class="modal-body">
			<h4><?php echo translate("Please enter the new password");?></h4>
			<form role="form" id="changePassForm">				
				<div class="form-group">
					<label for="inputPassChange"><?php echo translate("New Password");?></label>
					<input type="text" class="form-control" name="new_password" id="inputPassChange" />
				</div>
			</form>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo translate("Close");?></button>
			<button type="button" class="btn btn-primary" id="changeSubmit"><?php echo translate("Change Password");?></button>
		  </div>
		</div>
	  </div>
	</div>
	<div id="mainContainer" class="col-xs-12">
		<div class="well well-sm text-center">
			<h4><?php echo translate("K105"); ?></h4>
		</div>
		<div id="status-msg">
		<?php
			if (isset($_GET['msg']) && $_GET['msg'] == 'removed') {
				echo '<div class="alert alert-success">' . translate("K108") . '</div>';
			}
			else if (isset($_GET['msg']) && $_GET['msg'] == 'added') {
				echo '<div class="alert alert-success">' . translate("Successfully added the admin account.") . '</div>';
			}
		?>
		</div>
		<noscript>
			<div class='eolerror'><?php echo translate("K106"); ?>
				<a href='http://www.enable-javascript.com/'>http://www.enable-javascript.com/</a> <?php echo translate("K107"); ?>
			</div>
		</noscript>
		<center>
			<?php 
			/*
			if (isset($_GET['msg']) && $_GET['msg'] == 'removed') {
				echo "<div class='check'>" . translate("K108") . "<div id='checkspan'>" . translate("K102") . "</div></div>";
			}
			*/
			?>
			<div class="row">
				<div class="col-xs-12">
					<button class='btn btn-default' type='button' value='Add New Admin' id='createadmin'><?php echo translate("K831"); ?></button>
				</div>
			</div>
			<div class="row">
				<br />
				<div class="col-sm-6 col-sm-offset-3" id="fsform">
					<div class="well">						
						<form name='addAdmin' action='?action=add' method='post'>
							<div class="form-group">
								<label for="aname">
									<span class="req">*</span><?php echo translate("K110"); ?>
								</label>
								<input class='form-control ' type="text" name="aname" id="aname" maxlength="40" required />
							</div>
							<div class="form-group">
								<label for="aemail">
									<span class="req">*</span><?php echo translate("K111"); ?>
								</label>
								<input class='form-control ' type="text" name="aemail" id="aemail" maxlength="40"required />
							</div>
							<div class="form-group">
								<label for="apassword">
									<span class="req">*</span><?php echo translate("K112"); ?>
								</label>
								<input class='form-control' type="password" id="apassword" name="apassword" maxlength="40" required />
							</div>
							<div class="form-group">
								<label for="aaccess">
									<span class="req">*</span><?php echo translate("K113"); ?>
								</label>
								<select name="aaccess" class="form-control">
									<?php
										$adminAccountTypesCount = count($adminAccountTypes);
										for ($i = 0; $i < $adminAccountTypesCount; $i++) {
											echo "<option value=\"" . $adminAccountTypes[$i]['intAdminAccountTypeID'] . "\">" . $adminAccountTypes[$i]['strAdminAccountType'] . "</option>";
										}
									?>
									
								</select>
							</div>
							<div class='row'>
								<button class='btn btn-success btn-block'><span class="glyphicon glyphicon-ok"></span> <?php echo translate("K830"); ?></button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<br />
			<p><b><?php echo translate("K114"); ?>
				</b></p>
			<br />
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<div class="table-responsive">
						<table id="admintbl" class="table table-striped table-hover table-condensed">
							<thead>
							<tr>
								<th><?php echo translate("K115"); ?></th>
								<th><?php echo translate("K116"); ?></th>                    
								<th><?php echo translate("K118"); ?></th>
								<th><?php echo translate("K119"); ?></th>
							</tr>
							</thead>
							<tbody>
							<?php
							//Display all admins
							foreach ($adminAccounts as $k => $arr) {
								echo '<tr id=' . $arr['intAdminAccountID'] . ' style="cursor:pointer;">';
								echo '<td class="editfield" id="' . $arr['intAdminAccountID'] . '_name">' . $arr['strAdminName'] . '</td>';
								echo '<td class="editfield" id="' . $arr['intAdminAccountID'] . '_email">' . $arr['strAdminUserName'] . '</td>';
								// echo '<td class="editfield" id="' . $arr['intAdminAccountID'] . '_password">' . $arr['password'] . '</td>';
								echo '<td class="editfield" id="' . $arr['intAdminAccountID'] . '_access">' . ucwords($arr['strAdminAccountType']) . '</td>';
								echo '<td><div class="btn-group-vertical"><a class="btn btn-danger btn-sm" href="' . base_url("admin/admin_manager?action=remove&id=" . $arr['intAdminAccountID']) . '">'.translate("Remove").'</a><button class="btn btn-info btn-sm btn-change-pw" data-toggle="modal" data-target="#changePassModal" data-id="'.$arr['intAdminAccountID'].'">'.translate("Change Password").'</button></div>';
								echo '</tr>';
							}
							?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="row">
				<button type='button' class='btn btn-success' id='save'><?php echo translate("K242"); ?></button>
			</div>
					
		</center>
		<br/>
	</div>
</div>