<div class="container">

<div class="clearfix"></div>

<ul id="progressbar" class="staff-steps" data-progress-steps="6">

<?php
  $pages = $pagesArr;

  $pageLinks = array(
    "Event Selection" => "admin/staff_entry",
    "Participant Information" => "admin/personal",
    "Teams" => "admin/teams",
    "Charities" => "admin/charities",
    "Add-ons" => "admin/addons",
    "Confirmation" => "admin/confirmation"
    );
  $default = "";
	foreach($pages as $page=>$status){
		if($status == "active current"){
			echo "<li class=\"" . $status . "\">".translate($page)."</li>";
			$default = "";
		}else if($status == "active"){
			echo "<li class=\"" . $status . "\" onclick=\"location.href='".base_url($pageLinks[$page])."'\"><a href=\"".base_url($pageLinks[$page])."\"><span class=\"hidden-xs\">";
			if($page != "Confirmation"){ echo translate("Edit"); }
			echo " ".translate($page)."</span></a></li>";
		}else{
			echo "<li class=\"" . $default . "\"><span class=\"hidden-xs\">".translate($page)."</span></li>";
		}
	}
?>
</ul>
<div class="clearfix"></div>
</div>