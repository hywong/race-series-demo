<div id="wrapper">
	<div id="mainContainer" class="col-xs-12">
		<div class="well well-sm text-center">
			<h4><?php echo translate("Participant Modify Tool Availability Manager"); ?></h4>
		</div>	
		<?php 
	
		if (isset($msgDanger)) {
			echo "<div class=\"alert alert-danger\">" . translate($msgDanger) . "</div>";
		}
		else if (isset($msgSuccess)) {
			echo "<div class=\"alert alert-success\">" . translate($msgSuccess) . "</div>";
		}
		
		?>
		<?php if (empty($participantModifyAvailabilityDates)) { ?>
		<div class="row">
			<div class="col-xs-12">
				<a href="<?php echo base_url("admin/participant_modify_date_add"); ?>" class="btn btn-default">
				<span class="glyphicon glyphicon-plus"></span> <?php echo translate("Add Participant Modify Tool Availability Dates"); ?>
				</a>
			</div>
		</div>
		<?php } ?>
		<div class="row">
			<div class="col-xs-12">
				<table id='activtytbl' class="table table-striped table-hover">
					<thead>
					<tr>
						<th><?php echo translate("Participant Modify Open Date"); ?></th>
						<th><?php echo translate("Participant Modify Close Date"); ?></th>    
						<?php
							if (!empty($participantModifyAvailabilityDates)) {
								echo "<th>".translate('K356')."</th>";
							}
						?>
					</tr>
					</thead>
					<tbody>
					<?php
					$i = 0;
					if (!empty($participantModifyAvailabilityDates)) {
						echo '<td>' . $participantModifyAvailabilityDates['dttPublicModifyStartDate'] . '</td>';
						echo '<td>' . $participantModifyAvailabilityDates['dttPublicModifyEndDate'] . '</td>';
						echo '<td><a class="btn btn-info btn-sm" href="' . base_url("admin/participant_modify_date_edit") . '"><span class="glyphicon glyphicon-pencil"></span> ' . translate("K991") . '</a></td>';

						   
					} 
					else {
						echo '<td colspan="2" class="text-center">' . translate("No Participant Modify Dates Set") . '</td>';
					}
					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>