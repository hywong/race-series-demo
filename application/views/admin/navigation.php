<?php
	$uriString = uri_string(); 
?>
<script>
	$(document).ready(function () {
		$("#nav_event").change(function() {
			var anEventID = $("#nav_event").val();
			var aUrl = "<?php echo base_url("ajax/changeAdminEvent"); ?>";
			$.ajax({
				type: "POST",
				url: aUrl,
				data: { anEventID: anEventID },
				async: false,
				success: function (result) {
					//alert(result);	
					if (result == 1) {
						window.location.replace('<?php echo base_url("admin/"); ?>');
					}
					else {
						location.reload(true);
					}
				}
			});
		});
	});
</script>
<nav class="navbar navbar-fixed-top" role="navigation" id="navigationBar">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" target="_blank" href="https://secure.eventsonline.ca"><img src="<?php echo base_url('assets/img/eol_logo_sm.png');?>" alt="EventsOnline" title="Powered by EventsOnline" /></a>
    
  </div>
	<div class="collapse navbar-collapse" id="template-navbar-collapse">
		<ul class="nav navbar-nav">
			<li><a href="<?php echo base_url("/"); ?>"><span class="glyphicon glyphicon-home"></span> <?php echo translate("Registration"); ?></a></li>
			<li class="active"><a href="<?php echo base_url("admin/"); ?>"><span class="glyphicon glyphicon-user"></span> <?php echo translate("Admin Dashboard"); ?></a></li>
		</ul>

<?php
	//echo "<pre>"; print_r($_SESSION); echo "</pre>";
	
	echo '
    <ul class="nav navbar-nav navbar-right">
		<li>
			<form action="?lang=fr" method="post" id="translate-form">
				<ul class="nav navbar-nav">
					<li>';
						if (isset($_SESSION['language']) && $_SESSION['language'] == "fr") {
							echo '<a href="' . current_url() . '?lang=en" id="href-post">English</a>';
						}
						else {
							echo '<a href="' . current_url() . '?lang=fr" id="href-post">Fran&ccedil;ais</a>';
						}
	echo '
					</li>
				</ul>
			</form>		
		</li>';
		if (isset($_SESSION['admin']) && stripos($uriString,"login") === FALSE) {
	echo '
		<li>
			<a href="' . base_url("admin/logout") . '"><i class="fa fa-user"></i> ' . translate("Log Out") . '<b></b></a>				
		</li>';
		}
	echo '
		<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
    </ul>';
	
	if (isset($_SESSION['admin']) && stripos($uriString,"login") === FALSE) {
		echo '
	<!-- Collect the nav links, forms, and other content for toggling -->
	
		<ul class="nav navbar-nav side-nav">
			<li>
				<div class="input-group">
					<span class="input-group-addon">' . translate("Event") . '</span>
					<select name="nav_event" id="nav_event" class="form-control">';
						$countNavEvents = count($navEvents);
						for ($i = 0; $i < $countNavEvents; $i++) {
							echo "<option value=\"" . $navEvents[$i]['intEventID'] . "\" " . (($navEvents[$i]['intEventID'] == $_SESSION['admin']['adminEventID'])?'selected':'') . ">" . $navEvents[$i]['strEvent'] . "</option>";
						}
		echo '					
					</select>
				</div>
			</li>
			<li><a href="' . base_url("admin/") . '"><i class="fa fa-dashboard"></i> ' . translate("Dashboard") . '</a></li>
			<li class="side-nav-title"> ' . translate("Manage") . '<button type="button" class="btn btn-xs toggle-btn" data-toggle="collapse" data-target=".navbar-manager-list"><i class="glyphicon glyphicon-plus-sign" id="manage_plus" style="display:none;"></i><i class="glyphicon glyphicon-minus-sign" id="manage_minus"></i></button></li>
			<div class="clearfix"></div>
			<span class="navbar-manager-list collapse in" id="manage-btn">
				<li><a href="' . base_url("admin/admin_manager") . '"><i class="fa fa-users"></i> ' . translate("Administrators") . '</a></li>';
				
				if ($_SESSION['admin']['adminEventID'] != 1) {
		echo '
				<li><a href="' . base_url("admin/participant_search") . '"><i class="fa fa-users"></i> ' . translate("Participants") . '</a></li>
				<li><a href="' . base_url("admin/discount_code_manager") . '"><i class="fa fa-table"></i> ' . translate("Discount Codes") . '</a></li>
				<li><a href="' . base_url("admin/activity_cap_manager") . '"><i class="fa fa-edit"></i> ' . translate("Activity Caps") . '</a></li>
				<li><a href="' . base_url("admin/tshirt_cap_manager") . '"><i class="fa fa-edit"></i> ' . translate("T-Shirt Caps") . '</a></li>
				<li><a href="' . base_url("admin/transfer_date_manager") . '"><i class="fa fa-edit"></i> ' . translate("Transfer Avail.") . '</a></li>
				<li><a href="' . base_url("admin/participant_modify_date_manager") . '"><i class="fa fa-edit"></i> ' . translate("Public Modify Avail.") . '</a></li>
				<li><a href="' . base_url("admin/team_manager") . '"><i class="fa fa-edit"></i> ' . translate("Teams") . '</a></li>';
				}
		echo '
			</span>
			<li class="side-nav-title"> ' . translate("Data Downloads") . '<button type="button" class="btn btn-xs toggle-btn" data-toggle="collapse" data-target=".navbar-download-list"><i class="glyphicon glyphicon-plus-sign" id="download_plus"></i><i class="glyphicon glyphicon-minus-sign" id="download_minus" style="display:none;"></i></button></li>
			<div class="clearfix"></div>
			<span class="navbar-download-list collapse" id="download-btn">
				
				<li><a href="' . base_url("admin/download_race_data") . '"><i class="fa fa-desktop"></i> ' . translate("Registration") . '</a></li>
				<li><a href="' . base_url("admin/download_account_data") . '"><i class="fa fa-wrench"></i> ' . translate("Accounting") . '</a></li>
				<li><a href="' . base_url("admin/download_addons") . '"><i class="fa fa-file"></i> ' . translate("Addons") . '</a></li>
				<li><a href="' . base_url("admin/download_pledge_donation") . '"><i class="fa fa-file"></i> ' . translate("Pledge/Donation") . '</a></li>
			</span>';
			if ($_SESSION['admin']['adminEventID'] != 1) {
		echo '
			<li class="side-nav-title"> ' . translate("Misc") . '<button type="button" class="btn btn-xs toggle-btn" data-toggle="collapse" data-target=".navbar-misc-list"><i class="glyphicon glyphicon-plus-sign" id="misc_plus"></i><i class="glyphicon glyphicon-minus-sign" id="misc_minus" style="display:none;"></i></button></li>
			<div class="clearfix"></div>
			<span class="navbar-misc-list collapse" id="misc-btn">
				<li><a href="' . base_url("admin/confirmation_list") . '"><i class="fa fa-desktop"></i> ' . translate("Confirmation List") . '</a></li>
			<li><a href="' . base_url("admin/staff_entry") . '"><i class="fa fa-desktop"></i> ' . translate("Manual Entry") . '</a></li>
			<li><a href="' . base_url("admin/staff_entry_manager") . '"><i class="fa fa-desktop"></i> ' . translate("View Manual Entries") . '</a></li>
			</span>';
				}
		echo '
    </ul>';
	}
  echo '</div>';
	
?>
  <!-- /.navbar-collapse -->
</nav>