<?php
  
?>
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <div class="col-sm-offset-2 col-sm-8">
        <div class="well">
          <fieldset>
            <legend class="text-center"><?php echo translate("Transaction Summary");?></legend>
          </fieldset>
          <?php
            if($created === TRUE)
            {
              echo "<div class=\"alert alert-success text-center\"><h4><b>";
              echo "You have successfully registered a participant.<br /><br />";
              echo "<a href=\"".base_url('admin/reset_staff_entry')."\" class=\"btn btn-success\">Register another participant</a>";
              echo "</b></h4></div>";
            }
            else
            {
              echo "<div class=\"alert alert-success text-center\"><h4><b>";
              echo "An error has occurred, the participant was not registered. If this error persists, please contact your EventsOnline account manager.<br /><br />";
              echo "<a href=\"".base_url('admin/finalize')."\" class=\"btn btn-danger\">Try again</a>";
              echo "</b></h4></div>";
            }
          ?>
        </div>
      </div>
    </div>
  </div>
</div>