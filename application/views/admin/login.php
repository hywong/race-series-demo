<?php

	displayJsNoScript();
	
?>
<script>
	$(document).ready(function () {
		$("#mainContainer").show();
	});
</script>
<br /><br /><br /><br />
<div id="mainContainer" class="row" style="display:none">
	<div class="col-sm-6 col-sm-offset-3">
		<div class="well">
			<h3 class="well-title">
				<?php echo translate("K218"); ?>
			</h3>
			<div class="row">
				<div class="col-xs-12">
					<form name="myform" method="POST" action="<?php echo base_url("admin/login"); ?>">
						<?php 
	
						if (isset($msgDanger)) {
							echo "<div class=\"alert alert-danger\">" . translate($msgDanger) . "</div>";
						}
						else if (isset($msgSuccess)) {
							echo "<div class=\"alert alert-success\">" . translate($msgSuccess) . "</div>";
						}
						
						?>
						<div class='form-group'>
							<label for="email"><?php echo translate("Email"); ?></label>
							<input type="text" name="email" id="email" class="form-control" />
						</div>
						<div class='form-group'>
							<label for="password"><?php echo translate("K117"); ?></label>
							<input type="password" id="password" class="form-control" name="passcode" />
						</div>					
						<button class='btn btn-block btn-success'><?php echo translate("K882"); ?></button>			
					</form>
				</div>
			<?php if (isset($msg) && $msg == 1) {
				echo "<center><strong>" . translate("K219") . "</strong></center>";
			} ?>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<br />
					<a href='<?php echo base_url("/"); ?>' class='btn btn-default'>
						<span class="glyphicon glyphicon-circle-arrow-left"></span> 
						<?php echo translate("K748"); ?>
					</a>
				</div>
			</div>
		</div>
	</div>		
</div>