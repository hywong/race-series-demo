<?php

	$cid = "";
	$crid = "";
	$code = "";
	$amount = "";
	$qty = "";
	$type = "";
	$year = "";
	$month = "";
	$day = "";
	$event = "";
	$activity = "";
	$status = "";
	if (isset($formType) && $formType == "edit") {
		$cid = $discountCodeDetails['intDiscountCodeID'];
		$crid = $discountCodeDetails['intRelDiscountCodeID'];
		$code = $discountCodeDetails['strDiscountCode'];
		$amount = $discountCodeDetails['dblDiscountAmount'];
		$qty = $discountCodeDetails['intQuantity'];
		$type = $discountCodeDetails['intDiscountCodeTypeID'];
		$expiration = explode("-",$discountCodeDetails['dttExpiration']);
		$year = $expiration[0];
		$month = $expiration[1];
		$day = $expiration[2];
		$event = $discountCodeDetails['intEventID'];
		$activity = $discountCodeDetails['intActivityID'];
		$status = $discountCodeDetails['intActiveStatusID'];
	}

?>
    <script type='text/javascript'>
        $(document).ready(function () {
            new_cat_input = $('.new_cat');
            new_cat_input.hide();

            $('select[name=category]').on('change', function () {
                if ($('select[name=category] option:selected').val() == 'new_category') {
                    new_cat_input.show();
                }
                else {
                    new_cat_input.hide();
                }
            });
			
			$("#myform").validate({
                rules: {
                    qty: {
                        digits: true
                    },
                    amount: {
                        digits: true
                    }
                }
            });
			
			$('input[name=code]').bind('keypress', function (event) {
				var regex = new RegExp("^[a-zA-Z0-9\b\t]+$");
				//var regex = new RegExp("^[%\'\"]+$");
				var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
				if (!regex.test(key)) {
				   event.preventDefault();
				   return false;
				}
			});
			
			$("#event").change(function() {
				var anEventID = $("#event").val();
				var aUrl = "<?php echo base_url("ajax/buildActivityDropdownByEID"); ?>";
				$.ajax({
					type: "POST",
					url: aUrl,
					data: { anEventID: anEventID },
					async: false,
					success: function (result) {
						//alert(result);	
						$("#divActivity").html(result);
					}
				});
			});
        });
    </script>
<div id="wrapper">
	<div id="mainContainer" class="col-xs-12">
		<div class="well well-sm text-center">
			<h4 class="dividerbar">
				<?php echo (($formType == "add")?translate("K93"):translate("K205")); ?>
			</h4>
		</div>
		<div class="well">
			<form method="POST" action="<?php echo base_url("admin/discount_code_" . $formType); ?>" id="myform" name="myform">				
				<div class="form-group">
					<label for="code"><?php echo translate("K984"); ?></label>
					<input type="text" name="code" id="code" class="form-control required" value="<?php echo $code; ?>" />
				</div>
				<div class="form-group">
					<label for="amount"><?php echo translate("K170"); ?></label>
					<input type="text" name="amount" id="amount" class="form-control required" value="<?php echo $amount; ?>" />
				</div>
				<div class="form-group">
					<label for="qty"><?php echo translate("Quantity"); ?></label>
					<input type="text" name="qty" id="qty" class="form-control required" value="<?php echo $qty; ?>" />
				</div>
				<div class="form-group">
					<label for="type"><?php echo translate("K987"); ?></label>
					<select id="type" name="type" class="form-control">
						<?php
						$countDiscountCodeTypes = count($discountCodeTypes);
						for ($x = 0; $x < $countDiscountCodeTypes; $x++) {
							echo "<option value='" . $discountCodeTypes[$x]['intDiscountCodeTypeID'] . "' " . (($discountCodeTypes[$x]['intDiscountCodeTypeID'] == $type)?'selected':'') . ">" . translate($discountCodeTypes[$x]['strDiscountCodeType']) . "</option>";							
						}
						?>
					</select>
				</div>
				<div class="form-group">
					<label><?php echo translate("K95"); ?></label>
					<div class="row">
						<div class="col-md-4">
							<div class="input-group">
								<span class="input-group-addon">Year</span>
								<select name="year" id="year" class="form-control">
									<?php
									for ($k = date("Y"); $k < 2030; $k++) {
										echo "<option value='" . $k . "' " . (($k == $year)?'selected':'') . ">" . $k . "</option>";
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="input-group">
								<span class="input-group-addon">Month</span>
								<select name="month" id="month" class="form-control">
									<?php
									for ($i = 1; $i < 13; $i++) {
										echo "<option value='" . $i . "' " . (($i == $month)?'selected':'') . ">" . $i . "</option>";
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="input-group">
								<span class="input-group-addon">Date</span>
								<select name="day" id="day" class='form-control'>
									<?php
									for ($j = 1; $j < 32; $j++) {
										echo "<option value='" . $j . "' " . (($j == $day)?'selected':'') . ">" . $j . "</option>";
									}
									?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="event"><?php echo translate("K96"); ?></label>
					<select name="event" id="event" class="form-control required">
						<option value=""><?php echo translate("Select"); ?></option>
						<?php
						$countEvents = count($events);
						for ($z = 0; $z < $countEvents; $z++) {
							if (strtolower($events[$z]['strEvent']) != "all") {
								echo "<option value='" . $events[$z]['intEventID'] . "' " . (($events[$z]['intEventID'] == $event)?'selected':'') . ">" . $events[$z]['strEvent'] . "</option>";							
							}
						}
						?>
					</select>
				</div>
				<div id="divActivity" class="form-group">
					<label for="activity"><?php echo translate("Activity"); ?></label>
					<select name="activity" id="activity" class="form-control required">
						<option value=""><?php echo translate("Select"); ?></option>
						<?php
						$countActivities = count($activities);
						if ($countActivities > 0) {
							echo "<option value=\"1\" " . (($activity == 1)?'selected':'') . ">" . translate("All") . "</option>";
							for ($y = 0; $y < $countActivities; $y++) {
								echo "<option value='" . $activities[$y]['intActivityID'] . "' " . (($activities[$y]['intActivityID'] == $activity)?'selected':'') . ">" . $activities[$y]['strActivity'] . "</option>";							
							}
						}
						?>
					</select>
				</div>
				<div class="form-group">
					<label for="status"><?php echo translate("K210"); ?></label>
					<select name="status" id="status" class="form-control">
						<?php
						$countStatuses = count($statuses);
						for ($x = 0; $x < $countStatuses; $x++) {
							echo "<option value='" . $statuses[$x]['intActiveStatusID'] . "' " . (($statuses[$x]['intActiveStatusID'] == $status)?'selected':'') . ">" . translate($statuses[$x]['strActiveStatus']) . "</option>";							
						}
						?>
					</select>
				</div>
				<div class="form-group">
					<input type="hidden" name="cid" id="cid" value="<?php echo $cid; ?>">
					<input type="hidden" name="crid" id="crid" value="<?php echo $crid; ?>">
					<button class='btn btn-success btn-block'><span class="glyphicon glyphicon-ok"></span> <?php echo translate("K830"); ?></button>
				</div>
				<div class="form-group">
					<a href="<?php echo base_url("admin/discount_code_manager"); ?>" class='btn btn-danger btn-block'><span class="glyphicon glyphicon-remove"></span> <?php echo translate("Cancel"); ?></a>
				</div>
			</form>
		</div>
	</div>
</div>
