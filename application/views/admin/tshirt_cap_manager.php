<script>

	$(document).ready(function () {
		$("#selActivity").change(function() {
			var selActivity = $("#selActivity").val();
			window.location.replace("<?php echo base_url("admin/tshirt_cap_manager?aid="); ?>" + selActivity);
		});
	});
	
</script>
<div id="wrapper">
	<div id="mainContainer" class="col-xs-12">
		<div class="well well-sm text-center">
			<h4><?php echo translate("T-Shirt Cap Manager"); ?></h4>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="input-group">
				<?php
					echo '<span class="input-group-addon">' . translate("Activity") . '</span>';
				?>
				<select name="selActivity" id="selActivity" class="form-control">					
				<?php  
					
					echo "<option value=\"\">" . translate("Select") . "</option>";
					
					foreach ($activities as $activityID => $activityDetails) {
						echo "<option value=\"" . $activityID . "\" " . (($activityID == $selectedActivity)?'selected':'') . ">" . $activityDetails['strActivity'] . "</option>";
					}
				?>
				</select>
				</div>
			</div>
		</div>	
		<br /><br />
		<?php 
	
		if (isset($msgDanger)) {
			echo "<div class=\"alert alert-danger\">" . translate($msgDanger) . "</div>";
		}
		else if (isset($msgSuccess)) {
			echo "<div class=\"alert alert-success\">" . translate($msgSuccess) . "</div>";
		}
		
		?>
		<div class="row">
			<div class="col-xs-12">
				<div class="table-responsive">
					<table id='activtytbl' class="table table-striped table-hover">
						<thead>
						<tr>
							<th><?php echo translate("T-Shirt Size"); ?></th>
							<th><?php echo translate("K143"); ?></th>
							<th><?php echo translate("Available T-Shirts"); ?></th>
							<th><?php echo translate("T-Shirts Assigned"); ?></th>
							<th><?php echo translate("K356"); ?></th>
						</tr>
						</thead>
						<tbody>
						<?php
						$countTshirtCaps = count($tshirtCaps);
						for ($i = 0; $i < $countTshirtCaps; $i++) {
							$tshirtID = $tshirtCaps[$i]['intShirtID'];
							$numTshirtsAssigned = 0;
							if (isset($tshirtAssigned[$tshirtID])) {
								$numTshirtsAssigned = $tshirtAssigned[$tshirtID];
							}							
							$capLimit = $tshirtCaps[$i]['intShirtCap'];							
							$availableTshirts = $capLimit - $numTshirtsAssigned;
							
							echo '<tr>
								<td>' . $tshirtCaps[$i]['strShirtSize'] . '</td>
								<td>' . $capLimit . '</td>
								<td>' . $availableTshirts . '</td>
								<td>' . $numTshirtsAssigned . '</td>
								<td><a class="btn btn-info btn-sm" href="' . base_url("admin/tshirt_cap_edit?aid=" . $selectedActivity . "&tid=" . $tshirtID) . '"><span class="glyphicon glyphicon-pencil"></span> ' . translate("K991") . '</a></td>';
							echo "</tr>";
						}
												
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>