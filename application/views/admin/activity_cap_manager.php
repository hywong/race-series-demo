<div id="wrapper">
	<div id="mainContainer" class="col-xs-12">
		<div class="well well-sm text-center">
			<h4><?php echo translate("K141"); ?></h4>
		</div>
		<?php 
	
		if (isset($msgDanger)) {
			echo "<div class=\"alert alert-danger\">" . translate($msgDanger) . "</div>";
		}
		else if (isset($msgSuccess)) {
			echo "<div class=\"alert alert-success\">" . translate($msgSuccess) . "</div>";
		}
		
		?>
		<!--
		<div class="row">
			<div class="col-xs-12">
				<a class="btn btn-default" href="<?php echo base_url("admin/activity_cap_add"); ?>">
				<span class="glyphicon glyphicon-plus"></span> <?php echo translate("K198"); ?>
				</a>
			</div>
		</div>
		-->		
		<div class="row">
			<div class="col-xs-12">
				<div class="table-responsive">
					<table id='activtytbl' class="table table-striped table-hover">
						<thead>
						<tr>
							<th><?php echo translate("K142"); ?></th>
							<th><?php echo translate("K143"); ?></th>
							<th><?php echo translate("K144"); ?><br/><?php echo translate("K145"); ?></th>
							<th><?php echo translate("K146"); ?><br/><?php echo translate("K147"); ?></th>
							<th><?php echo translate("K356"); ?></th>
						</tr>
						</thead>
						<tbody>
						<?php
						
						foreach ($activities as $activityID => $activityDetails) {
							$numParticipantRegistered = 0;
							if (isset($participantsRegistered[$activityID])) {
								$numParticipantRegistered = $participantsRegistered[$activityID];
							}
							$capLimit = $activityDetails['intCap'];
							$availablePositions = $capLimit - $numParticipantRegistered;
							
							echo "<tr>";
							echo "<td>" . $activityDetails['strActivity'] . "</td>";
							echo "<td>" . $capLimit . "</td>";
							echo "<td>" . $availablePositions . "</td>";
							echo "<td>" . $numParticipantRegistered . "</td>";
							echo '<td><a class="btn btn-info btn-sm" href="' . base_url("admin/activity_cap_edit?aid=" . $activityID) . '"><span class="glyphicon glyphicon-pencil"></span> ' . translate("K991") . '</a></td>';
							echo "</tr>";
						}
						
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>