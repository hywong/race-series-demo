<?php
//The following array is needed for the autocomplete.  Unsure why... but the autocomplete won't work without it
$allUserNotes = array();
?>
<script>
	jQuery.fn.center = function () {
		this.css("position", "fixed");
		this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) +
			$(window).scrollTop()) + "px");
		this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +
			$(window).scrollLeft()) + "px");
		return this;
	}
	$(function () {
		var allUsers = <?php echo json_encode($participants) ?>;
		//var allNotes = <?php echo json_encode($allUserNotes) ?>;
		var namesArr = [];
		var namesData = {};
		var name = $('#name');
		var address = $('#address');
		var email = $('#email');
		var invoice = $('#invoice');
		var piType = $("piType");
		var viewn = $('#notes_count');
		var notebox = $('#box-note');
		var shadow = $('#shadowbox');
		var addnote = $('#add-note');
		var close = $('#close');
		var addnotebox = $('.add-note-box');
		var createnote = $('#create-note');
		var _id = '';
		notebox.center();
		
		var thusers = new Bloodhound({
			datumTokenizer: function(d) { return d.tokens; },
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			local: allUsers
		});
		thusers.initialize();
		
		$("#autocomplete")
			.typeahead(null, {
				name: 'participants',
				source: thusers.ttAdapter(),
				minLength: 3,
				templates: {
					suggestion: Handlebars.compile('<div class="row"><p class="tt-template-name">Participant ID: {{value}}</p><small class="text-muted">{{race}}</small><p class="tt-template-race">{{name}}</p><p class="tt-template-race">{{email}}</p></div>')
				}
			})
			.on("typeahead:selected", function(obj, datum){
				invoice.html(datum.value);
				$("#invoiceH").val(datum.value);
				name.html(datum.name);
				$("#nameH").val(datum.name);
				email.html(datum.email);
				$("#emailH").val(datum.email);
				//piType.html(datum.personalInformationType);
				$("#piType").val(datum.personalInformationType);
								
				var aUrl = "<?php echo base_url("ajax/buildActivityDropdownByPID"); ?>";
				var aPID = datum.value;

				$.ajax({
					type: "POST",
					url: aUrl,
					data: { aPID: aPID },
					async: false,
					success: function (result) {
						//alert(result);	
						$("#divActivity").html(result);
					}
				});

				/*
				if (typeof allNotes["id"+datum.value] == 'undefined') {
					$('#notes_count').html('<button type="button" class="btn btn-default"> Notes <div class="badge">0</div></button>');
				}
				else {
					$('#notes_count').html('<button type="button" class="btn btn-default"> Notes <div class="badge">' + allNotes["id"+datum.value].length + '</div></button>');
				}
				*/
			});

		if ($('input[name=invoice]').val() != '') {
			$('#invoice').html($('input[name=invoice]').val());
		}
		if ($('input[name=first_name]').val() != '') {
			$('#name').html($('input[name=name]').val());
		}
		if ($('input[name=email]').val() != '') {
			$('#email').html($('input[name=email]').val());
		}

		viewn.on('click', function () {
      
			$('.note-content').html('');
			var count = 0;
			var invoice = $('#invoiceH').val();
			_id = 'id' + invoice;
			//console.log(allNotes);
			/*
			for (property in allNotes[_id])          // should return 2
			{
				if (allNotes[_id].hasOwnProperty(property)) {
					count++;
				}
			}
			*/

			shadow.fadeIn();
			notebox.fadeIn();
			//alert(count);
			/*
			if (count > 0) {
				for (i = 0; i < count; i++) {
					$('.note-content').append('<div class="note"><h4 class="note-num">' + (i + 1) + '</h4><div class="note-text">' + ' ' + allNotes[_id][i].note + '</div><br /></div>');//<div class="note-edit eolbutton_small" id="'+notes[i].id+'">Edit</div>
				}
			}
			else {
				$('.note-content').html('<?php echo translate("K1005"); ?>!');
			}
			*/
		});

		addnote.on('click', function () {
			addnotebox.slideDown();
		});

		createnote.on('click', function () {
			//$.post('../includes/create_participant_note.php', {code: $('#invoiceH').val(), text: $('textarea[name=note]').val()}, function(data)
			$.post('../includes/ajaxHandler.php', {aFunction: "create_participant_note", code: $('#invoiceH').val(), text: $('textarea[name=note]').val()}, function (data) {
				$('textarea[name=note]').val('');
				addnotebox.slideUp();
				$('.note-content').html('');
				//$.post('../includes/get_participant_note.php', {code: $('#invoiceH').val()}, function(data)
				$.post('../includes/ajaxHandler.php', {aFunction: "get_participant_note", code: $('#invoiceH').val()}, function (data) {
					var notes = JSON.parse(data);
					var count = 0;

					for (property in notes)          // should return 2
					{
						if (notes.hasOwnProperty(property)) {
							count++;
						}
					}
					/*
					allNotes[_id] = [];
					for (i = 0; i < count; i++) {
						$('.note-content').append('<div class="note"><h4 class="note-num">' + (i + 1) + '</h4><div class="note-text">' + ' ' + notes[i].note + '</div><br /></div>');//<div class="note-edit eolbutton_small" id="'+notes[i].id+'">Edit</div>
						allNotes[_id].push(notes[i]);
					}

					$('#notes_count div').html(parseInt($('#notes_count div').html(), 10) + 1);
					*/
				});
			});
		});

		shadow.add(close).on('click', function () {
			shadow.fadeOut();
			notebox.fadeOut();
		});
		
		$("#myform").validate();
	});
</script>	
<div id="wrapper">
	<div id='shadowbox'></div>
	<div id="mainContainer" class="col-xs-12">
		<div class="row">
			<div class="col-xs-12">
				<div class="well well-sm text-center">
					<h4><?php echo translate("K244"); ?></h4>
				</div>
			</div>
		</div>
		<?php
		/*
		if (isset($_GET["error"])) {
			if ($_GET["error"] == "JME") {
				echo "<div class='eolerror'> <img src='../$supportDirectory/images/small_warning.png' class='warning'/><h4> " . translate("K245") . " </h4> </div> <br> <br>";
			} elseif ($_GET["error"] == "pass") {
				echo "<div class='eolerror'> <img src='../$supportDirectory/images/small_warning.png' class='warning'/> <h4> " . translate("K246") . " </h4> </div> <br> <br>";
			} elseif ($_GET["error"] == "dataError") {
				echo "<div class='eolerror'> <img src='../$supportDirectory/images/small_warning.png' class='warning'/> <h4> There was an error in the data with this participant.  Please contact <a href='mailto:techsupport@eventsonline.ca?subject=" . $eventname . " - Data Error - " . $_GET['invoice'] . "'>techsupport@eventsonline.ca</a> </h4> </div> <br> <br>";
			}
		}*/
		
		if (isset($msgDanger)) {
			echo "<div class=\"alert alert-danger\">" . translate($msgDanger) . "</div>";
		}
		else if (isset($msgSuccess)) {
			echo "<div class=\"alert alert-success\">" . translate($msgSuccess) . "</div>";
		}
		?>
		<div class="row">
			<div class="col-xs-12">
			<div class="form-group">
				<label for="autocomplete"><?php echo translate("K247"); ?></label>
				<input id='autocomplete' class="form-control">
			</div>

			<form name="myform" id="myform" method='POST' action="<?php echo base_url("admin/participant_search_modify"); ?>">
				<div class="form-group">
					<label><?php echo translate("Participant ID"); ?></label>
					<label class="form-control" id='invoice'></label>
					<input id='invoiceH' type='hidden' name='invoice' value=''/>
				</div>
				<div class="form-group">
					<label><?php echo translate("Full Name"); ?></label>
					<label class="form-control" id='name'></label>
					<input id='nameH' type='hidden' name='name' value=''/>
				</div>
				<div class="form-group">
					<label><?php echo translate("K251"); ?></label>
					<label class="form-control" id='email'></label>
					<input id='emailH' type='hidden' name='email' value=''/>
				</div>
				<div class="form-group">
					<label><?php echo translate("Activity"); ?></label>
					<div id="divActivity">
						<select id="activity" name="activity" class="form-control required" style="width:200px">
							<option value=""><?php echo translate("Select"); ?></option>							
						</select>
					</div>
				</div>
				<center>
					<input type="hidden" name="piType" id="piType" value="">
					<button class='btn btn-success btn-block' type="Submit" value="Modify"><span class='glyphicon glyphicon-pencil'></span> <?php echo translate("K254"); ?></button>
				</center>
			</form>
			</div>
		</div>
	</div>
</div>