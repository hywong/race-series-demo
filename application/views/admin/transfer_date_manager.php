<div id="wrapper">
	<div id="mainContainer" class="col-xs-12">
		<div class="well well-sm text-center">
			<h4><?php echo translate("Transfer Tool Availability Manager"); ?></h4>
		</div>	
		<?php 
	
		if (isset($msgDanger)) {
			echo "<div class=\"alert alert-danger\">" . translate($msgDanger) . "</div>";
		}
		else if (isset($msgSuccess)) {
			echo "<div class=\"alert alert-success\">" . translate($msgSuccess) . "</div>";
		}
		
		?>
		<?php if (empty($transferAvailabilityDates)) { ?>
		<div class="row">
			<div class="col-xs-12">
				<a href="<?php echo base_url("admin/transfer_date_add"); ?>" class="btn btn-default">
				<span class="glyphicon glyphicon-plus"></span> <?php echo translate("Add Transfer Tool Availability Dates"); ?>
				</a>
			</div>
		</div>
		<?php } ?>
		<div class="row">
			<div class="col-xs-12">
				<table id='activtytbl' class="table table-striped table-hover">
					<thead>
					<tr>
						<th><?php echo translate("Transfer Open Date"); ?></th>
						<th><?php echo translate("Transfer Close Date"); ?></th>    
						<?php
							if (!empty($transferAvailabilityDates)) {
								echo "<th>".translate('K356')."</th>";
							}
						?>
					</tr>
					</thead>
					<tbody>
					<?php
					$i = 0;
					if (!empty($transferAvailabilityDates)) {
						echo '<td>' . $transferAvailabilityDates['dttTransferStartDate'] . '</td>';
						echo '<td>' . $transferAvailabilityDates['dttTransferEndDate'] . '</td>';
						echo '<td><a class="btn btn-info btn-sm" href="' . base_url("admin/transfer_date_edit") . '"><span class="glyphicon glyphicon-pencil"></span> ' . translate("K991") . '</a></td>';

						   
					} 
					else {
						echo '<td colspan="2" class="text-center">' . translate("No Transfer Dates Set") . '</td>';
					}
					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>