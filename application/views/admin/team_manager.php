<script>

	$(document).ready(function () {
		$("#myform").validate();
		
		$("#selTeam").change(function() {			
			var aTeamName = $("#selTeam option:selected").text();
			$("#teamName").val(aTeamName);
		});
	});
	
</script>
<div id="wrapper">
	<div id="mainContainer" class="col-xs-12">
		<div class="well well-sm text-center">
			<h4><?php echo translate("Change Team Password"); ?></h4>
		</div>
		
		<?php 
	
		if (isset($msgDanger)) {
			echo "<div class=\"alert alert-danger\">" . translate($msgDanger) . "</div>";
		}
		else if (isset($msgSuccess)) {
			echo "<div class=\"alert alert-success\">" . translate($msgSuccess) . "</div>";
		}
		
		?>
		<div class="row">
			<div class="col-xs-12">
				<div class="well">
					<form method="POST" action="<?php echo base_url("admin/team_manager"); ?>" id="myform" name="myform">						
						<div class="form-group">
							<label><?php echo translate("Teams"); ?></label>
							<select name="selTeam" id="selTeam" class="form-control required">					
							<?php  
								
								echo "<option value=\"\">" . translate("Select") . "</option>";
								
								foreach ($teams as $teamID => $teamName) {
									echo "<option value=\"" . $teamID . "\">" . $teamName . "</option>";
								}
							?>
							</select>
						</div>
						<div class="form-group">
							<label for=""><?php echo translate("Team Name"); ?></label>
							<input type="text" id="teamName" name="teamName" class="form-control required" value="" />
						</div>
						<div class="form-group">
							<label for=""><?php echo translate("New Team Password"); ?></label>
							<input type="text" id="teamPassword" name="teamPassword" class="form-control required" value="" />
						</div>
						<div class="form-group">
							<button class='btn btn-success btn-block'><span class="glyphicon glyphicon-ok"></span> <?php echo translate("Change Team Password"); ?></button>
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
</div>