<?php
	//echo "<pre>"; print_r($charityTotals); echo "</pre>"; exit;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard - SB Admin</title>

    <!-- Bootstrap core CSS -->
    

    <!-- Add custom CSS here -->
	<!--
	<link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">
	-->
	<!--<script src="<?php //echo base_url("assets/js/pages/index.js");?>"></script>-->
	<script>
		$(document).ready(function() {
			$('#morris-chart-area').parent().removeClass("ajax-chart");

			Morris.Area({
			// ID of the element in which to draw the chart.
			element: 'morris-chart-area',
			// Example data
			data: [
			<?php 
				
				$arrIndivDateCount = array();
				$indivCountByDay = count($indivRegistrationCountByDay);
				for ($i = 0; $i < $indivCountByDay; $i++) {
					$arrIndivDateCount[$indivRegistrationCountByDay[$i]['dateOnly']] = $indivRegistrationCountByDay[$i]['aCount'];
				}
				
				$arrGroupDateCount = array();
				$groupCountByDay = count($groupRegistrationCountByDay);
				for ($i = 0; $i < $groupCountByDay; $i++) {
					$arrGroupDateCount[$groupRegistrationCountByDay[$i]['dateOnly']] = $groupRegistrationCountByDay[$i]['aCount'];
				}
					
				$startDate = "";
				$endDate = "";
				$dateRegistrationCount = array();
				for ($i = 30; $i >= 0; $i--) {					
					$buildDate = date("Y-m-d", strtotime('-'. $i .' days'));
					$aDateCount = ((isset($arrIndivDateCount[$buildDate]))?$arrIndivDateCount[$buildDate]:0) + ((isset($arrGroupDateCount[$buildDate]))?$arrGroupDateCount[$buildDate]:0);
					echo '{date: "' . $buildDate . '", registrations: ' . $aDateCount . '},';
					
					if ($i == 30) {
						$startDate = $buildDate;
					}
					else if ($i == 0) {
						$endDate = $buildDate;
					}
				}
			?>
			
			],
			// The name of the data record attribute that contains x-visitss.
			xkey: 'date',
			// A list of names of data record attributes that contain y-visitss.
			ykeys: ['registrations'],
			// Labels for the ykeys -- will be displayed when you hover over the
			// chart.
			labels: ['Registrations'],
			// Disables line smoothing
			smooth: false,
			
			resize: true
			});

			Morris.Bar ({
				element: 'morris-chart-bar',
				data: [
				<?php 
					
					foreach ($registrationCountByEventOrActivity as $aLabel => $aCount) {
						echo '{label: "' . $aLabel . '", value: ' . $aCount . '},';
					}
				?>

				],
				xkey: 'label',
				ykeys: ['value'],
				labels: ['Count'],
				barRatio: 0.4,
				xLabelAngle: 30,
				hideHover: 'auto',
				resize: true
			});	
			
			Morris.Donut({
				element: 'morris-chart-donut-charity',
				data: [
				<?php 
					
					foreach ($charityTotals as $aCharity => $anAmount) {
						echo '{label: "' . $aCharity . '", value: ' . $anAmount . '},';
					}
				?>
				
				],
				
				formatter: function (y) { return "$" + y;}
			});
			
	
		});
	</script>
  </head>

  <body>

    <div id="wrapper">



      <div id="page-wrapper">
<br /><br />
        <div class="row">
          <div class="col-lg-12">
            <h1><?php echo translate("Dashboard"); ?> <small><?php echo translate("Statistics Overview"); ?></small></h1>
            <ol class="breadcrumb">
              <li class="active"><i class="fa fa-dashboard"></i> <?php echo translate("Dashboard"); ?></li>
            </ol>
          </div>
        </div><!-- /.row -->

        <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> <?php echo translate("Registration Statistics"); ?> <?php echo $startDate; ?> - <?php echo $endDate; ?></h3>
              </div>
              <div class="panel-body ajax-chart">
                <div id="morris-chart-area"></div>
              </div>
            </div>
          </div>
        </div><!-- /.row -->

        <div class="row">
          <div class="col-lg-6">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> <?php echo (($_SESSION['admin']['adminEventID'] == 1)?translate("Number of Registrations by Event"):translate("Number of Registrations by Activity")); ?></h3>
              </div>
              <div class="panel-body">
                <div id="morris-chart-bar"></div>
				<!--
                <div class="text-right">
                  <a href="#">View Details <i class="fa fa-arrow-circle-right"></i></a>
                </div>
				-->
              </div>
            </div>
          </div>
		  
		  <div class="col-lg-6">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-clock-o"></i> <?php echo translate("Charity Totals"); ?></h3>
              </div>
              <div class="panel-body">
				<div id="morris-chart-donut-charity"></div>
              </div>
            </div>
          </div>
        </div>

		<div class="row">
          <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-money"></i> <?php echo translate("Recent Transactions"); ?></h3>
              </div>
              <div class="panel-body">
                <div class="table-responsive">
                  <table class="table table-bordered table-hover table-striped tablesorter">
                    <thead>
                      <tr>
                        <th><?php echo translate("K240"); ?> <i class="fa fa-sort"></i></th>
                        <th><?php echo translate("Type"); ?> <i class="fa fa-sort"></i></th>
                        <th><?php echo translate("Date"); ?> <i class="fa fa-sort"></i></th>
                        <th><?php echo translate("Amount"); ?> <i class="fa fa-sort"></i></th>
                      </tr>
                    </thead>
                    <tbody>
					<?php
					
						$countRecentTransactions = count($recentTransactions);
						for ($i = 0; $i < $countRecentTransactions; $i++) {
							//$dateConvert = strtotime($recentTransactions[$i]['dttDateCreated']);
							//$aDate = date("m/d/Y",$dateConvert);
							//$aTime = date("g:i A",$dateConvert);
							echo "<tr>
								<td>" . $recentTransactions[$i]['intCartID'] . "</td>
								<td>" . $recentTransactions[$i]['strTransactionType'] . "</td>
								<td>" . $recentTransactions[$i]['dttDateCreated'] . "</td>
								<td>$" . number_format($recentTransactions[$i]['dblFinalTotal'],2) . "</td>
							</tr>";							
						}
						
					?>
						
                    </tbody>
                  </table>
                </div>
				<!--
                <div class="text-right">
                  <a href="#">View All Transactions <i class="fa fa-arrow-circle-right"></i></a>
                </div>
				-->
              </div>
            </div>
          </div>
		  
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->

    <!-- JavaScript -->
	<!--
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>


    <script src="js/morris/raphael.js"></script>
    <script src="js/morris/morris.js"></script>
    <script src="js/morris/chart-data-morris.js"></script>
    <script src="js/tablesorter/jquery.tablesorter.js"></script>
    <script src="js/pages/index.js"></script>
    <script src="js/tablesorter/tables.js"></script>
	-->

  </body>
</html>
