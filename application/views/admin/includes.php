<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Race Series Demo Registration</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
		<!-- Optional theme -->
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
		<!-- Latest compiled and minified JavaScript -->		
		<link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap-datetimepicker.min.css");?>">
		<link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap-datepicker.css");?>">
				
		<link rel="stylesheet" href="<?php echo base_url("assets/css/admin.css");?>">
		<link rel="stylesheet" href="<?php echo base_url("assets/css/font-awesome.min.css");?>">
		<link rel="stylesheet" href="<?php echo base_url("assets/css/typeahead.css");?>">
		<link rel="stylesheet" href="<?php echo base_url("assets/css/morris.css");?>">
		<link rel="stylesheet" href="<?php echo base_url("assets/css/sb-admin.css");?>">
		
		<link rel="stylesheet" href="<?php echo base_url("assets/css/core-themeB.css");?>">
<link rel="stylesheet" href="<?php echo base_url("assets/css/core.css");?>">
		
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script src="<?php echo base_url("assets/js/bootstrap.min.js");?>"></script>
		<script src="<?php echo base_url("assets/js/jquery.validation/jquery.validate.min.js");?>"></script>
		<script src="<?php echo base_url("assets/js/moment.min.js");?>"></script>
		<script src="<?php echo base_url("assets/js/bootstrap-datetimepicker.min.js");?>"></script>
		<script src="<?php echo base_url("assets/js/jquery.tablesorter.min.js");?>"></script>
		<script src="<?php echo base_url("assets/js/typeahead.js");?>"></script>
		<script src="<?php echo base_url("assets/js/custom.js");?>"></script>
		<script src="<?php echo base_url("assets/js/handlebars-v1.3.0.js");?>"></script>
		
		<script src="<?php echo base_url("assets/js/morris/raphael.js");?>"></script>
		<script src="<?php echo base_url("assets/js/morris/morris.js");?>"></script>
		<script src="<?php echo base_url("assets/js/morris/chart-data-morris.js");?>"></script>
		<script src="<?php echo base_url("assets/js/tablesorter/jquery.tablesorter.js");?>"></script>
		
		<script src="<?php echo base_url("assets/js/bootstrap-datepicker.js");?>"></script>
		<script>
		$(function(){
		  $('.date-picker').datepicker({
		    format: 'yyyy/mm/dd',
		    startView: 2,
		    autoclose: true
		  });
		  
		  $('#manage-btn').on('show.bs.collapse', function () {
			  $('#manage_plus').css({"display":"none"});
			  $('#manage_minus').css({"display":"block"});
			});
		 $('#manage-btn').on('hidden.bs.collapse', function () {
			  $('#manage_plus').css({"display":"block"});
			  $('#manage_minus').css({"display":"none"});
			});
			
			 $('#download-btn').on('show.bs.collapse', function () {
			  $('#download_plus').css({"display":"none"});
			  $('#download_minus').css({"display":"block"});
			});
		 $('#download-btn').on('hidden.bs.collapse', function () {
			  $('#download_plus').css({"display":"block"});
			  $('#download_minus').css({"display":"none"});
			});
			
			 $('#misc-btn').on('show.bs.collapse', function () {
			  $('#misc_plus').css({"display":"none"});
			  $('#misc_minus').css({"display":"block"});
			});
		 $('#misc-btn').on('hidden.bs.collapse', function () {
			  $('#misc_plus').css({"display":"block"});
			  $('#misc_minus').css({"display":"none"});
			});
		});
		</script>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-6190158-1', 'canadarunningseries.com');
		  ga('send', 'pageview');

		</script>
	</head>
