<?php

	$participantModifyOpenDate = "";
	$participantModifyCloseDate = "";
	$submitButton = translate("K830");
	if (isset($formType) && $formType == "edit") {
		$participantModifyOpenDate = $participantModifyAvailabilityDates['dttPublicModifyStartDate'];
		$participantModifyCloseDate = $participantModifyAvailabilityDates['dttPublicModifyEndDate'];
		$submitButton = translate("K876");
	}

?>
<script>
	$(document).ready(function() {	
	
		$('#txtParticipantModifyOpenDate').datetimepicker({ language: 'en', pick12HourFormat: false });
		$('#txtParticipantModifyCloseDate').datetimepicker({ language: 'en', pick12HourFormat: false });
		
		jQuery.validator.addMethod("validateDates", function (value, element) {
			var openDate = $("#txtParticipantModifyOpenDate").val();
			var closeDate = $("#txtParticipantModifyCloseDate").val();
			var openTime = $("#txtParticipantModifyOpenTime").val();
			var closeTime = $("#txtParticipantModifyCloseTime").val();
			var newOpenDate = new Date(openDate + " " + openTime);
			var newCloseDate = new Date(closeDate + " " + closeTime);		
			
			if (newOpenDate > newCloseDate) {
				return false;
			}
			else {
				return true;
			}
		}, $("#hidInvalidDates").val());						
		
		$("#myform").validate({
			rules: {
				txtParticipantModifyCloseDate: {
					validateDates: true
				}				
			}
		});
	});
</script>
<style>

	.form-control[readonly] {
		background-color: white;
		cursor: auto;
	}

</style>
<div id="wrapper">
	<div id="mainContainer" class="col-xs-12">
		<div class="row">
			<div class="col-xs-12">
				<div class="well well-sm text-center">
					<h4><?php echo translate("Add Participant Modify Tool Availability Dates"); ?></h4>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<div class="well">
					<form method="POST" action="<?php echo base_url("admin/participant_modify_date_" . $formType); ?>" id="myform" name="myform">
						<input type="hidden" id="hidInvalidDates" name="hidInvalidDates" value="<?php echo translate("The Participant Modify Close Date cannot be before the Participant Modify Open Date"); ?>">
						<div class="form-group">
							<label><?php echo translate("Participant Modify Open Date"); ?> (EST)</label>
							<input type="text" id="txtParticipantModifyOpenDate" name="txtParticipantModifyOpenDate" class="form-control required" value="<?php echo $participantModifyOpenDate; ?>" readonly />
						</div>
						<div class="form-group">
							<label for=""><?php echo translate("Participant Modify Close Date"); ?> (EST)</label>
							<input type="text" id="txtParticipantModifyCloseDate" name="txtParticipantModifyCloseDate" class="form-control required" value="<?php echo $participantModifyCloseDate; ?>" readonly />
						</div>
						<div class="form-group">
							<button class='btn btn-success btn-block'><span class="glyphicon glyphicon-ok"></span> <?php echo $submitButton; ?></button>
						</div>
						<div class="form-group">
							<a href="<?php echo base_url("admin/participant_modify_date_manager"); ?>" class='btn btn-danger btn-block'><span class="glyphicon glyphicon-remove"></span> <?php echo translate("Cancel"); ?></a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>