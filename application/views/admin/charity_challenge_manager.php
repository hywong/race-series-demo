<div id="wrapper">
	<div id="mainContainer" class="col-xs-12">
		<div class="well well-sm text-center">
			<h4><?php echo translate("Charity Challenge Manager"); ?></h4>
		</div>
		<noscript>
			<div class='eolerror'><?php echo translate("K106"); ?>
				<a href='http://www.enable-javascript.com/'>http://www.enable-javascript.com/</a> <?php echo translate("K107"); ?>
			</div>
		</noscript>
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<ul class="nav nav-tabs" role="tablist">
				  <li class="active width33"><a href="#pending" role="tab" data-toggle="tab"><b>Pending</b></a></li>
				  <li class="width33"><a href="#live" role="tab" data-toggle="tab"><b>Live</b></a></li>
				  <li class="width33"><a href="#custom" role="tab" data-toggle="tab"><b>Custom</b></a></li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
				  <div class="tab-pane active" id="pending">
				  	<table class="table table-condensed table-hover">
				  		<thead>
				  			<tr>
				  				<th><?php echo translate("Charity"); ?></th>
				  				<th><?php echo translate("Settings"); ?></th>
				  			</tr>
				  		</thead>
				  		<tbody>
				  	<?php
				  		if(!empty($pending))
				  		{
				  			foreach($pending as $charity)
				  			{
				  				echo "<tr><td>".$charity['artez_charity']."</td><td></td></tr>";
				  			}
				  		}
				  		else
				  		{
				  			echo "<tr><td colspan=\"2\" class=\"text-center\"><h3>".translate("No charities are pending your approval")."</h3></td></tr>";
				  		}
				  	?>
				  		</tbody>
				  	</table>
				  </div>
				  <div class="tab-pane" id="live">
				  	<table class="table table-condensed table-hover">
				  		<thead>
				  			<tr>
				  				<th><?php echo translate("Charity"); ?></th>
				  				<th><?php echo translate("Pin"); ?></th>
				  				<th><?php echo translate("Donation ID"); ?></th>
				  				<th><?php echo translate("Qty Cap"); ?></th>
				  				<th><?php echo translate("Dollar Cap"); ?></th>
				  				<th><?php echo translate("Actions"); ?></th>
				  			</tr>
				  		</thead>
				  		<tbody>
				  	<?php
				  		if(!empty($live))
				  		{
				  			foreach($live as $charity)
				  			{
				  				echo "<tr><td>".$charity['artez_charity']."</td><td>".$charity['pin']."</td><td>".$charity['artez_donation_id'].'</td><td>'.$charity['qty']."</td><td>".$charity['amount']."</td><td><button class=\"btn btn-default\" type=\"button\">Action</button></td></tr>";
				  			}
				  		}
				  		else
				  		{
				  			echo "<tr><td colspan=\"6\" class=\"text-center\"><h3>".translate("No charities are set live")."</h3></td></tr>";
				  		}
				  	?>
				  		</tbody>
				  	</table>
				  </div>
				  <div class="tab-pane" id="custom">
				  	<table class="table table-condensed table-hover">
				  		<thead>
				  			<tr>
				  				<th><?php echo translate("Charity"); ?></th>
				  				<th><?php echo translate("Pin"); ?></th>
				  				<th><?php echo translate("URL"); ?></th>
				  				<th><?php echo translate("Qty Cap"); ?></th>
				  				<th><?php echo translate("Dollar Cap"); ?></th>
				  				<th><?php echo translate("Actions"); ?></th>
				  			</tr>
				  		</thead>
				  		<tbody>
				  	<?php
				  		if(!empty($pending))
				  		{

				  		}
				  		else
				  		{
				  			echo "<tr><td colspan=\"6\" class=\"text-center\"><h3>".translate("No custom charities created")."</h3></td></tr>";
				  		}
				  	?>
				  		</tbody>
				  	</table>
				  </div>
				</div>
			</div>
		</div>
	</div>
</div>