<?php

	$aCap = "";
	if (isset($formType) && $formType == "edit") {
		$aCap = $activities['intCap'];
	}
	
?>
<script>
	$(document).ready(function () {
		$("#myform").validate({
			rules: {
				cap: {
					digits: true
				}
			}
		});
	});
</script>
<div id="wrapper">	
	<div id="mainContainer" class="col-xs-12">
		<div class="row">
			<div class="col-xs-12">
				<div class="well well-sm text-center">
					<h4><?php echo translate("K80"); ?></h4>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="well">
					<form method="POST" action="<?php echo base_url("admin/activity_cap_" . $formType); ?>" id="myform" name="myform">
						<div class="form-group">
							<label for="activity"><?php echo translate("K82"); ?></label>					
							<select name='activity' id="activity" class="form-control required">
								<?php
								
								if (isset($formType) && $formType == "add") {
									echo '<option value="">' . translate("K83") . '</option>';
	
									foreach ($activities as $activityID => $details) {
										if (trim($details['intCap']) == "") {
											echo "<option value=\"" . $activityID . "\">" . $details['strActivity'] . "</option>";
										}
									} 
								}
								else if (isset($formType) && $formType == "edit") {
									echo "<option value=\"" . $activities['intActivityID'] . "\">" . $activities['strActivity'] . "</option>";
								}
								
								?>
							</select>
						</div>
						<div class="form-group">
							<label for="cap"><?php echo translate("K983"); ?></label>
							<div class="input-group">
								<span class="input-group-addon">#</span>
								<input type="text" id="cap" name="cap" class="form-control required" value="<?php echo $aCap; ?>">
							</div>
						</div>
						<div class="form-group">
							<button class='btn btn-success btn-block'><span class="glyphicon glyphicon-ok"></span> <?php echo translate("K876"); ?></button>
						</div>
						<div class="form-group">
							<a href="<?php echo base_url("admin/activity_cap_manager"); ?>" class='btn btn-danger btn-block'><span class="glyphicon glyphicon-remove"></span> <?php echo translate("Cancel"); ?></a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>