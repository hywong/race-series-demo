<script src="<?php echo base_url("assets/js/jquery.vticker-min.js");?>"></script>
<script type="text/javascript">
    $(function () {
        $('#news-container').vTicker({
            speed: 600,
            pause: 2500,
            showItems: 2
        });
    });
</script>
<script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
		<?php
		if($pledgeProfileDetails != FALSE)
		{
		?>
		<div class="col-sm-8">
			<div class="well">
				<div class="row">
					<div class="col-sm-5 text-center">
						<div class="row text-center">
							<a href="<?php echo base_url('pledge/athlete').'/'.$pledgeProfileDetails['profileID']; ?>" class="btn btn-success">
								<i class="glyphicon glyphicon-heart"></i> <?php echo translate("Pledge athlete"); ?>
							</a>
						</div>
						<h4><?php echo $pledgeProfileDetails['firstName'] . " " . $pledgeProfileDetails['lastName']; ?></h4>
							<span class='st_sharethis_large' displayText='ShareThis'></span>
							<span class='st_facebook_large' displayText='Facebook'></span>
							<span class='st_twitter_large' displayText='Tweet'></span>
							<span class='st_linkedin_large' displayText='LinkedIn'></span>
							<span class='st_email_large' displayText='Email'></span>
						
							<div class="row">
							<img class="img-responsive" style="margin:auto;" src="https://secure.eventsonline.ca/profile_uploads/pledge_profile/default/_load.php?id=<?php echo $pledgeProfileDetails['picture']; ?>.jpg&blank=blank.jpg&<?php echo strtotime("now");?>" />
							</div>
					</div>
					<div class="col-sm-7 text-left">
						<h4><?php echo translate("Charity"). ': ' . $pledgeProfileDetails['charity']; ?></h4><br />
						<h4><?php echo translate("Pledge Goal"); ?>: $<?php echo $pledgeProfileDetails['goal']; ?></h4>	<br />		
						<h4><?php echo translate("K870"); ?>:</h4>
						<br/>
						<?php
						echo $pledgeProfileDetails['message'];
						?>
					</div>
				</div>
			</div>
		</div>

		<div class="col-sm-4 text-left">
			<div class="well well-sm text-center">
				<h4><?php echo translate("Recent Pledges"); ?></h4>
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div id="news-container">
							<ul style="list-style-type:none;">
								<?php
								
								$totalRaised = 0;
								$percentRaisedSoFar = 0;

								$countPledges = count($pledgesForProfile);

								if ($countPledges == 0) {
									echo "<li>" . translate("K837") . "</li>";
								}
								else {
									for ($i = 0; $i < $countPledges; $i++) {
										echo "<li style='word-wrap: break-word;'><strong>" . $pledgesForProfile[$i]['strFirstName'] . " " . $pledgesForProfile[$i]['strLastName'] . " $" . $pledgesForProfile[$i]['dblDonationSubTotal'] . "</strong>";
										if (trim($pledgesForProfile[$i]['strPledgingMessage']) != "") {
											echo "<br />" . $pledgesForProfile[$i]['strPledgingMessage'];
										}
										echo "</li>";
										$totalRaised += $pledgesForProfile[$i]['dblDonationSubTotal'];
									}
								}
								if($totalRaised > 0)
								{
									$percentRaisedSoFar = round((($totalRaised / $pledgeProfileDetails['goal']) * 100),0);
								}
								else
								{
									$percentRaisedSoFar = 0;
								}
								
								?>
							</ul>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 text-center">
						<?php 						
						echo "<span>" . $percentRaisedSoFar . "% " . translate("K887") . "</span><br />";
						echo translate("K839"); echo "$".number_format($totalRaised, 2, '.', '');
						?>
						<img src="<?php echo base_url('assets/img/0.png');?>"/>
					</div>
				</div>
			</div>
		</div>
		<?php
		}
		else
		{
		?>
		<div class="col-xs-12">
			<div class="well text-center">
				<h3>No profile information found</h3>
			</div>
		</div>
		<?php
		}
		?>
		<br />	
		<a href="<?php echo base_url("/"); ?>" class="btn btn-default"><span class="glyphicon glyphicon-circle-arrow-left"></span> <?php echo translate("K223"); ?></a>
