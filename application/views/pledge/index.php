<?php
  $formitForm = array(
    "method" => "POST",
    "action" => base_url("pledge/confirm"),
    "sections" => $sections
    );

  $formitSettings = array(
      "label_columns" => "col-sm-3",
      "field_columns" => "col-sm-9",
      "class" => "customClass",
      "id" => "myForm",
      "type" => "horizontal"
    );
?>
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <div class="well">
        <div class="row">
          <div class="col-xs-12">
          <h3><b><?php echo translate("You are pledging: ").$_SESSION['pledge']['athleteName'];?></b></h3>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <?php echo formit($formitForm, $formitSettings); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>