<?php
  
  $formitSettings = array(
    "id" => "paymentForm"
    );
  // echo "<PRE>";
  // print_r($_SESSION);
  // echo "</PRE>";
?>
<div class="container">
  <div class="row">
    <div class="col-sm-offset-2 col-sm-8">
      <?php
      foreach($summary as $event => $breakdown)
      {
        echo "<div class=\"row\"><h3>".$event."</h3></div><hr />";
        foreach($breakdown as $section => $itemDetails)
        {
          echo "<div class=\"row\"><div class=\"col-xs-12\">".$section."</div></div>";
          echo "<div class=\"row\"><div class=\"col-xs-12\">";
          foreach($itemDetails as $item => $qty)
          {
            $itemCount = count($qty);
            for($i = 0; $i < $itemCount; $i++)
            {
              if($item == "Sub-total")
              {
                echo "<div class=\"row bottom-border\"><div class=\"col-xs-5 col-xs-offset-1\"><b>".$item."</b></div>";
                echo "<div class=\"col-xs-6 text-right\"><b>$".number_format($qty[$i], 2, '.', '')."</b></div></div>";
              }
              else
              {
                echo "<div class=\"row bottom-border\"><div class=\"col-xs-5 col-xs-offset-1\">".$item."</div>";
                echo "<div class=\"col-xs-6 text-right\">$".number_format($qty[$i], 2, '.', '')."</div></div>";
              }
            }
          }
          echo"</div></div>";
        }
      }
      ?>
    </div>
  </div>
  <br />
  <div class="row">
    <div class="col-xs-12">
      <div class="col-sm-offset-2 col-sm-8">
        <div class="well">
          <div id="cardInformation">
            <fieldset>
              <legend class="text-center"><?php echo translate("Supported Credit Cards");?></legend>
            </fieldset>
            <div class="row">
              <div class="col-sm-offset-3 col-sm-6 text-center">
                <img src="<?php echo base_url("assets/img/visa_48.png");?>" title="Visa" alt="Visa" />
                <img src="<?php echo base_url("assets/img/mastercard_48.png");?>" title="Master Card" alt="Master Card" />
                <img src="<?php echo base_url("assets/img/amex_48.png");?>" title="American Express" alt="American Express" />
              </div>
            </div>
            <?php
              formit($paymentForm, $formitSettings);
            ?>
          </div>
          <div id="processingCard">
            <fieldset>
              <legend class="text-center"><?php echo translate("Processing Payment");?></legend>
            </fieldset>
            <div class="row">
              <div class="col-sm-offset-3 col-sm-6 text-center">
                <img src="<?php echo base_url("assets/img/loading_large.gif"); ?>" title="Loading" alt="Loading"/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(function(){
  $("#paymentForm").on("submit", function(){
    $("#cardInformation").slideUp();
    $("#processingCard").slideDown();
  });
});
</script>