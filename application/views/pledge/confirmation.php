<?php
  // echo "<PRE>";
  // print_r($_SESSION);
  // echo "</PRE>";
  // exit;
?>

<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <div class="well">
        <div class="well">
          <h3>Personal Information </h3>
          <table class="table table-condensed table-hover">
          <thead>
            <tr><th>Field</th><th>Input</th></tr>
          </thead>
          <tbody>
            <?php
            $hideFields = array(
              "athleteID",
              "athleteCharityID",
              "athleteEventID"
              );
            foreach($_SESSION['pledge'] as $field => $value)
            {
              if(!in_array($field, $hideFields))
                echo "<tr><td class=\"half-width\">".ucwords(str_replace("_", " ", $field))."</td><td class=\"half-width\">".$value."</td></tr>";
            }
            ?>
          </tbody>
          </table>
        </div>      
        <div class="row">
          <div class="col-sm-offset-3 col-sm-5">
            <a href="<?php echo base_url("pledge/payment");?>" class="btn btn-block btn-success"><i class="glyphicon glyphicon-ok"></i> Confirm</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>