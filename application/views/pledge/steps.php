<ul id="progressbar" data-progress-steps="7">
<?php
  $pages = array(
    "Personal Information" => "",
    "Confirmation" => "",
    "Payment and Receipt" => ""
    );
  $pageLinks = array(
    "Personal Information" => "register/personal",
    "Confirmation" => "register/confirmation",
    "Payment and Receipt" => "payment"
    );
  $default = "";
	foreach($pages as $page=>$status){
		if($status == "active"){
			echo "<li class=\"" . $status . "\"><a href=\"".base_url($pageLinks[$page])."\">".$page."</a></li>";
			$default = "";
		}else{
			echo "<li class=\"" . $default . "\"><a href=\"".base_url($pageLinks[$page])."\"><span class=\"hidden-xs\">".$page."</span></a></li>";
		}
	}
?>
</ul>
<div class="clearfix"></div>