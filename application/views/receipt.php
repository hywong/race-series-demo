<?php
  
?>
<!-- Modal -->
<div class="modal fade feedback-modal" tabindex="-1" role="dialog" aria-labelledby="feedbackModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo translate("Feedback"); ?></h4>
      </div>
      <form role="form" class="feedback-form">
        <div class="modal-body">
            <div class="form-group">
              <label for="fbEmail"><?php echo translate("Email"); ?></label>
              <input type="email" id="fbEmail" name="fbemail" class="form-control" required />
            </div>
            <div class="form-group">
              <label for="fbName"><?php echo translate("Name"); ?></label>
              <input type="text" id="fbName" name="fbname" class="form-control" required />
            </div>
            <div class="form-group">
              <label for="feedback"><?php echo translate("Feedback"); ?></label>
              <textarea name="fbfeedback" id="feedback" class="form-control" required></textarea>
            </div>
            <div class="form-group">
              <label for="rating"><?php echo translate("Rate your experience"); ?></label>
              <br />
              <h3>
                <i data-rating="1" class="glyphicon glyphicon-star-empty rating"></i>
                <i data-rating="2" class="glyphicon glyphicon-star-empty rating"></i>
                <i data-rating="3" class="glyphicon glyphicon-star-empty rating"></i>
                <i data-rating="4" class="glyphicon glyphicon-star-empty rating"></i>
                <i data-rating="5" class="glyphicon glyphicon-star-empty rating"></i> 
              </h3>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo translate("Close"); ?></button>
          <button class="btn btn-success" id="sendFeedback"><div id="sendingFeedback"></div> <i class="glyphicon glyphicon-thumbs-up"></i> <?php echo translate("Tell us!"); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <div class="col-sm-offset-1 col-sm-10">
        <div class="well">
          <fieldset>
            <legend class="text-center"><?php echo translate("Transaction Summary");?></legend>
          </fieldset>
          <?php
            if($response['status'] === TRUE)
            {
              echo "<div class=\"row\"><div class=\"col-xs-12 text-center\">
    <span class='st_sharethis_large' displayText='ShareThis'></span>
    <span class='st_reddit_large' displayText='Reddit'></span>
    <span class='st_facebook_large' displayText='Facebook'></span>
    <span class='st_googleplus_large' displayText='Google +'></span>
    <span class='st_twitter_large' displayText='Tweet'></span>
    <span class='st_linkedin_large' displayText='LinkedIn'></span>
    <span class='st_pinterest_large' displayText='Pinterest'></span>
    <span class='st_email_large' displayText='Email'></span></div></div>
    <br/>";
              echo "<div class=\"alert alert-success text-center\"><h4><b>".translate("Congratulations, you have been registered successfully!")."<br /><br />";
            }
            else
            {
              echo "<div class=\"alert alert-danger text-center\">
              <h4><b>";
            }
            if(isset($response['bank_error_code1']) && isset($response['bank_error_code2']) && isset($response['bank_error_message']))
              echo "Bank: (".$response['bank_error_code1']."-".$response['bank_error_code2'].") ".$response['bank_error_message']."<br />";

            if(isset($response['gateway_error_code']) && isset($response['gateway_error_message']))
            echo "System: (".$response['gateway_error_code'].") ".$response['gateway_error_message']."<br /><br />";
            if(isset($response['receipt']))
            {
              echo $response['receipt'];
            }
            if($response['status'] === FALSE)
            {
              echo "<br /> <br />
              <a href=\"".base_url("payment")."\" class=\"btn btn-success\">".translate("Try Again")."</a>";
            }
            echo "</b></h4></div>";
          ?>
          <?php
            if($response['status'] === TRUE)
            {
        ?>
			  <div class="row">
                <div class="col-xs-12 feedback-btn-info">
                  <button type="button" data-toggle="modal" data-target=".feedback-modal" class="btn btn-info btn-lg btn-block"><?php echo translate("How was your registration process?"); ?><br /><?php echo translate("Click here to leave us your feedback!"); ?></button>
                </div>
                <div class="col-xs-12 text-center">
                  <small><?php echo translate("We would really appreciate it, this helps us consider new features and improve your registration experience.")?> <?php echo translate("Thank you");?>.</small>
                </div>
              </div>
              <br /><br />
              <div class="row">
                <div class="col-sm-6">
                  <a class="btn btn-default btn-block" href="<?php echo base_url("participant"); ?>" target="_blank"><i class="glyphicon glyphicon-user"></i> <?php echo translate("Participant Dashboard");?></a>
                </div>
                <div class="col-sm-6">
                  <a class="btn btn-default btn-block" href="<?php echo base_url("confirmation/participant_search"); ?>" target="_blank"><i class="glyphicon glyphicon-search"></i> <?php echo translate("Search Participant");?></a>
                </div>
              </div>
              <br />
              <div class="row">
                <div class="col-sm-6">
                  <a class="btn btn-default btn-block" href="https://secure.eventsonline.ca" target="_blank" style=""><i class="glyphicon glyphicon-link"></i> <?php echo translate("Visit EventsOnline");?></a>
                </div>
                <div class="col-sm-6">
                  <a class="btn btn-default btn-block" href="<?php echo base_url("confirmation/resend"); ?>" target="_blank"><i class="glyphicon glyphicon-ok"></i> <?php echo translate("K404");?></a>
                </div>
              </div>
              <br />
              <div class="row">
                <div class="col-sm-6">
                  <a class="btn btn-default btn-block" href="<?php echo base_url("register"); ?>"><i class="glyphicon glyphicon-circle-arrow-left"></i> <?php echo translate("K748");?></a>
                </div>
              </div>
              <?php
            }
          ?>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(function(){

  ratingSelected = 0;

  $("#paymentForm").on("submit", function(){
    $("#cardInformation").slideUp();
    $("#processingCard").slideDown();
  });

  $('.rating').on('mouseenter', function(){
    if(ratingSelected == 0)
    {
      var ratingHover = $(this).data('rating');

      if($(this).hasClass("glyphicon-star-empty"))
      {
        for(i = ratingHover; i >= 1; i--)
        {
          $('*[data-rating="'+i+'"]').removeClass('glyphicon-star-empty');
          $('*[data-rating="'+i+'"]').addClass('glyphicon-star');
        }
      }      
    }
  });

  $('.rating').on('mouseleave', function(){
    if(ratingSelected == 0)
    {
      $('.rating').removeClass('glyphicon-star');
      $('.rating').addClass("glyphicon-star-empty");
    }
  });

  $('.rating').on('click', function(){
    ratingSelected = $(this).data('rating');

    $('.rating').removeClass('glyphicon-star');
    $('.rating').addClass("glyphicon-star-empty");
    for(i = ratingSelected; i >= 1; i--)
    {
      $('*[data-rating="'+i+'"]').removeClass('glyphicon-star-empty');
      $('*[data-rating="'+i+'"]').addClass('glyphicon-star');
    }
  });

  $('.feedback-form').on('submit', function(e){
    $('#sendFeedback').prop("disabled", true);
    $.post('<?php echo base_url("ajax/send_feedback"); ?>', {form: $('.feedback-form').serializeObject(), rating: ratingSelected}, function(response){
      $('.feedback-modal').modal('hide');
      $('.feedback-btn-info').slideUp('fast', function() {
        $('.feedback-btn-info').html("<div class=\"alert alert-info text-center\"><h4><i class=\"glyphicon glyphicon-heart\"></i> <?php echo translate('Thank you'); ?></h4></div>");
        $('.feedback-btn-info').slideDown();
      });
    });

    return false;
  });

});
</script>
<script type="text/javascript">var switchTo5x = true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "ur-223c79d-7fee-f5ec-617-bec1875662", doNotHash: true, doNotCopy: true, hashAddressBar: false});</script>