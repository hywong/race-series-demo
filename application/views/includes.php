<!DOCTYPE html>
<html>
<head>
<title>Race Series Demo Registration</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta property="og:title" content="Register for the Race Series Demo now!"/>
<meta property="og:description" content="Click here to register for the Race Series Demo today." />
<meta property="og:image:secure_url" content="https://secure.eventsonline.ca/events/core_support_files/images/fb_register_now.jpg" />
<meta property="og:image" content="http://secure.eventsonline.ca/events/core_support_files/images/fb_register_now.jpg" />
<meta property="og:url" content="<?php echo base_url('register'); ?>" />
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<!-- CDN
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
-->
<!-- Latest compiled and minified JavaScript -->
<link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap3-1-1.min.css");?>">
<link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap3-1-1.theme.min.css");?>">
<link rel="stylesheet" href="<?php echo base_url("assets/css/core-themeB.css");?>">
<link rel="stylesheet" href="<?php echo base_url("assets/css/core.css");?>">
<link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap-datepicker.css");?>">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo base_url("assets/js/custom.js"); ?>"></script>
</head>
<body>