<div class="col-xs-12">
<center><a href="<?php echo base_url("register/restart");?>" onclick="return confirm('<?php echo translate("Are you sure you want to restart your registration?"); ?>')" class="btn btn-default col-centered"><i class="glyphicon glyphicon-trash"></i> Restart Registration</a></center>
</div>
<div class="clearfix"></div>
<ul id="progressbar" data-progress-steps="4">
<?php
  $pages = $pagesArr;
  $pageLinks = array(
	"Billing Information" => "register/group",
    "Group Information" => "register/members",
    "Confirmation" => "register/confirmation",
    "Payment and Receipt" => "payment"
    );
  $default = "";
	foreach($pages as $page=>$status){
		if($status == "active current"){
			echo "<li class=\"" . $status . "\">".translate($page)."</li>";
			$default = "";
		}else if($status == "active"){
			echo "<li class=\"" . $status . "\" onclick=\"location.href='".base_url($pageLinks[$page])."'\"><a href=\"".base_url($pageLinks[$page])."\"><span class=\"hidden-xs\">";
			if($page != "Confirmation"){ echo translate("Edit"); }
			echo " ".translate($page)."</span></a></li>";
		}else{
			echo "<li class=\"" . $default . "\"><span class=\"hidden-xs\">".$page."</span></li>";
		}
	}
?>
</ul>
<div class="clearfix"></div>
