//AJAX to retrieve the transactions for the month
//$.post('ajax/get_transactions.php', {type: "recent", count: true }, function(response){
$(document).ready(function() {
  $('#morris-chart-area').parent().removeClass("ajax-chart");
  
  Morris.Area({
    // ID of the element in which to draw the chart.
    element: 'morris-chart-area',
    // Example data
    data: [
    { date: '2012-10-01', registrations: 802 },
    { date: '2012-10-02', registrations: 783 },
    { date: '2012-10-03', registrations:  820 },
    { date: '2012-10-04', registrations: 839 },
    { date: '2012-10-05', registrations: 792 },
    { date: '2012-10-06', registrations: 859 },
    { date: '2012-10-07', registrations: 790 },
    { date: '2012-10-08', registrations: 1680 },
    { date: '2012-10-09', registrations: 1592 },
    { date: '2012-10-10', registrations: 1420 },
    { date: '2012-10-11', registrations: 882 },
    { date: '2012-10-12', registrations: 889 },
    { date: '2012-10-13', registrations: 0 },
    { date: '2012-10-14', registrations: 2 },
    { date: '2012-10-15', registrations: 0 },
    { date: '2012-10-16', registrations: 3 },
    { date: '2012-10-17', registrations: 0 },
    { date: '2012-10-18', registrations: 1224 },
    { date: '2012-10-19', registrations: 1329 },
    { date: '2012-10-20', registrations: 1329 },
    { date: '2012-10-21', registrations: 1239 },
    { date: '2012-10-22', registrations: 1190 },
    { date: '2012-10-23', registrations: 1312 },
    { date: '2012-10-24', registrations: 1293 },
    { date: '2012-10-25', registrations: 1283 },
    { date: '2012-10-26', registrations: 1248 },
    { date: '2012-10-27', registrations: 1323 },
    { date: '2012-10-28', registrations: 1390 },
    { date: '2012-10-29', registrations: 1420 },
    { date: '2012-10-30', registrations: 1529 },
    { date: '2012-10-31', registrations: 1892 },
    ],
    // The name of the data record attribute that contains x-visitss.
    xkey: 'date',
    // A list of names of data record attributes that contain y-visitss.
    ykeys: ['registrations'],
    // Labels for the ykeys -- will be displayed when you hover over the
    // chart.
    labels: ['Registrations'],
    // Disables line smoothing
    smooth: false
  });
  
 
  
	Morris.Donut({
		element: 'morris-chart-donut',
		data: [
		{label: "Referral", value: 42.7},
		{label: "Direct", value: 8.3},
		{label: "Social", value: 12.8},
		{label: "Organic", value: 36.2}
		],
		formatter: function (y) { return y + "%" ;}
	});
	
});

//AJAX to retrieve the 10 most recent transactions and details
/*
$.post("ajax/get_transactions.php", {limit: 10, type: "recent"}, function(){
  var data = [
    {}
  ];
});
*/